#ifndef __HEADER_DATA_ANALYSER__
#define __HEADER_DATA_ANALYSER__

#include "Types.hpp"
#include "Thread.hpp"
#include "StatisticCounter.hpp"
#include "lib/globals.hpp"
#include "lib/functional_units.hpp"
#include <vector>
#include <bitset>
#include <fstream>
#include <pthread.h>
#include <unistd.h>
#include <set>
#include <map>
#include <list>

#include "Heuristic.hpp"

#define NUM_PRF 168 //180 //4T SMT = 336 //8T SMT = 672

#define EXTRA_PATHS 20

class Thread;


class DataAnalyser{
public:
	
	
	struct HeuristicData{
		Heuristic* (*new_heuristic)(UINT, UINT, Thread*, const char*);
		void (*delete_heuristic)(Heuristic*);
		const char* parameter;
	};

	struct issueQueue{
		std::vector<DVInst> iq;
		std::vector<std::vector<int> > sources, pirat_src; //for instructions with register sources, OR path sources (phi, uop, branch)
		std::vector<std::vector<char> > src_type; //s - source (#), p = path (ID)

		std::vector<char> dest_type; 
		std::vector<std::vector<bool> > ready; //source status (branch has both)
		std::vector<LONG> rob_id;
		std::vector<bool> execute; //ready to execute status, 0 = no, 1 = ready, 
		std::vector<int> warp_no;
	};

	struct LSQ{
		std::vector<LONG> address;
		std::vector<bool> valid;
		std::vector<LONG> id;
	};

	//Global counters
	LONG div_counter; // = 0;
	LONG dv_count;// = 0; //counter to reference ROB/IQ entries

	bool *cold; // = true;

	int fetch_warp; 
	//IS Table //////////////////////////////////
	IS_Table *isTable; //, commit_isTable;
	
	//Path table//////////////////////////////////
	Path_Table *pathtable;

	//PIRAT structures ////////////////////////////
	PIRAT_Table *pirat_int, *pirat_fp;
	PIRAT_Table *commit_pirat_int, *commit_pirat_fp;
	std::list<std::list<int> > recycle_registers, recycle_registers_fp;

	//freelist
	Free_List freelist_int, freelist_fp;
	int freelist_ptr, freelist_ptr_fp;

	//Physical Register Files
	PhysicalRegFile prf;
	PhysicalRegFile vrf;

	LSQ loadQ, storeQ;
	std::vector<DVInst> waiting_on_loads;	
	std::vector<DVInst> waiting_on_stores;

	//////////   Pipeline Buffers //////////////////
	LONG dv_seq_no;
	std::deque<DVInst> decode_to_rename;
	ROB reorderB;
	/////////////////////////////////////////////

	Heuristic* fetch_heuristic;
	HeuristicData* fetch_heur_data;
	Heuristic* alter_fetch_heuristic;
	HeuristicData* alter_fetch_heur_data;

	int commit_current_warp;
	int ls_current_warp;

	int readahead_horizon;
	std::set<int> skip_thread;
	LONG periodic_logger;

	static const UINT NUM_BITS = 8*sizeof(void*);
	typedef LONG Values[NUM_BITS];

	PREDICTOR *branchpredictor;

	FunctionalUnits fu;

	Cache l1_cache;
	int coreid;
	Cache l2_cache;


	MSHR mshr;
	RegFile reg; //****

	DVIQ dviqm;
	std::vector<issueQueue> issueQ;

	LONG totalinstructions;

	std::vector< std::deque<Instruction> > loadstorebuffer;

	std::deque<DVInstLoad> *load_buffer;
	std::deque<DVInstStore> *store_buffer;
	std::deque<DVInst> *issued_instructions;

	Thread* threads;
	UINT num_threads;
	UINT num_fetches;
	UINT num_warps;

	LONG *warp_seqno;

	UINT threads_per_warp;

	LONG simulation_instructions;

	LONG mem_operation_count;
	LONG bank_conflict_count;
	LONG bank_conflict_count_reduced;
	LONG tag_conflict_count;
	Statistics statistics;

	int current_warp; // = 0;
	int minsp_current_warp; // = 0;
	int *warp_cycle;
	int pick_count; // = 0;
	int timeout_cycle; // = 0;
	bool *minsp_blocked;
	int current_thread; // = 0;
	bool *active;
	LONG *heur_cycle;
	LONG fetch_heur_cycle; // = 0;
	LONG dviq_cycle; //=0;

	LONG current_fetch_cycle; // = 0;

	//Stats
	LONG *_stat_fetch_fair;
	LONG *_stat_fetch_blocked;
	LONG *_stat_fetch_picked;
	LONG *fetch_rr_blocked;
	LONG *_stat_fetch_rr_picked;
	LONG *issue_dep_blocked;
	LONG *issue_fu_blocked;

	LONG * _stat_fetch_qfull;
	LONG * _stat_fetch_stall;
	LONG * _stat_ignore;
	LONG * _stat_fetch_qempty;
	LONG * _stat_iq_full;
	LONG * _stat_active_thread_count;

	int rrcounter; // = 0;

#ifndef SA_TRACER
	MmtInstruction mmtinst;
#else
	DVInstLatency mmtinst;
#endif

	Config conf;

	UINT *fetch_counter;

	LONG* instructions; //counter

#ifdef ALTER
	bool *store_in_flight;
#endif

	//New statistics
	typedef std::bitset<64> Bitset; //helper
	//Bitset current_cycle; //helper
	LONG number_cycles;

	bool fetch_started; // = false;


	int inst_waiting_in_barrier;
	int finished_threads;

	uint fetch_count;

	bool stall_next_cycle;

public:
	DataAnalyser();
	~DataAnalyser();

	void init(Thread* thrds, UINT nt, UINT nf, Config c, int readahead, HeuristicData* fetch_h, Cache *_l2,int _coreid);

	void analyse();
	bool issue(int &first, int &running);
	void post_issue(bool reset, int first, int running);
	void commit(int threadid);
	bool dependant(int threadid);
	bool dependantdvinst(DVInst &inst);

	bool mem_load_done(Instruction ins, int threadid, bool start);

	bool load(Instruction ins);

	bool outstanding_loads(int clusterid, int dvqid);
	bool outstanding_stores(std::vector<int> smask, int clusterid);
	bool stores_done(int clusterid, int dvqid);

	void poll_cache();

	void loadstore(int clusterid);
	void updategatherscatter(Instruction ins, bool remove = false);

	int combinecount(Instruction ins);

	void check_result();
	void print();

	LONG sameOpCount();
	UINT fetch(bool alter, int cyc);
	//UINT fetch_smt();
	bool isFinished();
	void create(Config conf, LONG seqno, int _inst_count = 0);

	bool execute();
	bool issue_inst_with_loads();

	void commit_dv();

	void increment_cycle();

	bool logger();

	void issue_schedule();
	void wakeup_dispatch();
	void update_issueQ_entries();
	void rename();
	void issue_push();
	void setup_rename();
	void execute_bypass();
	bool check_empty(int offset);

	int thread_schedule_minsp_rr();
	int thread_schedule_minsp(int &istable_number, int &ISentry, int cyc);
	int thread_schedule_roundrobin();

	int thread_schedule_minsp_rr(int &istable_number, int &ISentry, int cyc);
	int thread_schedule_ext_minsp(int &istable_number, int &ISentry, int cyc);
	int adjust_empty_warpThreads(int current_thread, int current_warp);

	void print_decode_queue();
	void evict_pathtable_entries();
	bool is_check(int warpid);
	LONG return_mispr_pc(int x_smask[], int offset);
	bool ROB_check();
	//void clear_rob(int warpid, int robid);


private:
	struct CommonVar{
		int first;
		UINT running;
		//issue
	};
			void printSQ();
	void memory_analysis(CommonVar* cv);
	void registers_analysis(CommonVar* cv);
	void divergence_statistics(CommonVar* cv);
	void setup_analyzer_vars(int num_warps, int threads_per_warp);
	void setup_warps(int num_warps);
	void check_valid_commit_threads(std::deque<DVInst>::iterator ii, int val_index);	
	void check_for_branch_mispr(std::deque<DVInst>::iterator ii);
        int  check_barriers(int &barrier_count, int offset);
	
	int to_fetch_or_not_to_fetch(int offset, bool &fetch, int x_smask[], int tag, int path_mask[]);
	void update_inst_count(int &inst_count, int x_smask[]);
	
	void collect_instruction_stats(DVInst dv_temp, int i, int &combine_count, int offset);
	void refill_prefetch_IQB(int i, int offset);
	void gather_dv_stats(int combine_count, DVInst dv_temp, bool divergent, int x_smask[]);
	
	void update_warp_thread_tags(int offset, int tag, int x_smask[]);
	int  get_last_tag(int smask[], int offset);
	//bool assign_warp_type(int last_tag, int current_warp, int offset, int x_smask[], int &type);
	LONG return_pc(int x_smask[], int offset);
	//int assign_mask_tags(int last_tag, DVInst &dv_temp);
	LONG return_sp(int x_smask[], int offset);

	void update_structures_reconverge(int x_smask[], int offset, int inst_count, int last_tag, int other_tag, int warp_no);
	void update_structure_divergent(int x_smask[], DVInst &dv_temp, int offset, int last_tag, int warp_no, bool mispredict);
	void update_structure_coldcase(int x_smask[], DVInst &dv_temp, int offset, int last_tag);
	void update_is_table(DVInst cuop, int x_smask[], int offset, int tag, int insert, int warp_no);
	void newpathtable_entry(int mask, int valid, int actual_mask, int warp_no); 
	void update_path_and_is(int tag_count, int x_smask[], int offset, int warp_no);
	void rename_sources_pirat(std::vector<int> src, std::vector<int> type, int num_src, DVInst &dv, std::vector<int> &ow, std::vector<int> &owl);
	void rename_dest_pirat(std::vector<int> dest, std::vector<int> type, int num_dest, DVInst &dv, std::vector<int> &ow, std::vector<int> &owl, std::vector<int> &owt, int i);
	void update_PIRAT_entry(int new_reg, bool is_partial, bool mt_bit, int tag, int entry, bool is_int, int warp_no);

	void update_rename_stats(std::string classification);
	void update_dv_source(DVInst &dv, int reg, int type, int ready);
	void update_dv_dest(DVInst &dv, int reg, int type, int orig);
	int get_mask_pathTable(int tag, int warpid);
	int get_free_intreg(DVInst &dv, bool is_source);
	int get_free_FPreg(DVInst &dv, bool is_source);
	void release_register(int reg, int pirat_type);
	
	
	int generate_mask(int tag1, int tag2, int length, int warp_no);
	int return_branch_mask(DVInst dv_temp);

	void print_periodic();
	void print_IQ(QueueTypes type);
	void remove_IQ_entry(LONG element, LONG id);

	LONG generate_DVNum();
	void print_is_table(int table);
	void debug_cuop(LONG rob_id);
	bool assert_ow_correct(int wmask, int rmask);
	void update_freePointer(int warp_id);
	void print_pathtable(int warp_no);
	void copy_IS_table(IS_Table &tmp_is, int warpid);
	//int get_istable_index(int path);
	void create_isTable_entry(int x_smask[], int offset, int tag);
	void create_new_path_is(int mask[], int offset, int actual_mask);
	bool detect_reconvergence(int &tag1, int &tag2);
	void generate_entries(std::vector<int> &entries, int free, int tag, int warp_no);
	bool in_ISTable(int path, int wrp);
	void create_IStable_entries();
	int get_issueQ_size();	
	bool check_SQ(std::vector<LONG> addr, std::vector<bool> &valid);
	bool cache_request(std::vector<std::vector<int> > mask, int count, int length);
	bool SQ_ready(LONG addr, int rob_id, int dep_id);
	void get_LOAD_addresses(DVInst &it_inst, std::vector<LONG> &addresses, int tmp_mask[], bool is_commit);
	void get_STORE_addresses(std::vector<LONG> &addresses, int tmp_mask[]);
	void update_rob_inst(DVInst it_dv, LONG robid);

	void omit_invalid_addresses(std::vector<int> &smask, DVInst dvi, int iteration);
	bool enough_registers(DVInst inst, bool &rename_stalled);
	bool active_threads(int tag, int warp, int offset);

	bool tso_verify(LONG rob_id);
	bool store_load_order(LONG rob_id);
	bool tso_ldstr(LONG rob_id);
	void get_addresses_for_tso(std::vector<LONG> &addresses, LONG rob_id);
	int count_inst_active(int mask);


	void nextPC_error_check(int x_smask[], int offset);
	void update_path_mask(int tag, int exception_mask[], DVInst &dv);
	void update_exception_threads(int mask[], int offset);
	bool no_pending_exceptions(int tag1, int tag2, int offset);
	void generate_exception_entries(std::vector<int> &entries, int free, int tag, int warp_no);
	void fix_mask(int exception_mask[], int x_smask[], int offset);
	void clear_pathtable_exceptions(int offset);
	//bool update_fetchq_empty_warps(int offset, int mask[], int path_tag);
	void clear_storeQ_entries();
	//bool no_tag(int thread, int offset);
	void erase_thread_from_path(int tno, int offset, int warp);
	void unstall_branch_threads(int add_offset, int x_smask[], bool full_mispr);
	void update_branchstall_path(int warp_no, int tag_count);
	int update_structure_mispredict(int x_smask[], int inv_mask[], DVInst &dv_temp, int offset, int &last_tag, int warp_no);
	bool pirat_stall_check(std::vector<std::vector<int> > waw_dest, std::vector<std::vector<int> > waw_dest_type, std::vector<int> waw_warp);

	//void printSQ();

	//void print_div_debug(int smask[], DVInst dv_temp);
};

#endif
