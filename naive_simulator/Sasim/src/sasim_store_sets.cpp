#include "sasim_store_sets.hpp"

// ***************************************************************************
// Begin: store_sets class

/*! 
 * Invalidates all the entries of Store Set ID Table SSIT
 * 
 * @return void. This function does not return any data.
 */ 
void 
store_sets::clear_ssit()
{
  for (int i=0; i<SSIT_SIZE; i++) {
    ssit[i].valid = false;
  }
}

/*! 
 * Invalidates all the entries of Last Fetched Store Table LFST
 * 
 * @return void. This function does not return any data.
 */ 
void 
store_sets::clear_lfst()
{
  for (int i=0; i<LFST_SIZE; i++) {
    lfst[i].valid = false;
  }
}

/*!
 * Creates tables SSIT and LFST and invalites all the entries of them
 * 
 * @param _ssit_size Size of table SSIT
 * @param _lfst_size Size of table LFST
 * @param _clearperiod Number of Cycles to clear tables
 * @return void. This function does not return any data.
 */ 
void 
store_sets::init(int _ssit_size, int _lfst_size, int _clearperiod)
{
  SSIT_SIZE = _ssit_size;
  LFST_SIZE = _lfst_size;
  CLEARPERIOD = _clearperiod;
  SASSERT(ISPOW2(SSIT_SIZE));
  SASSERT(ISPOW2(LFST_SIZE));
  ssit = new ssit_entry [SSIT_SIZE];
  lfst = new lfst_entry [LFST_SIZE];
  clear_ssit();
  clear_lfst();
  clear_cycle = CLEARPERIOD;
}

store_sets::store_sets()
{
  ssit = NULL;
  lfst = NULL;
}

store_sets::~store_sets()
{
  delete [] ssit;
  delete [] lfst;
}

/*!
 * Returns the index of table SSIT given by the address of the load (PC)
 * The SSID of the respective load is stored in the entry given by index,
 * 
 * @param pc Address of the load given by the PC
 * @return Load index.
 */ 
int 
store_sets::ssit_ldindex(sa_addr pc)
{
  int h = pc & (SSIT_SIZE-1);
  return h;
}

/*!
 * Returns the index of table SSIT given by the address of the store (PC)
 * The SSID of the respective store is stored in the entry given by index,
 * 
 * @param pc Address of the store given by the PC
 * @return Store index.
 */ 
int 
store_sets::ssit_stindex(sa_addr pc)
{
  int h = (pc ^ (SSIT_SIZE-1)) & (SSIT_SIZE-1);
  return h;
}

/*!
 * Computes the Store Set Identifier (SSID) based on the PC value
 * 
 * @param pc Value of the PC
 * @return SSID Index.
 */ 
int 
store_sets::get_ssid(sa_addr pc)
{
  return (int) pc;
}

/*!
 * TO BE REVISED
 * Registers a store dependency for the current load or store being evaluated.
 * The SSID of the store and the current load/store being evaluated are checked in order to see
 * whether there is a dependence between the two instructions.
 * 
 * @param store_pc Address for the store in which the load or store being evaluated depends on
 * @param pc Address for the current load or store being evaluated
 * @param twostores Boolean value the indicates if the current instruction being evaluated is a store (true)
 * @return void. This function does not return any data.
 */ 
void 
store_sets::record_dep(sa_addr store_pc, sa_addr pc, bool twostores)
{
  int i = ssit_stindex(store_pc);
  int j = (twostores)? ssit_stindex(pc) : ssit_ldindex(pc);
  //cout << "HELLO " << hex << store_pc << " " << pc << dec << " : " << ((ssit[i].valid)? ssit[i].ssid:-1) << " " << ((ssit[j].valid)? ssit[j].ssid:-1) << " --> ";
  int ssid;
  if (ssit[i].valid && ssit[j].valid) {
    // arbitrate
    ssid = (ssit[i].ssid < ssit[j].ssid)? ssit[i].ssid : ssit[j].ssid;
  } else if (ssit[i].valid) {
    ssid = ssit[i].ssid;
  } else if (ssit[j].valid) {
    ssid = ssit[j].ssid;
  } else {
    ssid = get_ssid(pc);
  }
  ssit[i].valid = true;
  ssit[i].ssid = ssid;
  ssit[j].valid = true;
  ssit[j].ssid = ssid;
  //cout << ssit[i].ssid << " " << ssit[j].ssid << endl;
}

/*!
 * Returns the index entry of table LFST that contains the Store Set Identifier given by SSID
 * 
 * @param ssid Store Set Identifier value
 * @return Index entry of LFST
 */ 
int
store_sets::search_lfst(int ssid)
{
  for (int i=0; i<LFST_SIZE; i++) {
    if (lfst[i].valid && (lfst[i].ssid == ssid)) {
      return i;
    }
  }
  return -1;
}

/*!
 * For a given store, this instruction writes into the LFST table its num value that identifies it.
 * If there is no a valid entry in table SSIT means that no store sets have been created for the
 * current store.
 * 
 * @param store_pc Address of the Store
 * @param num Value that uniquely identifies the instance of the store
 * @return void. This function does not return any data.
 */ 
void 
store_sets::process_store(sa_addr store_pc, int64_t num)
{
  int i = ssit_stindex(store_pc);
  if (! ssit[i].valid) {
    return;
  }
  int k = search_lfst(ssit[i].ssid);
  if (k<0) {
    k = LFST_SIZE-1;
  }
  lfst_entry e;
  e.valid = true;
  e.ssid = ssit[i].ssid;;
  e.num = num;
  // move to MRU pos
  for (int j=k; j>0; j--) {
    lfst[j] = lfst[j-1];
  }
  lfst[0] = e;
}

/*!
 * Clears the SSIT if it has elapsed n cycle after the last clear
 * 
 * @param cycle Current execution cycle
 * @return void. This function does not return any data.
 */ 
void
store_sets::periodic_clearing(int64_t cycle)
{
  if (cycle >= clear_cycle) {
    clear_ssit();
    clear_cycle = cycle + CLEARPERIOD;
  }
}

/*! 
 * Looks at the SSIT table if the load has a valid Store Sets. If so, 
 * this function returns the value that identified the store in which the load depends.
 * If there is a valid store set but there are no stores in which the load depends, 
 * the NOTHING value is returned.
 * 
 * @param load_pc Address of the Load
 * @return Store number or NOTHING value.
 */ 
int64_t 
store_sets::get_memdep(sa_addr load_pc)
{
  int i = ssit_ldindex(load_pc);
  if (!ssit[i].valid) {
    return NOTHING;
  }
  int k = search_lfst(ssit[i].ssid);
  if (k>=0) {
    return lfst[k].num;
  }
  return NOTHING;
}

/*!
 * Removes any dependency for the load being evaluated
 * 
 * @param load_pc Address of the Load
 * @return void. This function does not return any data.
 */ 
void
store_sets::clear_deps(sa_addr load_pc)
{
  int i = ssit_ldindex(load_pc);
  ssit[i].valid = false;
}

// End: store_sets class
// ***************************************************************************
