#include "sasim_engine.hpp"

using namespace std;

// ***************************************************************************
// Begin: sasim class

/*!
 * Initializes the following elements required by the simulator:
 *  Register Renaming (rr and ret_rr).
 *  Simulator Instruction Window, represented by a queue of type inst_ptr (window) with size MAXWINDOW.
 *  Free Pool of Instructions, represented by a queue of type inst_ptr (freepool) with size MAXWINDOW. It contains a sasim_inst object (storage) at each entry.
 *  Predicted Instructions Stream Queue, represented by a queue of type inst_ptr (predq) with size PREDQ_SIZE and latency PREDQ_LATENCY.
 *  Artifical Queue for simulating front-end pipe stages, represented by a queue of type inst_ptr (feq) with size FEQ_SIZE and latency FEQ_DELAY.
 *  Re-Order Buffer, represented by a queue of type uop_ptr (rob) with size ROB_SIZE and latency ROB_LATENCY cycles.
 *  Artifical Queue for simulating back-end pipe stages, represented by a queue of type uop_ptr (beq) with size BEQ_SIZE and latency BEQ_DELAY.
 *  Load Queues, represented by an array of queues of type uop_ptr (ldq[NUM_LDQ]) with size LDQ_SIZE each queue and latency LD_LATENCY.
 *  Store Queue, represented by a queue of uop_ptr (stq) with size STQ_SIZE and latency ST_LATENCY
 *  Post Retirement Store Queue, represented by a queue of uop_ptr (prstq) with size PRSTQ_SIZE and latency PRST_LATENCY.
 *  Instruction Steering (steer). It could be based on round-robin or dependence-based approach.
 *  Integer Steering Queues, represented by an array of queues of type uop_ptr (intq[NUM_ALU]) with size INTQ_SIZE each queue and latency INTQ_LATENCY.
 *  ALUs, represented by an array of queues of type uop_ptr (alu[NUM_ALU]) with size ALU_LAT and latency ALU_LAT.
 *  Floating-Point Steering Queues, represented by an array of queues of type uop_ptr (fpq[NUM_FPADDMUL]) with size FPQ_SIZE each queue and latency FPQ_LATENCY.
 *  FP-ALUs, represented by an array of queues of type uop_ptr (fpaddmul[NUM_FPADDMUL]) with size FPADDMUL_LAT and latency FPADDMUL_LAT (full pipelined).
 *  FP-MOVs, represented by an array of queues of type uop_ptr (fpmov[NUM_FPADDMUL]) with size FPMOV_LAT and latency FPMOV_LAT.
 *  Integer Multiplier, represented by a queue of type uop_ptr (imul) with size IMUL_LAT and latency IMUL_LAT (full pipelined).
 *  Integer Divider, represented by a queue of type uop_ptr (idiv) with size 1 and latency IDIV_LAT (non pipelined).
 *  FP Divider, represented by a queue of type uop_ptr (fpdiv) with size 1 and latency FPDIV_LAT (non pipelined).
 *  Branch Prediction (bp), based on the object branchpred_full.
 *  Store Sets (ss).
 *  Page Renaming and TLB (pagerename - tlb).
 *  Instruction Cache L1 (il1), Data Cache L1 (dl1), Cache L2 (l2), Cache L3 (l3), Memory (mem). 
 * 
 * @return void. This function does not return any value.
 */ 
void
sasim::init()
{
  ninst = 0;
  nuops = 0;
  cycle = 0;
  
  rr.init();		
  ret_rr.init();
  
  window.init(MAXWINDOW-PRSTQ_SIZE); 
  freepool.init(MAXWINDOW);
  for (int i=0; i<MAXWINDOW; i++) {
    freepool.enqueue(&storage[i]);
  }

  currentline = 0;
  predq.init(PREDQ_SIZE,PREDQ_LATENCY);
  feq.init(FEQ_SIZE,FEQ_DELAY);
  
  rob.init(ROB_SIZE,ROB_LATENCY);
  rob_loads = 0;
  
  beq.init(BEQ_SIZE,BEQ_DELAY);
  
  for (int i=0; i<NUM_LDQ; i++) {
    ldq[i].init(LDQ_SIZE,LD_LATENCY);
  }
  stq.init(STQ_SIZE,ST_LATENCY);
  prstq.init(PRSTQ_SIZE,PRST_LATENCY);

  prefetchinst = 0;
  last_retire_cycle = 0;
  
  steer.init();

  for (int i=0; i<NUM_ALU; i++) {
    intq[i].init(INTQ_SIZE,INTQ_LATENCY);
    alu[i].init(ALU_LAT,ALU_LAT); 
  }
  for (int i=0; i<NUM_FPADDMUL; i++) {
    fpq[i].init(FPQ_SIZE,FPQ_LATENCY);
    fpaddmul[i].init(FPADDMUL_LAT,FPADDMUL_LAT); // fully pipelined
    fpmov[i].init(FPMOV_LAT,FPMOV_LAT);
  }
  imul.init(IMUL_LAT,IMUL_LAT); // fully pipelined 
  idiv.init(1,IDIV_LAT); // non pipelined
  fpdiv.init(1,FPDIV_LAT); // non pipelined

  bp.init();
  pendingmisp = NULL;

  ss.init(SS_SSIT_SIZE,SS_LFST_SIZE,SS_CLEARPERIOD);

  pagerename.init();
  tlb.init_tlb(this);

  il1.init_il1(this,IL1_SIZE_BYTES,IL1_ASSO,IL1_POL,IL1_LAT_CYCLES,IL1_RATE,IL1_MQ_SIZE,IL1_FQ_SIZE);
  dl1.init_dl1(this,DL1_SIZE_BYTES,DL1_ASSO,DL1_POL,DL1_LAT_CYCLES,DL1_RATE,DL1_MQ_SIZE,DL1_WB_SIZE,DL1_FQ_SIZE);
  l2.init_l2(&tlb,&il1,&dl1,L2_SIZE_BYTES,L2_ASSO,L2_POL,L2_LAT_CYCLES,L2_RATE,L2_MQ_SIZE,L2_WB_SIZE,L2_FQ_SIZE,ENABLE_L2_PREFETCH,L2_PFB_SIZE,L2_PFR_SIZE,L2_PF_MAXSTEP,L2_PF_MINSTEP,L2_PF_HISTLEN,L2_PF_ROUNDS,L2_PF_LOGZONE);
#ifdef ENABLE_L3
  l3.init("l3",&l2,L3_SIZE_BYTES,L3_ASSO,L3_POL,L3_LAT_CYCLES,L3_RATE,L3_MQ_SIZE,L3_WB_SIZE,L3_FQ_SIZE,ENABLE_L3_PREFETCH,L3_PFB_SIZE,L3_PFR_SIZE,L3_PF_MAXSTEP,L3_PF_MINSTEP,L3_PF_HISTLEN,L3_PF_ROUNDS,L3_PF_LOGZONE);
  mem.init(&l3,MEM_LAT_CYCLES,MEM_BANDWIDTH);
#else
  mem.init(&l2,MEM_LAT_CYCLES,MEM_BANDWIDTH);
#endif

  numloads = 0;
  numstores = 0;
  fpwriteint = 0;
  fpreadint = 0;
  fpsseonint = 0;
  nfpuops = 0;
  artificialdelay = 0;
  nnops = 0;
  fpmovs = 0;
  nfpsse = 0;
  fpother = 0;
  nshift = 0;
  nimul = 0;
  nidiv = 0;
  nfpdiv = 0;
  nintlong = 0;
  nfplong = 0;
  nbranchmisp = 0;
  nmemtrap = 0;
  nbytes_problem = 0;
  minexecdelay = INFINITE_INTVAL;
  minretiredelay = INFINITE_INTVAL;
  //l3.pf.hist.debugprint();
}

/*!
 * Constructor that initializes the branch predictor and the different
 * queues, functional units, caches and memory.
 */ 
sasim::sasim() : bp(TAGE_BSIZE,TAGE_GSIZE,ITTAGE_BSIZE,ITTAGE_GSIZE)
{
  init();
}

/*!
 * For debugging purposes: prints the instruction and address accessed by it
 * 
 * @return void. This function does not return any value.
 */ 
void 
sasim::debugprint(sa_inst & inst, sa_addr raddr[], sa_addr waddr[])
{
  inst.debugprint();
  cout << hex << "r: ";
  for (int i=0; i < inst.nloads; i++) {
    cout << raddr[i];
  }
  cout << endl;
  cout << "w: ";
  for (int i=0; i < inst.nstores; i++) {
    cout << waddr[i];
  }
  cout << endl;
}

/*!
 * Returns a pointer to the uop identified by the given number only if it exists in the Re-Order Buffer.
 *
 * This function is used externally.
 * 
 * @param uopnum Number of uop to be searched 
 * @return A reference to the uop found.
 */ 
uop_ptr
sasim::find_uop_in_rob(int64_t uopnum)
{
  if (rob.empty()) {
    return NULL;
  }
  if (uopnum < rob.oldest()->num) {
    return NULL;
  }
  int k = uopnum - rob.oldest()->num;
  uop_ptr uptr = rob.access(k);
  SASSERT(uptr);
  SASSERT(uptr->num == uopnum);
  return uptr;
}

/*!
 * Returns a pointer to the uop identified by the given number only if it exists in the Post-Retirement Store Queue
 *
 * This function is used externally.
 * 
 * @param uopnum Number of uop to be searched 
 * @return A reference to the uop found.
 */ 
uop_ptr
sasim::find_uop_in_prstq(int64_t uopnum)
{
  if (prstq.empty()) {
    return NULL;
  }
  if (uopnum < prstq.oldest()->num) {
    return NULL;
  }
  prstq.scan_forward();
  while (uop_ptr * p = prstq.read()) {
    uop_ptr uptr = *p;
    SASSERT(uptr);
    if (uptr->num == uopnum) {
      return uptr;
    }
  }
  return NULL;
}

/*!
 * Prepares and inserts a new instruction into the instruction window queue (Simulator Window).
 * One cycle of pipeline simulation is performed when the Simulator Instruction Window has been
 * filled of instructions taken from the traces. One there is an entry available in the IW, 
 * the instruction is prepared to be inserted into it. Each uop of the instruction is processed 
 * to determine access to memory, type of uop and so on. Elements of the instruction such as 
 * line address, virtual page, and others are also prepared. If the IW alerady contents other 
 * instructions, the last instruction inserted will be modified to point to the new instruction 
 * added to the IW.
 * Finally the instruction is inserted into the instruction window queue.
 *
 * This function is used externally to feed the simulator with instructions.
 * See file: sasim_tracer.cpp, function: simulate.
 * 
 * @param inst Instruction to be added
 * @param raddr Vector that contains the different read memory references.
 * @param waddr Vector that contains the different write memory references.
 * @return void. This function does not return any value.
 */ 
void 
sasim::input(sa_inst & inst, sa_addr raddr[], sa_addr waddr[], sa_addr sp_addr, bool is_branch_taken)
{
  //inst.debugprint();

  while (window.full()) {
    onecycle();
  }

  SASSERT(!freepool.empty()); // if this assert triggers, increase MAXWINDOW
  inst_ptr siminst = freepool.dequeue();
  SASSERT(siminst);
  
  siminst->inst = & inst;
  siminst->num = ninst;

  int nloads = 0;
  int nstores = 0;
  for (int i=0; i<inst.nuops; i++) {
    sasim_uop & simuop = siminst->simuop[i];
    simuop.uop = & inst.uop[i];
    simuop.iptr = siminst;
    int64_t uopnum = nuops + i;
    simuop.num = uopnum;
    switch (inst.uop[i].uoptype) {
    case SA_UOP_LOAD:
//FIXME
      //simuop.addr = raddr[nloads];
      if (inst.mreadsize > MAXMEMACCWIDTH) {
	if (inst.uop[i].rd == NOREG) {
	  // probably a prefetch inst (FIXME)
	  prefetchinst++;
	} else {
	  // FIXME: e.g., FLDENV
	  nbytes_problem++;
	}
	simuop.nbytes = 1;
      } else {
	simuop.nbytes = inst.mreadsize;
      }
      simuop.set_dl1banks();
      nloads++;
      break;
    case SA_UOP_STORE:
//FIXME
      //simuop.addr = waddr[nstores];
      if (inst.mwritesize > MAXMEMACCWIDTH) {
	// FIXME: e.g., FSTENV
	nbytes_problem++;
	simuop.nbytes = 1;
      } else {
	simuop.nbytes = inst.mwritesize;
      }
      simuop.set_dl1banks();
      nstores++;
      break;
    default:
      simuop.addr = 0;
      simuop.nbytes = 0;
      simuop.dl1banks = 0;
      break;
    }

    if (! ISPOW2(simuop.nbytes)) {
      // FIXME: what's that ???
      nbytes_problem++;
      simuop.nbytes = 1;
    }

    if (simuop.uop->is_mem()) {
      simuop.vpage = pagerename.getpage(simuop.addr / PAGESIZE_BYTES);
      // FIXME: simuop.addr and simuop.vpage are not consistent, is this a problem ?
      pagerename.check_pagetablezone(simuop.addr / CACHELINE_BYTES);
      //cout << hex << simuop.addr << dec << endl;
    } else {
      simuop.vpage = 0;
    }

    simuop.lastuop = (i==(inst.nuops-1))? true : false;

    simuop.is_fp = false; // will be determined at rename
    simuop.storesetdep = NOTHING;
    simuop.reset_uop();
  }
  SASSERT(nloads==inst.nloads);
  SASSERT(nstores==inst.nstores);
  siminst->lineaddr = (inst.fallthrough-1) / CACHELINE_BYTES;
  siminst->vpage = pagerename.getpage(siminst->lineaddr / PAGESIZE_LINES);
  // FIXME: siminst->lineaddr and siminst->vpage are not consistent, is this a problem ?
  siminst->crossline = false;
  pagerename.check_pagetablezone(siminst->lineaddr);
  siminst->nextinst = NULL;
  siminst->block_end = false;
  siminst->mispredict = false;
  siminst->memtrap = false;
  siminst->memtrap_storepc = 0;
  siminst->pred_cycle = INFINITE_INTVAL;
  siminst->exec_cycle = INFINITE_INTVAL;
  siminst->store_completed = false;


  if (!window.empty()) {
    inst_ptr iptr = window.last();
    SASSERT(iptr);
    iptr->nextinst = siminst;
    sa_addr line0 = inst.pc / CACHELINE_BYTES;
    siminst->crossline = ((iptr->inst->fallthrough / CACHELINE_BYTES) != line0) && (line0 != siminst->lineaddr);
    if (siminst->crossline) {
      SASSERT(line0 == (siminst->lineaddr-1));
    }
  }

  //printf("pc %#x sp %#x is_branch %d \n",inst.pc, sp_addr, (inst.is_branch_cond || inst.is_branch_ret || inst.is_branch_ind) );
  printf("pc %#x sp %llx is_branch %d is_branch_taken %d nloads %d nstores %d \n", inst.pc, sp_addr, (inst.is_branch_cond || inst.is_branch_ret || inst.is_branch_ind), is_branch_taken, inst.nloads, inst.nstores );
  //printf("pc %#x is_branch %d nloads %d nstores %d \n", inst.pc, (inst.is_branch_cond || inst.is_branch_ret || inst.is_branch_ind), inst.nloads, inst.nstores );
#if 0
  nloads = nstores = 0;
  for (int i=0; i<inst.nuops; i++) {
    if (inst.uop[i].uoptype == SA_UOP_LOAD) {
      printf("raddr[%d] %#x \n",nloads, raddr[nloads]);
      nloads++;
    }
    if (inst.uop[i].uoptype == SA_UOP_STORE) {
      printf("waddr[%d] %#x \n",nstores, waddr[nstores]);
      nstores++;
    }
  }
#endif
  printf("####################\n");

  window.enqueue(siminst);
  ninst++;
  nuops += inst.nuops;
}

/*!
 * Performs as many one cycle simulation as required to drain the instruction window queue
 * 
 * @return void. This function does not return any value.
 */ 
void
sasim::drain()
{
  VERBOSE(cerr << "\033[31m Draining...\033[m" << endl);
  while (! window.empty()) {
    onecycle();
  }
}

/*!
 * Prints information about the configuration of the micro-architecture being modeled
 * 
 * @return void. This function does not return any value.
 */ 
void
sasim::printconfig(FILE * stream)
{
  fprintf(stream,"clock freq (GHz) = %.2f\n",CLOCK_FREQ);
  fprintf(stream,"dl1 latency (cycles) = %d\n",dl1.latency);
  fprintf(stream,"l2 latency (cycles) = %d\n",l2.latency);
#ifdef ENABLE_L3
  fprintf(stream,"l3 latency (cycles) = %d\n",l3.latency);
#endif
  fprintf(stream,"mem latency (cycles) = %d\n",mem.latency);
  fprintf(stream,"l2 prefetch buffer size = %d\n",l2.pf.pfb.size);
#ifdef ENABLE_L3
  fprintf(stream,"l3 prefetch buffer size = %d\n",l3.pf.pfb.size);
#endif
}

/*!
 * Prints simulation results
 * 
 * @return void. This function does not return any value.
 */ 
void
sasim::printstats(FILE * stream)
{
  fprintf(stream,"\n");
  bp.printstats(stream,"bpred");
  fprintf(stream,"\n");
  tlb.printstats(stream);
  fprintf(stream,"\n");
  il1.printstats(stream);
  fprintf(stream,"\n");
  dl1.printstats(stream);
  fprintf(stream,"\n");
  l2.printstats(stream);
  fprintf(stream,"\n");
#ifdef ENABLE_L3
  l3.printstats(stream);
  fprintf(stream,"\n");
#endif
  mem.printstats(stream);
  fprintf(stream,"\n");
  fprintf(stream,"ninst = %lld\n",(long long) ninst);
  fprintf(stream,"nuops = %lld\n",(long long) nuops);
  fprintf(stream,"loads = %lld\n",(long long) numloads);
  fprintf(stream,"stores = %lld\n",(long long) numstores);
  fprintf(stream,"nops = %lld\n",(long long) nnops);
  fprintf(stream,"prefetch inst = %lld\n",(long long) prefetchinst);
  fprintf(stream,"nshift = %lld\n",(long long) nshift);
  fprintf(stream,"imul = %lld\n",(long long) nimul);
  fprintf(stream,"idiv = %lld\n",(long long) nidiv);
  fprintf(stream,"int long = %lld\n",(long long) nintlong);
  fprintf(stream,"fp uops = %lld\n",(long long) nfpuops);
  fprintf(stream,"fp movs = %lld\n",(long long) fpmovs);
  fprintf(stream,"fp sse = %lld\n",(long long) nfpsse);
  fprintf(stream,"fp div = %lld\n",(long long) nfpdiv);
  fprintf(stream,"fp long = %lld\n",(long long) nfplong);
  fprintf(stream,"fp other = %lld\n",(long long) fpother);
  fprintf(stream,"fp write int = %lld\n",(long long) fpwriteint);
  fprintf(stream,"fp read int = %lld\n",(long long) fpreadint);
  fprintf(stream,"fp/sse on int = %lld\n",(long long) fpsseonint);
  fprintf(stream,"artificial delay = %lld\n",(long long) artificialdelay);
  fprintf(stream,"nbytes problem = %lld\n",(long long) nbytes_problem);
  fprintf(stream,"branch mispredicts = %lld\n",(long long) nbranchmisp);
  fprintf(stream,"memory traps = %lld\n",(long long) nmemtrap);
  fprintf(stream,"min exec delay = %lld\n",(long long) minexecdelay);
  fprintf(stream,"min retire delay = %lld\n",(long long) minretiredelay);
  fprintf(stream,"page count = %lld\n",(long long) pagerename.count);
#ifdef STRIDE_PREFETCH
  fprintf(stream,"stride prefetch req = %lld\n",(long long) spf.nspfreq);
#endif

  fprintf(stream,"\n");
  fprintf(stream,"cycles = %lld\n",(long long) cycle);
  fprintf(stream,"inst/cycle = %f\n",(double) ninst / cycle);
  fprintf(stream,"uops/cycle = %f\n",(double) nuops / cycle);
  double timesec = 1e-9 * (double)(cycle) / CLOCK_FREQ;
  fprintf(stream,"total time (sec) = %.2e\n",timesec);
  fprintf(stream,"inst/sec = %.2e\n",(double)ninst / timesec);
}

// ###################
// ONE_CYCLE_EXECUTION
// ###################

/*!
 * This function is in charge of feeding the prediction queue with instructions from
 * the predicted path. The instructions are taken from the traces (Instruction
 * Window in the simulator) and inserted in the prediction queue to be then processed
 * by the fetch pipeline stage.
 * Instructions are also requested to the instruction cache L1.
 * If a misprediction is currently active, neither instructions are added to the 
 * queue nor requested to the instruction cache (fetching is stalled).
 * When the instruction taken from the Instruction Window is determined to be a
 * branch instruction, a prediction of the next instruction is performed. The 
 * instruction predicted is then compared with the next available instruction in the
 * IW. If they are different means that there was a misprediction and therefore
 * the simulator core must take care of this situation. Internal variable pendingmisp
 * will be set with a reference to the mispredicted branch instruction.
 * If the instruction being evaluated crosses a cache line an additional request to 
 * the instruction cache L1 is performed.
 * This function also determines if the instruction being evaluated is the end of
 * the cache line.
 *
 * This function is called from onecycle() in this class.
 * 
 * @return void. This function does not return any value.
 */ 
void
sasim::onecycle_pred()
{

#ifndef PERFECT_BPRED
  if (pendingmisp) {
    return;
  }
#endif

  while (1) {

    if (predq.full() || (il1.fetchreq.free_entries() < 2)) {
      break;
    }

    inst_ptr * p = window.read();

    if (!p) {
      // proc is draining
      SASSERT(!window.full()); // if this assert triggers, increase MAXWINDOW
      break;
    }

    inst_ptr iptr = *p;
    SASSERT(iptr);
    SASSERT(iptr->lineaddr != 0);

    if (iptr->crossline) {
      // instruction is the target of a jump and crosses a cache line
      // ==> generate an extra request
      il1.fetchreq.enqueue(il1_req(iptr->lineaddr-1,iptr->vpage/*FIXME*/),cycle);
      currentline = iptr->lineaddr-1;
    }

    if (iptr->lineaddr != currentline) {
      // new block, request the line
      il1.fetchreq.enqueue(il1_req(iptr->lineaddr,iptr->vpage),cycle);
      currentline = iptr->lineaddr;
    }

    predq.enqueue(iptr,cycle);
    iptr->pred_cycle = cycle;

    if (! iptr->nextinst) {
      // proc is draining
      SASSERT(!window.full()); // if this assert triggers, increase MAXWINDOW
      iptr->block_end = true;
      currentline = 0;
      break;
    }

    bp.checkpoint(iptr);

#ifndef PERFECT_BPRED
    // branch prediction
    if (iptr->inst->is_branch()) {
      bool ok = bp.predict(iptr,iptr->nextinst->inst->pc);
      bp.updatehist(iptr,iptr->nextinst->inst->pc); // speculative history update
      iptr->mispredict = ! ok; 
      if (iptr->mispredict) {
	iptr->block_end = true;
	pendingmisp = iptr;
	currentline = 0;
	//break;
      }
    }
#endif

    if (pendingmisp) {
      break;
    }

    // stop on line change
    if (iptr->nextinst->lineaddr != iptr->lineaddr) {
      iptr->block_end = true;
      break;
    }

    // stop if jump or taken branch in same line, will need new fetch
    if (iptr->inst->fallthrough != iptr->nextinst->inst->pc) {
      currentline = 0;
      iptr->block_end = true;
      break; 
    }

  }
}

/*!
 * Performs one instruction cache L1 simulation cycle. If the cache IL1 has completed
 * a previous fetch request means that there are instructions ready into the fetch buffer
 * and therefore they can be inserted into the front end queue and removed from the
 * prediction one. 
 * If one of the instructions retired from the prediction queue is located at the end of
 * the cache line, the corresponding line will be removed from the fetch buffer. 
 * In addition, if a cache line was put into the fetch buffer due to an instruction 
 * crossline, it will also be removed from that buffer.
 *
 * This function is called from onecycle(), in this class.
 * 
 * @return void. This function does not return any value.
 */ 
void
sasim::onecycle_ifetch()
{
  il1.onecycle(cycle);

  while (predq.ready(cycle) && il1.fetchbuf.ready(cycle) && !feq.full()) {
    inst_ptr iptr = predq.oldest();
    SASSERT(iptr);
    if (iptr->lineaddr != il1.fetchbuf.oldest()) {
      SASSERT(iptr->crossline);
      il1.fetchbuf.dequeue(); // Retires the additional request performed because this instruction crosses a cache line
      break;
    }
    predq.dequeue();
    SASSERT(iptr->lineaddr == il1.fetchbuf.oldest());
    feq.enqueue(iptr,cycle);
    if (iptr->block_end) {
      // last inst in the line
      il1.fetchbuf.dequeue();
      break; // at most one line per cycle
    }
  }
}

/*!
 * Performs one cycle of decode pipeline stage.
 * This function decodes n instructions according to the DECODE_RATE value. For each
 * instruction to be decoded, the following conditions must be met:
 *  1) The instruction is ready in the front-end queue.
 *  2) If the instruction being decoded is a complex one, it is the first instruction in the group.
 *  3) There must be enough entries in the ROB to add all the uop of the instruction.
 *  4) The total number of loads (ROB + loads in the instruction) must not exceed the limit LOAD_LIMIT.
 *  5) There must be enough entries in the back-end queue to add all the uop of the instruction.
 * If all the conditions are met, the function will process each uop of the instruction as follows:
 *  1) All the registers for each uop are renamed.
 *  2) It is determined if the instruction performs Floating-Point operations and gets the number
 *     of registers used.
 *  3) Loads and stores are prepared to determine dependencies between them. Store sets approach 
 *     is used.
 *  4) Each uop is inserted into the ROB and also into the back-end queue.
 * Finally, each decoded instruction is removed from the front-end queue.
 *
 * This function is called from onecycle(), in this class.
 * 
 * @return void. This function does not return any value.
 */ 
void
sasim::onecycle_decode()
{
  for (int i=0; i<DECODE_RATE; i++) {
    if (! feq.ready(cycle)) {
      break;
    }
    inst_ptr iptr = feq.oldest();
    SASSERT(iptr);
#ifdef SINGLE_COMPLEX_DECODER
    if (iptr->inst->nuops > SIMPLE_INST_UOPS) {
      // complex inst
      // can only be first instruction in group
      if (i>0) {
	break;
      }
    }
#endif

    if (rob.free_entries() < iptr->inst->nuops) {
      break;
    }

    if ((rob_loads + iptr->inst->nloads) > LOAD_LIMIT) {
      break;
    }

    if (beq.free_entries() < iptr->inst->nuops) {
      break;
    }

    // UOPS REALLY START "EXISTING" HERE 

    for (int j=0; j<iptr->inst->nuops; j++) {
      uop_ptr uptr = & iptr->simuop[j];

      rr.rename_uop(*this,uptr);
      rr.update(uptr);

      uptr->set_isfp(); // determine if it is an FP uop

      if (uptr->is_fp && ! uptr->uop->is_mem()) {
	nfpuops++;
        if (uptr->uop->rdtype == SA_REG_INT) {
	  fpwriteint++;
	}
	for (int k=0; k<UOPNS; k++) {
	  if (uptr->uop->rstype[k] == SA_REG_INT) {
	    fpreadint++;
	    break;
	  }
	}
      }

      if (uptr->uop->uoptype == SA_UOP_STORE) {
	ss.process_store(uptr->iptr->inst->pc,uptr->num);
	uptr->storesetdep = NOTHING;
      } else if (uptr->uop->uoptype == SA_UOP_LOAD) {
	uptr->storesetdep = ss.get_memdep(uptr->iptr->inst->pc);
      }

      // insert in ROB
      rob.enqueue(uptr,cycle);

      if (uptr->uop->uoptype == SA_UOP_LOAD) {
	rob_loads++;
	SASSERT(rob_loads <= LOAD_LIMIT);
      }

      beq.enqueue(uptr,cycle);
    } // end of uop processing

    feq.dequeue();
  } // end if instructions decoding
}

/*!
 * This function is in charge of dispatching up to n uops to the different queues (scheduling queues).
 * The number n is the value given by the macro DISPATCH_RATE. If there are no uops ready in the back-end queue,
 * this function will be aborted.
 * According to the type of uop, uops are steered to the queues as follows:
 *  1) NOP: this uop is considered to be already executed and therefore is not steered to any queue.
 *  2) LOAD: a few queues are employed to steer the different loads found in the instruction stream. Round Robin
 *           approach is used to steer a load uop to these queues. Basically each load is added to the next queue 
 *           available, it does not matter whether the current queue still has entries available.
 *  3) STORE: Two copies of the store are maintained during its execution. One is steered to the scheduling
 *            queues (integer or floating point queues) using Round Robin or Dependence-Based steering approach.
 *            It will be treated as a MOV uop. The other copy is sent to the store queue.
 *  4) IMUL, IDIV and INTLONG: they go to the first queue of the scheduling (integer). In the same way uops such 
 *     as FPDIV and FPLONG go to the first queue (floating point).
 *  5) The rest of the uops are treated as the store uop, but without using the store queue.
 * Statistics for some special uops are collected.
 * Finally, uops dispatched are removed from the back-end queue.
 * When a uop is being dispatched, if one of the queues are full, the process will be postponed until the 
 * microprocessor makes room for dispatching more uops.
 *
 * This function is called from onecycle(), in this class.
 * 
 * @return void. This function does not return any value.
 */ 
void
sasim::onecycle_dispatch()
{
  for (int i=0; i<DISPATCH_RATE; i++) {

    if (! beq.ready(cycle)) {
      break;
    }
    uop_ptr uptr = beq.oldest();
    SASSERT(uptr);

    // insert in dedicated queue
    switch (uptr->uop->uoptype) {
    case SA_UOP_NOP:
      nnops++;
      //uptr->set_executed(cycle);
      uptr->executed = true;
      break;
    case SA_UOP_LOAD:
      {
	int k = steer.chooseld(ldq,uptr);
	if ((k<0) || ldq[k].full()) return;
	SASSERT(k<NUM_LDQ);
	uptr->which_ldq = k;
	ldq[k].enqueue(uptr,cycle);
	numloads++;
      }
      break;
    case SA_UOP_STORE:
      // one copy goes in the STQ, another copy goes in the schedulers
      // this second copy will be treated like a MOV uop
      if (stq.full()) return;
      if (uptr->is_fp) {
	int k = steer.choosefp(fpq,uptr);
	if ((k<0) || fpq[k].full()) return;
	fpq[k].enqueue(uptr,cycle);
      } else {
	int k = steer.chooseint(intq,uptr);
	if ((k<0) || intq[k].full()) return;
	intq[k].enqueue(uptr,cycle);
      }
      stq.enqueue(uptr,cycle);
      numstores++;
      break;
    case SA_UOP_IMUL:
      if (intq[0].full()) return;
      intq[0].enqueue(uptr,cycle);
      nimul++;
      break;
    case SA_UOP_IDIV:
      if (intq[0].full()) return;
      intq[0].enqueue(uptr,cycle);
      nidiv++;
      break;
    case SA_UOP_INTLONG:
      if (intq[0].full()) return;
      intq[0].enqueue(uptr,cycle);
      nintlong++;
      break;
    case SA_UOP_FPDIV:
      if (fpq[0].full()) return;
      fpq[0].enqueue(uptr,cycle);
      nfpdiv++;
      break;
    case SA_UOP_FPLONG:
      if (fpq[0].full()) return;
      fpq[0].enqueue(uptr,cycle);
      nfplong++;
      break;
    default: 
      if (uptr->is_fp) {
	int k = steer.choosefp(fpq,uptr);
	if ((k<0) || fpq[k].full()) return;
	fpq[k].enqueue(uptr,cycle);
      } else {
	int k = steer.chooseint(intq,uptr);
	if ((k<0) || intq[k].full()) return;
	intq[k].enqueue(uptr,cycle);
      }
      break;
    }

    if (uptr->is_fp) {
      if (uptr->uop->uoptype == SA_UOP_MOV) {
	fpmovs++;
      } else if (uptr->uop->is_fpsse()) {
	nfpsse++;
      } else if (! uptr->uop->is_mem()) {
	fpother++;
      }
    } else {
      if (uptr->uop->is_fpsse()) {
	fpsseonint++;
      }
      if (uptr->uop->uoptype == SA_UOP_SHIFT) {
	nshift++;
      }
    }
    
    beq.dequeue();
  }
}

/*!
 * Looks for load uops in the Re-order buffer that were already scheduled and whose range of addresses 
 * accessed overlaps with the ones accessed by the store that is just being retired. It means, this
 * function looks for loads that might depend on the store being retired and that have been already
 * scheduled.
 * 
 * This function is used when store uops are retired at Retire Pipeline Stage.
 * 
 * @param stuop Pointer to the store being evaluated
 * @return void. This function does not return any value.
 */ 
void
sasim::check_memtrap(uop_ptr stuop)
{
  SASSERT(stuop);
  SASSERT(stuop->uop->uoptype == SA_UOP_STORE);
  rob.save_readptr();
  rob.scan_forward();
  while (uop_ptr * p = rob.read()) {
    uop_ptr uptr = *p;
    SASSERT(uptr);
    if (uptr->uop->uoptype != SA_UOP_LOAD) {
      continue;
    }
    if (uptr->forwardingstore >= stuop->num) {
      // that LOAD got its data from a subsequent STORE, ignore any dep so far
      continue; // continue checking
    }
    if (uptr->scheduled && OVERLAP(stuop->addr,stuop->nbytes,uptr->addr,uptr->nbytes)) {
      uptr->iptr->memtrap = true;
      uptr->iptr->memtrap_storepc = stuop->iptr->inst->pc;
      break;
    }
  }
  rob.restore_readptr();
}

/*!
 * Looks for stores in the store queue upon which the load being evaluated depends and
 * whose range of addresses accessed overlaps. For the stores found, if they  
 * already have their address and data computed, this function returns TRUE.
 * This instruction also returns TRUE if there are no dependencies.
 * 
 * This function is used when load uops are scheduled using ORACLE_FORWARDER. If the
 * load has dependencies and they are not ready, the load will not be scheduled.
 * 
 * @param lduop Pointer to the load being evaluated
 * @return Boolean value the indicate if the dependencies are ready (if they exist).
 */ 
bool
sasim::check_memdepready(uop_ptr lduop)
{
  // returns TRUE iff dependent STOREs in the STQ have their address and data ready
  SASSERT(lduop);
  SASSERT(lduop->uop->uoptype == SA_UOP_LOAD);
  stq.scan_forward();
  while (uop_ptr * p = stq.read()) {
    uop_ptr uptr = *p;
    SASSERT(uptr);
    SASSERT(uptr->uop->uoptype==SA_UOP_STORE);
    if (uptr->num >= lduop->num) {
      break;
    }
    if (OVERLAP(lduop->addr,lduop->nbytes,uptr->addr,uptr->nbytes)) {
      if (!uptr->ready()) {
	return false;
      }
    }
  }
  return true;
}

/*!
 * Looks for the store in the store queue that can forward the data required by the load 
 * being evaluated in this function. 
 *
 * This function is employed when loads are scheduling using ORACLE_REALFWD. If any store
 * in the queue can forward data to the load being evaluated, it will be compared with
 * the predicted store in the store sets to determine the most suitable one to forward the
 * data.
 * 
 * @param lduop Pointer to the load whose source data will be forwarded from the stores
 *              previously executed
 * @return 64bit integer representing the number of the uop that can forward its value
 */ 
int64_t
sasim::check_forwarding(uop_ptr lduop)
{
  // returns unum of STORE in the STQ that can forward its data to the LOAD
  // (possible mispeculation here)
  SASSERT(lduop);
  SASSERT(lduop->uop->uoptype == SA_UOP_LOAD);
  stq.scan_backward();
  while (uop_ptr * p = stq.read()) {
    uop_ptr uptr = *p;
    SASSERT(uptr);
    SASSERT(uptr->uop->uoptype==SA_UOP_STORE);
    if (uptr->num >= lduop->num) {
      continue;
    }
    if (OVERLAP(lduop->addr,lduop->nbytes,uptr->addr,uptr->nbytes) && uptr->address_ready()) {
      // dependence is detected
      if (uptr->store_data_ready() && (lduop->addr >= uptr->addr) && ((lduop->addr+lduop->nbytes) <= (uptr->addr+uptr->nbytes))) {
	// STORE can provide full data to LOAD
	return uptr->num;
      } else {
	return NOTHING;
      }
    }
  }
  return NOTHING;
}

/*!
 * Flushes the contents of the entire Instruction Window Queues: ROB, RR, Instruction/Data Cache L1, 
 * Front-End, Back-End, Load, Store, Scheduling, Functional Units, TLB, Post Retirement, Store sets.
 * 
 * This function is used at Retire Pipeline Stage when a load is detected to be mispeculated.
 * 
 * @param memtrap Indicates if the instructions stored in the window queue must be reset.
 * @return void. This function does not return any value.
 */ 
void
sasim::flush(bool memtrap)
{
  if (memtrap) {
    // the instruction triggering the flush is the oldest instruction in the ROB
    // that instruction is squashed too
    SASSERT(!rob.empty()); 

    SASSERT(window.scandir >= 0);
    inst_ptr iptr = NULL;
    do {
      DECPTR(window.readptr,window.size);
      window.unread++;
      SASSERT(window.unread <= window.occupancy);
      iptr = window.q[window.readptr];
      SASSERT(iptr);
      iptr->reset_inst();
    } while (iptr != rob.oldest()->iptr);
    SASSERT(window.head == window.readptr);
  }

  rob.flush();
  rob_loads = 0;
  rr.repair(ret_rr);
  pendingmisp = NULL;
  predq.flush();
  currentline = 0;
  il1.fetchreq.flush();
  il1.fetchbuf.flush();
  il1.mq.flush();
  feq.flush();
  beq.flush();
  for (int i=0; i<NUM_LDQ; i++) {
    ldq[i].flush();
  }
  stq.flush();
  for (int i=0; i<NUM_ALU; i++) {
    intq[i].flush();
    alu[i].flush();
  }
  for (int i=0; i<NUM_FPADDMUL; i++) {
    fpq[i].flush();
    fpaddmul[i].flush();
    fpmov[i].flush();
  }
  imul.flush();
  idiv.flush();
  fpdiv.flush();
  dl1.mq.flush();
  dl1.flush();
  dl1.pm.clear();
  tlb.flush();
  prstq.scan_forward();
  while (uop_ptr * p = prstq.read()) {
    uop_ptr uptr = *p;
    SASSERT(uptr);
    uptr->reschedule(cycle);
  }
  ss.clear_lfst();
}

/*!
 * This function distributes the loads stored in the load queue to the different ports of the
 * data cache memory level one. There are three ways to do that according to the macros defined
 * in the sasim_uarch_defs.hpp file:
 *  1) Oracle forwarder: the instruction is assigned to one of the ports only when the dependencies
 *     are already being processed (if they exist) or when there are no dependencies.
 *  2) Store sets, forward: determines the instruction in the store queue that might forward the
 *     required data to the load. Also determines if the load belongs to a store set previously
 *     defined. If so, the most suitable store is selected to forward the data and if it has
 *     already produced its data, the load is assigned to one of the ports.
 *  3) Store sets, no forward: determines if the instruction belongs to one of the store sets. If so,
 *     the predicted store is checked to determine if it has already produced its data to be able
 *     to assign the load to one of the cache ports. No checks into the store queue are performed.
 * 
 * This function is called from onecycle_schedule.
 * 
 * @param dl1_uptr Vector of references to the data cache memory level-one ports.
 * @return void. This function does not return any data.
 */ 
void
sasim::schedule_load(uop_ptr dl1_uptr[DL1_PORTS])
{
  SASSERT(NUM_LDQ <= DL1_PORTS);
  int k = 0;
  for (int i=0; i<DL1_PORTS; i++) {
    if (dl1_uptr[i]) {
      k++; // some stores have priority for this cycle
    }
  }
  
  for (int i=0; i<NUM_LDQ; i++) {
    int j = k+i;
    SASSERT(j<DL1_PORTS);
    SASSERT(dl1_uptr[j]==NULL);
    ldq[i].scan_forward();
    while (uop_ptr * p = ldq[i].read(cycle)) {
      uop_ptr uptr = *p;
      SASSERT(uptr);
      SASSERT(uptr->uop->uoptype == SA_UOP_LOAD);
#if defined (LOADSTORE_ORACLEFWD)
      if (uptr->scheduled || ! uptr->ready() || ! check_memdepready(uptr)) {
	continue;
      }
#elif defined (LOADSTORE_REALFWD)
      // store sets, forward
      if (uptr->scheduled || ! uptr->ready()) {
	continue;
      }
      uptr->forwardingstore = check_forwarding(uptr);
      if ((uptr->storesetdep != NOTHING) && (uptr->storesetdep > uptr->forwardingstore)) {
	// not a safe forwarding
	uptr->forwardingstore = NOTHING;
	// check if predicted dependent store still in STQ
	if (stq.ready(cycle) && (uptr->storesetdep >= stq.oldest()->num)) {
	  continue; // store not yet retired, try another LOAD
	}
      }
      // LOAD can execute (possible mispeculation)
      // LOAD whose data is obtained from a forwarding STORE still accesses the DL1
#else
      // store sets, no forward
      if (uptr->scheduled || ! uptr->ready()) {
	continue;
      }
      if (uptr->storesetdep != NOTHING) {
	// check if predicted dependent store still in STQ
	if (stq.ready(cycle) && (uptr->storesetdep >= stq.oldest()->num)) {
	  continue; // store not yet retired
	}
      }
      // speculate that there is no dependence
#endif      
      dl1_uptr[j] = uptr;
      break; // go to next LDQ
    }
  }
}

/*!
 * This function distributes stores located in the post-retirement store queue to the
 * available ports in the data cache memory level-one to finally write the data.
 * 
 * This function is called from onecycle_schedule.
 * 
 * @param dl1_uptr Vector of reference to the data cache memory level-one ports.
 * @return void. This function does not return any data.
 */ 
void
sasim::schedule_store(uop_ptr dl1_uptr[DL1_PORTS])
{
  int nmem = 0;
  int k = -1;
  for (int i=0; i<DL1_PORTS; i++) {
    if (dl1_uptr[i]) {
      k = i; // some loads have been scheduled in the same cycle
    }
  }
  k++;
  prstq.scan_forward();
  while (uop_ptr * p = prstq.read(cycle)) {
    SASSERT((k+nmem) < DL1_PORTS);
    uop_ptr uptr = *p;
    SASSERT(uptr);
    SASSERT(uptr->uop->uoptype == SA_UOP_STORE);
    if (uptr->scheduled) {
      continue;
    }
    SASSERT(uptr->ready()); // stores in post-retire store queue are ready
    SASSERT(dl1_uptr[k+nmem]==NULL);
    dl1_uptr[k+nmem] = uptr;
    nmem++;
    if (nmem==DL1_WRITE_PORTS) {
      break;
    }
  }
}

// TODO: rescheduling penalty is not modeled

/*!
 * This function steers all the different uops to the functional units and data cache memory ports.
 * Basically, the wakeup and selection of instructions are performed in this function.
 * 
 * The first uops to be scheduled are the store and load ones. Initially, all the cache ports are
 * checked to determine that none of them is full. If one of the ports is full, the scheduling of 
 * load/stores is postponed. Otherwise, those uops are distributed to the different ports using the
 * functions previously described. The order depends on the amount of stores/loads already inserted
 * in the different queues (post-retirement and load queues). The information provided by the
 * functions schedule_load() and schedule_store() is then used to fill the different ports of the
 * cache as long as there is no any bank conflict.
 * 
 * Then, integer uops are taken from the integer scheduling queues and steered to the integer
 * functional units. At most one uop from each integer queue is steered to the respective
 * integer ALU, given priority to the oldest one that has all its operands ready.
 * 
 * Finally, fp uops are taken from the fp scheduling queues and steered to the fp functional 
 * units. At most one uop from each floating point queue is steered to the respective
 * floating point ALU, given priority to the oldest one that has all its operands ready.
 * 
 * This function is called from onecycle(), in this class.
 * 
 * @return void. This function does not return any data.
 */ 
void
sasim::onecycle_schedule()
{
  // schedule for the DL1 cache

  bool ports_ok = true;
  uop_ptr dl1_uptr[DL1_PORTS];
  for (int i=0; i<DL1_PORTS; i++) {
    dl1_uptr[i] = NULL;
  }

  for (int i=0; i<DL1_PORTS; i++) {
    if (dl1.port[i].full()) {
      ports_ok = false;
      break;
    }
  }

  if (ports_ok) {

    int ldqoccup = 0;
    for (int i=0; i<NUM_LDQ; i++) {
      ldqoccup += ldq[i].occupancy;
    }
    if (prstq.full() || (prstq.occupancy >= ldqoccup)) {
      // stores have priority
      schedule_store(dl1_uptr);
      schedule_load(dl1_uptr);
    } else {
      // loads have priority
      schedule_load(dl1_uptr);
      schedule_store(dl1_uptr);
    }

    uint64_t dl1banks = 0;

    for (int i=0; i<DL1_PORTS; i++) {
      if (! dl1_uptr[i]) {
	continue;
      }
#ifndef PERFECT_DL1_PORTS
      if ((dl1banks & dl1_uptr[i]->dl1banks) != 0) {
	// bank conflict
	dl1_uptr[i]->forwardingstore = NOTHING;
	continue; 
      }
#endif
      dl1.port[i].enqueue(dl1_uptr[i],cycle);
      dl1_uptr[i]->scheduled = true;
      dl1banks |= dl1_uptr[i]->dl1banks;
    }
  
  }

  // schedule for the int Qs
  for (int i=0; i<NUM_ALU; i++) {
    intq[i].scan_forward();
    while (uop_ptr * p = intq[i].read()) {
      uop_ptr uptr = *p;
      SASSERT(uptr);
      if (uptr->uop->uoptype == SA_UOP_STORE) {
	if (uptr->scheduled || ! uptr->store_data_ready()) {
	  continue;
	}
      } 
      else if (uptr->scheduled || ! uptr->ready()) {
	continue;
      }
      
      if ((uptr->uop->uoptype == SA_UOP_IMUL) || (uptr->uop->uoptype == SA_UOP_INTLONG)) {
	if (!imul.full()) {
	  imul.enqueue(uptr,cycle);
	  uptr->scheduled = true;
	  break;
	}
      } 
      else if (uptr->uop->uoptype == SA_UOP_IDIV) {
	if (!idiv.full()) {
	  idiv.enqueue(uptr,cycle);
	  uptr->scheduled = true;
	  break;
	}
      } 
      else {
	if (!alu[i].full()) {
	  alu[i].enqueue(uptr,cycle);
	  uptr->scheduled = true;
	  break;
	}
      }
    }
  }

  // schedule for the fp Qs
  for (int i=0; i<NUM_FPADDMUL; i++) {
    fpq[i].scan_forward();
    while (uop_ptr * p = fpq[i].read()) {
      uop_ptr uptr = *p;
      SASSERT(uptr);
      if (uptr->uop->uoptype == SA_UOP_STORE) {
	if (uptr->scheduled || ! uptr->store_data_ready()) {
	  continue;
	}
      } else if (uptr->scheduled || ! uptr->ready()) {
	continue;
      }
      if ((uptr->uop->uoptype == SA_UOP_FPDIV) || (uptr->uop->uoptype == SA_UOP_FPLONG)) {
	if (! fpdiv.full()) {
	  fpdiv.enqueue(uptr,cycle);
	  uptr->scheduled = true;
	  break;
	}
      } else if (uptr->uop->is_fpsse()) {
	if (! fpaddmul[i].full()) {
	  fpaddmul[i].enqueue(uptr,cycle);
	  uptr->scheduled = true;
	  break;
	}
      } else {
	// assume it's a mov uop or a store
	if (! fpmov[i].full()) {
	  fpmov[i].enqueue(uptr,cycle);
	  uptr->scheduled = true;
	  break;
	}
      }
    }
  }

}

/*!
 * This function calls the respective one cycle function of the blocks
 * Memory, L3 cache (if it exists), L2 Cache and L1 data cache.
 * 
 * This function is called from onecycle_execute.
 * 
 * @return void. This function does not return any data.
 */ 
void
sasim::onecycle_execute_mem()
{
  mem.onecycle(cycle);
#ifdef ENABLE_L3
  l3.onecycle(cycle);
#endif
  l2.onecycle(cycle);
  dl1.onecycle(cycle);
}

/*!
 * Executes one cycle in each integer functional unit. If the uop is ready, its state is updated
 * to indicate that the uop was executed. In addition, the uop is removed from the scheduling
 * queue that contains it.
 * 
 * Branches could be resolved here (if it was specified in the configuration). The branch 
 * predictor is repaired and updated at this point and also the misprediction flag is reset
 * to allow the fetch stage to continue fetching instructions (it was stall because of the
 * misprediction). Branches can also be resolved at retirement stage.
 * 
 * This function is called from onecycle_execute.
 * 
 * @return void. This function does not return any data.
 */ 
void
sasim::onecycle_execute_int()
{
  // IMUL
  if (imul.ready(cycle)) {
    uop_ptr uptr = imul.dequeue();
    SASSERT(uptr);
    uptr->set_executed(cycle,&intq[0]);
  }

  // IDIV
  if (idiv.ready(cycle)) {
    uop_ptr uptr = idiv.dequeue();
    SASSERT(uptr);
    uptr->set_executed(cycle,&intq[0]);
  }

  // ALUs
  for (int i=0; i<NUM_ALU; i++) {
    if (alu[i].ready(cycle)) {
      uop_ptr uptr = alu[i].dequeue();
      SASSERT(uptr);
      uptr->set_executed(cycle,&intq[i]);
#ifndef PERFECT_BPRED
#ifdef BRANCH_RESOL_AT_EXEC
      if (uptr->uop->is_branch()) {
	SASSERT(uptr->iptr);
	if (uptr->iptr->mispredict) {
	  SASSERT(pendingmisp == uptr->iptr);
	  pendingmisp = NULL;
	  //uptr->iptr->mispredict = false;
	  bp.repair(uptr->iptr,uptr->iptr->nextinst->inst->pc);
	  bp.updatehist(uptr->iptr,uptr->iptr->nextinst->inst->pc);
	}
      }
#endif
#endif
    }
  }
}

/*!
 * Executes one cycle in each FP functional unit. If the uop is ready, its state is updated
 * to indicate that the uop was executed. In addition, the uop is removed from the scheduling
 * queue that contains it.
 * 
 * This function is called from onecycle_execute.
 * 
 * @return void. This function does not return any data.
 */ 
void
sasim::onecycle_execute_fp()
{
  // FDIV
  if (fpdiv.ready(cycle)) {
    uop_ptr uptr = fpdiv.dequeue();
    SASSERT(uptr);
    uptr->set_executed(cycle,&fpq[0]);
  }

  // FP add/mul
  for (int i=0; i<NUM_FPADDMUL; i++) {
    if (fpaddmul[i].ready(cycle)) {
      uop_ptr uptr = fpaddmul[i].dequeue();
      SASSERT(uptr);
      uptr->set_executed(cycle,&fpq[i]);
    }
    if (fpmov[i].ready(cycle)) {
      uop_ptr uptr = fpmov[i].dequeue();
      SASSERT(uptr);
      uptr->set_executed(cycle,&fpq[i]);
    }
  }
}

/*!
 * Performs one cycle of the execution pipeline stage by using the functions: 
 * onecycle_execute_mem() for memory (cache and memory), 
 * onecycle_execute_int() for integer functional units and
 * onecycle_execute_fp() for floating point functional units.
 * 
 * This function is called from onecycle, in this class.
 * 
 * @return void. This function does not return any data.
 */ 
void
sasim::onecycle_execute()
{
  onecycle_execute_mem();
  onecycle_execute_int();
  onecycle_execute_fp();
}

/*!
 * This function removes instructions from the ROB that have already completed their operations. 
 * They are removed in in-order form. The number of uops to retire is given by the macro 
 * RETIRE_RATE.
 * Uops are explored until the one that corresponds to the end of an instruction is found.
 * During this process, if there are uops without having finished their operations, the retire
 * process is cancelled.
 * Uops are retired from the ROB one by one until reaching the maximum number of uops to
 * retire. It means that an instruction that has all its uops ready might not be completely
 * retired in the current cycle.
 * When uops are retired from the ROB, the following operations are also performed:
 *  1) LOAD uop: the load counter is decreased.
 *  2) STORE uop: the uop is retired from the store queue and added to the post-retirement
 *     queue. The uop is also rescheduled to perform the real written to the memory.
 *  3) Last uop: the instruction is removed from the instruction window. Also the reference
 *     that was got from the freepool is returned back only if there are no store uops in
 *     the post-retirement queue. Otherwise, the reference is returned back to the freepool
 *     in the post-retirement execution.
 *  4) If the number of uops retired have reached the maximum number allowed, the retire
 *     process is cancelled. The instruction will be completely retired in the next cycle.
 * 
 * If an instruction has the memtrap field set, the Re-Order Buffer must be flushed (it might
 * indicate that there was a mispeculated load for example). This instruction will be the 
 * oldest one in the ROB.
 * 
 * Branches can be resolved here if the simulator was configured to do that and not at 
 * execute stage. When a branch is resolved, the misprediction flag is reset if the branch
 * instruction was mispredicted at prediction stage.
 * 
 * This function is called from onecycle, in this class.
 * 
 * @return void. This function does not return any data.
 */ 
void
sasim::onecycle_retire()
{
  int n = 0, nret = 0;

  rob.scan_forward();

  while (uop_ptr *p = rob.read(cycle)) {

    uop_ptr rob_uptr = *p;
    SASSERT(rob_uptr);

    if (! rob_uptr->executed) {
      break;
    }

    if (rob_uptr->uop->uoptype == SA_UOP_STORE) {
      SASSERT(rob_uptr->address_ready()); 
      SASSERT(rob_uptr->iptr->inst->nstores <= 1);
      if (prstq.full()) {
	// no room in post-retirement store queue
	break;
      }
    }

    if (rob_uptr->iptr->memtrap) {
      nmemtrap++;
      bp.repair(rob_uptr->iptr,rob_uptr->iptr->nextinst->inst->pc);
      ss.record_dep(rob_uptr->iptr->memtrap_storepc,rob_uptr->iptr->inst->pc);
      flush(true);
      return;
    }

    n++;

    if (rob_uptr->lastuop) {
      // inst is executed, can retire all or part of its uops

      for (int i=0; i<n; i++) {

	uop_ptr uptr = rob.dequeue();

	ret_rr.update(uptr);

	switch (uptr->uop->uoptype) { 
	case SA_UOP_LOAD:
	  SASSERT(rob_loads > 0);
	  rob_loads--;
	  SASSERT(uptr->memtranslated);
#ifdef STRIDE_PREFETCH
	  spf.update(uptr,cycle);
#endif
	  break;
	case SA_UOP_STORE:
	  SASSERT(uptr == stq.oldest());
	  stq.dequeue();
#ifndef LOADSTORE_ORACLEFWD
	  check_memtrap(uptr);
#endif
	  // the store has only been pre-executed so far (data read, virtual address computed)
	  // TLB access and actual write happen at post-retirement
	  uptr->reschedule(cycle);
	  prstq.enqueue(uptr,cycle);
	  break;
	default: break;
	}

	if (uptr->lastuop) {
	  // inst is completely retired
	  SASSERT(i==(n-1));
	  SASSERT(!window.empty());
	  inst_ptr iptr = window.dequeue();
	  SASSERT(iptr);
	  SASSERT(iptr == rob_uptr->iptr);

	  if (iptr->inst->nstores == 0) {
	    // inst has no store, window entry can be freed right away
	    freepool.enqueue(iptr);
	  } 
	  n = 0;
	  if (! iptr->inst->is_nop()) {
	    if ((iptr->exec_cycle - iptr->pred_cycle) < minexecdelay) {
	      minexecdelay = iptr->exec_cycle - iptr->pred_cycle;
	    }
	    if ((cycle - iptr->pred_cycle) < minretiredelay) {
	      minretiredelay = cycle - iptr->pred_cycle;
	    }
	  }
#ifndef PERFECT_BPRED
	  if (iptr->nextinst) {
	    bp.update(iptr,iptr->nextinst->inst->pc);
	  } else {
	    SASSERT(rob.empty()); // end of trace
	  }
	  if (iptr->mispredict) {
	    nbranchmisp++;
#ifndef BRANCH_RESOL_AT_EXEC
	    SASSERT(pendingmisp == iptr);
	    pendingmisp = NULL;
	    bp.repair(iptr,iptr->nextinst->inst->pc);
	    bp.updatehist(iptr,iptr->nextinst->inst->pc);
#endif
	  }
#endif
	}
	last_retire_cycle = cycle;
	nret++;
	if (nret == RETIRE_RATE) {
	  return;
	}
      }
    }
  }
}

/*!
 * This function removes the store uops from the post-retirement queue when they have
 * finished their operations. 
 * They are removed in in-order form. The number of uops to retire is given by the macro 
 * STORE_RETIRE_RATE.
 * 
 * This function is called from onecycle, in this class.
 * 
 * @return void. This function does not return any data.
 */ 
void
sasim::onecycle_post_retirement()
{
  // retire stores from the post-retirement store queue
  int ns = 0;
  prstq.scan_forward();
  while (uop_ptr *p = prstq.read(cycle)) {
    uop_ptr uptr = *p;
    SASSERT(uptr);
    if (! uptr->executed) {
      break;
    }
    SASSERT(uptr->memtranslated);
    // cannot free the inst entry if some uops from same inst not yet retired
    if (rob.ready(cycle) && (rob.oldest()->iptr == uptr->iptr)) {
      artificialdelay++;
      break;
    }
    prstq.dequeue();
    SASSERT(uptr->iptr->inst->nstores == 1);
    if (window.empty() || (window.oldest()->num > uptr->iptr->num)) {
      freepool.enqueue(uptr->iptr);
    } else {
      uptr->iptr->store_completed = true;
    }
    ns++;
    if (ns==STORE_RETIRE_RATE) {
      break;
    }
  }
}

/*!
 * This function calls all the functions that perform one stage of the pipeline.
 * 
 * This functions is called from the function input in this class. It is performed when
 * the instruction window (simulator instruction window) is full of instructions taken
 * from the traces.
 * 
 * @return void. This function does not return any data.
 */ 
void 
sasim::onecycle()
{ 
  // Pipeline stages are executed backwards to simulate execution in parallel.
  tlb.onecycle(cycle);
  onecycle_post_retirement();
  onecycle_retire();
  onecycle_execute(); // execute must be called before schedule
  onecycle_schedule();
  onecycle_dispatch();
  onecycle_decode();
  onecycle_ifetch();
  onecycle_pred();

  cycle++;
  SASSERT(cycle < INFINITE_INTVAL);

  ss.periodic_clearing(cycle);
  int checkpool = window.occupancy + freepool.occupancy + prstq.occupancy - MAXWINDOW;
  SASSERT((checkpool==0) || (checkpool==1));
  // the case checkpool=1 happens when all uops from same inst cannot 
  // be retired in the same cycle because retirement rate is exceeded
  // STORE may be retired and corresponding inst stays in window for one extra cycle

  if ((cycle-last_retire_cycle) > SASIM_TIMEOUT) {
    cerr << "SASIM: TIMEOUT" << endl;
    cerr << "last retire cycle = " << last_retire_cycle << endl;
    cerr << "ninst = " << ninst << endl;
    cerr << "nuops = " << nuops << endl;
    tlb.debugprint_queues();
    il1.debugprint_queues();
    dl1.debugprint_queues();
    l2.debugprint_queues();
#ifdef ENABLE_L3
    l3.debugprint_queues();
#endif
    mem.debugprint_queues();
    DBPRINTQ(predq);
    DBPRINTQ(feq);
    cerr << "ROB occupancy = " << rob.occupancy << endl;
    cerr << "post-retire store queue occupancy = " << prstq.occupancy << endl;
    if (!rob.empty()) {
      cerr << "oldest : " << endl;
      cerr << "  uop num = " << rob.oldest()->num << endl;
      cerr << "  uop = " << rob.oldest()->uop->tostring() << endl;
      cerr << "  uop scheduled = " << rob.oldest()->scheduled << endl;
      cerr << "  uop executed = " << rob.oldest()->executed << endl;
      cerr << "  inst uops = " << rob.oldest()->iptr->inst->nuops << endl;
    }
    if (rob.occupancy > 1) {
      cerr << "second oldest : " << endl;
      cerr << "  uop num = " << rob.second_oldest()->num << endl;
      cerr << "  uop = " << rob.second_oldest()->uop->tostring() << endl;
      cerr << "  uop scheduled = " << rob.second_oldest()->scheduled << endl;
      cerr << "  uop executed = " << rob.second_oldest()->executed << endl;
      cerr << "  inst uops = " << rob.second_oldest()->iptr->inst->nuops << endl;
    } 
    SASSERT(0); 
  }
}

// End: sasim class
// ***************************************************************************
