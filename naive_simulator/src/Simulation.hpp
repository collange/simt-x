#ifndef __HEADER_SIMULATION__
#define __HEADER_SIMULATION__

#include "DataAnalyser.hpp"
#include <vector>
#include "lib/globals.hpp"
#include <pthread.h>
#include "sasim_tracer.hpp"


class Thread;
//class Heuristic;

struct Workerargs{
	Thread* thread;
	std::vector<uint64_t> *horizon_sequence;
	int readahead_horizon;
	int *thread_count;	
	UINT *numthreads;
	UINT *fin_count;
	UINT *thread_fin_count;
	UINT *wait_in_barrier;
	bool *wakeup;
	int *sampling_size;
};

class Simulation{
public:

	static unsigned int const readahead_horizon = 3000;
	//static unsigned int const readahead_horizon = 1000000;

private:
	Config conf;
	bool cache_warm;
	LONG cache_warm_cycles;
	UINT num_threads;
	UINT orig_num_threads;
	Thread* threads;
#ifdef REAL_MULTI_CORE
	std::vector<DataAnalyser*> analyser;
	Cache l2;
#else
	DataAnalyser *analyser;
#endif
	LONG fetch_cycle;
	bool wakeup_from_barrier;

	std::vector<uint64_t> horizon_sequence;

	UINT finished; //Number of finished threads
	UINT reader_finished_count;
	UINT waiting_in_barrier;

	LONG cache_warm_insts;

	LONG total_instruction_count;
	LONG simulation_instructions;
	LONG *thread_instruction_count;


	void check_valid();
	bool check_valid_fix();
	void read_instruction(Instruction & insn, FILE * file) const;

	pthread_t* refill();
	pthread_t*  fill();

	//void* getins(void* wa);

	int thread_exit_count;

	//For Dynamic vectorization. Added by Sajith.
	//This holds the mask for registers
	int **reg_mask;

	void log_ditva_stats();

public:
	Simulation(UINT num_threads, UINT num_fetches, const char** arqvs, DataAnalyser::HeuristicData* fetch_h,Config conf);
	~Simulation();

	void simulator();

	LONG getTotalInstructions(){
		return total_instruction_count;
	}

	LONG getInstructionCountOfThread(int threadid){
		return thread_instruction_count[threadid];
	}



	/*DataAnalyser* get_analyser(){
		return analyser;
	}*/
	bool exit_sim();

	//void cache_warm();
};

#endif
