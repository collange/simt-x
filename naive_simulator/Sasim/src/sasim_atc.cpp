#include "sasim_atc.hpp"

#ifdef USE_ATC

// ***************************************************************************
// Begin: astream class

void astream::open(char atcmode, const char filename[], const char ext[], const char command[])
{
  atc_open(&fileaddr,atcmode,filename,ext,command);
}

void astream::close()
{
  atc_close(&fileaddr);
}

void astream::write(sa_addr addr) 
{
  atc_code(&fileaddr,(ATC_Addr) addr);
}

bool astream::read(sa_addr & addr)
{
  int r = atc_decode(&fileaddr,(ATC_Addr *) (& addr));
  if (r==0) {
    return true; // EOF
  } else {
    return false;
  }
}

// End: astream class
// ***************************************************************************

#endif
