#!/usr/bin/env bash

directory=OoO/8T_niave
outfile=benchmark_stats_8Tnaive.csv

firstfile="$directory"/fft.csv
line=$(head -n 1 $firstfile)

rm $outfile
echo $line >> $outfile

for filename in "$directory"/*.csv 
do
    sed -n '2p' $filename >> $outfile
done

