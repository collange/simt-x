#include "sasim_steering.hpp"

// ***************************************************************************
// Begin: steering class

int 
steering::chooseld(saqueue<uop_ptr> q[], uop_ptr uptr)
{
  // round robin
  int j = kl;
  INCPTR(kl,LOAD_ISSUE);
  return j;
}

// End: steering class
// ***************************************************************************

// ***************************************************************************
// Begin: steering_rr class

void 
steering_rr::init()
{
  ki = 0;
  kf = 0;
  kl = 0;
}

/*!
 * Chooses one queue to steer the current integer uop using a round-robin approach
 * 
 * @param q[] Array of uops
 * @param uptr Uop to steer
 * @return Integer value that indicates the queue selected
 */ 
int 
steering_rr::chooseint(saqueue<uop_ptr> q[], uop_ptr uptr)
{
  int j = ki;
  INCPTR(ki,NUM_ALU);
  return j;
}

/*!
 * Chooses one queue to steer the current floating point uop using a round-robin approach
 * 
 * @param q[] Array of uops
 * @param uptr Uop to steer
 * @return Integer value that indicates the queue selected
 */ 
int 
steering_rr::choosefp(saqueue<uop_ptr> q[], uop_ptr uptr)
{
  int j = kf;
  INCPTR(kf,NUM_FPADDMUL);
  return j;
}

// End: steering_rr class
// ***************************************************************************

// ***************************************************************************
// Begin: steering_dep class

void 
steering_dep::init()
{
  kl = 0;
}

/*!
 * Searches if a certain uop already exists in the last entry of one of the uop queues (dependence-based)
 * 
 * @param q[] Array of uops
 * @param n Number of queues
 * @param uptr Uop to search
 * @return Integer value that indicates the queue. -1 if the function fails.
 */ 
int 
steering_dep::depq(saqueue<uop_ptr> q[], int n, uop_ptr uptr) 
{
  for (int i=0; i<n; i++) {
    if (!q[i].empty() && (q[i].last() == uptr)) {
      return i;
    }
  }
  return -1;
}

/*!
 * Searches the queue with the lowest occupancy 
 * 
 * @param q[] Array of uops
 * @param n Number of queues
 * @return Integer value that indicates the queue with the lowest occupancy
 */ 
int 
steering_dep::minq(saqueue<uop_ptr> q[], int n)
{
  int m = q[0].size+1;
  int k = -1;
  for (int i=0; i<n; i++) {
    if (q[i].occupancy < m) {
      m = q[i].occupancy;
      k = i;
    }
  }
  SASSERT(k>=0);
  return k;
}

/*!
 * Searches for the queue that contains instructions that produce the source(s) for the current uop.
 * If no instructions are found, this function will take the queue with the lowest occupancy value
 * 
 * @param q[] Array of uops
 * @param n Number of queues
 * @param uptr Uop to steer
 * @return Integer value that indicates the queue.
 */ 
int 
steering_dep::choose(saqueue<uop_ptr> q[], int n, uop_ptr uptr)
{
  int k = -1;
  for (int i=0; i<UOPNS; i++) {
    int j = depq(q,n,uptr->uoprs[i]);
    if (j<0) continue;
    if (q[j].full()) continue;
    if ((k<0) || (q[j].occupancy < q[k].occupancy)) { // If there are dependencies, use the take with more entries available
      k = j;
    }
  }
  if (k < 0) {
    k = minq(q,n);
  }
  SASSERT(k < n);
  return k;
}

/*!
 * Chooses the most appropriate integer queue for the current uop
 * 
 * @param q[] Array of uops
 * @param uptr Uop to steer
 * @return Integer value that indicates the queue.
 */ 
int 
steering_dep::chooseint(saqueue<uop_ptr> q[], uop_ptr uptr)
{
  return choose(q,NUM_ALU,uptr);
}

/*!
 * Chooses the most appropriate floating point queue for the current uop
 * 
 * @param q[] Array of uops
 * @param uptr Uop to steer
 * @return Integer value that indicates the queue.
 */ 
int 
steering_dep::choosefp(saqueue<uop_ptr> q[], uop_ptr uptr)
{
  return choose(q,NUM_FPADDMUL,uptr);
}

// End: steering_dep class
// ***************************************************************************

