#include "sasim_branchpred.hpp"
#include "sasim_misc.hpp"
// The following file must be included here and not in sasim_branchpred.hpp due to declaration issues
#include "sasim_uop_inst.hpp"

// ***************************************************************************
// Begin: Path History Class

/*!
 * Sets all entries of the circular buffers with zeros (they will set also valid)
 */ 
path_history::path_history() 
{
  for (int i=0; i<SASIM_MAXPH; i++) {
    h[i] = 0;
    valid[i] = true;
  }
  ptr = 0;
}

/*!
 * Pushes a new value into the circular buffer h and decrements its pointer by 1. The same entry of circular buffer valid is set to true
 * if all the entries have already been written, the oldest element is replaced
 * 
 * @param val Element to be pushed into the circular buffer
 * @return void The function does not return any value
 */ 
void 
path_history::insert(unsigned val)
{
  DECPTR(ptr,SASIM_MAXPH);
  h[ptr] = val;
  valid[ptr] = true;
}

/*!
 * Returns the reference of position n in the circular buffer (0 is the last element pushed)
 * 
 * @param n Element to be returned
 * @return unsigned References 
 */ 
unsigned & 
path_history::operator [] (int n)
{
  SASSERT((n>=0) && (n<SASIM_MAXPH));
  int k = ptr + n;
  if (k >= SASIM_MAXPH) {
    k -= SASIM_MAXPH;
  }
  SASSERT((k>=0) && (k<SASIM_MAXPH));
  SASSERT(valid[k]);
  return h[k];
}

/*!
 * Returns the current value of the circular buffer pointer
 * 
 * @return int The value of the pointer
 */ 
int 
path_history::checkpoint()
{
  return ptr;
}

/*!
 * Restores the value of the circular buffer pointer and invalidates all the entries that were previously written
 * 
 * @param checkpoint Value returned by checkpoint instruction last time it was used
 * @return void The function does not return any value
 */ 
void 
path_history::repair(int checkpoint)
{
  while (ptr != checkpoint) {
    SASSERT(valid[ptr]);
    valid[ptr] = false;
    INCPTR(ptr,SASIM_MAXPH);
  }
}

// End: Path History Class
// ***************************************************************************

// ***************************************************************************
// Begin: Compressed History Class

void 
compressed_history::init(int original_length, int compressed_length, int injected_bits)
{
  comp = 0;
  olength = original_length;
  clength = compressed_length;
  nbits = injected_bits;
  outpoint = olength % clength;
  SASSERT(olength < SASIM_MAXPH);
  SASSERT(clength < 31);
  SASSERT(nbits <= clength);
  mask1 = (1<<clength)-1;
  mask2 = (1<<nbits)-1;
}

/*!
 * Rotates the bits of x to the left m times
 * 
 * @param x Reference to the variable which contents will be rotated
 * @param m Number of positions to be rotated
 * @return void. The function does not return any value
 */ 
void 
compressed_history::rotateleft(unsigned & x, int m)
{
  SASSERT(m < clength);
  SASSERT((x>>clength) == 0);
  unsigned y = x >> (clength-m);
  x = (x << m) | y;
  x &= mask1;
}

/*!
 * Compresses the history path given by h and store its result into the local variable comp
 * 
 * @param h Path History
 * @return void. The function does not return any value
 */ 
void 
compressed_history::update(path_history & h)
{
  rotateleft(comp,1);
  comp ^= h[0] & mask2;
  unsigned outbits = h[olength] & mask2;
  rotateleft(outbits,outpoint);
  comp ^= outbits;
}

/*!
 * Copies the compressed history stored in c and returns a reference to this object
 * 
 * @param c Compressed History Object
 * @return compressed_history. Returns a reference to this object
 */ 
compressed_history & 
compressed_history::operator = (const compressed_history & c)
{
  comp = c.comp;
  return *this;
}

// End: Compressed History Class
// ***************************************************************************

// ***************************************************************************
// Begin: TageHist Class

/*!
 * Initializes the compressed history objects chg, cht and chtt based in the information provided by the parameters
 * 
 * @param numg
 * @param minhist
 * @param alpha
 * @param logg
 * @param tagbits
 * @param pathbits
 * @return void. The function does not return any value
 */ 
void 
tagehist::init(int numg, int minhist, double alpha, int logg, int tagbits, int pathbits) 
{
  SASSERT(numg <= MAX_NUMG);
  n = numg;
  int ghlen = minhist;
  for (int i=numg-1; i>=0; i--) {
    chg[i].init(ghlen,logg,pathbits);
    cht[i].init(ghlen,tagbits,pathbits);
    chtt[i].init(ghlen,tagbits-1,pathbits);
    ghlen *= alpha;
  }
}

/*!
 * Copies all the values of the compressed history included in the object tagehist c
 * 
 * @param c Reference to a tagehist object
 * @return tagehist. Returns a reference to this object
 */ 
tagehist & 
tagehist::operator = (const tagehist & c)
{
  n = c.n;
  for (int i=0; i<n; i++) {
    chg[i] = c.chg[i];
    cht[i] = c.cht[i];
    chtt[i] = c.chtt[i];
  }
  return *this;
}

/*! 
 * Updates all the compressed history objects with the information path history provided
 * 
 * @param ph Path History
 * @return void. The function does not return any value
 */ 
void 
tagehist::update(path_history & ph)
{
  for (int i=0; i<n; i++) {
    chg[i].update(ph);
    cht[i].update(ph);
    chtt[i].update(ph);
  }
}

// End: TageHist Class
// ***************************************************************************

// ***************************************************************************
// Begin: sasim_bpred class

/*!
 * Updates the value of the branch predictor counter according to the condition “taken/not taken” 
 * and to the previous value of the counter.
 * 
 * @param ctr Branch Prediction Counter
 * @param taken Boolean value that indicates the branch outcome
 * @param ctrbits Number of bits used for the counter
 * @param signedctr Indicates if the counter is signed or unsigned
 * @return void. The function does not return any value
 */ 
void
sasim_bpred::updctr(int & ctr, bool taken, int ctrbits, bool signedctr)
{
  int ctrmax,ctrmin;
  if (signedctr) {
    ctrmax = (1 << (ctrbits-1)) - 1;
    ctrmin = -(1 << (ctrbits-1));
  } else {
    ctrmax = (1 << ctrbits) - 1;
    ctrmin = 0;
  }
  if (taken) {
    INCSAT(ctr,ctrmax);
  } else {
    DECSAT(ctr,ctrmin);
  }
}

void
sasim_bpred::reset_stats()
{
  ninst = 0;
  nbranch = 0;
  ncond = 0;
  nret = 0;
  nind = 0;
  nmisp = 0;
  nmispcond = 0;
  nmispind = 0;
}

void
sasim_bpred::update_stats(sa_inst & inst)
{
  ninst++;
  if (! inst.is_branch()) {
    return;
  }
  nbranch++;
  if (inst.is_branch_cond) ncond++;
  if (inst.is_branch_ret) nret++;
  if (inst.is_branch_ind && !inst.is_branch_ret) nind++;
}

void
sasim_bpred::printstats(FILE * stream, const char * predname)
{
  fprintf(stream,"%s inst = %lld\n",predname,(long long) ninst);
  fprintf(stream,"%s branch = %lld\n",predname,(long long) nbranch);
  fprintf(stream,"%s cond = %lld\n",predname,(long long) ncond);
  fprintf(stream,"%s ind = %lld\n",predname,(long long) nind);
  fprintf(stream,"%s ret = %lld\n",predname,(long long) nret);
  fprintf(stream,"%s misp cond = %lld\n",predname,(long long) nmispcond);
  fprintf(stream,"%s misp ind = %lld\n",predname,(long long) nmispind);
  fprintf(stream,"%s misp = %lld\n",predname,(long long) nmisp);
  fprintf(stream,"%s MPKI = %f\n",predname,1000*(double)nmisp/ninst);
}

// End: sasim_bpred class
// ***************************************************************************

// ***************************************************************************
// Begin: bpred_tage class

bpred_tage::bpred_tage(int numg, int bsize, int gsize, int minhist, double alpha, int tagbits) : numg(numg), bsize(bsize), gsize(gsize), minhist(minhist), alpha(alpha), tagbits(tagbits) {}

void 
bpred_tage::init() 
{
  SASSERT(ISPOW2(bsize));
  SASSERT(ISPOW2(gsize));
  logg = sasim_log2(gsize);
  SASSERT(logg > numg);
  b = new int [bsize];
  for (int i=0; i<bsize; i++) {
    b[i] = 0;
  }
  g = new int * [numg];
  t = new int * [numg];
  u = new int * [numg];
  for (int i=0; i<numg; i++) {
    g[i] = new int [gsize];
    t[i] = new int [gsize];
    u[i] = new int [gsize];
  }
  for (int i=0; i<numg; i++) {
    for (int j=0; j<gsize; j++) {
      g[i][j] = 0;
      t[i][j] = 0;
      u[i][j] = 0;
    }
  }
  ch.init(numg,minhist,alpha,logg,tagbits,pathbits);
  gi = new int [numg];
  pwin = 0;
  seed = 0xdede;
  tick = 0;
  reset_stats();
  VERBOSE(int storage = bsize * bctrbits + numg * gsize * (gctrbits + tagbits + ubits));
  VERBOSE(cerr << "tage storage = " << storage << " bits" << endl);
}

int 
bpred_tage::bindex(sa_addr pc)
{
  return pc & (bsize-1);
}

int 
bpred_tage::gindex(sa_addr pc, tagehist & ch, int bank)
{
  SASSERT(bank < numg);
  return (pc ^ (pc >> (logg-numg+bank+1)) ^ ch.chg[bank].comp) & (gsize-1);
}

int 
bpred_tage::gtag(sa_addr pc, tagehist & ch, int bank)
{
  SASSERT(bank < numg);
  return (pc ^ ch.cht[bank].comp ^ (ch.chtt[bank].comp<<1)) & ((1<<tagbits)-1);
}

bool 
bpred_tage::get_prediction(inst_ptr iptr)
{
  if (! iptr->inst->is_branch_cond) {
    return true;
  }
  sa_addr pc = iptr->inst->pc;
  for (int i=0; i<numg; i++) {
    gi[i] = gindex(pc,iptr->chkpt_cond,i);
  }
  bi = bindex(pc);
  predtaken = (b[bi] >= 0);
  bankhit = numg;
  int hitctr = 0;
  for (int i=0; i<numg; i++) {
    if (t[i][gi[i]] == gtag(pc,iptr->chkpt_cond,i)) {
      bankhit = i;
      hitctr = g[i][gi[i]];
      predtaken = (hitctr >= 0);
      break;
    }
  }
  pseudonewalloc = false;
  altpredtaken = predtaken;
  if (bankhit < numg) {
    pseudonewalloc = (u[bankhit][gi[bankhit]]==0) && ((hitctr==0) || (hitctr==-1));
    altpredtaken = (b[bi] >= 0);
    for (int i=bankhit+1; i<numg; i++) {
      if (t[i][gi[i]] == gtag(pc,iptr->chkpt_cond,i)) {
	altpredtaken = (g[i][gi[i]] >= 0);
	break;
      }
    }
    if ((pwin>=0) && pseudonewalloc) {
      return altpredtaken;
    }
  }
  return predtaken;
}

bool 
bpred_tage::predict(inst_ptr iptr, sa_addr next_pc)
{
  update_stats(*iptr->inst);
  if (! iptr->inst->is_branch()) {
    return true; 
  }
  bool taken = (iptr->inst->fallthrough != next_pc) || (! iptr->inst->is_branch_cond);
  bool ok = true;
  if (iptr->inst->is_branch_cond) {
    bool pt = get_prediction(iptr);
    ok = (pt == taken);
    if (!ok) {
      nmisp++;
      nmispcond++;
    }
  }
  return ok;
}

int 
bpred_tage::rando()
{
  seed = ((1 << 2 * numg) + 1) * seed + 0xf3f531;
  seed = (seed & ((1 << (2 * (numg))) - 1));
  return seed;
}

void 
bpred_tage::update(inst_ptr iptr, sa_addr next_pc)
{
  if (! iptr->inst->is_branch()) {
    return; 
  }
  bool taken = (iptr->inst->fallthrough != next_pc) || (! iptr->inst->is_branch_cond);

  if (iptr->inst->is_branch_cond) {

    // update the hitting counter
    if (bankhit < numg) {
      updctr(g[bankhit][gi[bankhit]],taken,gctrbits);
    } else {
      updctr(b[bi],taken,bctrbits);
    }

    // allocate new entry ?
    if ((bankhit > 0) && (predtaken != taken)) {
      int umin = (1<<ubits)-1;
      for (int i=0; i<bankhit; i++) {
	if (u[i][gi[i]] < umin) {
	  umin = u[i][gi[i]];
	}
      }
      SASSERT(umin>=0);
      if (umin > 0) {
	// no useless entry
	for (int i=0; i<bankhit; i++) {
	  u[i][gi[i]]--;
	}
      } else {
	// allocate one entry
	int y = rando() & ((1<<(bankhit-1))-1);
	int x = bankhit-1;
	while ((y & 1) != 0) {
	  x--;
	  y >>= 1;
	}
	SASSERT(x >= 0);
	for (int i=x; i>=0; i--) {
	  if (u[i][gi[i]]==0) {
	    t[i][gi[i]] = gtag(iptr->inst->pc,iptr->chkpt_cond,i);
	    g[i][gi[i]] = (taken)? 0 : -1;
	    u[i][gi[i]] = 0;
	  }
	}
      }
    }

    // update pwin
    if ((bankhit < numg) && pseudonewalloc && (predtaken != altpredtaken)) {
      updctr(pwin,(altpredtaken == taken),pwinbits);
    }

    // update u counter
    if (predtaken != altpredtaken) {
      SASSERT(bankhit < numg);
      if (predtaken == taken) {
	if (u[bankhit][gi[bankhit]] < ((1<<ubits)-1)) {
	  u[bankhit][gi[bankhit]]++;
	}
      } else if (pwin < 0) {
	if (u[bankhit][gi[bankhit]] > 0) {
	  u[bankhit][gi[bankhit]]--;
	}
      }
    }

    //periodic reset of ubit
    tick++;
    if ((tick & ((1 << 18) - 1)) == 0) {
      for (int i = 0; i < numg; i++) {
	for (int j = 0; j < gsize; j++) {
	  u[i][j] = u[i][j] >> 1;
	}
      }
    }

  }
}

void 
bpred_tage::updatehist(inst_ptr iptr, sa_addr next_pc)
{
  if (iptr->inst->is_branch()) {
    ph.insert(next_pc);
    ch.update(ph);
  }
}

// End: bpred_tage class
// ***************************************************************************

// ***************************************************************************
// Begin: ittage_entry class

ittage_entry::ittage_entry() 
{
  target = 0;
  hyst = 0;
  tag = 0;
  u = 0;
}

// End: ittage_entry class
// ***************************************************************************

// ***************************************************************************
// Begin: bpred_ittage class

bpred_ittage::bpred_ittage(int numg, int bsize, int gsize, int minhist, double alpha, int tagbits) : numg(numg), bsize(bsize), gsize(gsize), minhist(minhist), alpha(alpha), tagbits(tagbits) {}

void 
bpred_ittage::init() 
{
  SASSERT(ISPOW2(bsize));
  SASSERT(ISPOW2(gsize));
  logg = sasim_log2(gsize);
  SASSERT(logg > numg);
  b = new ittage_entry [bsize];
  g = new ittage_entry * [numg];
  for (int i=0; i<numg; i++) {
    g[i] = new ittage_entry [gsize];
  }
  ch.init(numg,minhist,alpha,logg,tagbits,pathbits);
  gi = new int [numg];
  pwin = 0;
  seed = 0x16ec;
  tick = 0;
  reset_stats();
  int storage = bsize * (bhystbits + 8 * sizeof(sa_addr));
  storage += numg * gsize * (ghystbits + tagbits + 8 * sizeof(sa_addr) + ubits);
  VERBOSE(cerr << "ittage storage = " << storage << " bits" << endl);
}

int 
bpred_ittage::bindex(sa_addr pc)
{
  return pc & (bsize-1);
}

int 
bpred_ittage::gindex(sa_addr pc, tagehist & ch, int bank)
{
  SASSERT(bank < numg);
  return (pc ^ (pc >> (logg-numg+bank+1)) ^ ch.chg[bank].comp) & (gsize-1);
}

int 
bpred_ittage::gtag(sa_addr pc, tagehist & ch, int bank)
{
  SASSERT(bank < numg);
  return (pc ^ ch.cht[bank].comp ^ (ch.chtt[bank].comp<<1)) & ((1<<tagbits)-1);
}

sa_addr 
bpred_ittage::get_prediction(inst_ptr iptr)
{
  if (! iptr->inst->is_branch_ind || iptr->inst->is_branch_ret) {
    return 0;
  }
  sa_addr pc = iptr->inst->pc;    
  for (int i=0; i<numg; i++) {
    gi[i] = gindex(pc,iptr->chkpt_ind,i);
  }
  bi = bindex(pc);
  pred = b[bi].target;
  bankhit = numg;
  ittage_entry * e = NULL;
  for (int i=0; i<numg; i++) {
    e = & g[i][gi[i]];
    if (e->tag == gtag(pc,iptr->chkpt_ind,i)) {
      bankhit = i;
      pred = e->target;
      break;
    }
  }
  pseudonewalloc = false;
  altpred = pred;
  if (bankhit < numg) {
    SASSERT(e);
    pseudonewalloc = (e->u == 0) && (e->hyst == 0);
    altpred = b[bi].target;
    for (int i=bankhit+1; i<numg; i++) {
      e = & g[i][gi[i]];
      if (e->tag == gtag(pc,iptr->chkpt_ind,i)) {
	altpred = e->target;
	break;
      }
    }
    if ((pwin>=0) && pseudonewalloc) {
      return altpred;
    }
  }
  return pred;
}

bool 
bpred_ittage::predict(inst_ptr iptr, sa_addr next_pc)
{
  update_stats(*iptr->inst);
  if (! iptr->inst->is_branch()) {
    return true; 
  }
  bool ok = true;
  if (iptr->inst->is_branch_ind && ! iptr->inst->is_branch_ret) {
    sa_addr pt = get_prediction(iptr);
    bool ok = (pt == next_pc);
    if (!ok) {
      nmisp++;
      nmispind++;
    }
  }
  return ok;
}

int 
bpred_ittage::rando()
{
  seed = ((1 << 2 * numg) + 1) * seed + 0xf3f531;
  seed = (seed & ((1 << (2 * (numg))) - 1));
  return seed;
}

void 
bpred_ittage::update(inst_ptr iptr, sa_addr next_pc)
{
  if (! iptr->inst->is_branch()) {
    return; 
  }
  if (iptr->inst->is_branch_ind && ! iptr->inst->is_branch_ret) {

    // update the hitting entry (longest match)
    if (bankhit < numg) {
      ittage_entry * e = & g[bankhit][gi[bankhit]];
      updctr(e->hyst,(pred==next_pc),ghystbits,false);
      if (e->hyst==0) {
	e->target = next_pc;
      }
    } else {
      updctr(b[bi].hyst,(pred==next_pc),bhystbits,false);
      if (b[bi].hyst==0) {
	b[bi].target = next_pc;
      }	
    }

    // allocate new entry ?

    if ((bankhit > 0) && (pred != next_pc)) {
      int umin = (1<<ubits)-1;
      for (int i=0; i<bankhit; i++) {
	if (g[i][gi[i]].u < umin) {
	  umin = g[i][gi[i]].u;
	}
      }
      SASSERT(umin>=0);
      if (umin > 0) {
	// no useless entry
	for (int i=0; i<bankhit; i++) {
	  g[i][gi[i]].u--;
	}
      } else {
	// allocate one entry
	int y = rando() & ((1<<(bankhit-1))-1);
	int x = bankhit-1;
	while ((y & 1) != 0) {
	  x--;
	  y >>= 1;
	}
	SASSERT(x >= 0);
	for (int i=x; i>=0; i--) {
	  ittage_entry * e = & g[i][gi[i]];
	  if (e->u == 0) {
	    e->tag = gtag(iptr->inst->pc,iptr->chkpt_ind,i);
	    e->target = next_pc;
	    e->hyst = 0;
	    e->u = 0;
	  }
	}
      }
    }

    // update pwin
    if ((bankhit < numg) && pseudonewalloc && (pred != altpred)) {
      updctr(pwin,(altpred==next_pc),pwinbits);
    }

    // update u counter
    if (pred != altpred) {
      SASSERT(bankhit < numg);
      ittage_entry * e = & g[bankhit][gi[bankhit]];
      if (pred == next_pc) {
	if (e->u < ((1<<ubits)-1)) {
	  e->u++;
	}
      } else if (pwin < 0) {
	if (e->u > 0) {
	  e->u--;
	}
      }
    }

    //periodic reset of ubit
    tick++;
    if ((tick & ((1 << 18) - 1)) == 0) {
      for (int i = 0; i < numg; i++) {
	for (int j = 0; j < gsize; j++) {
	  g[i][j].u = g[i][j].u >> 1;
	}
      }
    }

  }
}

void 
bpred_ittage::updatehist(inst_ptr iptr, sa_addr next_pc)
{
  if (iptr->inst->is_branch()) {
    ph.insert(next_pc);
    ch.update(ph);
  }
}

// End: bpred_ittage class
// ***************************************************************************

// ***************************************************************************
// Begin: bpred_full class

bpred_full::bpred_full(int tage_bsize, int tage_gsize, int ittage_bsize, int ittage_gsize) :
  tage(tage_numg,tage_bsize,tage_gsize,tage_minhist,tage_alpha,tage_tagbits),
  ittage(ittage_numg,ittage_bsize,ittage_gsize,ittage_minhist,ittage_alpha,ittage_tagbits) {}

void 
bpred_full::init()
{
  tage.init();
  ittage.init();
}

bool 
bpred_full::predict(inst_ptr iptr, sa_addr next_pc)
{
  if (! iptr->inst->is_branch()) {
    return true; 
  }
  bool ok = true;
  if (iptr->inst->is_branch_cond) {
    bool taken = (iptr->inst->fallthrough != next_pc);
    bool pt = tage.get_prediction(iptr);
    ok = (pt == taken);
    if (!ok) {
      nmispcond++;
    }
  } else if (iptr->inst->is_branch_ind && ! iptr->inst->is_branch_ret) {
    sa_addr pt = ittage.get_prediction(iptr);
    ok = (pt == next_pc);
    if (!ok) {
      nmispind++;
    }
  }
  if (!ok) {
    nmisp++;
  }
  return ok;
}

void 
bpred_full::update(inst_ptr iptr, sa_addr next_pc)
{
  update_stats(*iptr->inst);
  tage.get_prediction(iptr); // no need for this in real hardware (checkpointed info)
  tage.update(iptr,next_pc);
  ittage.get_prediction(iptr); // no need for this in real hardware (checkpointed info)
  ittage.update(iptr,next_pc);
}

void 
bpred_full::updatehist(inst_ptr iptr, sa_addr next_pc)
{
  if (iptr->inst->is_branch()) {
    ph.insert(next_pc);
    tage.ch.update(ph);
    ittage.ch.update(ph);
  }
}

void 
bpred_full::checkpoint(inst_ptr iptr)
{
  iptr->chkpt_ph = ph.checkpoint();
  iptr->chkpt_cond = tage.ch;
  iptr->chkpt_ind = ittage.ch;
}

void 
bpred_full::repair(inst_ptr iptr, sa_addr next_pc)
{
  ph.repair(iptr->chkpt_ph);
  tage.ch = iptr->chkpt_cond;
  ittage.ch = iptr->chkpt_ind;
}

// End: bpred_full class
// ***************************************************************************
