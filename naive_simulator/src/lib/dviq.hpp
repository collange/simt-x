#ifndef DVIQ_HPP
#define DVIQ_HPP

#include <vector>
#include <stdlib.h>
#include <deque>
#include "dviq.hpp"
#include "globals.hpp"

class DVIQ{

		int* blocked;
		bool *inorder_select;
		int* inorder_blocked;

		int num_threads;
		int threads_per_warp;
		int dviq_size;

		int* warp_cycle;
		LONG *dviq_cycle;
		bool* active;
		int** q_mask;
		LONG* issue_seq_no;

		int rr_position;

		int current_warp;
		int num_warps;
		bool base;
	public:

		//Stats
		LONG *_stat_zero;
		LONG *_stat_blocked;
		LONG *_stat_inorder;
		LONG *_stat_picked;

		//enum INSERT_TYPE {CONV,RECONV,NEW, DIV};
		std::deque<DVInst> * dviq;
		int div_pred[2]; //2-bit bimodal divergence predictor

		DVIQ(Config config){
			rr_position = 0;
			current_warp = 0;

			//divergence predictor. Set intially to Not-divergent
			div_pred[0] = 1; div_pred[1] = 0;

			base = config.base;
			num_warps = config.num_warps;
			num_threads = config.num_threads;
			threads_per_warp = config.threads_per_warp;
			dviq_size = config.dviq_size;
			dviq = new std::deque<DVInst>[num_threads];
			q_mask = new int*[num_threads];

			blocked = new int[num_threads];
			inorder_blocked = new int[num_threads];
			inorder_select = new bool[num_warps];

			_stat_inorder = new LONG[num_warps];
			_stat_blocked = new LONG[num_warps];
			_stat_zero = new LONG[num_warps];
			_stat_picked = new LONG[num_warps];

			dviq_cycle = new LONG[num_warps];
			issue_seq_no = new LONG[num_threads];

			warp_cycle = new int[num_warps];
			active = new bool[num_threads];
			for(int i = 0 ; i < config.num_warps;i++){
				//diverge_load_list[i].init(threads_per_warp);
				//warp_cycle.push_back(0);
				warp_cycle[i] = 0;
				inorder_select[i] = false;
				dviq_cycle[i] = 0;

				_stat_zero[i] = 0;
				_stat_inorder[i] = 0;
				_stat_blocked[i] = 0;
				_stat_picked[i] = 0;
			}
			for(int i=0;i<num_threads;i++){
				blocked[i] = false;
				inorder_blocked[i] = false;
				issue_seq_no[i] = 0 ;
				q_mask[i] = new int[threads_per_warp];
				active[i] = 0;
			}
			for(int i = 0;i<num_threads;i++){
				for(int j = 0 ; j < threads_per_warp;j++){
					q_mask[i][j] = 0;
				}
			}
		}
		DVIQ(){

		};
		~DVIQ(){

		}

		bool issame(int *lhs , int *rhs, int length);
		bool issubset(int *qmask , int *smask, int length);

		bool almost_full(int  dvqid);
		void set_load_started(int dviq_id);
		void end_of_cachewarm(std::vector<LONG> seqno);
		bool inorder_issue(int dviqid);
		
		bool peek(int &id, LONG current_cycle);
		int pick(LONG current_cycle);
		int pick_old(LONG current_cycle);
		void update_queue_status(int dviqid, bool block);
		bool isEmpty();

		void clear();

		void empty_queue();
		int blocked_count();

		void diverge_loadblocks(int warpid);
		void print_queue_masks();
		void print_queue_seq();

		bool branch_div_predictor(int x_smask[], DVInst &dv_temp, int offset, bool speculating);
		void get_new_mask(DVInst &dv_temp, int active_mask[]);
		void update_dv_type(int x_smask[], int threads_per_warp, INSERT_TYPE &type);
		
		bool all_active_threads(int smask[], int length);
		bool divergence_predictor();
		void divergence_predictor_udpate(bool correct, bool updown);
		void compute_branch_mask(int x_smask[], int inv_smask[], DVInst &dv_temp);

	private:
		bool is_issuable(){
			int zero_count = 0;
			int blocked_count = 0;
			int inorder_count = 0;
			for(int i = 0 ;i < num_threads;i++){
				if(dviq[i].size() == 0){
					zero_count++;
				}else if(blocked[i]){
					blocked_count++;
				}else if(inorder_blocked[i]){
					inorder_count++;
				}
			}
			return ((zero_count + blocked_count + inorder_count) < num_threads);
		}

	};

#endif
