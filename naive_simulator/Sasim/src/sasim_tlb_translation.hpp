#ifndef SASIM_TLB_TRANSLATION_HPP
#define SASIM_TLB_TRANSLATION_HPP

#include <set>

#include "sasim_cache.hpp"

class sasim_translation {
 public:
  // implements a pseudo virtual to physical page translation
  map<sa_addr,sa_addr> trans;
  int64_t count;
  int64_t ptzhit;

  void init();
  sa_addr getpage(sa_addr vpage);
#ifdef STRIDE_PREFETCH
  bool lookup(sa_addr & vpage);
#endif
  void check_pagetablezone(sa_addr lineaddr);
};

class sasim_tlb : public sasim_cache {
 public:
  sasim * core;
  cache_array itlb1;
  cache_array dtlb1;
  cache_array tlb2;
  saqueue<sa_req> tlbmq;
  int64_t dtlb_miss;
  int64_t itlb_miss;
  set<sa_addr> dtlbmiss_vpages;
  bool itlbmiss_pending;
  sa_addr itlbmiss_vpage;

  void init_tlb(sasim * proc);
  void flush();
  bool pendingmiss(sa_addr vpage);
  bool pendingmiss(sa_addr lineaddr, sa_addr & vpage);
  void reschedule();
  void dtranslate(uop_ptr uptr, int64_t cycle);
  bool itranslate(sa_addr vpage, int64_t cycle);
  void tlb_miss_finished(sa_req req);
  void tlb2_miss_finished(sa_req req);
  void onecycle(int64_t cycle);
  void printstats(FILE * stream);
  void debugprint_queues();
};

#endif // End ifndef SASIM_TLB_TRANSLATION_HPP