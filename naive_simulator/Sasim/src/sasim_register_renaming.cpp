#include "sasim_register_renaming.hpp"
// The following file must be included here and not in sasim_register_renaming.hpp due to declaration issues
#include "sasim_engine.hpp"

// ***************************************************************************
// Begin: register_renaming class

void 
register_renaming::init()
{
  flags = NOTHING;
}

/*!
 * Returns a reference to the entry r of vector regs
 * If the entry r does not exist in vector regs, it will be resized
 * 
 * @param r The register entry number
 * @return 64 bit integer value.
 */ 
int64_t & 
register_renaming::rename(sa_reg r)
{
  SASSERT(r >= 0);
  if (r >= (int) regs.size()) {
    regs.resize(r+1+regs.size(),NOTHING);
  }
  return regs[(int)r];
}

/*!
 * For each source register, this function assigns a new physical register. If the register was
 * already assigned to another instruction, the source operand will point to the uop that
 * produces the source operand. Otherwise, the pointer is null and it is assumed the value is in the
 * register file.
 * 
 * @param core Main core of the Simulator
 * @param uptr Pointer to the current uop
 * @return void. The function does not return any value
 */ 
void 
register_renaming::rename_uop(sasim & core, uop_ptr uptr)
{
  // find dependent (source) uops
  for (int k=0; k<UOPNS; k++) {
    uop_ptr srcptr = NULL;
    if (uptr->uop->rs[k] != NOREG) {
      int64_t srcnum = rename(uptr->uop->rs[k]);
      if (srcnum != NOTHING) {
	srcptr = core.find_uop_in_rob(srcnum);
      }
    }
    uptr->uoprs[k] = srcptr;
  }

  if (uptr->uop->read_flags && (flags != NOTHING)) {
    uptr->uopflags = core.find_uop_in_rob(flags);
  } else {
    uptr->uopflags = NULL;
  }
}

/*!
 * Renames the destination register of the uop. The contents of the associated entry in vector regs
 * will be the number of the uop.
 * 
 * @param uptr Pointer to the uop.
 * @return void. The function does not return any value
 */ 
void
register_renaming::update(uop_ptr uptr)
{
  if (uptr->uop->rd != NOREG) {
    rename(uptr->uop->rd) = uptr->num;
  }
  if (uptr->uop->write_flags) {
    flags = uptr->num;
  }
}

/*! 
 * Repairs the current map of the register renaming vector with a previously stored copy
 * 
 * @param rr Register Renaming copy
 * @return void. The function does not return any value
 */ 
void
register_renaming::repair(register_renaming & rr)
{
  regs = rr.regs;
  flags = rr.flags;
}

// End: register_renaming class
// ***************************************************************************
