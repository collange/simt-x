#include "DataAnalyser.hpp" //needed for trace reader?! 
#include "Simulation.hpp"
#include <stdio.h>
#include <dlfcn.h>
#include <config4cpp/Configuration.h>

#define DEBUG_MODE 7
#define ZERO_TMP   0   
#define NUM_ENTRIES 8;

using namespace config4cpp;

//Load the configuration data from the .cfg file (specified core architectural parameters)
Config getConfiguration(int num_threads, int tpw){
	Configuration *  cfg = Configuration::create();
	const char *     scope = "";
	const char *     configFile = "ditva-core-config.cfg";
	Config conf;
	try {
		cfg->parse(configFile);

		conf.threads_per_core = cfg->lookupInt(scope,"threads_per_core"); ///////////////////
		conf.num_threads = num_threads;
		conf.issue_combine = true;
		conf.issue_width = cfg->lookupInt(scope,"issue_width");
		conf.issue_entries = NUM_ENTRIES;
		conf.num_fp_fus = cfg->lookupInt(scope, "num_int_fus");
		conf.num_int_fus = cfg->lookupInt(scope, "num_int_fus");
		conf.num_mem_fus = cfg->lookupInt(scope, "num_mem_fus");

		bool cond;
		cond = ((conf.issue_entries-conf.num_fp_fus-conf.num_int_fus-conf.num_mem_fus-2) == 0) ? true : false;
		assert(cond);

		conf.sim_insts = cfg->lookupInt(scope,"sim_insts");
		conf.scalarize = false;
		conf.alu = cfg->lookupInt(scope,"scalar_fu");
		conf.vector_fu =  cfg->lookupInt(scope,"vector_fu");
		conf.num_ports = cfg->lookupInt(scope,"ports_per_cluster");
		conf.fetch_width = cfg->lookupInt(scope, "fetch_width");
		conf.fetch_q_size =cfg->lookupInt(scope, "fetch_q_size");
		conf.cache_warm = cfg->lookupInt(scope, "cache_warm");
		conf.fp_count = cfg->lookupInt(scope,"fp_count");
		conf.dviq_size = cfg->lookupInt(scope, "dviq_size");
		conf.commit_width = cfg->lookupInt(scope, "commit_width");		

		conf.primary_heuristic = "MinSP_PC_Fetch_Warp"; //"MinSP_PC_RR_Fetch_Warp";
		conf.secondary_heuristic = "RoundRobinFC_Fetch_Warp";
		conf.pause_thread = false;
		conf.split_on_block = false;
		conf.icount = false;
		conf.primary_priority_iqfull = false;
		conf.use_secondary_heuristic = false;
#ifdef SAMPLING
		conf.sampling_size = cfg->lookupInt(scope,"sampling_size");
#endif

		if(tpw != 0){
			conf.threads_per_warp = tpw;
			
		}else{
			conf.threads_per_warp = cfg->lookupInt(scope,"threads_per_warp");
		}
		conf.num_warps = (int)conf.num_threads/conf.threads_per_warp;
		cout << "TPW = " << conf.threads_per_warp << ", NW = " << conf.num_warps << ", NT = " << conf.num_threads << endl;
		
		if(conf.threads_per_warp == 1){
			conf.base = true;
			cout<<"Running base simulation"<<endl;
		}else{
			conf.base = false;
		}
	} catch(const ConfigurationException & ex) {
		cerr << ex.c_str() << endl;
		cfg->destroy();
		assert(false);
		return conf;
	}
	cfg->destroy();
	return conf;
}

/**************************Help menu *********************/
void call_help_menu(){
	cout <<  "======================================================================================================="<<endl;
	   cout << "            ~~~~~~~~~~~~~~~~~~~~~~ OoO DITVA Simulator Usage: ~~~~~~~~~~~~~~~~~~~~~~"<<endl;
	   cout << "======================================================================================================"<<endl;
	   cout << "     ./ditva  {no_threads} {threads/warp} {benchmark} {traceRepo_folder} {output_folder}" << endl << endl;
           cout << "======================================================================================================"<<endl;
}
/////////////////////////////////////////////
int check_threads(int nt, int tpw){
	if (nt < 1){
		cout << "Invalid number of parameters: %d threads. Exiting..." << nt << endl;
		return -1;
	}
	return 0;
}

//DL Library checkers/////////////////////////
int check_lib_handler(void* fetch_lib_handle, string heurstr){
	if (fetch_lib_handle == NULL){
		cout << "[Error] Lib Handle --> " << dlerror() << endl;
		return -1;
	}else{
		cout << endl << "Loading heuristic library = " << heurstr.c_str() << endl;
	}
	return 0;
}
/////////////////////////////////////////////
int check_fetch_heur(DataAnalyser::HeuristicData fetch_heur){
	if (fetch_heur.new_heuristic == NULL){
		cout << dlerror() << endl;
		return -1;
	}
	return 0;
}
////////////////////////////////////////////
int check_del_heur(DataAnalyser::HeuristicData fetch_heur){
	if (fetch_heur.delete_heuristic == NULL){
		cout << dlerror() << endl;
		return -1;
	}
	return 0;
}
//////////////////////////////////////////////
string make_trace_filename(int n, int k, int zero, string* files, string repo, string bench){
	n = k;
	stringstream ss;
	if(zero == 0) ss << n;
	else ss << n+1;
		
	string str = ss.str();
	return files[k].append(repo).append(bench).append(str);
}
//////////////////////////////////////////////////

int main(int argc, const char** argv){
	//Help Menu Display 
	if(argc == 1 || string(argv[1]) == "-h"){
		call_help_menu();
		return 0;
	}
	else
		cout << endl << "\t\t~*~*~*~*~*~*~  STARTING OoO DITVA ~*~*~*~*~*~*~ " << endl << endl;
	//assign arguments to their respective variables for DITVA	
	int num_threads;
	int zero = ZERO_TMP;
	int tpw;
	int mode = DEBUG_MODE;
	
	std::istringstream numt(argv[1]); numt >> num_threads; 
	std::istringstream numtpw(argv[2]); numtpw >> tpw;

	//read ditva configuration file
	Config conf = getConfiguration(num_threads, tpw);
	if(check_threads(num_threads, tpw) == -1) return -1;
	UINT num_fetches = num_threads;

	//Load the primary heuristic
	void* fetch_lib_handle;
	string heurstr="Heuristics/bin/";
	heurstr.append(conf.primary_heuristic).append(".so");
	fetch_lib_handle = dlopen(heurstr.c_str(), RTLD_LAZY);
	if(check_lib_handler(&fetch_lib_handle, heurstr) == -1) return -1;
	
	//Load the Fetch heuristics (secondary) for the dataAnalyzer
	DataAnalyser::HeuristicData fetch_heur;
	fetch_heur.new_heuristic = (Heuristic* (*)(UINT, UINT,Thread*, const char*)) dlsym(fetch_lib_handle, "new_heuristic");
	if(check_fetch_heur(fetch_heur) == -1) return -1;
	fetch_heur.delete_heuristic = (void (*)(Heuristic*)) dlsym(fetch_lib_handle, "delete_heuristic");
	fetch_heur.parameter = (const char*)1; 

	//Setup IO files for Simulation, then Simulate
	try {
		//Setup file names for retrieving traces based on args
		string files[num_threads];
		string repo = string(argv[4]) + string("/");
		string benchmark= argv[3]; 
		int n;
		const char *files_char[num_threads];
		for(int k = 0 ; k < num_threads; k++){
			files[k] = make_trace_filename(n, k, zero, files, repo, benchmark);
			files_char[k] = files[k].c_str();
			cout<<"File open "<<files[k]<<endl;
		}

		//setup output file names
		conf.outputfile = string(argv[5]).append("/").append(string(benchmark).append(".output")); 
		conf.csv = string(argv[5]).append("/").append(string(benchmark).append(".csv"));
		cout << endl << "<> Statistics available in " << conf.csv << endl;
		conf.benchmark = benchmark;
		conf.periodicfile = string(argv[5]).append("/").append(string(benchmark).append(".periodic"));
		//cout<<"Output available in "<<conf.outputfile<<endl;

		//setup for simulation
		Simulation simulation(num_threads, num_fetches, files_char, &fetch_heur, conf);

		cout << " >>>>>>>>>>>>>>>>>>>>>>>>   SIMULATION STARTING  <<<<<<<<<<<<<<<<<<<<<<<<<<< " << endl << endl;
		//Simulate
		simulation.simulator();


	}
	catch(Exception& excp){
		cout << excp.get_message() << endl;
		return -1;
	}

	dlclose(fetch_lib_handle);

	return 0;
}
