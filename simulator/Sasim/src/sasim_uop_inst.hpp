#ifndef SASIM_UOP_INST_HPP
#define SASIM_UOP_INST_HPP

#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>


#include "sasim_saqueue_template.hpp"
#include "sasim_defs.hpp"
//#include "sasim_branchpred.hpp"

#define UOPNS 			2      //from sasim_uarch_defs.hpp
#define MAXUOPS 		8      //--
#define NOREG 			((sa_reg)(-1)) //--
#define NOINSTNUM 		(-1) //--
//#define DL1_BANKS 		8 //--
//#define DL1_BANK_WIDTH 	8 //--
#define DL1_BANKS 		16 //--
#define DL1_BANK_WIDTH 		16 //--
#define MAXMEMACCWIDTH 		16 //--

#define MAX_OPERANDS_DVSIM 6

using namespace std;

/*typedef enum {
SA_UOP_UNKNOWN, 
SA_UOP_SYSCALL,
SA_UOP_MAGIC,
SA_UOP_NOP,
SA_UOP_ALU,  
SA_UOP_MOV,
SA_UOP_BRANCH, 
SA_UOP_SHIFT,
SA_UOP_ADDR, 
SA_UOP_LOAD, 
SA_UOP_STORE,
SA_UOP_FP,
SA_UOP_SSE,
SA_UOP_MMX,
SA_UOP_IMUL,
SA_UOP_IDIV,
SA_UOP_INTLONG,
SA_UOP_FPDIV,
SA_UOP_FPLONG,
SA_UOP_LAST
} sa_uoptype;
 */
typedef enum {
	SA_UOP_UNKNOWN,
	SA_UOP_SYSCALL,
	SA_UOP_MAGIC,
	SA_UOP_NOP,
	SA_UOP_ALU,
	SA_UOP_MOV,
	SA_UOP_BRANCH,
	SA_UOP_SHIFT,
	SA_UOP_ADDR,
	SA_UOP_LOAD,
	SA_UOP_STORE,
	SA_UOP_FP,
	SA_UOP_SSE,
	SA_UOP_SSE_FPVEC,
	SA_UOP_SSE_FPSCAL,
	SA_UOP_SSE_FPMULVEC,
	SA_UOP_SSE_FPMULSCAL,
	SA_UOP_SSE_FPDIVVEC,
	SA_UOP_SSE_FPDIVSCAL,
	SA_UOP_SSE_IMUL,
	SA_UOP_SSE_IDIV,
	SA_UOP_SSE_IMOV,
	SA_UOP_SSE_FPMOVVEC,
	SA_UOP_SSE_FPMOVSCAL,
	SA_UOP_AVX,
	SA_UOP_AVX_FPVEC,
	SA_UOP_AVX_FPSCAL,
	SA_UOP_AVX_FPMULVEC,
	SA_UOP_AVX_FPMULSCAL,
	SA_UOP_AVX_FPDIVVEC,
	SA_UOP_AVX_FPDIVSCAL,
	SA_UOP_AVX_IMUL,
	SA_UOP_AVX_IDIV,
	SA_UOP_AVX_IMOV,
	SA_UOP_AVX_FPMOVVEC,
	SA_UOP_AVX_FPMOVSCAL,
	SA_UOP_SSE2,
	SA_UOP_MMX,
	SA_UOP_IMUL,
	SA_UOP_IDIV,
	SA_UOP_INTLONG,
	SA_UOP_FPDIV,
	SA_UOP_FPMUL,
	SA_UOP_FPLONG,
	SA_UOP_LAST,
	SA_UOP_POP,
	SA_UOP_PUSH,
	SA_UOP_SEMAPHORE,
	SA_UOP_SYSTEM,
} sa_uoptype;

static string uop_string[] = {
		"SA_UOP_UNKNOWN",
		"SA_UOP_SYSCALL",
		"SA_UOP_MAGIC",
		"SA_UOP_NOP",
		"SA_UOP_ALU",
		"SA_UOP_MOV",
		"SA_UOP_BRANCH",
		"SA_UOP_SHIFT",
		"SA_UOP_ADDR",
		"SA_UOP_LOAD",
		"SA_UOP_STORE",
		"SA_UOP_FP",
		"SA_UOP_SSE",
		"SA_UOP_SSE_FPVEC",
		"SA_UOP_SSE_FPSCAL",
		"SA_UOP_SSE_FPMULVEC",
		"SA_UOP_SSE_FPMULSCAL",
		"SA_UOP_SSE_FPDIVVEC",
		"SA_UOP_SSE_FPDIVSCAL",
		"SA_UOP_SSE_IMUL",
		"SA_UOP_SSE_IDIV",
		"SA_UOP_SSE_IMOV",
		"SA_UOP_SSE_FPMOVVEC",
		"SA_UOP_SSE_FPMOVSCAL",
		"SA_UOP_AVX",
		"SA_UOP_AVX_FPVEC",
		"SA_UOP_AVX_FPSCAL",
		"SA_UOP_AVX_FPMULVEC",
		"SA_UOP_AVX_FPMULSCAL",
		"SA_UOP_AVX_FPDIVVEC",
		"SA_UOP_AVX_FPDIVSCAL",
		"SA_UOP_AVX_IMUL",
		"SA_UOP_AVX_IDIV",
		"SA_UOP_AVX_IMOV",
		"SA_UOP_AVX_FPMOVVEC",
		"SA_UOP_AVX_FPMOVSCAL",
		"SA_UOP_SSE2",
		"SA_UOP_MMX",
		"SA_UOP_IMUL",
		"SA_UOP_IDIV",
		"SA_UOP_INTLONG",
		"SA_UOP_FPDIV",
		"SA_UOP_FPMUL",
		"SA_UOP_FPLONG",
		"SA_UOP_LAST",
		"SA_UOP_POP",
		"SA_UOP_PUSH",
		"SA_UOP_SEMAPHORE",
		"SA_UOP_SYSTEM",
};

typedef enum {
	SA_REG_INT,
	SA_REG_FP,
	SA_REG_TMP,
	SA_REG_XMM,
	SA_REG_YMM
} sa_reg_type;

class sa_uop {
public:
	sa_uoptype uoptype;
	sa_reg rd;
	sa_reg rs[UOPNS];
	bool write_flags;
	bool read_flags;
	sa_reg_type rdtype;
	sa_reg_type rstype[UOPNS];

	void clear();
	sa_uop();
	void output(FILE *fd);
	void input(FILE *fd);
	string uoptype_string();
	string tostring(string (*regtostring)(sa_reg) = NULL);
	bool is_branch();
	bool is_mem();
	bool is_fpsse();
};

class sasim_uop;
class sasim_inst;
typedef sasim_uop * uop_ptr;
typedef sasim_inst * inst_ptr;

class sasim_uop {
public:
	sa_uop * uop;
	inst_ptr iptr;
	int64_t num;
	uop_ptr uoprs[UOPNS]; // uops providing reg sources
	uop_ptr uopflags; // uop providing flags
	sa_addr addr;
	int nbytes;
	uint64_t dl1banks;
	bool lastuop;
	bool scheduled;
	bool executed;
	bool memdone[2];
	bool is_fp;
	int64_t storesetdep; // for LOADs
	int64_t forwardingstore; // for LOADs
	bool memtranslated;
	sa_addr vpage;
	int which_ldq;

	void reschedule(int64_t cycle = 0);
	void reset_uop();
	bool ready();
	bool store_data_ready();
	bool address_ready();
	void set_dl1banks();
	void set_isfp();
	void set_executed(int64_t cycle, saqueue<uop_ptr> * issueq);
};
struct dvsim_inst{
	sa_uoptype optype;
	uint8_t flags;
	sa_reg iregs[MAX_OPERANDS_DVSIM]; // input register for the inst
	sa_reg oregs[MAX_OPERANDS_DVSIM]; // output registers for the inst
	int num_iregs;
	int num_oregs;
	string assembly;
	bool is_branch;
	bool is_scalar;
	sa_reg_type rdtype[MAX_OPERANDS_DVSIM];
	sa_reg_type rstype[MAX_OPERANDS_DVSIM];
};
class sa_inst {
public:
	bool decoded;
	int inum;
	sa_addr pc;
	sa_addr fallthrough;
	int nuops;
	sa_uop uop[MAXUOPS];
	int nloads;
	int nstores;
	int mreadsize;
	int mwritesize;
	bool is_branch_cond;
	bool is_branch_ret;
	bool is_branch_ind;
	//Added by sajith
	dvsim_inst idvsim;


	sa_inst();
	void clear();
	void output(FILE *fd);
	bool input(FILE *fd);
	void debugprint();
	bool is_branch();
	bool is_nop();
	string regtostring(sa_reg reg);
};


class sasim_inst {
public:
	sa_inst * inst;
	int64_t num;
	sasim_uop simuop[MAXUOPS];
	sa_addr lineaddr;
	sa_addr vpage;
	bool crossline;
	inst_ptr nextinst;
	bool block_end;
	bool mispredict;
	bool memtrap;
	sa_addr memtrap_storepc;
	int64_t pred_cycle;
	int64_t exec_cycle;
	// checkpoint info
	int chkpt_ph;
	//tagehist chkpt_cond; //not needed for tracing
	//tagehist chkpt_ind;  //not needed for tracing
	bool store_completed; // for managing the inst pool

	void reset_inst();
	sasim_inst();
};

#endif // End ifndef SASIM_UOP_INST_HPP
