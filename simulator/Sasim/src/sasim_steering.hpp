#ifndef SASIM_STEERING_HPP
#define SASIM_STEERING_HPP

#include "sasim_saqueue_template.hpp"
#include "sasim_uop_inst.hpp"

class steering {
 public:
  int kl;

  virtual void init() = 0;
  virtual int chooseint(saqueue<uop_ptr> q[], uop_ptr uptr) = 0;
  virtual int choosefp(saqueue<uop_ptr> q[], uop_ptr uptr) = 0;
  int chooseld(saqueue<uop_ptr> q[], uop_ptr uptr);
};

class steering_rr : public steering {
 public:
  int ki;
  int kf;

  void init();
  int chooseint(saqueue<uop_ptr> q[], uop_ptr uptr);
  int choosefp(saqueue<uop_ptr> q[], uop_ptr uptr);
};

class steering_dep : public steering {
 public:
  int depq(saqueue<uop_ptr> q[], int n, uop_ptr uptr) ;
  int minq(saqueue<uop_ptr> q[], int n);
  int choose(saqueue<uop_ptr> q[], int n, uop_ptr uptr);
  void init();
  int chooseint(saqueue<uop_ptr> q[], uop_ptr uptr);
  int choosefp(saqueue<uop_ptr> q[], uop_ptr uptr);
};

#endif // End ifndef SASIM_STEERING_HPP