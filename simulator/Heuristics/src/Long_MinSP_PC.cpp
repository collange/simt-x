#include "Heuristic.hpp"
#include <stdio.h>

class Sync : public Heuristic {
	UINT size_FHB;
	ADDRINT** table_FHB;
	UINT* last_pos;
	short ref;

public:
	Sync(UINT nt, Thread* t, const char* param);
	~Sync();

	void choose_threads();
	int thread_schedule(LONG current_cycle){}
	virtual  bool lockstep(){
		return true;
	}
	int getCurrentWarp(){
		return -1;
	}
};

Heuristic* new_heuristic(UINT nt, Thread* t, const char* param){
	return new Sync(nt, t, param);
}
void delete_heuristic(Heuristic* h){
	delete h;
}


Sync::Sync(UINT nt, Thread* t, const char* param)
: Heuristic(nt, t)
{
	sscanf(param, "%d", &size_FHB);
	ref = -1;

	//Creating the FHB each thread
	table_FHB = new ADDRINT*[num_threads];
	last_pos = new UINT[num_threads];
	for (UINT i=0; i<num_threads; i++){
		last_pos[i] = 0;
		table_FHB[i] = new ADDRINT[size_FHB];
		for (UINT j=0; j<size_FHB; j++){
			table_FHB[i][j] = 0;
		}
	}

}

Sync::~Sync(){
	for (UINT i=0; i<num_threads; i++){
		delete [] table_FHB[i];
	}
	delete [] table_FHB;
	delete [] last_pos;
}


void Sync::choose_threads(){
	if ( ref != -1 && !(t[ref].basic_block || t[ref].bb_priority > 0) && !t[ref].ignore ){
		for (UINT i=0; i<num_threads; i++){
			t[i].is_active = !t[i].ignore && t[i].pc == t[ref].pc;
		}
		return;
	}

	//Find the PC of each thread of the FHB other and calculate priority.
	UINT priority[num_threads];
	for (UINT k=0; k<num_threads; k++){//For each thread
		priority[k] = 0; //The priority of each thread.
		for (UINT i=0; i<num_threads; i++){ //Para cada uma das demais threads
			if (i != k){
				for (UINT j=0; j<size_FHB; j++){ //Para cada posição da FHB de outra thread
					if (t[k].pc == table_FHB[i][j]){ //O PC está na tabela
						priority[k]++;
						break;
					}
				}
			}
		}
	}

	//Escolher thread de maior prioridade
	int first = -1;
	for (UINT i=0; i<num_threads; i++){
		if (!t[i].ignore){
			if (first == -1 || priority[i] > priority[first]){
				first = i;
			}
		}
	}


	bool execute[num_threads];

	//Ver quais threads tem a mesma prioridade da maior, e colocar para executar cada uma delas por vez.
	for (UINT i=0; i<num_threads; i++){
		execute[i] = false;
		if (!t[i].ignore){
			if (priority[i] == priority[first]){
				execute[i] = true;
			}
		}
	}


	//Escolhendo thread de maior prioridade segundo sub-heurística
	ref = -1;
	for (UINT i=0; i<num_threads; i++){
		if (execute[i]){
			if ( ref == -1 || t[i].sp < t[ref].sp || (t[i].sp == t[ref].sp && t[i].pc < t[ref].pc ) ){
				ref = i;
			}
		}
	}

	if (ref == -1){
		throw Exception("Error in heuristic code: No thread was chosen to execute.");
	}

	//Marcando threads em execução
	for (UINT i=0; i<num_threads; i++){
		t[i].is_active = !t[i].ignore && t[i].pc ==  t[ref].pc;

		if (t[i].is_active){

			//Adicionando instrução na tabela FHB
			table_FHB[i][ last_pos[i] ] = t[i].pc;
			last_pos[i]++;
			last_pos[i] %= size_FHB;
		}
	}

}
