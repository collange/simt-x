#ifndef SASIM_TRACER_HPP
#define SASIM_TRACER_HPP

#include <vector>

#include "sasim_atc.hpp"
#include "sasim_bstream_template.hpp"
//#include "sasim_engine.hpp"
#include "sasim_uop_inst.hpp"
#include "sasim_defs.hpp"

using namespace std;

class sa_tracer {
  static const int BUFSIZE = 16384;
  static const int MAXSTRLEN = 128;

 public:

  FILE * fd1; // static instruction info
  bstream<int,BUFSIZE> trace_inum; // dynamic instruction stream
#if 0
#ifdef USE_ATC
  astream trace_addr; // dynamic data addresses
#else
  bstream<sa_addr,BUFSIZE> trace_addr; // dynamic data addresses
#endif
#endif
  bstream<sa_addr,BUFSIZE> trace_addr; // dynamic data addresses
#if 0
#ifdef USE_ATC
  astream trace_sp; // dynamic stack pointer addresses
#else
  bstream<sa_addr,BUFSIZE> trace_sp; // dynamic stack pointer addresses
#endif
#endif
  bstream<sa_addr,BUFSIZE> trace_sp; // dynamic stack pointer addresses
  bstream<bool,BUFSIZE> trace_br_taken; // dynamic stack pointer addresses
#ifdef SCALARISE
  bstream<uint,BUFSIZE> trace_addr_status; // dynamic addr_status - r/w and op_size //For scalarise it is uint else char
#else
  bstream<uint,BUFSIZE> trace_addr_status;
#endif

  bstream<uint,BUFSIZE> trace_flags;

  vector<sa_inst> statinst;

  int64_t ninst;
  int64_t nuops;

  bool active;

  void reset();
  sa_tracer();
  ~sa_tracer();
  void open(const char dirname[]);
  sa_inst & getinst(int inum);
  //void simulate(sasim & proc, int64_t maxinst = 0, int64_t skipinst = 0);
  void close();
  sa_inst * readinst();
};

#endif // End ifndef SASIM_TRACER_HPP
