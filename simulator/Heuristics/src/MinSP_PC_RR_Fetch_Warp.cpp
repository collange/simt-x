#include "Heuristic.hpp"


class Sync : public Heuristic {
	//ADDRINT pc;
	//ADDRINT sp;
	//std::vector< int >  mask;
	//int invalid;
	int cwid;
	// Round robin among active threads


public:
	Sync(UINT nt, UINT nw, Thread* t, const char* param);
	~Sync();

	virtual int getCurrentWarp(){
		return cwid;
	}
	void choose_threads();
	int thread_schedule(LONG current_cycle);
	virtual  bool lockstep(){
		return true;
	}
};

Heuristic* new_heuristic(UINT nt, UINT nw, Thread* t, const char* param){
	return new Sync(nt, nw ,t, param);
}
void delete_heuristic(Heuristic* h){
	delete h;
}

Sync::Sync(UINT nt, UINT nw, Thread* t, const char* param)
: Heuristic(nt,nw, t)
{
	//current_warp = 0;
	threads_in_warp = (int)num_threads/num_warps;
	for( uint j = 0 ; j < num_warps ; j++){
		heur_cycle[j] = 0Ull;
	}
	//for( uint j = 0 ; j < num_threads ; j++){
	//	invalid = true;
	//	pc = 0ULL;
	//	sp = 0ULL;
	//	cwid = -1;
	//	for (UINT i=0; i<num_threads; i++){
	//		mask.push_back(0);
	//	}
	//}
}

Sync::~Sync(){

}

void Sync::choose_threads(){

}

int Sync::thread_schedule(LONG current_cycle){
	static int current_warp = 0;
	static int warp_cycle[MAX_THREADS_HEUR];
	static bool active[MAX_THREADS_HEUR];
	//bool incr_cycle = false;

	/*if(current_cycle > heur_cycle){
		incr_cycle = true;
		heur_cycle = current_cycle;
		cout<<"Cycle "<< current_cycle<<endl;
	}*/
	//static bool active_last_fetch[num_warps][threads_in_warp];

	//int ref;
	bool is_active = false;
	uint warp_count = 0;
	//bool rr = false;

	for (UINT i=0; i<num_threads; i++){
		t[i].fetch_isactive = false;
		t[i].active_last_fetch = false;
	}
	while(warp_count < num_warps){
		int ref = -1;
		uint offset = current_warp * threads_in_warp;
		if(warp_cycle[current_warp] > threads_in_warp + 1){
			//	rr = true;
			for (UINT i=0+offset; i<threads_in_warp+offset; i++){
				if (!t[i].fetch_qfull && !t[i].fetch_stall && !t[i].ignore && !t[i].fetch_qempty && !t[i].iq_full && !active[i]){
					if ( ref == -1 ){
						is_active = true;
						ref = i;
					}
				}
			}
			if(ref == -1){
				//cout<<"Ref -1"<<endl;
				warp_count++;
				warp_cycle[current_warp] = 0;
				for (UINT i=0+offset; i<threads_in_warp+offset; i++){
					active[i] = false;
				}
				cwid = current_warp;
				current_warp = (current_warp+1)%num_warps;
				continue;
			}
			//cout<<"RR [";
			for (UINT i=0+offset; i<threads_in_warp+offset; i++){
				t[i].fetch_isactive =  !t[i].fetch_qempty && !t[i].fetch_qfull && t[i].fetch_pc == t[ref].fetch_pc && !t[i].iq_full && !t[i].fetch_stall && !t[i].ignore;
				if(t[i].fetch_isactive){
			//				cout<<i<<" ";
					active[i] = true;
				}
			}
			//cout<<"]warp "<<current_warp<<endl;
			warp_count = 0;
			cwid = current_warp;
			current_warp = (current_warp+1)%num_warps;
			return cwid;
		}else{
			for (UINT i=0+offset; i<threads_in_warp+offset; i++){
				//cout<<"i"<<i<<endl;
				if (!t[i].fetch_qfull && !t[i].fetch_stall && !t[i].ignore && !t[i].fetch_qempty && !t[i].iq_full){
					if ( ref == -1 || t[i].fetch_sp < t[ref].fetch_sp || ( t[i].fetch_sp == t[ref].fetch_sp && t[i].fetch_pc < t[ref].fetch_pc ) ){
						ref = i;
						is_active = true;
					}
				}
			}
			if(ref == -1){
				//cout<<"Ref -1 "<<current_warp<<endl;
			//	cout<<"Cycle [R] "<<current_cycle<<endl;
				if(current_cycle > heur_cycle[current_warp]){
					heur_cycle[current_warp] = current_cycle;
					warp_cycle[current_warp]++;
				}
				warp_count++;
				cwid = current_warp;
				current_warp = (current_warp+1)%num_warps;
				continue;
			}

			//cout<<"MinSP [ ";
			for (UINT i=0+offset; i<threads_in_warp+offset; i++){
				t[i].fetch_isactive = !t[i].fetch_qfull && t[i].fetch_pc == t[ref].fetch_pc && !t[i].fetch_stall && !t[i].ignore && !t[i].fetch_qempty&& !t[i].iq_full;
				if(t[i].fetch_isactive){
					active[i] = true;
			//				cout<<i<<" ";
				}
				//t[i].active_last_fetch = t[i].fetch_isactive;
				//active_last_fetch[current_warp][i] = t[i].fetch_isactive;
				/*if(t[i].fetch_isactive){
				cout<<"I "<<i<<" is active ref "<<ref<<" pc "<<t[i].fetch_pc<<endl;
			}*/
			}
		//	cout<<"] warp "<<current_warp << " warp_cycle "<< warp_cycle[current_warp] <<endl;

			//warp_count++;
			warp_count = 0;
			cwid = current_warp;
		//	cout<<"Cycle "<<current_cycle<<endl;
			if(current_cycle > heur_cycle[current_warp]){
				heur_cycle[current_warp] = current_cycle;
				warp_cycle[current_warp]++;
			}
			current_warp = (current_warp+1)%num_warps;
			//cout<<"Exit"<<endl;
			return cwid;
		}
	}
	if(!is_active){
		for (UINT i=0; i<num_threads; i++){
			t[i].fetch_isactive = false;
		}
		return -1;
	}

}

