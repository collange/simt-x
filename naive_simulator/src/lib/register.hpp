#ifndef REGISTER_HPP
#define REGISTER_HPP

#include <stdlib.h>
#include <map>
#include <vector>
#include <string>
#include "globals.hpp"

#define NOREG_DVSIM 99

const int write_reg_start_index = 1000;

class RegFile{
	private:
		unordered_map< sa_reg, int> * write_reg;
		int numthreads;
	public:
		RegFile(){
			numthreads = 0;
		}

		RegFile(uint _numthreads){
			numthreads = (int)_numthreads;
			write_reg = new unordered_map< sa_reg, int>[numthreads];
		}
		//sets as a write register for all the instructions which need to read this (dependant() function)
		void write(int threadid, sa_reg reg){
			auto it = write_reg[threadid].find(reg); //find if previously writing to this register (thread)
			if(it == write_reg[threadid].end()){
				assert(write_reg[threadid].size()<100);
				write_reg[threadid].insert(make_pair(reg, 1)); //if not written, insert wreg
			}else{
				it->second = 1; //if written, ensure second field is '1'
			}
		}
		//checks is sa_reg belongs to any of its write registers 
		bool dependant(int threadid, sa_reg reg){
			auto it = write_reg[threadid].find(reg);
			if(it == write_reg[threadid].end()){ //if no deps, return false
				return false;
			}else{
				return (it->second == 1); //else returns true
			}
		}

		void clear(int threadid, sa_reg reg){
			auto it = write_reg[threadid].find(reg);
			//assert(it != write_reg[threadid].end());
			if(it != write_reg[threadid].end()){
				it->second = 0;
			}
		}
	};


struct Register {
	private:
		std::vector<std::string> index_to_regname;
		std::map<std::string, uint32_t> regname_to_index;

	public:
		int getReadRegIndex(string reg);
		int getWriteRegIndex(string reg);
		string getRegName(int index);
		bool isnoreg(int index);
		int getRegType(int index);

		Register() {
			index_to_regname.push_back(string("RAX"));
			index_to_regname.push_back(string("RBX"));
			index_to_regname.push_back(string("RCX"));
			index_to_regname.push_back(string("RDX"));
			index_to_regname.push_back(string("RSI"));
			index_to_regname.push_back(string("RDI"));
			index_to_regname.push_back(string("RBP"));
			index_to_regname.push_back(string("RSP"));
			index_to_regname.push_back(string("R8"));
			index_to_regname.push_back(string("R9"));
			index_to_regname.push_back(string("R10"));
			index_to_regname.push_back(string("R11"));
			index_to_regname.push_back(string("R12"));
			index_to_regname.push_back(string("R13"));
			index_to_regname.push_back(string("R14"));
			index_to_regname.push_back(string("R15"));
			index_to_regname.push_back(string("EAX"));
			index_to_regname.push_back(string("EBX"));
			index_to_regname.push_back(string("ECX"));
			index_to_regname.push_back(string("EDX"));
			index_to_regname.push_back(string("ESI"));
			index_to_regname.push_back(string("EDI"));
			index_to_regname.push_back(string("EBP"));
			index_to_regname.push_back(string("ESP"));
			index_to_regname.push_back(string("R8D"));
			index_to_regname.push_back(string("R9D"));
			index_to_regname.push_back(string("R10D"));
			index_to_regname.push_back(string("R11D"));
			index_to_regname.push_back(string("R12D"));
			index_to_regname.push_back(string("R13D"));
			index_to_regname.push_back(string("R14D"));
			index_to_regname.push_back(string("R15D"));
			index_to_regname.push_back(string("XMM0"));
			index_to_regname.push_back(string("XMM1"));
			index_to_regname.push_back(string("XMM2"));
			index_to_regname.push_back(string("XMM3"));
			index_to_regname.push_back(string("XMM4"));
			index_to_regname.push_back(string("XMM5"));
			index_to_regname.push_back(string("XMM6"));
			index_to_regname.push_back(string("XMM7"));
			index_to_regname.push_back(string("XMM8"));
			index_to_regname.push_back(string("XMM9"));
			index_to_regname.push_back(string("XMM10"));
			index_to_regname.push_back(string("XMM11"));
			index_to_regname.push_back(string("XMM12"));
			index_to_regname.push_back(string("XMM13"));
			index_to_regname.push_back(string("XMM14"));
			index_to_regname.push_back(string("XMM15"));
			index_to_regname.push_back(string("NOREG"));
			regname_to_index["RAX"] = 0;
			regname_to_index["RBX"] = 1;
			regname_to_index["RCX"] = 2;
			regname_to_index["RDX"] = 3;
			regname_to_index["RSI"] = 4;
			regname_to_index["RDI"] = 5;
			regname_to_index["RBP"] = 6;
			regname_to_index["RSP"] = 7;
			regname_to_index["R8"] = 8;
			regname_to_index["R9"] = 9;
			regname_to_index["R10"] = 10;
			regname_to_index["R11"] = 11;
			regname_to_index["R12"] = 12;
			regname_to_index["R13"] = 13;
			regname_to_index["R14"] = 14;
			regname_to_index["R15"] = 15;
			regname_to_index["EAX"] = 16;
			regname_to_index["EBX"] = 17;
			regname_to_index["ECX"] = 18;
			regname_to_index["EDX"] = 19;
			regname_to_index["ESI"] = 20;
			regname_to_index["EDI"] = 21;
			regname_to_index["EBP"] = 22;
			regname_to_index["ESP"] = 23;
			regname_to_index["R8D"] = 24;
			regname_to_index["R9D"] = 25;
			regname_to_index["R10D"] = 26;
			regname_to_index["R11D"] = 27;
			regname_to_index["R12D"] = 28;
			regname_to_index["R13D"] = 29;
			regname_to_index["R14D"] = 30;
			regname_to_index["R15D"] = 31;
			regname_to_index["XMM0"] = 32;
			regname_to_index["XMM1"] = 33;
			regname_to_index["XMM2"] = 34;
			regname_to_index["XMM3"] = 35;
			regname_to_index["XMM4"] = 36;
			regname_to_index["XMM5"] = 37;
			regname_to_index["XMM6"] = 38;
			regname_to_index["XMM7"] = 39;
			regname_to_index["XMM8"] = 40;
			regname_to_index["XMM9"] = 41;
			regname_to_index["XMM10"] = 42;
			regname_to_index["XMM11"] = 43;
			regname_to_index["XMM12"] = 44;
			regname_to_index["XMM13"] = 45;
			regname_to_index["XMM14"] = 46;
			regname_to_index["XMM15"] = 47;
			regname_to_index["NOREG"] = 48;
		}

		/*char *regname[] = {
	 "RAX",
	 "RBX",
	 "RCX",
	 "RDX",
	 "RSI",
	 "RDI",
	 "RBP",
	 "RSP",
	 "R8",
	 "R9",
	 "R10",
	 "R11",
	 "R12",
	 "R13",
	 "R14",
	 "R15",
	 "EAX",
	 "EBX",
	 "ECX",
	 "EDX",
	 "ESI",
	 "EDI",
	 "EBP",
	 "ESP",
	 "R8D",
	 "R9D",
	 "R10D",
	 "R11D",
	 "R12D",
	 "R13D",
	 "R14D",
	 "R15D",
	 "XMM0",
	 "XMM1",
	 "XMM2",
	 "XMM3",
	 "XMM4",
	 "XMM5",
	 "XMM6",
	 "XMM7",
	 "XMM8",
	 "XMM9",
	 "XMM10",
	 "XMM11",
	 "XMM12",
	 "XMM13",
	 "XMM14",
	 "XMM15",
	 "EOL"
	 };*/

	};

#endif
