#ifndef SASIM_BSTREAM_TEMPLATE_HPP
#define SASIM_BSTREAM_TEMPLATE_HPP

#include <iostream>
#include <cstdio>

//#include <errno.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <sys/types.h>
//#include <sys/wait.h>
//#include <unistd.h>


#include "sasim_defs.hpp"
#include "popen_noshell.h"

using namespace std;

template <class BTYPE,int BSIZE> class bstream {
public:
	int fake[10000];
	FILE * fd;
	int fake2[10000];
	char m;
	BTYPE buf[BSIZE];
	int n;
	int nr;

	struct popen_noshell_pass_to_pclose pclose_arg;

	struct pinfo plist;// = NULL;


	bstream()
	{
		n = 0;
		nr = 0;
		fd = NULL;
		m = ' ';
	}


	/*!
	 * Opens a stream for reading/writing.
	 *
	 * @param command[] Command parameter for opening the stream
	 * @param mode[] Mode parameter for opening the stream
	 * @return void. The function does not return any value
	 */
	void bopen(const char command[], const char mode[])
	{
		m = mode[0];
		//cout<<"CMD "<<command<<endl;
		fd = popen(command,mode);
		//cout<<"Mode "<<mode<<endl;
		//plist = (struct pinfo *) malloc(sizeof(struct pinfo));
		plist.file = NULL;
		plist.next = NULL;
		//fd = mypopen(command,mode, &plist);

		SASSERT(fd);
		//SASSERT(plist);
		std::cout<<"PID "<<plist.pid <<std::endl;
	}

	/*!
	 * Writes all the data that has not been yet saved and then closes the stream if it was opened for written.
	 *
	 * @return void. The function does not return any value
	 */
	void bclose()
	{
		SASSERT(fd);
		//SASSERT(plist);
		if (m=='w') {
			SASSERT(n<=BSIZE);
			if (n > 0) {
				//cout<<"StW"<<endl;
				int x = fwrite(buf,sizeof(BTYPE),n,fd);
				if (x!=n) SASSERT(0);
				n = 0;
			}
		}
		fflush(fd);
		//cout<<"ClW"<<fd<<endl;
		//mypclose(fd,&plist);
		//pclose_noshell(&pclose_arg);
		pclose(fd);
		//cout<<"EoW"<<endl;
		fd = NULL;
	}

	/*!
	 * Writes the data of type BTYPE into the next available entry of the buffer.
	 * If after this operation the buffer buf becomes full (BSIZE elements), all the data inside are written into the file and the buffer is flushed
	 *
	 * @param data Value to be written into the buffer
	 * @return void. The function does not return any value
	 */
	void write(BTYPE data)
	{
		SASSERT(fd);
		SASSERT(m=='w');
		SASSERT(n<BSIZE);
		buf[n] = data;
		n++;
		if (n==BSIZE) {
			int x = fwrite(buf,sizeof(BTYPE),n,fd);
			if (x!=n) SASSERT(0);
			n = 0;
		}
	}

	/*!
	 * Gets the next available element of the buffer.
	 *
	 * @param data Reference to the variable of type BTYPE that will contain the data read
	 * @return boolean. It is true if data was read from the buffer
	 */
	bool read(BTYPE & data)
	{
		SASSERT(fd);
		SASSERT(m=='r');
		if (nr==n) {
			n = fread(buf,sizeof(BTYPE),BSIZE,fd);
			nr = 0;
		}
		SASSERT(n <= BSIZE);
		if (nr < n) {
			data = buf[nr];
			nr++;
			return false;
		} else {
			return true; // EOF
		}
	}

	bool read(BTYPE & data,int nothing)
	{
		SASSERT(fd);
		SASSERT(m=='r');
		if (nr==n) {
			n = fread(buf,sizeof(BTYPE),BSIZE,fd);
			nr = 0;
			/*cout<<"DAT :"<<endl;
			for(int i = 0 ; i < n ; i++){
				cout<<buf[i]<<" ";
				if(buf[i] == 362){
					SASSERT(buf[i+1] != 107);
				}
			}
			cout<<endl;*/
		}
		SASSERT(n <= BSIZE);
		if (nr < n) {
			data = buf[nr];
			nr++;
			for(int i = 0 ; i < n ; i++){
				if(buf[i] == 362){
					if(buf[i+1] == 107){
						cout<<"Data "<<i+1<<" is corrupt when nr = "<<nr<<endl;
					}
					SASSERT(buf[i+1] != 107);
				}
			}
			//cout<<endl;
			if(data == 362){
				if(buf[nr] == 107){
					cout<<"CORRUPT DAT :"<<endl;
					for(int i = 0 ; i < n ; i++){
						cout<<buf[i]<<" ";
					}
					cout<<endl;
				}
				SASSERT(buf[nr] != 107);
			}
			return false;
		} else {
			return true; // EOF
		}
	}

};

#endif // End ifndef SASIM_BSTREAM_TEMPLATE_HPP
