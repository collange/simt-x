#include "sasim_tracer.hpp"

// ***************************************************************************
// Begin: sa_tracer class

/*!
 * Resets the number of instructions and uops decoded.
 * 
 * @return void. This function does not return any data.
 */ 
void
sa_tracer::reset()
{
	SASSERT(statinst.empty());
	ninst = 0;
	nuops = 0;
	fd1 = NULL;
	active = false;
}

/*!
 * Returns the reference to one of the entries of vector statinst that contains or 
 * will contain instruction information such as registers, type of instruction, 
 * pc address, etc.
 * 
 * @param inum Instruction number
 * @return Reference to the entry that contains the instruction
 */ 
sa_inst & 
sa_tracer::getinst(int inum)
{
	SASSERT(inum >= 0);
	if (inum >= (int) statinst.size()) {
		statinst.resize(inum+1+statinst.size());
	}
	return statinst[inum];
}

sa_tracer::sa_tracer()
{
	reset();
}

/*!
 * This function loads the traces into the Simulator Memory.
 * 
 * Basically the traces are split into three different files:
 * The first file (trace1) contains the information of each instruction in the program:
 * registers used, type of instruction, PC address, number of loads/stores, etc.
 * The instruction information is taken from the file and stored in one entry of vector 
 * statinst. Each instruction has also a unique number to identify it. This number is used 
 * as index to store/read the instruction in vector statinst.
 * A second file (trace2) contains a list of unique numbers that corresponds to the sequential
 * execution of the program. Having this file avoids repeating the same data along the trace1 
 * file (the same instruction is executed several times). 
 * Normally, instructions such as loads and stores access to different memory addresses each
 * time they are invoked (the address is computed at run-time). For this reason, a third 
 * file (trace3) has been defined with the purpose of storing the different addresses the
 * load and store instructions use along the execution.
 * 
 * @return void. This function does not return any data.
 */ 
void
sa_tracer::open(const char dirname[])
{
	active = true;
	char com[MAXSTRLEN];
	SASSERT(statinst.empty());
	sprintf(com,"%s %s/trace1.%s",SA_DECOMPRESSOR,dirname,SA_EXT);
	fd1 = popen(com,"r");
	//fd1 = popen(com,"r");
	SASSERT(fd1);
	sa_inst inst;
	while (inst.input(fd1)) {
		getinst(inst.inum) = inst;
	}
	pclose(fd1);
	//fd1 = NULL;
	sprintf(com,"%s %s/trace2.%s",SA_DECOMPRESSOR,dirname,SA_EXT);
	trace_inum.bopen(com,"r");
#if 0
#ifdef USE_ATC
	char fname[MAXSTRLEN];
	sprintf(fname,"%s/trace3.atc",dirname);
	trace_addr.open('d',fname,SA_EXT,SA_DECOMPRESSOR);
#else
	sprintf(com,"%s %s/trace3.%s",SA_DECOMPRESSOR,dirname,SA_EXT);
	trace_addr.open(com,"r");
#endif
#endif
	sprintf(com,"%s %s/trace3.%s",SA_DECOMPRESSOR,dirname,SA_EXT);
	trace_addr.bopen(com,"r");
#if 0
#ifdef USE_ATC
	char fname2[MAXSTRLEN];
	sprintf(fname2,"%s/trace4.atc",dirname);
	trace_sp.open('d',fname2,SA_EXT,SA_DECOMPRESSOR);
#else
	sprintf(com,"%s %s/trace4.%s",SA_DECOMPRESSOR,dirname,SA_EXT);
	trace_sp.open(com,"r");
#endif
#endif
	sprintf(com,"%s %s/trace4.%s",SA_DECOMPRESSOR,dirname,SA_EXT);
	trace_sp.bopen(com,"r");

	sprintf(com,"%s %s/trace5.%s",SA_DECOMPRESSOR,dirname,SA_EXT);
	trace_br_taken.bopen(com,"r");

	sprintf(com,"%s %s/trace6.%s",SA_DECOMPRESSOR,dirname,SA_EXT);
	trace_addr_status.bopen(com,"r");
	sprintf(com,"%s %s/trace7.%s",SA_DECOMPRESSOR,dirname,SA_EXT);
	trace_flags.bopen(com,"r");

}

/*!
 * Closes the stream buffers for files trace2 and trace3 that were opened before.
 * 
 * @return void. This function does not return any data.
 */ 
void 
sa_tracer::sa_tracer::close()
{
	if (!active) {
		return;
	}
	trace_inum.bclose();
	//trace_addr.close();
	trace_sp.bclose();
	trace_br_taken.bclose();
	trace_addr_status.bclose();
	trace_flags.bclose();
	active = false;
}

sa_tracer::~sa_tracer()
{
	VERBOSE(cerr << "sa_tracer destructor called" << endl);
	if (active) {
		close();
		active = false;
	}
}

/*!
 * This function feeds the simulator core with the instructions that were read
 * from the trace files (trace1, trace2 and trace3).
 * Unique numbers that identify each instruction executed in sequential order
 * are read from the buffer trace_inum. Each number is used to access the 
 * corresponding entry in vector statinst to obtain instruction information.
 * The addresses required by load/stores instructions are taken from the
 * buffer trace_addr.
 *
 * @return void. This function does not return any data.
 */ 
/*void
sa_tracer::simulate(sasim & proc, int64_t maxinst, int64_t skipinst)
{
  while (1) {
    int inum;
    bool eot = trace_inum.read(inum);
    if (eot) {
      cout << "trace_inum break " << endl;
      break;
    }
    ninst++;
    cout << "incr ninst " << endl;
    sa_inst & inst = getinst(inum);
    SASSERT(inst.decoded);
    nuops += inst.nuops;
    int nr = inst.nloads;
    int nw = inst.nstores;
    SASSERT(nr <= MAXMEMREAD);
    SASSERT(nw <= MAXMEMWRITE);
    sa_addr raddr[MAXMEMREAD];
    sa_addr waddr[MAXMEMWRITE];
    sa_addr sp_addr;
    bool is_branch_taken = false;	
    char addr_status;
    char addr_op_size = (char) 0;	
    int addr_nr = 0;
    int addr_nw = 0;
    sa_addr addr_val;
#if 0
    for (int i=0; i<nr; i++) {
      bool eof = trace_addr.read(raddr[i]);
      if (eof) SASSERT(0);
    }
    for (int i=0; i<nw; i++) {
      bool eof = trace_addr.read(waddr[i]);
      if (eof) SASSERT(0);
    }
#endif
    cout << "read traces for inum  " << inum << " pc " << hex << inst.pc << dec << " inst.nloads " << inst.nloads << " inst.nstores " << inst.nstores << endl;
    {
      bool eof = trace_sp.read(sp_addr);
      if (eof) SASSERT(0);
      cout << "trace_sp.read sp_addr " << hex << sp_addr << dec << endl;
    }

    if (inst.is_branch_cond || inst.is_branch_ret || inst.is_branch_ind)  {
      bool eof = trace_br_taken.read(is_branch_taken);
      if (eof) SASSERT(0);
      cout << "trace_br_taken.read is_branch_taken " << is_branch_taken << endl;
    } 
#if 1
    // read addr_status
    if (inst.nloads || inst.nstores) {
      while (1) {	
	bool eof;

	eof = trace_addr_status.read(addr_status);
        if (eof) SASSERT(0);
        cout << "trace_addr_status.read addr_status ch " << addr_status << endl;

        if ( !((addr_status == 'r') || (addr_status == 'w') || (addr_status == 'e')) ) {
      	  cout << "trace_addr_status.read *invalid* addr_status ch " << addr_status << endl;
	  SASSERT(0);
        }

        if (addr_status == 'e') {
	  break;
        }
        if (addr_status == 'r') {
      	  cout << "trace_addr_status.read *invalid* addr_status ch " << addr_status << " reading addr_op_size " << " addr_nr " << addr_nr << endl;
	  eof = trace_addr_status.read(addr_op_size);
          if (eof) SASSERT(0);

          eof = trace_addr.read(addr_val);
          if (eof) SASSERT(0);
      	  cout << "trace_addr_status.read *invalid* addr_status ch " << addr_status << " reading addr_op_size " << " addr_nr " << addr_nr << " addr_val " << hex << addr_val << dec << endl;

	  raddr[addr_nr] = addr_val;
          addr_nr++;
        }
        if (addr_status == 'w') {
      	  cout << "trace_addr_status.read *invalid* addr_status ch " << addr_status << " reading addr_op_size " << " addr_nr " << addr_nr << endl;
	  eof = trace_addr_status.read(addr_op_size);
          if (eof) SASSERT(0);

          eof = trace_addr.read(addr_val);
          if (eof) SASSERT(0);
      	  cout << "trace_addr_status.read *invalid* addr_status ch " << addr_status << " reading addr_op_size " << " addr_nr " << addr_nr << " addr_val " << hex << addr_val << dec << endl;

	  waddr[addr_nw] = addr_val;
	  addr_nw++;
        }
      }
      SASSERT (inst.nloads == addr_nr);
      SASSERT (inst.nstores == addr_nw);
    }
#endif

    if (ninst > skipinst) {
      cout << "calling proc.input " << endl;
      proc.input(inst,raddr,waddr,sp_addr, is_branch_taken);
    }
    if ((maxinst > skipinst) && (ninst >= maxinst)) {
      break;
    }
  }
  proc.drain();
  close();
}
 */
sa_inst *
sa_tracer::readinst()
{
	int inum;
	bool eot = trace_inum.read(inum);
	if (eot) {
		return NULL;
	}
	sa_inst & inst = getinst(inum);
	return & inst;
}

// End: sa_tracer class
// ***************************************************************************
