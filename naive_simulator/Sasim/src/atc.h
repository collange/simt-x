/*
Copyright (C) 2009 Pierre Michaud
Copyright (C) 2009 INRIA

ATC is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

ATC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with ATC; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef ATC_H
#define ATC_H


typedef unsigned long long ATC_Addr;


/* The compression ratio increases with ATC_BUFFER 
   (but memory requirement too)
*/
#define ATC_BUFFER (1000000)

/* Number of addresses per interval */
#define ATC_INTERVAL (10000000)

/* ATC_MATCHVAL is used for lossy compression 
   The compression ratio increases with ATC_MATCHVAL 
   (but information quality degrades)
*/
#define ATC_MATCHVAL 0.1

/* ATC_NOTRANSBYTES is the number of low-order address bytes for which
   byte translation is disabled when doing lossy compression.
*/
#define ATC_NOTRANSBYTES 2

#define ATC_SOA (sizeof(ATC_Addr))

#define ATC_PSIZE 4
#define ATC_PASSO 64

#define ATC_DNLEN 120
#define ATC_EXTLEN 10
#define ATC_COMLEN 80


typedef struct {
  int h[ATC_SOA][256]; /* byte histogram (most significant byte first) */
  int num;
  unsigned char t[ATC_SOA][256]; /* permutations */
  int n;
} Phase;


typedef struct {
  unsigned char b[ATC_BUFFER];
  int hb[256];
} bblock;


struct atc {
  ATC_Addr a[2][ATC_BUFFER];
  bblock bb[ATC_SOA]; /* most signicant byte first */
  int nb;
  int n;
  int read;
  int x;
  char mode;
  Phase *p[ATC_PASSO][ATC_PSIZE];
  int num;
  char dirname[ATC_DNLEN+1];
  char ext[ATC_EXTLEN+1];
  char command[ATC_COMLEN+1];
  FILE *fd;
  FILE *infofd;
  char translate;
  Phase ph;
  char DISABLE_TRANSLATE;
};


void atc_open(struct atc *p, char mode, const char *dirname, const char *ext, const char *command);
void atc_close(struct atc *p);
void atc_skip(struct atc *p, int num);
void atc_code(struct atc *p, ATC_Addr v);
int atc_decode(struct atc *p, ATC_Addr *v);

#endif

