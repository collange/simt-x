#ifndef SASIM_UARCH_DEFS_HPP
#define SASIM_UARCH_DEFS_HPP

// ***************************************************************************
// Begin: Microarchitecture Definitions

#define __MAGIC_DTLB
#define __MAGIC_DL1
#define __MAGIC_L2
#define __MAGIC_L3
#define __PERFECT_DL1_PORTS
#define __HARMLESS_DL1_FILL

// Last Fetched Store Table
#define SS_LFST_SIZE 		16
#define SS_SSIT_SIZE 		(4*1024)
#define SS_CLEARPERIOD 		10000000

#define SA_COMPRESSOR 		"xz -2 -c"
#define SA_DECOMPRESSOR 	"xz -dc"
#define SA_EXT 			"xz"
#define SA_COMPRESSOR_EXEC "xz"

/*#define SA_COMPRESSOR 		"bzip2 -2 -c"
#define SA_DECOMPRESSOR 	"bzip2 -dc"
#define SA_EXT 			"bzip2"
*/
#define NOREG 			((sa_reg)(-1))
#define NOINSTNUM 		(-1)
#define INFINITE_INTVAL 	(((int64_t) 1) << 60)

// max number of source operands per uop
#define UOPNS 			2
// max number of uops per inst
#define MAXUOPS 		8
// max size of a memory access in bytes
#define MAXMEMACCWIDTH 		16

#define MAXMEMREAD 		2
#define MAXMEMWRITE 		1

#define SASIM_TIMEOUT 		(MEM_LAT_CYCLES*100)

#define MAXWINDOW 		16384

// clock frequency in GHz
#define CLOCK_FREQ 		3.0

#define ROB_SIZE 		64	// Reorder buffer
#define ROB_LATENCY		1

#define INTQ_SIZE 		8	 
#define INTQ_LATENCY 		1	 
#define FPQ_SIZE 		8
#define FPQ_LATENCY 		1

// PREDQ_SIZE is artificial, take it large enough
#define PREDQ_SIZE 		16
#define PREDQ_LATENCY		1

#define LOAD_ISSUE 		2
// LOAD_LIMIT is the maximum number of LOADs in the ROB
// (this corresponds to what is generally called load queue size)
#define LOAD_LIMIT 		32
#define NUM_LDQ 		4
#define LDQ_SIZE 		64
#define LD_LATENCY		1

#define STQ_SIZE 		16
#define PRSTQ_SIZE 		16
#define ST_LATENCY		1
#define PRST_LATENCY		1

// decode rate in inst/cycle
#define DECODE_RATE 		4
#define SIMPLE_INST_UOPS 	2
#define SINGLE_COMPLEX_DECODER
// dispatch rate and retire rate in uops/cycle
#define DISPATCH_RATE 		6
#define RETIRE_RATE 		6
#define STORE_RETIRE_RATE 	2

#define NUM_ALU 		4
#define NUM_FPADDMUL 		2
#define ALU_LAT 		1
#define FPADDMUL_LAT 		4
#define FPMOV_LAT 		1

#define IMUL_LAT 		3
#define IDIV_LAT 		20
#define FPDIV_LAT 		7

#define __PERFECT_BPRED
#define BRANCH_RESOL_AT_EXEC
#define MAX_NUMG 		4
#define TAGE_BSIZE 		4096
#define TAGE_GSIZE 		1024
#define ITTAGE_BSIZE 		128
#define ITTAGE_GSIZE 		32

//#define LOADSTORE_ORACLEFWD
#define LOADSTORE_REALFWD

//#define __STEERING_ROUNDROBIN

#define DL1_WRITE_PORTS 	2
#define DL1_PORTS 		8
//#define DL1_BANKS 		8
// bank width in bytes
//#define DL1_BANK_WIDTH 		8
// fill width in number of lines
#define DL1_FILL_WIDTH 		((DL1_BANKS*DL1_BANK_WIDTH)/CACHELINE_BYTES)
#define DL1_FQ_DELAY 		4
#define DL1_FQ_RATE 		8
#define DL1_FQ_RATEMAX 		127

#define IL1_NUM_REQS 		6
#define IL1_BUFSIZE 		6

// FEQ_DELAY simulates front-end pipe stages
#define FEQ_DELAY 		4
// FEQ_SIZE in instructions
#define FEQ_SIZE 		(FEQ_DELAY * DECODE_RATE * 2)

// BEQ_DELAY simulates back-end pipe stages
#define BEQ_DELAY 		4
// BEQ_SIZE in uops
#define BEQ_SIZE 		(BEQ_DELAY * DISPATCH_RATE)

#define MQ_SIZE 		16
#define WB_SIZE 		8

#define CACHELINE_BYTES 	64

#define ENABLE_L3

#define IL1_SIZE_BYTES 		(32*1024)
#define IL1_ASSO 		8
#define IL1_POL 		reppol(CLOCK)
// IL1 latency in nanoseconds
#define IL1_LATENCY 		0.5
#define IL1_LAT_CYCLES 		((int) ceil(IL1_LATENCY*CLOCK_FREQ))
#define IL1_MQ_SIZE 		16
#define IL1_FQ_SIZE 		(L2_LAT_CYCLES)
// IL1 rate in cycles per line
#define IL1_RATE 		1

#define DL1_SIZE_BYTES 		(32*1024)
#define DL1_ASSO 		8
#define DL1_POL 		reppol(CLOCK)
// DL1 latency in nanoseconds
#define DL1_LATENCY 		0.5
#define DL1_LAT_CYCLES 		((int) ceil(DL1_LATENCY*CLOCK_FREQ))
#define DL1_MQ_SIZE 		DL1_MQ_ENTRIES
#define DL1_WB_SIZE 		WB_SIZE
#define DL1_FQ_SIZE 		(L2_LAT_CYCLES)
#define DL1_MQ_UOPS 		16
#define DL1_MQ_ENTRIES 		16
// DL1 rate in cycles per line
#define DL1_RATE 		1

#define L2_SIZE_BYTES 		(512*1024)
#define L2_ASSO 		8
#define L2_POL 			reppol(CDIP,32,11)

// L2 latency in nanoseconds
#define L2_LATENCY 		3.0
#define L2_LAT_CYCLES 		((int) ceil(L2_LATENCY*CLOCK_FREQ))
#define L2_MQ_SIZE 		MQ_SIZE
#define L2_WB_SIZE 		WB_SIZE
#ifdef ENABLE_L3
#define L2_FQ_SIZE 		(L3_LAT_CYCLES)
#else
#define L2_FQ_SIZE 		(MEM_LAT_CYCLES*MEM_BANDWIDTH/CACHELINE_BYTES)
#endif
// L2 rate in cycles per line
#define L2_RATE 		1

#define L3_SIZE_BYTES 		(8*1024*1024)
#define L3_ASSO 		16
#define L3_POL 			reppol(CDIP,128,14)

// L3 latency in nanoseconds
#define L3_LATENCY 		6.0
#define L3_LAT_CYCLES 		((int) ceil(L3_LATENCY*CLOCK_FREQ))
#define L3_MQ_SIZE 		MQ_SIZE
#define L3_WB_SIZE 		WB_SIZE
#define L3_FQ_SIZE 		(MEM_LAT_CYCLES*MEM_BANDWIDTH/CACHELINE_BYTES)
// L3 rate in cycles per line
#define L3_RATE 		1

// mem latency in nanoseconds
#define MEM_LATENCY 		70.0
#define MEM_LAT_CYCLES 		((int) ceil(MEM_LATENCY*CLOCK_FREQ))
// bandwidth in bytes per clock cycle
#define MEM_BANDWIDTH 		16

#define PAGESIZE_BYTES 		(4*1024*1024) 
#define PAGESIZE_LINES 		(PAGESIZE_BYTES / CACHELINE_BYTES)
#define PTESIZE_BYTES 		8

#define PAGETABLE_ADDR 		0x80000000ULL

#define MAX_DTLB_MISS 		2

#define ITLB1_ENTRIES 		64
#define ITLB1_ASSO 		4
#define ITLB1_POL 		reppol(LRU)

#define DTLB1_ENTRIES 		64
#define DTLB1_ASSO 		4
#define DTLB1_POL 		reppol(LRU)

#define TLB2_ENTRIES 		512
#define TLB2_ASSO 		4
#define TLB2_POL 		reppol(LRU)
#define TLB2_LAT_CYCLES 	4

#define STRIDE_PREFETCH
#define SPFILTERSIZE 		16
#define SPLT_ENTRIES 		64
#define SPMAXCONF 		15
#define SPSTEP 			16

#define ENABLE_L2_PREFETCH true
#ifdef ENABLE_L3
#define L2_PFR_SIZE 		8
#define L2_PF_HISTLEN 		16
#define L2_PF_MINSTEP 		1
#define L2_PF_MAXSTEP 		64
#define L2_PFB_SIZE 		32
#define L2_PF_ROUNDS 		100
#define L2_PF_LOGZONE 		4
#define L2_PF_DEFSTEP 		2
#else
#define L2_PFR_SIZE 		8
#define L2_PF_HISTLEN 		16
#define L2_PF_MINSTEP 		1
#define L2_PF_MAXSTEP 		128
#define L2_PFB_SIZE 		32
#define L2_PF_ROUNDS 		100
#define L2_PF_LOGZONE 		4
#define L2_PF_DEFSTEP 		2
#endif

#define ENABLE_L3_PREFETCH true
#define L3_PFR_SIZE 		8
#define L3_PF_HISTLEN 		16
#define L3_PF_MINSTEP 		1
#define L3_PF_MAXSTEP 		128
#define L3_PFB_SIZE 		64
#define L3_PF_ROUNDS 		100
#define L3_PF_LOGZONE 		4
#define L3_PF_DEFSTEP 		4

// End: Microarchitecture Definitions
// ***************************************************************************

#endif // End ifndef SASIM_UARCH_DEFS_HPP
