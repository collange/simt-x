#include "SimpleHeuristic.hpp"

void Sync::choose_threads()
{
	// Round robin among active threads
	static int rrcounter = 0;
	unsigned int ref = -1;
	for (UINT ii=0; ii<num_threads; ii++){
		unsigned int i = (ii + rrcounter) % num_threads;
		if (!t[i].ignore){
			ref = i;
			break;
		}
	}
	bool isact = false;
	for (UINT i=0; i<num_threads; i++){
		t[i].is_active = !t[i].ignore && t[i].pc == t[ref].pc;
		if(t[i].is_active){
			isact = true;
		}
		//t[i].is_active = (i == ref);    // No fetch combining
	}


	/*bool problem = true;
	for (UINT i=0; i<num_threads; i++){
		if(t[i].is_active){
			problem = false;
		}
	}
	if (problem){
		for (UINT i=0; i<num_threads; i++){
			cout<<"Before Thread id "<<i<<" ";
			printf(" %.16lx \n",t[i].pc );
			if(t[i].ignore){
				cout<<" IG ";
			}else{
				cout<<" NIG ";
			}
			if(t[i].barrier){
				cout<<" BA ";
			}
			else{
				cout<<" NBA ";
			}
			if(t[i].is_finished){
				cout<<" FIN ";
			}else{
				cout<<" NFIN ";
			}
			cout<<endl;
		}
	}*/
	// Next
	rrcounter = (ref + 1) % num_threads;
	if(!isact){
		t[rrcounter].is_active = true;
	}
}

bool Sync::lockstep(){
	return true;
}
