#include "sasim_memory.hpp"

// ***************************************************************************
// Begin: sasim_memory class

/*!
 * Initializes the parameters of the memory such as memory latency, bandwidth, 
 * cache level closest to the memory
 * 
 * @param p Pointer to the closest cache level
 * @param mem_lat Memory Latency in Cycles
 * @param bandwidth Bandwidth of the memory in bytes per clock cycle
 * @return void. The function does not return any value
 */ 
void 
sasim_memory::init(sasim_cache * p, int mem_lat, int bandwidth)
{
  SASSERT(p);
  prev = p;
  latency = mem_lat;
  // bandwidth in bytes per clock cycle
  SASSERT(ISPOW2(bandwidth));
  int rate = CACHELINE_BYTES / bandwidth;
  SASSERT(rate>=1); // rate in cycles per line
  bus.init(1,rate);
  nread = 0;
  nwrite = 0;
  VERBOSE(cerr << "mem latency = " << MEM_LAT_CYCLES << " cycles" << endl);
}

/*!
 * Simulates one cycle of memory writing. If there is a data ready from the cache memory (write-back) and
 * the bus is not full (only one request), the address is taken from the cache (wb queue) and written into
 * the bus queue of the memory.
 * 
 * @param cycle Current cycle number
 * @return void. The function does not return any value
 */ 
void 
sasim_memory::onecycle_write(int64_t cycle)
{
  SASSERT(prev);
  if (prev->wb.ready(cycle) && !bus.full()) {
    sa_addr lineaddr = prev->wb.dequeue();
    bus.enqueue(lineaddr,cycle);
    nwrite++;
  }
}

/*!
 * Simulates one cycle of memory reading. If there is a request ready from the next cache level and the bus is
 * not full (only one read/write request) and also the fill queue of the cache is not full, the address requested
 * will be taken from the cache and put it into the memory bus queue. The fill queue of the cache is also
 * written and the latency considers both the latency of the bus and the latency of the cache.
 * 
 * @param cycle Current cycle number
 * @return void. The function does not return any value
 */ 
void 
sasim_memory::onecycle_read(int64_t cycle)
{
  SASSERT(prev); 
  if (prev->mq.ready(cycle) && ! bus.full() && ! prev->fq.full()) {
    sa_req req = prev->mq.dequeue();
    bus.enqueue(req.lineaddr,cycle);
    prev->fq.enqueue(req,cycle+latency);
    //cout << req.lineaddr << " " << cycle << " " << cycle+latency << endl;
    nread++;
    if (prev->pf.active) {
      prev->pf.pfb.push(req,cycle); // for training prefetch distance
    }
  }
}

/*!
 * If there is any petition from the prefetcher of the cache memory and the bus is not full,
 * the address requested will be written into the bus and removed from the prefetcher queue
 * request. The data moving is simulated by pushing the address into one of the prefetcher
 * queues and considering as latency the time required by both the prefetcher and the bus.
 * 
 * @param cycle Current cycle number
 * @return void. The function does not return any value
 */ 
void 
sasim_memory::onecycle_prefetch(int64_t cycle)
{
  SASSERT(prev); 
  if (! prev->pf.active || ! prev->pf.pfr.ready(cycle) || bus.full()) {
    return;
  }
  sa_addr pfline = prev->pf.pfr.dequeue();
  prev->pf.npfreq++;
  bus.enqueue(pfline,cycle);
  prev->pf.pfb.push(sa_req(pfline,SA_REQ_NOREQ),cycle+latency);
  //cout << "\033[31m" << pfline << " " << cycle << " " << cycle+latency << " (" << lineaddr << ") " << "\033[m" << endl;
  nread++;
}

/*!
 * Simulates one memory cycle (read or write)
 * If the previous request for reading or writting in the bus was already performed, one cycle 
 * of memory processing is performed (writting, reading or prefetching). Notice that if the 
 * write back queue of the cache is full, it is necessary to first try to perform a writting cycle
 * and then a reading one.
 * 
 * @param cycle Current cycle number
 * @return void. The function does not return any value
 */ 
void 
sasim_memory::onecycle(int64_t cycle)
{
  while (bus.ready(cycle)) {
    bus.dequeue();
  }
  // order here is important
  if (prev->wb.full()) {
    onecycle_write(cycle);
    onecycle_read(cycle);
    onecycle_prefetch(cycle);
  } else {
    onecycle_read(cycle);
    onecycle_write(cycle);
    onecycle_prefetch(cycle);
  }
}

void
sasim_memory::debugprint_queues()
{
  cerr << "memory" << endl;
  DBPRINTQ(bus);
}

void
sasim_memory::printstats(FILE *stream)
{
  fprintf(stream,"mem read = %lld\n",(long long) nread);
  fprintf(stream,"mem write = %lld\n",(long long) nwrite);
}

// End: sasim_memory class
// ***************************************************************************
