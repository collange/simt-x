#include "DataAnalyser.hpp"

#include <stdio.h>
#include <math.h>
//#include <__tree>
#include <algorithm>
#include <cassert>
#include <deque>
#include <iomanip>
#include <iostream>
#include <iterator>
#include "lib/dviq.hpp"
#include "reg_names.hpp"

#include "../../Tage/predictor.h"


std::map<int, std::string> register_masks;

DataAnalyser::DataAnalyser(){
	//memset(this, 0, sizeof(DataAnalyser));
}

DataAnalyser::~DataAnalyser(){
	delete [] instructions;
}

/*******************************************************************
	Helper Functions
*******************************************************************/
void convert_int_to_vectormask(int mask, int new_mask[], int length){
	int rem;
	int count = 0; 
	while(count < length){
		rem = mask%2;
		mask = mask/2;
		new_mask[count] = rem; 
		count++;
	}
}

void convert_vector_to_array(std::vector<std::vector<int> > cache_mask, int ld_count, int new_mask[], int length){
	std::vector<std::vector<int> >::iterator it = cache_mask.begin();
	std::advance(it, ld_count);
	std::vector<int>::iterator tt = (*it).begin();
	for(int i = 0; i < length; i++){
		new_mask[i] = (*tt);
		tt++;
	}
} 

int DataAnalyser::get_mask_pathTable(int tag, int warpid){
	return pathtable[warpid].mask[tag];
}

int get_int_mask(int smask[], int threads_per_warp){
	int maskval = 0;
	for(int i = 0; i < threads_per_warp; i++)
		if(smask[i])
			maskval += pow(2, i);

	return maskval;
}


void DataAnalyser::setup_warps(int num_warps){
	for (int i=0; i<num_warps; ++i) {
		loadstorebuffer.push_back(std::deque<Instruction>());
		warp_cycle[i] = 0;
		heur_cycle[i] = 0;
		warp_seqno[i] = 0;

		_stat_fetch_blocked[i] = 0;
		_stat_fetch_picked[i] = 0;
		fetch_rr_blocked[i] = 0;
		_stat_fetch_rr_picked[i] = 0;
		issue_dep_blocked[i] = 0;
		issue_fu_blocked[i] = 0;

		_stat_fetch_qfull[i] = 0;
		_stat_fetch_stall[i] = 0;
		_stat_ignore[i] = 0;
		_stat_fetch_qempty[i] = 0;
		_stat_iq_full[i] = 0;
		_stat_fetch_fair[i] = 0;
		_stat_active_thread_count[i] = 0;

	}

}

void DataAnalyser::setup_analyzer_vars(int num_warps, int threads_per_warp){
	commit_current_warp = 0;
	ls_current_warp = 0;
	
	instructions = new LONG[num_threads];
	warp_cycle = new int[num_warps];
	active = new bool[num_threads];
	heur_cycle = new LONG[num_warps];
	warp_seqno = new LONG[num_warps];	

	load_buffer = new std::deque<DVInstLoad>[num_warps];
	store_buffer = new std::deque<DVInstStore>[num_warps];
	issued_instructions = new std::deque<DVInst>[num_warps];

	_stat_fetch_fair = new LONG[num_warps];
	_stat_fetch_blocked = new LONG[num_warps];
	_stat_fetch_picked = new LONG[num_warps];
	fetch_rr_blocked = new LONG[num_warps];
	_stat_fetch_rr_picked = new LONG[num_warps];
	issue_dep_blocked = new LONG[num_warps];
	issue_fu_blocked = new LONG[num_warps];
	_stat_fetch_qfull = new LONG[num_warps];
	_stat_fetch_stall = new LONG[num_warps];
	_stat_ignore = new LONG[num_warps];
	_stat_fetch_qempty = new LONG[num_warps];
	_stat_iq_full = new LONG[num_warps];
	_stat_active_thread_count = new LONG[num_warps];

	bank_conflict_count = 0Ull;
	bank_conflict_count_reduced = 0Ull;
	mem_operation_count = 0Ull;
	fetch_count = 0;

	setup_warps(num_warps);
	
	#ifdef ALTER
	store_in_flight = new bool[num_threads];
	#endif

}

/***************************************
   Setup: - Branch predictor
	- Dv-Statistics
 	- Cache parameters
	- Regfile
****************************************/
void DataAnalyser::init(Thread* thrds, UINT nt, UINT nf, Config c, int readahead, HeuristicData* fetch_h, Cache *_l2, int _coreid){

	if (conf.pause_thread)
		skip_thread.insert(2);

	//setup branch predictor
	cout << endl << "Initiating branch predictor" << endl; 
	branchpredictor = new PREDICTOR(nt); cout << endl;

	//setup dv variables
	coreid = _coreid;
	threads = thrds;
	assert(threads[0].coreid == _coreid); 
	num_threads = nt;
	num_fetches = nf;
	conf = c;
	conf.num_threads = nt;
	num_warps = conf.num_warps;
	threads_per_warp = conf.threads_per_warp;

	fetch_heur_data = fetch_h;
	fetch_heuristic = fetch_heur_data->new_heuristic(num_threads,num_warps,threads, fetch_h->parameter); //can be found in the Heuristics folder
	readahead_horizon = readahead;
	inst_waiting_in_barrier = 0;
	finished_threads = 0;
	periodic_logger = 0;
	stall_next_cycle = false;

	simulation_instructions = (((LONG)conf.sim_insts) * 1000000); //in million
	assert(simulation_instructions == ((LONG)conf.sim_insts) * 1000000);
	dviqm = DVIQ(c); //All DV related constructs are placed in src/lib/globals files - TODO: revise or eliminate
	issueQ.resize(2+conf.num_fp_fus+conf.num_int_fus+conf.num_mem_fus); //conf.issue_entries));	//OoO issue queue
	
	//setup and initialize caches 
	l2_cache.init(conf, &statistics, 4096, 16, 15 , 2 ,NULL);//int size, int associativity, int latency, int level,Cache &next
	l1_cache.init(conf, &statistics, 32, 16, 2, 1, &l2_cache); //cout<<"Byte offset "<<l1_cache.getparams().byte_offset<<endl;
	mshr = MSHR(l1_cache.getparams().byte_offset,MSHR_SIZE);
	l1_cache.add_mshr(&mshr);
	//setup register file
	reg = RegFile(num_threads); //TODO eliminate

	fetch_counter = new UINT[conf.num_warps];
	fu =FunctionalUnits(c); //src/lib/globals.cpp/hpp -- sets up all Functional Units, maximum units etc
	for(int i = 0 ;i< c.num_warps;i++)
		fetch_counter[i] = 0;
	
	//setup more analyzer variables and statistic variables
	setup_analyzer_vars(conf.num_warps, conf.threads_per_warp);
	statistics.init(conf);
	timeout_cycle = conf.num_warps*(conf.threads_per_warp + 1);
	minsp_blocked = new bool[conf.num_threads]; // <---------------------------------??

	for (int i=0; i<conf.num_threads; ++i) {
#ifdef ALTER
		store_in_flight[i] = false;
#endif
		active[i] = false;
	}
	std::fill(instructions, instructions+num_threads, 0);
	number_cycles = 0;


}

/*********************************************************
   Determines whether simulation is finished
*********************************************************/
bool DataAnalyser::isFinished(){
	//LONG total_inst=0;
	int finished_count = 0;
	int offset = 0;	
	bool ret_value = false;

	
	for(UINT i= 0;i< conf.num_threads;i++){ //num_threads
		if(threads[i].fetch_qempty)
			finished_count++;

	}
	
	
	//cout << "Total INST executed = " << statistics.total_dv_committed << ", vs necessary = " << simulation_instructions << endl;
	//statistics.fetch_inst_count = total_inst; //+= count_inst_active(dv_temp.actual_mask);	

	if( (UINT)finished_count == conf.num_threads){ //|| statistics.total_ins_count >= 1143420300){   //929752361){  //TODO ERASE WHEN DEBUGGED  
		//cout << "Total INST executed = " << statistics.total_ins_count << ", vs necessary = " << simulation_instructions << endl;
		//cout << "finish count = " << finished_count << ", and num threads = " << conf.num_threads << endl;
		if(reorderB.data_valid.empty()){// && ((UINT)finished_count == conf.num_threads)){ 
			for(UINT i= 0;i<conf.num_threads;i++){
				pthread_mutex_lock( &threads[i].iq_mutex );
				threads[i].end_of_simulation = true;
				threads[i].prefetch_instruction_queue.clear();
				pthread_cond_signal(&threads[i].prefetchq_notfull);
				pthread_mutex_unlock( &threads[i].iq_mutex );
				//threads[i].finished = true;
			}
			ret_value = true;
		}
		
		
	}

	if(ret_value){
		cout<<"Status of active threads "<<endl; 
			for(int i = 0 ;i< conf.num_threads;i++){
				if(!threads[i].fetch_isactive){
					cout<<"Thread "<<i<< " is inactive "; 
				}else{
					cout<<"Thread "<<i<< " is active "; 
				}
				cout<<"fq_size "<<threads[i].prefetch_instruction_queue.size()  << ", fqb_size " << threads[i].prefetch_instruction_queue_buffer.size() << ", "; 
				cout << "exception = " << threads[i].exception << ", empty = " <<  threads[i].fetch_qempty	 << ", stall = " << threads[i].fetch_stall;
				cout << ", barrier = " << threads[i].ignore << endl;
			}
	}


	return ret_value;
}


void DataAnalyser::copy_IS_table(IS_Table &tmp_is, int warpid){
	tmp_is.path = isTable[warpid].path;
	tmp_is.pc = isTable[warpid].pc;
	tmp_is.call_depth = isTable[warpid].call_depth;
	tmp_is.barrier = isTable[warpid].barrier;
	tmp_is.time_stamp = isTable[warpid].time_stamp;
}

////////////////////////////////////////////////////
//	MinSP-PC Warp Scheduling Heuristic
////////////////////////////////////////////////////


int find_first_active(int mask[], int length){
	for(int i = 0; i < length; i++){
		if(mask[i])
			return i;
	}
	return -1;
}

int ret_mask(int x_smask[], int length){
	int mask = 0;
	for(int i = 0; i < length; i++){
		if(x_smask[i]){
			mask += pow(2,i);		
		}
	}
	return mask;
}


void DataAnalyser::create_isTable_entry(int x_smask[], int offset, int tag){
	LONG pc_push = return_pc(x_smask, offset);
	isTable[current_warp].pc.push_back(pc_push);
	LONG sp_push = return_sp(x_smask, offset);
	isTable[current_warp].call_depth.push_back(sp_push);
	isTable[current_warp].path.push_back(tag); 
	isTable[current_warp].barrier.push_back(0);
	isTable[current_warp].time_stamp.push_back(number_cycles);

	isTable[current_warp].sp = sp_push;
	isTable[current_warp].prog_counter = pc_push;
	isTable[current_warp].last_scheduled = number_cycles;
}


void DataAnalyser::create_new_path_is(int mask[], int offset, int actual_mask){
	//Pathtable entry creation
	int pushmask = ret_mask(mask, threads_per_warp);
	int tag = pathtable[current_warp].free_ptr; 
	newpathtable_entry(pushmask, 1, actual_mask, current_warp);
	update_warp_thread_tags(offset, tag, mask); //update thread's data

	//cout << "<><>< GOING INTO CREATE WITH w = " << current_warp << ", m = " << pushmask << endl;
	//IS Table entry creation
	create_isTable_entry(mask, offset, tag);
}


bool DataAnalyser::detect_reconvergence(int &tag1, int &tag2){
	std::vector<int>::iterator tag = isTable[current_warp].path.begin();
	std::vector<LONG>::iterator sp = isTable[current_warp].call_depth.begin();
	for(std::vector<LONG>::iterator pc = isTable[current_warp].pc.begin(); pc != isTable[current_warp].pc.end(); pc++){
		
		std::vector<int>::iterator inner_tag = isTable[current_warp].path.begin(); //check for barriers as well
		std::vector<int>::iterator bar = isTable[current_warp].barrier.begin();
		std::vector<LONG>::iterator inner_sp = isTable[current_warp].call_depth.begin();

		for(std::vector<LONG>::iterator in_pc = isTable[current_warp].pc.begin(); in_pc != isTable[current_warp].pc.end(); in_pc++){
			if(!(*bar) && (*inner_sp == *sp) && (*pc == *in_pc) && (*inner_tag) != (*tag) && pathtable[current_warp].rbit[*tag] && pathtable[current_warp].rbit[*inner_tag]){
				tag1 = *tag;
				tag2 = *inner_tag;
				return true;
			}
			inner_tag++; bar++; inner_sp++;
		}
		tag++; sp++;
	}
	return false;
}

void DataAnalyser::create_IStable_entries(){
	int offset = current_warp * threads_per_warp;
	int tmp_mask[threads_per_warp], entry_created[threads_per_warp];

	for(int i = offset; i < threads_per_warp + offset; i++){
		if(threads[i].fetch_pc == threads[offset].fetch_pc){
			tmp_mask[i-offset] = 1;
			entry_created[i-offset] = 1;
		}else{
			tmp_mask[i-offset] = 0;
			entry_created[i-offset] = 0;
		}
	}

	create_new_path_is(tmp_mask, offset, get_int_mask(tmp_mask, threads_per_warp));//create pathtable entry, with that tag create IStable entry

	//while entry_created not all full -- need to create more paths so all threads are mapped to warps
	bool finished = true;
	int start;
	
	for(int i = 0; i < threads_per_warp; i++){
		if(!entry_created[i]){	
			finished = false;
			start = i;
			break;
		}			
	}
	while(!finished){ //Assigns all threads to a path in tables		
		for(int i = 0; i < threads_per_warp; i++)
			tmp_mask[i] = 0;
		
		for(int i = start + offset; i < threads_per_warp + offset; i++){
			if(threads[i].fetch_pc == threads[start].fetch_pc){
				tmp_mask[i-offset-start] = 1;
				entry_created[i-offset-start] = 1;
			}else{
				tmp_mask[i-offset-start] = 0;
				entry_created[i-offset-start] = 0;
			}
		}

		create_new_path_is(tmp_mask, offset, get_int_mask(tmp_mask, threads_per_warp));
		start = threads_per_warp;
		for(int i = 0; i < threads_per_warp; i++){
			if(!entry_created[i]){	
				finished = false;
				start = i;
				break;
			}			
		}
		if(start == threads_per_warp) finished = true;			
	}
		
}

void print_smask(int x_smask[], int length){
//#ifdef DEBUG
	cout << "smask = ";
	for(int i = 0; i < length; i++){
		if(x_smask[i])
			cout << "1";
		else
			cout << "0";
	}
	cout << endl;
//#endif
}

bool DataAnalyser::no_pending_exceptions(int tag1, int tag2, int offset){
	int maskA[threads_per_warp], maskB[threads_per_warp];
	convert_int_to_vectormask(pathtable[current_warp].mask[tag1], maskA, threads_per_warp);
	convert_int_to_vectormask(pathtable[current_warp].mask[tag2], maskB, threads_per_warp);

	LONG fetch_pc = 0;

	//assert that all threads converging will be of same next pc (due to some trace PLT issues)
	for(int i = offset; i < threads_per_warp+offset; i++){
		if(maskA[i-offset] || maskB[i-offset]){
			if(!fetch_pc) fetch_pc = threads[i].fetch_pc;
			else{
				if(fetch_pc != threads[i].fetch_pc)
					return false;
			}
		}
	}	

	for(int i = offset; i < threads_per_warp+offset; i++){
		if(threads[i].exception)
			if(maskA[i-offset] || maskB[i-offset])
				return false;
	}

	return true;

}

int DataAnalyser::thread_schedule_minsp(int &istable_number, int &ISentry, int cyc){ 
	bool is_active = false, branching_issue = false;
	int cwid = -1;
	uint offset;

	//get current table/warp/thread statuses/////////////////////////////////
	if(cyc == 0){
		//get next ISTable (RR) = outer warp index
		current_warp = (istable_number+1)%num_warps;  
		int count_warps = 0;
		while(true){
			offset = current_warp * threads_per_warp; //thread index
			if(!check_empty(offset)){
				break;
			}else{
				count_warps++;
				if(count_warps == num_warps) return -1; //out of thread schedule. None available
				current_warp = (istable_number+1)%num_warps; 
			}
		}
		offset = current_warp * threads_per_warp; 
		fetch_warp = current_warp;
		istable_number = fetch_warp;
	}else{
		//use previous warp number. recalc offset
		offset = current_warp * threads_per_warp;
	}
	
	//Init conditions - if no IStable entry exists, create its entries based on threads[] of the given warp
	if(isTable[fetch_warp].pc.empty() && cold[fetch_warp]){
		create_IStable_entries(); 
		current_warp = fetch_warp; 
		istable_number = fetch_warp;
		offset = current_warp * threads_per_warp; 
		cold[fetch_warp] = false;

	}else if(isTable[fetch_warp].pc.empty()){ //no more threads/paths to schedule. Finish

		return -1;
	} 

	//check for any branching PST issues. Will need to fetch from this until resolved at execute
	for (UINT i=0+offset; i<threads_per_warp+offset; i++){
		if(threads[i].branch_stall){
			branching_issue = true;
		}
	}

	//check for empty threads
	for (UINT i=0+offset; i<threads_per_warp+offset; i++){
		if(threads[i].fetch_qempty){
			return -1;
		}
	}

	if(number_cycles == dviq_cycle){ //heuristic already scheduled warp, check if warp should fetch or stall)
		if(threads[current_thread].fetch_qempty){ //cant fetch the thread
			return -1;
		}
		if(!fetch_started){
			if (!threads[current_thread].iq_full){ //double check IQ is not full (1st time in loop)
				fetch_started = true;
			}
		}
		if(fetch_started){
			int ref1 = -1;
			bool dvthread_isactive = false;
			offset = current_warp * threads_per_warp;
	
			for (UINT i=0+offset; i<threads_per_warp+offset; i++){ //double check each thread in the warp
				//threads[i].fetch_isactive = threads[i].fetch_pc == threads[current_thread].fetch_pc && !threads[i].ignore && !threads[i].fetch_qempty && !threads[i].fetch_stall; 
				
				if(threads[i].fetch_isactive){ 
					active[i] = true;
					ref1 = i;
					dvthread_isactive = true;
				}
			}
			if(dvthread_isactive){ //if given thread is active in warp
				if(threads[current_thread].fetch_stall){
					current_thread = ref1;
				}
				return current_warp;
			}else{ //inactive	
				return -1;
			}
		}
	

	}else{ //1st time in the fetch heuristic for a new clock cycle's fetch
		dviq_cycle = number_cycles;
	}
	fetch_started = false;


	//round robin between processor warps
	//minsp-pc between IS Table warps
	int num_searched = 0; //, stream;
	int ref, entry = 0;
	bool finish = true; 

	for (UINT i=0+offset; i<threads_per_warp+offset; i++){ //update threads in the warp (see if finished)
		if(!threads[i].fetch_qempty){
			finish = false;
		}			
	}
		
	if(finish){ //if a fetch_q is empty, stop simulation for that warp ---temporary. 
		for (UINT i=0+offset; i<threads_per_warp+offset; i++){ 
			threads[i].finish_cycle = number_cycles; 
			threads[i].set_finish_cycle = true;	
		}
		isFinished(); 
		return -1; //if all warps are finished	
	}


	////////	Reconvergence detection ///////////////
	int tag1, tag2;
	
	if(detect_reconvergence(tag1, tag2) && no_pending_exceptions(tag1, tag2, offset) && !branching_issue){ 
		int index = 0; 
		ref = -1;

		std::vector<LONG>::iterator pc = isTable[fetch_warp].pc.begin(), sp = isTable[fetch_warp].call_depth.begin(); 
		std::vector<int>::iterator bar = isTable[fetch_warp].barrier.begin();
		for(; pc != isTable[fetch_warp].pc.end(), sp != isTable[fetch_warp].call_depth.end(), bar != isTable[fetch_warp].barrier.end(); 
											pc++, sp++, bar++){
			if(ref == -1 || (!(*bar) && (*sp < isTable[fetch_warp].call_depth[ref] || 
				(*sp == isTable[fetch_warp].call_depth[ref] && *pc < isTable[fetch_warp].pc[ref])))){
					ref = index; //get index of thread with lowest fetch SP ---OR--- same SP and lowest fetch PC
			}						
			index++;
		}	
	
		//Get corresponding thread mask
		int active_mask[threads_per_warp];
		convert_int_to_vectormask(pathtable[fetch_warp].mask[isTable[fetch_warp].path[ref]], active_mask, threads_per_warp);
#ifdef DEBUGFETCH
		cout << "Detected [CONV] between path " << tag1 << " and path " << tag2 << endl;
#endif
		update_structures_reconverge(active_mask, offset, 1, tag1, tag2, fetch_warp);
#ifdef DEBUGFETCH
		print_pathtable(fetch_warp);
#endif
		statistics.convergence_count++;
	} /////////////////////////////////////
		
		//copy isTable to a temporary variable
		IS_Table tmp_is;
		copy_IS_table(tmp_is, fetch_warp);
#ifdef DEBUGFETCH
		cout << "IS Table size is " << tmp_is.pc.size() << endl;
#endif	
		

		while((num_searched < isTable[fetch_warp].pc.size()) || !tmp_is.pc.empty()){
			ref = -1;
			//find warp candidate in given IStable -- update the warp's status (MinSP-PC)
			//i.e. Go through the IS Table and check for lowest SP and PC 
			int index = 0, barrier_count = 0; 
		

			std::vector<LONG>::iterator pc = tmp_is.pc.begin(), sp = tmp_is.call_depth.begin(); 
			std::vector<int>::iterator bar = tmp_is.barrier.begin();

			for(; pc != tmp_is.pc.end(), sp != tmp_is.call_depth.end(), bar != tmp_is.barrier.end(); pc++, sp++, bar++){
				if(ref == -1 && !(*bar)){	
					ref = index;
				}else if(*bar){
					index++; barrier_count++; continue;
				}else if(*sp < tmp_is.call_depth[ref] || (*sp == tmp_is.call_depth[ref] && *pc < tmp_is.pc[ref])){
					ref = index; //get index of thread with lowest fetch SP ---OR--- same SP and lowest fetch PC
				}						
				index++;
			}	
		
			/////////////////////////// 	Barrier Detection 	///////////////////////////////////////////////////
			if(barrier_count == index){ //all paths have reached the barrier. Remove and continue their execution
				for(std::vector<int>::iterator bar = tmp_is.barrier.begin(); bar != tmp_is.barrier.end(); bar++){
					(*bar) = 0;
				}
				for(std::vector<int>::iterator bar = isTable[fetch_warp].barrier.begin(); bar != isTable[fetch_warp].barrier.end(); bar++){
					(*bar) = 0;
				}

				cout << "BARRIERS RECONVERGE!!!" << endl; 
				//create a mask for all active threads of collective warps, i.e. takes into consideration finished prefetchQs
				int barrier_mask[threads_per_warp];

				for(int j = 0; j < threads_per_warp; j++) barrier_mask[j] = 0;

				for(std::vector<int>::iterator pth = tmp_is.path.begin(); pth != tmp_is.path.end(); pth++){
					int tmp_mask[threads_per_warp];
					convert_int_to_vectormask(pathtable[fetch_warp].actual_mask[*pth], tmp_mask, threads_per_warp);
					for(int j = 0; j < threads_per_warp; j++){
						if(tmp_mask[j] && !barrier_mask[j]){
							barrier_mask[j] = 1;
						}
					}
				}

				for (UINT i=0+offset; i<threads_per_warp+offset; i++){ //update threads - remove barriers
					if(barrier_mask[i-offset] && !threads[i].fetch_qempty){
						threads[i].ignore = false;
						threads[i].fetch_isactive = true;
						threads[i].active_last_fetch = true;
						threads[i].unset_barrier();
					}

				}				
				
			
				//return so minsp can be called again in next cycle since all barriers reached
				return -1;
			}

#ifdef DEBUGFETCH
			cout << "Mapped to ISTable entry " << ref << ", with mask = " << pathtable[fetch_warp].mask[tmp_is.path[ref]] << endl;
#endif
			//Get corresponding thread mask
			int active_mask[threads_per_warp];
			convert_int_to_vectormask(pathtable[fetch_warp].actual_mask[tmp_is.path[ref]], active_mask, threads_per_warp);
		
			//get corresponding thread entry from IS Table, i.e. thread offset for the IS table entry
			bool active_dvthread = false;
			int add = find_first_active(active_mask, threads_per_warp); 
			if(add != -1){
				entry = offset + add;

				//update active threads in the mask based on any structural obstructions or barriers
				int ref1 = -1;
				for (UINT i=0+offset; i<threads_per_warp+offset; i++){
					if(active_mask[i-offset]){
						if(threads[i].fetch_qempty){
							continue;
						}
						if(threads[i].fetch_pc == threads[entry].fetch_pc){ //check other threads in the warp (and compare to first thread)
							_stat_active_thread_count[current_warp]++;
						}
						//threads[i].fetch_isactive = threads[i].fetch_pc == threads[entry].fetch_pc  && !threads[i].ignore && !threads[i].fetch_qempty; 
						//&& !threads[i].iq_full && !threads[i].fetch_stall;
						if(threads[i].fetch_isactive){ //if this thread in the warp is active, then set as active within the warp
							active[i] = true;
							ref1 = i; //active thread's index
							active_dvthread = true;
			
						}else if(threads[i].fetch_pc == threads[entry].fetch_pc){ //PC matches BUT has a structural or barrier obstruction exists, cannot run
							if(threads[i].iq_full){
								_stat_fetch_qfull[current_warp]++;
							}
							if(threads[i].fetch_stall){
								_stat_fetch_stall[current_warp]++;
							}
							if(threads[i].ignore){
								_stat_ignore[current_warp]++;
							}
							if( threads[i].fetch_qempty){
								_stat_fetch_qempty[current_warp]++;
							}
							if(threads[i].iq_full){
								_stat_iq_full[current_warp]++;
							}
						}
					}

				}
			}
			if(!active_dvthread){ //no threads in this warp are active 
			//	print_pathtable();
			//	print_is_table();
				for(int i = 0 ;i< threads_per_warp;i++){
					if(!threads[i+offset].fetch_isactive){
						cout<<"Thread "<<i<< " is inactive "; 
					}else{
						cout<<"Thread "<<i<< " is active "; 
					}
					cout<<"fq_size "<<threads[i+offset].prefetch_instruction_queue.size()  << ", fqb_size " << threads[i+offset].prefetch_instruction_queue_buffer.size() << ", "; 
					cout << "exception = " << threads[i+offset].exception << ", empty = " <<  threads[i+offset].fetch_qempty  << ", stall = " << threads[i+offset].fetch_stall;
					cout << ", barrier = " << threads[i+offset].ignore << ",  MY TAG = " << threads[i+offset].tag <<  endl;
					
				}

				cwid = -1;
				_stat_fetch_blocked[current_warp]++; //update stats
				warp_cycle[current_warp]++;
				num_searched++;  //Try again
				//need to search for next candidate, erase this from the tmp ISTable and look for next candidate
				std::vector<int>::iterator p = tmp_is.path.begin();
				std::vector<LONG>::iterator pcp = tmp_is.pc.begin(), sps = tmp_is.call_depth.begin();
				std::vector<int>::iterator bar = tmp_is.barrier.begin();
 				int k = 0;
				for(; p != tmp_is.path.end(), pcp != tmp_is.pc.end(), sps != tmp_is.call_depth.end(), bar != tmp_is.barrier.end(); p++, pcp++, sps++, bar++){
					if(k == ref){
						tmp_is.path.erase(p);
						tmp_is.pc.erase(pcp); tmp_is.call_depth.erase(sps); tmp_is.barrier.erase(bar);
						break;
					}
					k++;
				}
				if(!tmp_is.pc.empty())
					continue;
				else{ 
					return -1;
				}
			}

			if(!tmp_is.pc.empty()){
				is_active = true; //this warp is active (at least one thread matches criteria) 
				_stat_fetch_picked[current_warp]++;
				cwid = current_warp; warp_cycle[current_warp]++;
				current_thread = entry; 
				//find real IStable entry
				ISentry = ref;
				return fetch_warp; //return the current warp's ID
			}else{
				return -1; //no active warps at the moment
			}
		}
	
		if(!is_active){  //no warps are active, can not schedule
			return -1; //nothing to fetch for given IS table
		}

	return -1;

}

/////////////////////		NEW attempt	/////////////////////////////////////////////

int DataAnalyser::thread_schedule_ext_minsp(int &istable_number, int &ISentry, int cyc){ 
	bool is_active = false, branching_issue = false;
	int cwid = -1;
	uint offset;

	if(cyc == 0 && cold[fetch_warp] && isTable[fetch_warp].pc.empty()){
		for(int i = 0; i < num_warps; i++){
		//cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>attempting to create a new ISTAble entry for " << i << ", empty = " <<  isTable[i].pc.empty() << " cold = " << cold[i] << endl;
			current_warp = i; fetch_warp = current_warp;  istable_number = fetch_warp;
			create_IStable_entries(); 
			offset = current_warp * threads_per_warp; 
			cold[i] = false;
		}
	}


	//get current table/warp/thread statuses/////////////////////////////////
	if(cyc == 0){
		LONG min_sp = 0; 
		//check all ISTables for RR and < SP
		for(int i = 0; i < num_warps; i++){
			offset = i * threads_per_warp;
#ifdef DEBUG
			cout << "warp " << i << ", SP = " << hex16(isTable[i].sp) << ", PC = " << hex16(isTable[i].prog_counter) <<", last scheduled " << hex16(isTable[i].last_scheduled) << endl;
#endif
			if((isTable[i].sp < min_sp || (isTable[i].sp == min_sp && isTable[i].prog_counter < isTable[current_warp].prog_counter)) && !check_empty(offset)){
				min_sp = isTable[i].sp;
				current_warp = i;
			}
			if((isTable[i].last_scheduled + timeout_cycle) <= number_cycles && !check_empty(offset)){ //round robin kicks in. Schedule this
				//cout << hex16(isTable[i].last_scheduled) << "+" << timeout_cycle << " >= " << number_cycles << endl;
				current_warp = i; 
				break;
			}
		}
#ifdef DEBUG
		cout << "Scheduled Warp " << current_warp << endl;
#endif
		fetch_warp = current_warp;
		istable_number = fetch_warp;
		offset = current_warp * threads_per_warp; //thread index
		isTable[current_warp].last_scheduled = number_cycles; //incase leads to unschedulable


	}else{
		//use previous warp number. recalc offset
		offset = current_warp * threads_per_warp;
	}
	
	//Init conditions - if no IStable entry exists, create its entries based on threads[] of the given warp
	if(isTable[fetch_warp].pc.empty() && cold[fetch_warp]){
		create_IStable_entries(); 
		current_warp = fetch_warp; 
		istable_number = fetch_warp;
		offset = current_warp * threads_per_warp; 
		cold[fetch_warp] = false;
		isTable[current_warp].last_scheduled = number_cycles; //incase leads to unschedulable

	}else if(isTable[fetch_warp].pc.empty()){ //no more threads/paths to schedule. Finish

		return -1;
	} 

	//check for empty threads
	for (UINT i=0+offset; i<threads_per_warp+offset; i++){
		if(threads[i].fetch_qempty){
			return -1;
		}
	}

	//check for any branching PST issues. Will need to fetch from this until resolved at execute
	for (UINT i=0+offset; i<threads_per_warp+offset; i++){
		if(threads[i].branch_stall){
			branching_issue = true;
		}
	}

	if(number_cycles == dviq_cycle){ //heuristic already scheduled warp, check if warp should fetch or stall)
		if(threads[current_thread].fetch_qempty){ //cant fetch the thread
			return -1;
		}
		if(!fetch_started){
			if (!threads[current_thread].iq_full){ //double check IQ is not full (1st time in loop)
				fetch_started = true;
			}
		}
		if(fetch_started){
			int ref1 = -1;
			bool dvthread_isactive = false;
			offset = current_warp * threads_per_warp;
	
			for (UINT i=0+offset; i<threads_per_warp+offset; i++){ //double check each thread in the warp
				//threads[i].fetch_isactive = threads[i].fetch_pc == threads[current_thread].fetch_pc && !threads[i].ignore && !threads[i].fetch_qempty && !threads[i].fetch_stall; 
				
				if(threads[i].fetch_isactive){ 
					active[i] = true;
					ref1 = i;
					dvthread_isactive = true;
				}
			}
			if(dvthread_isactive){ //if given thread is active in warp
				if(threads[current_thread].fetch_stall){
					current_thread = ref1;
				}
				return current_warp;
			}else{ //inactive	
				return -1;
			}
		}
	

	}else{ //1st time in the fetch heuristic for a new clock cycle's fetch
		dviq_cycle = number_cycles;
	}
	fetch_started = false;


	//round robin between processor warps
	//minsp-pc between IS Table warps
	int num_searched = 0; //, stream;
	int ref, entry = 0;
	bool finish = true; 

	for (UINT i=0+offset; i<threads_per_warp+offset; i++){ //update threads in the warp (see if finished)
		if(!threads[i].fetch_qempty){
			finish = false;
		}			
	}
		
	if(finish){ //if a fetch_q is empty, stop simulation for that warp ---temporary. 
		for (UINT i=0+offset; i<threads_per_warp+offset; i++){ 
			threads[i].finish_cycle = number_cycles; 
			threads[i].set_finish_cycle = true;	
		}
		isFinished(); 
		return -1; //if all warps are finished	
	}


	////////	Reconvergence detection ///////////////
	int tag1, tag2;
	
	if(detect_reconvergence(tag1, tag2) && no_pending_exceptions(tag1, tag2, offset) && !branching_issue){ 
		int index = 0; 
		ref = -1;

		std::vector<LONG>::iterator pc = isTable[fetch_warp].pc.begin(), sp = isTable[fetch_warp].call_depth.begin(); 
		std::vector<int>::iterator bar = isTable[fetch_warp].barrier.begin();
		for(; pc != isTable[fetch_warp].pc.end(), sp != isTable[fetch_warp].call_depth.end(), bar != isTable[fetch_warp].barrier.end(); 
											pc++, sp++, bar++){
			if(ref == -1 || (!(*bar) && (*sp < isTable[fetch_warp].call_depth[ref] || 
				(*sp == isTable[fetch_warp].call_depth[ref] && *pc < isTable[fetch_warp].pc[ref])))){
					ref = index; //get index of thread with lowest fetch SP ---OR--- same SP and lowest fetch PC
			}						
			index++;
		}	
	
		//Get corresponding thread mask
		int active_mask[threads_per_warp];
		convert_int_to_vectormask(pathtable[fetch_warp].mask[isTable[fetch_warp].path[ref]], active_mask, threads_per_warp);
#ifdef DEBUGFETCH
		cout << "Detected [CONV] between path " << tag1 << " and path " << tag2 << endl;
#endif
		update_structures_reconverge(active_mask, offset, 1, tag1, tag2, fetch_warp);
#ifdef DEBUGFETCH
		print_pathtable(fetch_warp);
#endif
		statistics.convergence_count++;
	} /////////////////////////////////////
		
		//copy isTable to a temporary variable
		IS_Table tmp_is;
		copy_IS_table(tmp_is, fetch_warp);
	
		while((num_searched < isTable[fetch_warp].pc.size()) || !tmp_is.pc.empty()){
			ref = -1;
			//find warp candidate in given IStable -- update the warp's status (MinSP-PC)
			//i.e. Go through the IS Table and check for lowest SP and PC 
			int index = 0, barrier_count = 0; 
		

			std::vector<LONG>::iterator pc = tmp_is.pc.begin(), sp = tmp_is.call_depth.begin(); 
			std::vector<int>::iterator bar = tmp_is.barrier.begin();

			for(; pc != tmp_is.pc.end(), sp != tmp_is.call_depth.end(), bar != tmp_is.barrier.end(); pc++, sp++, bar++){
				if(ref == -1 && !(*bar)){	
					ref = index;
				}else if(*bar){
					index++; barrier_count++; continue;
				}else if(*sp < tmp_is.call_depth[ref] || (*sp == tmp_is.call_depth[ref] && *pc < tmp_is.pc[ref])){
					ref = index; //get index of thread with lowest fetch SP ---OR--- same SP and lowest fetch PC
				}						
				index++;
			}	
		
			/////////////////////////// 	Barrier Detection 	///////////////////////////////////////////////////
			if(barrier_count == index){ //all paths have reached the barrier. Remove and continue their execution
				for(std::vector<int>::iterator bar = tmp_is.barrier.begin(); bar != tmp_is.barrier.end(); bar++){
					(*bar) = 0;
				}
				for(std::vector<int>::iterator bar = isTable[fetch_warp].barrier.begin(); bar != isTable[fetch_warp].barrier.end(); bar++){
					(*bar) = 0;
				}

				cout << "BARRIERS RECONVERGE!!!" << endl; 
				//create a mask for all active threads of collective warps, i.e. takes into consideration finished prefetchQs
				int barrier_mask[threads_per_warp];

				for(int j = 0; j < threads_per_warp; j++) barrier_mask[j] = 0;

				for(std::vector<int>::iterator pth = tmp_is.path.begin(); pth != tmp_is.path.end(); pth++){
					int tmp_mask[threads_per_warp];
					convert_int_to_vectormask(pathtable[fetch_warp].actual_mask[*pth], tmp_mask, threads_per_warp);
					for(int j = 0; j < threads_per_warp; j++){
						if(tmp_mask[j] && !barrier_mask[j]){
							barrier_mask[j] = 1;
						}
					}
				}

				for (UINT i=0+offset; i<threads_per_warp+offset; i++){ //update threads - remove barriers
					if(barrier_mask[i-offset] && !threads[i].fetch_qempty){
						threads[i].ignore = false;
						threads[i].fetch_isactive = true;
						threads[i].active_last_fetch = true;
						threads[i].unset_barrier();
					}

				}				
				
			
				//return so minsp can be called again in next cycle since all barriers reached
				return -1;
			}

#ifdef DEBUGFETCH
			cout << "Mapped to ISTable entry " << ref << ", with mask = " << pathtable[fetch_warp].mask[tmp_is.path[ref]] << endl;
#endif
			//Get corresponding thread mask
			int active_mask[threads_per_warp];
			convert_int_to_vectormask(pathtable[fetch_warp].actual_mask[tmp_is.path[ref]], active_mask, threads_per_warp);
		
			//get corresponding thread entry from IS Table, i.e. thread offset for the IS table entry
			bool active_dvthread = false;
			int add = find_first_active(active_mask, threads_per_warp); 
			if(add != -1){
				entry = offset + add;

				//update active threads in the mask based on any structural obstructions or barriers
				int ref1 = -1;
				for (UINT i=0+offset; i<threads_per_warp+offset; i++){
					if(active_mask[i-offset]){
						if(threads[i].fetch_qempty){
							continue;
						}
						if(threads[i].fetch_pc == threads[entry].fetch_pc){ //check other threads in the warp (and compare to first thread)
							_stat_active_thread_count[current_warp]++;
						}
						//threads[i].fetch_isactive = threads[i].fetch_pc == threads[entry].fetch_pc  && !threads[i].ignore && !threads[i].fetch_qempty; 
						//&& !threads[i].iq_full && !threads[i].fetch_stall;
						if(threads[i].fetch_isactive){ //if this thread in the warp is active, then set as active within the warp
							active[i] = true;
							ref1 = i; //active thread's index
							active_dvthread = true;
			
						}else if(threads[i].fetch_pc == threads[entry].fetch_pc){ //PC matches BUT has a structural or barrier obstruction exists, cannot run
							if(threads[i].iq_full){
								_stat_fetch_qfull[current_warp]++;
							}
							if(threads[i].fetch_stall){
								_stat_fetch_stall[current_warp]++;
							}
							if(threads[i].ignore){
								_stat_ignore[current_warp]++;
							}
							if( threads[i].fetch_qempty){
								_stat_fetch_qempty[current_warp]++;
							}
							if(threads[i].iq_full){
								_stat_iq_full[current_warp]++;
							}
						}
					}

				}
			}
			if(!active_dvthread){ //no threads in this warp are active 
			//	print_pathtable();
			//	print_is_table();
				for(int i = 0 ;i< threads_per_warp;i++){
					if(!threads[i+offset].fetch_isactive){
						cout<<"Thread "<<i<< " is inactive "; 
					}else{
						cout<<"Thread "<<i<< " is active "; 
					}
					cout<<"fq_size "<<threads[i+offset].prefetch_instruction_queue.size()  << ", fqb_size " << threads[i+offset].prefetch_instruction_queue_buffer.size() << ", "; 
					cout << "exception = " << threads[i+offset].exception << ", empty = " <<  threads[i+offset].fetch_qempty  << ", stall = " << threads[i+offset].fetch_stall;
					cout << ", barrier = " << threads[i+offset].ignore << ",  MY TAG = " << threads[i+offset].tag <<  endl;
					
				}

				cwid = -1;
				_stat_fetch_blocked[current_warp]++; //update stats
				warp_cycle[current_warp]++;
				num_searched++;  //Try again
				//need to search for next candidate, erase this from the tmp ISTable and look for next candidate
				std::vector<int>::iterator p = tmp_is.path.begin();
				std::vector<LONG>::iterator pcp = tmp_is.pc.begin(), sps = tmp_is.call_depth.begin();
				std::vector<int>::iterator bar = tmp_is.barrier.begin();
 				int k = 0;
				for(; p != tmp_is.path.end(), pcp != tmp_is.pc.end(), sps != tmp_is.call_depth.end(), bar != tmp_is.barrier.end(); p++, pcp++, sps++, bar++){
					if(k == ref){
						tmp_is.path.erase(p);
						tmp_is.pc.erase(pcp); tmp_is.call_depth.erase(sps); tmp_is.barrier.erase(bar);
						break;
					}
					k++;
				}
				if(!tmp_is.pc.empty())
					continue;
				else{ 
					return -1;
				}
			}

			if(!tmp_is.pc.empty()){
				is_active = true; //this warp is active (at least one thread matches criteria) 
				_stat_fetch_picked[current_warp]++;
				cwid = current_warp; warp_cycle[current_warp]++;
				current_thread = entry; 
				//find real IStable entry
				ISentry = ref;
				return fetch_warp; //return the current warp's ID
			}else{
				return -1; //no active warps at the moment
			}
		}
	
		if(!is_active){  //no warps are active, can not schedule
			return -1; //nothing to fetch for given IS table
		}

	return -1;

}


int DataAnalyser::adjust_empty_warpThreads(int current_thread, int current_warp){ //possible bugs = pathtable.mask vs pathtable.actual_mask, dv_commit .exception if condition
	//lookup current mask
#ifdef DEBUGFETCH
	cout << " ---- empty ---- thread = " << current_thread%threads_per_warp << endl;
#endif
	int mask = pathtable[current_warp].mask[threads[current_thread].tag];
	int active_mask[threads_per_warp];
	convert_int_to_vectormask(mask, active_mask, threads_per_warp);

	active_mask[current_thread%threads_per_warp] = 0;
	int push_mask = ret_mask(active_mask, threads_per_warp);
	if(push_mask == 0) return -1; //else continue, still work left by other threads 

	int tmp_offset = current_warp * threads_per_warp;

	for(int i = 0; i < threads_per_warp; i++){
		if(active_mask[i]){
			current_thread = tmp_offset+i;
			break;
		}

	}

	//update pathtable 
	pathtable[current_warp].mask[threads[current_thread].tag] = push_mask;
	pathtable[current_warp].actual_mask[threads[current_thread].tag] = push_mask;

	//and IS table, along with effective PC and SP and new mask
	if(isTable[current_warp].path.empty()) return -1;
	
	return current_thread;
}

bool DataAnalyser::check_empty(int offset){
	for(int i = 0+offset; i < threads_per_warp+offset; i++){
		if(!threads[i].fetch_qempty){ //threads[i].prefetch_instruction_queue.empty() && threads[i].prefetch_instruction_queue_buffer.empty()){
#ifdef DEBUGFETCH
	cout << "\tThread empty = " << i << endl;
#endif
			current_thread = i;
			return false;
		}
	} 
	return true;
}


bool DataAnalyser::active_threads(int tag, int warp, int offset){
	int mask = pathtable[warp].mask[tag]; //actual_mask?
	int active_mask[threads_per_warp];
	convert_int_to_vectormask(mask, active_mask, threads_per_warp);

	for(int i = 0; i < threads_per_warp; i++){
		if(active_mask[i]){
			if(!threads[i+offset].fetch_qempty){//threads[i+offset].prefetch_instruction_queue.empty() || !threads[i+offset].prefetch_instruction_queue_buffer.empty()){
				return true;

			}
		}
	}
	
	return false;
}

int DataAnalyser::thread_schedule_minsp_rr(int &istable_number, int &ISentry, int cyc){ 
	bool is_active = false, branching_issue = false;
	int cwid = -1;
	uint offset;

	//get current table/warp/thread statuses/////////////////////////////////
	if(cyc == 0){
		//get next ISTable (RR) = outer warp index
		current_warp = (istable_number+1)%num_warps;  
		int count_warps = 0;
		while(true){
			offset = current_warp * threads_per_warp; //thread index
			if(!check_empty(offset)){
#ifdef DEBUGFETCH
			cout << "scheduling WARP " << current_warp << endl;
#endif
				break;

			}else{
				count_warps++;
				if(count_warps == num_warps){
#ifdef DEBUGFETCH
				cout << "can't find a warp" << endl;
#endif
					 return -1; //out of thread schedule. None available
				}
				current_warp = (current_warp+1)%num_warps; 
			}
		}
		offset = current_warp * threads_per_warp; 
		fetch_warp = current_warp;
		istable_number = fetch_warp;
		
	}else{
		//use previous warp number. recalc offset
		offset = current_warp * threads_per_warp;
	}
	
	//Init conditions - if no IStable entry exists, create its entries based on threads[] of the given warp
	if(isTable[fetch_warp].pc.empty() && cold[fetch_warp]){
		create_IStable_entries(); 
		current_warp = fetch_warp; 
		istable_number = fetch_warp;
		offset = current_warp * threads_per_warp; 
		cold[fetch_warp] = false;

	}else if(isTable[fetch_warp].pc.empty()){ //no more threads/paths to schedule. Finish
#ifdef DEBUGFETCH
		cout << "ISTable " << current_warp << " .empty()" << endl;
#endif

		return -1;
	} 

	//check for empty threads
	//for (UINT i=0+offset; i<threads_per_warp+offset; i++){
		if(check_empty(offset)){
#ifdef DEBUGFETCH
		cout << "WARP " << current_warp << " is empty -- exiting" << endl;
#endif
			return -1;
		}
	//}
	

	//check for any branching PST issues. Will need to fetch from this until resolved at execute
	for (UINT i=0+offset; i<threads_per_warp+offset; i++){
		if(threads[i].branch_stall){
			branching_issue = true;
		}
	}

	if(current_thread == -1){
#ifdef DEBUGFETCH
	cout << "can't find a current thread?" << endl;
#endif

		 return -1;
	}

	if(number_cycles == dviq_cycle){ //heuristic already scheduled warp, check if warp should fetch or stall)
		if(threads[current_thread].fetch_qempty){ //cant fetch the thread
			return -1;
		}
		if(!fetch_started){
			if (!threads[current_thread].iq_full){ //double check IQ is not full (1st time in loop)
				fetch_started = true;
			}
		}
		if(fetch_started){
			int ref1 = -1;
			bool dvthread_isactive = false;
			offset = current_warp * threads_per_warp;
	
			for (UINT i=0+offset; i<threads_per_warp+offset; i++){ //double check each thread in the warp
				//threads[i].fetch_isactive = threads[i].fetch_pc == threads[current_thread].fetch_pc && !threads[i].ignore && !threads[i].fetch_qempty && !threads[i].fetch_stall; 
				
				if(threads[i].fetch_isactive){ 
					active[i] = true;
					ref1 = i;
					dvthread_isactive = true;
				}
			}
			if(dvthread_isactive){ //if given thread is active in warp
				if(threads[current_thread].fetch_stall){
					current_thread = ref1;
				}
				return current_warp;
			}else{ //inactive	
				return -1;
			}
		}
	

	}else{ //1st time in the fetch heuristic for a new clock cycle's fetch
		dviq_cycle = number_cycles;
	}
	fetch_started = false;


	//round robin between processor warps
	//minsp-pc/RR between IS Table warps
	int num_searched = 0; //, stream;
	int ref, entry = 0;
	bool finish = true; 

	for (UINT i=0+offset; i<threads_per_warp+offset; i++){ //update threads in the warp (see if finished)
		if(!threads[i].fetch_qempty){
			finish = false;
		}			
	}
		
	if(finish){ //if a fetch_q is empty, stop simulation for that warp ---temporary. 
		for (UINT i=0+offset; i<threads_per_warp+offset; i++){ 
			threads[i].finish_cycle = number_cycles; 
			threads[i].set_finish_cycle = true;	
		}
		isFinished(); 
		return -1; //if all warps are finished	
	}


	////////	Reconvergence detection ///////////////
	int tag1, tag2;
	
	if(detect_reconvergence(tag1, tag2) && no_pending_exceptions(tag1, tag2, offset) && !branching_issue){ 
		int index = 0; 
		ref = -1;

		std::vector<LONG>::iterator pc = isTable[fetch_warp].pc.begin(), sp = isTable[fetch_warp].call_depth.begin(); 
		std::vector<int>::iterator bar = isTable[fetch_warp].barrier.begin();
		for(; pc != isTable[fetch_warp].pc.end(), sp != isTable[fetch_warp].call_depth.end(), bar != isTable[fetch_warp].barrier.end(); 
											pc++, sp++, bar++){
			if(ref == -1 || (!(*bar) && (*sp < isTable[fetch_warp].call_depth[ref] || 
				(*sp == isTable[fetch_warp].call_depth[ref] && *pc < isTable[fetch_warp].pc[ref])))){
					ref = index; //get index of thread with lowest fetch SP ---OR--- same SP and lowest fetch PC
			}						
			index++;
		}	
	
		//Get corresponding thread mask
		int active_mask[threads_per_warp];
		convert_int_to_vectormask(pathtable[fetch_warp].mask[isTable[fetch_warp].path[ref]], active_mask, threads_per_warp);
#ifdef DEBUGFETCH
		cout << "Detected [CONV] between path " << tag1 << " and path " << tag2 << endl;
#endif
		update_structures_reconverge(active_mask, offset, 1, tag1, tag2, fetch_warp);
#ifdef DEBUGFETCH
		print_pathtable(fetch_warp);
#endif
		statistics.convergence_count++;
	} /////////////////////////////////////
		
		//copy isTable to a temporary variable
		IS_Table tmp_is;
		copy_IS_table(tmp_is, fetch_warp);
	

		/////////////////////////// 	Barrier Detection 	///////////////////////////////////////////////////
		int barrier_count = 0;
		for(std::vector<int>::iterator bar = isTable[fetch_warp].barrier.begin(); bar != isTable[fetch_warp].barrier.end(); bar++){
			if((*bar)){
				barrier_count++;
			}		
		}
		
		if(barrier_count == isTable[fetch_warp].barrier.size()){ //index){ //all paths have reached the barrier. Remove and continue their execution
			for(std::vector<int>::iterator bar = tmp_is.barrier.begin(); bar != tmp_is.barrier.end(); bar++){
				(*bar) = 0;
			}
			for(std::vector<int>::iterator bar = isTable[fetch_warp].barrier.begin(); bar != isTable[fetch_warp].barrier.end(); bar++){
				(*bar) = 0;
			}

			cout << "BARRIERS RECONVERGE!!!" << endl; 
			//create a mask for all active threads of collective warps, i.e. takes into consideration finished prefetchQs
			int barrier_mask[threads_per_warp];

			for(int j = 0; j < threads_per_warp; j++) barrier_mask[j] = 0;

			for(std::vector<int>::iterator pth = tmp_is.path.begin(); pth != tmp_is.path.end(); pth++){
				int tmp_mask[threads_per_warp];
				convert_int_to_vectormask(pathtable[fetch_warp].actual_mask[*pth], tmp_mask, threads_per_warp);
				for(int j = 0; j < threads_per_warp; j++){
					if(tmp_mask[j] && !barrier_mask[j]){
						barrier_mask[j] = 1;
					}
				}
			}

			for (UINT i=0+offset; i<threads_per_warp+offset; i++){ //update threads - remove barriers
				if(barrier_mask[i-offset] && !threads[i].fetch_qempty){
					threads[i].ignore = false;
					threads[i].fetch_isactive = true;
					threads[i].active_last_fetch = true;
					threads[i].unset_barrier();
				}
			}				
		
			//return so minsp can be called again in next cycle since all barriers reached
			return -1;
		}

		//num_searched < isTable[fetch_warp].pc.size())
		while((num_searched < tmp_is.pc.size()) && !tmp_is.pc.empty()){
			ref = -1;
			//find warp candidate in given IStable -- update the warp's status (MinSP-PC)
			//i.e. Go through the IS Table and check for lowest SP and PC 
			int index = 0; 
		

			std::vector<LONG>::iterator pc = tmp_is.pc.begin(), sp = tmp_is.call_depth.begin(); 
			std::vector<int>::iterator bar = tmp_is.barrier.begin(); std::vector<int>::iterator path = tmp_is.path.begin();
			std::vector<LONG>::iterator time = tmp_is.time_stamp.begin();
			LONG Tmin = 0;

			for(; pc != tmp_is.pc.end(), sp != tmp_is.call_depth.end(), bar != tmp_is.barrier.end(), time != tmp_is.time_stamp.end(), path != tmp_is.path.end(); 
			pc++, sp++, bar++, time++, path++){
#ifdef DEBUGFETCH
			cout << "[index]: " << index << ", time: "  << *time << " Timeout: " << timeout_cycle << ", number_cycles = " << number_cycles << endl;
#endif
				if(ref == -1 && !(*bar) && active_threads(*path,fetch_warp,offset)){	
					ref = index;
					Tmin = *time;
				}else if(*bar){
					//index++; continue; barrier_count++;
				}else if((Tmin > *time) && ((*time + timeout_cycle) < number_cycles) && active_threads(*path,fetch_warp,offset)){ //round robin kicks in. Schedule this
					ref = index; //break;
					Tmin = *time;
				}else if((*sp < tmp_is.call_depth[ref] || (*sp == tmp_is.call_depth[ref] && *pc < tmp_is.pc[ref])) && active_threads(*path,fetch_warp,offset)){
					if(Tmin > *time){ 
						ref = index; //get index of thread with lowest fetch SP ---OR--- same SP and lowest fetch PC, considering timeout RR
						Tmin = *time;
					}
				}						
				index++;
			}	
				
			if(ref == -1){
#ifdef DEBUGFETCH
			cout << "Can not find a path in warp" << endl;
			print_is_table(fetch_warp);
#endif

			 return -1;
			}
		

#ifdef DEBUGFETCH
			cout << "Mapped to ISTable entry " << ref << ", with mask = " << pathtable[fetch_warp].mask[tmp_is.path[ref]] << endl;
#endif
			//Get corresponding thread mask
			int active_mask[threads_per_warp];
			convert_int_to_vectormask(pathtable[fetch_warp].actual_mask[tmp_is.path[ref]], active_mask, threads_per_warp);
		
			//get corresponding thread entry from IS Table, i.e. thread offset for the IS table entry
			bool active_dvthread = false;
			int add = find_first_active(active_mask, threads_per_warp); 
			if(add != -1){
				entry = offset + add;

				//update active threads in the mask based on any structural obstructions or barriers
				int ref1 = -1;
				for (UINT i=0+offset; i<threads_per_warp+offset; i++){
					if(active_mask[i-offset]){
						if(threads[i].fetch_qempty){
							continue;
						}
						if(threads[i].fetch_pc == threads[entry].fetch_pc){ //check other threads in the warp (and compare to first thread)
							_stat_active_thread_count[current_warp]++;
						}
						//threads[i].fetch_isactive = threads[i].fetch_pc == threads[entry].fetch_pc  && !threads[i].ignore && !threads[i].fetch_qempty; 
						//&& !threads[i].iq_full && !threads[i].fetch_stall;
						if(threads[i].fetch_isactive){ //if this thread in the warp is active, then set as active within the warp
							active[i] = true;
							ref1 = i; //active thread's index
							active_dvthread = true;
			
						}else if(threads[i].fetch_pc == threads[entry].fetch_pc){ //PC matches BUT has a structural or barrier obstruction exists, cannot run
							if(threads[i].iq_full){
								_stat_fetch_qfull[current_warp]++;
							}
							if(threads[i].fetch_stall){
								_stat_fetch_stall[current_warp]++;
							}
							if(threads[i].ignore){
								_stat_ignore[current_warp]++;
							}
							if( threads[i].fetch_qempty){
								_stat_fetch_qempty[current_warp]++;
							}
							if(threads[i].iq_full){
								_stat_iq_full[current_warp]++;
							}
						}
					}

				}
			}
			if(!active_dvthread){ //no threads in this warp are active 
			//	print_pathtable();
			//	print_is_table();
				for(int i = 0 ;i< threads_per_warp;i++){
					if(!threads[i+offset].fetch_isactive){
						cout<<"Thread "<<i<< " is inactive "; 
					}else{
						cout<<"Thread "<<i<< " is active "; 
					}
					cout<<"fq_size "<<threads[i+offset].prefetch_instruction_queue.size()  << ", fqb_size " << threads[i+offset].prefetch_instruction_queue_buffer.size() << ", "; 
					cout << "exception = " << threads[i+offset].exception << ", empty = " <<  threads[i+offset].fetch_qempty  << ", stall = " << threads[i+offset].fetch_stall;
					cout << ", barrier = " << threads[i+offset].ignore << ",  MY TAG = " << threads[i+offset].tag <<  endl;
					
				}

				cwid = -1;
				_stat_fetch_blocked[current_warp]++; //update stats
				warp_cycle[current_warp]++;
				num_searched++;  //Try again
				//need to search for next candidate, erase this from the tmp ISTable and look for next candidate
				std::vector<int>::iterator p = tmp_is.path.begin();
				std::vector<LONG>::iterator pcp = tmp_is.pc.begin(), sps = tmp_is.call_depth.begin();
				std::vector<LONG>::iterator tt = tmp_is.time_stamp.begin();
				std::vector<int>::iterator bar = tmp_is.barrier.begin();
 				int k = 0;
				for(; p != tmp_is.path.end(), pcp != tmp_is.pc.end(), sps != tmp_is.call_depth.end(), bar != tmp_is.barrier.end(), tt != tmp_is.time_stamp.end(); 
						p++, pcp++, sps++, bar++, tt++){
					if(k == ref){
						tmp_is.path.erase(p);
						tmp_is.pc.erase(pcp); tmp_is.call_depth.erase(sps); tmp_is.barrier.erase(bar);
						tmp_is.time_stamp.erase(tt);
						break;
					}
					k++;
				}
				if(!tmp_is.pc.empty())
					continue;
				else{ 
					return -1;
				}
			}

			if(!tmp_is.pc.empty()){
				is_active = true; //this warp is active (at least one thread matches criteria) 
				_stat_fetch_picked[current_warp]++;
				cwid = current_warp; warp_cycle[current_warp]++;
				current_thread = entry; 
				//find real IStable entry
				ISentry = ref;
				return fetch_warp; //return the current warp's ID
			}else{
				return -1; //no active warps at the moment
			}
		}//end while
	
		if(!is_active){  //no warps are active, can not schedule
			return -1; //nothing to fetch for given IS table
		}

	return -1;

}


int DataAnalyser::check_barriers(int &barrier_count, int offset){
	int fini=0;
	for(int k = 0 ; k < num_threads; k++){
		if(threads[k+offset].finished){
			fini++;
		}
		if (threads[k+offset].prefetch_instruction_queue_buffer.front().barrier){
			barrier_count++;
		}
	}
	
	if((UINT)barrier_count == ((UINT)num_threads - (UINT)fini)){
		cout<<"UNSET = Threads leaving the barrier "<<endl;
		exit(1);

		for(int k = 0+offset ; k < num_threads+offset; k++){
			//if(threads[k].barrier
			threads[k].ignore = false;
			if(!threads[k].fetch_qempty) threads[k].fetch_isactive = true;
			threads[k].unset_barrier();
		}
	}
	return fini;

}


void DataAnalyser::erase_thread_from_path(int tno, int offset, int warp){
#ifdef DEBUGFETCH
	cout << "erasing Thread " << tno+offset <<", or warp thread " << tno << " from warp " << warp << endl;
#endif
	int actual_mask[threads_per_warp], mask[threads_per_warp];
	//convert_int_to_vectormask(pathThread, subtract, threads_per_warp);
	convert_int_to_vectormask(pathtable[warp].actual_mask[threads[tno+offset].tag], actual_mask, threads_per_warp);
	convert_int_to_vectormask(pathtable[warp].mask[threads[tno+offset].tag], mask, threads_per_warp);
	//tno+offset
	actual_mask[tno] = 0;
	mask[tno] = 0;


#ifdef DEBUGFETCH
	cout << "[[NEW ACTUAL MASK]] " << get_int_mask(actual_mask, threads_per_warp) <<", {{MASK}} " << get_int_mask(mask, threads_per_warp) << endl;
#endif

	//change pathtable mask	and Istable if necessary
	pathtable[warp].actual_mask[threads[tno+offset].tag] = get_int_mask(actual_mask, threads_per_warp);
	pathtable[warp].mask[threads[tno+offset].tag] = get_int_mask(mask, threads_per_warp);; //???

}

/**************************************************************************
// 	check for any inactive threads in the warp or barriers
// 	and determine if the thread/warp should be fetched or not
**************************************************************************/
int DataAnalyser::to_fetch_or_not_to_fetch(int offset, bool &fetch, int x_smask[], int tag, int path_mask[]){
	for(UINT i=0+offset ; i < threads_per_warp + offset; i++){ //for every thread in the given warp
		
		if(threads[i].prefetch_instruction_queue.empty() && threads[i].prefetch_instruction_queue_buffer.empty()) { //check if prefetch Q is empty ( = thread innactive)
			x_smask[i-offset] = false; //cout << "mask[" << i%threads_per_warp << "] inactive bc PrefetchQ empty" << endl;
			/*threads[i].fetch_qempty = true;
			threads[i].fetch_isactive = false;
			threads[i].active_last_fetch = false;
			threads[i].finished = true;*/

			bool all_mt = true;
			for(UINT j= 0+offset;j<threads_per_warp + offset;j++){
				if(!threads[j].fetch_qempty){ //and check if the other threads are empty !threads[j].prefetch_instruction_queue.empty() && !threads[i].prefetch_instruction_queue_buffer.empty() && 
					all_mt = false; 
				}
			}

			if(all_mt){
				/*for(UINT j= 0+offset;j<threads_per_warp + offset;j++){ //if all the threads of the warp are empty, set their status to false too
					threads[j].fetch_isactive = false;
					threads[j].active_last_fetch = false; threads[j].fetch_qempty = true;
					threads[j].finished = true; //threads[j].empty = true;
#ifdef DEBUGFETCH
	cout << "THREAD " << i+offset << " marked as finished" << endl;
#endif
					erase_thread_from_path(j, offset);
				}*/	
				return -1;
			}
			//set the rest of the variables accordingly
			
			
			continue;
		}/*else{
			threads[i].fetch_qempty = false;
		}*/
		//if(threads[i].prefetch_instruction_queue.empty() && threads[i].prefetch_instruction_queue_buffer.empty()) return -1;


		//deal with any potential barriers experienced by the thread
		if unlikely(threads[i].prefetch_instruction_queue_buffer.front().barrier && path_mask[i-offset] && !threads[i].fetch_qempty && !threads[i].ignore){
			cout << "FOUND a BARRIER" << endl;	
			for(UINT i=0+offset ; i < threads_per_warp + offset; i++){ //All threads in active path are also barriers (Deal with trace bug)
				if(path_mask[i-offset]){
					if(!threads[i].prefetch_instruction_queue_buffer.front().barrier){
						//cout << "Thread " << i << " didn't actually hit a barrier?" << endl;
						threads[i].prefetch_instruction_queue_buffer.front().barrier = true;
					}
					threads[i].ignore = true;
					threads[i].fetch_isactive = false;
					threads[i].active_last_fetch = false;
					cout<<"Thread "<<i <<" hits barrier"<<endl;
				}
			}
			//update PST entry	
			isTable[(int)(offset/threads_per_warp)].barrier[tag] = 1;
			print_is_table((int)(offset/threads_per_warp));
			return 2;
		}
					
		//else if IQ not empty or thread not experience a barrier/barrier cleared, mask may be set to '1' and may be fetched (passed by reference)
		if(path_mask[i-offset] && !threads[i].prefetch_instruction_queue_buffer.front().barrier){ 
			x_smask[i-offset] = true;
			fetch = true; threads[i].ignore = false;
			threads[i].fetch_isactive = true;
			threads[i].active_last_fetch = true;
		}else{
			x_smask[i-offset] = false; 
		}
		
	} 
	if(isTable[(int)(offset/threads_per_warp)].pc.empty()){ 
		return -2;
	}
	return 0;
}

/*********************************************************
Helper function for Fetch():
	Keeps a tally of the dv-instruction count based
	on the type of instruction fetched
***********************************************************/
void DataAnalyser::update_inst_count(int &inst_count, int x_smask[]){

	if(!conf.base && threads[current_thread].prefetch_instruction_queue_buffer.front().is_simd){
		if(threads[current_thread].prefetch_instruction_queue_buffer.front().is_sse  && threads_per_warp > 2){
			//For SSE subsequent threads cause register bank conflict
			for(UINT k=0 ; k < threads_per_warp; k = k+2){ 
				if(x_smask[k]){
					inst_count++;
					break;
				}
			}
			for(UINT k=1 ; k < threads_per_warp; k = k+2){
				if(x_smask[k]){
					inst_count++;
					break;
				}
			}
		}else if(threads[current_thread].prefetch_instruction_queue_buffer.front().is_avx) {
			for(UINT k=0 ; k < threads_per_warp; k++){
				if(x_smask[k])
					inst_count++;				
			}
		}else
			inst_count = 1;
		
	}else
			inst_count = 1;	
}

/**************************************************************
	Stat count for Fetch, helper function for fetch()
**************************************************************/
void DataAnalyser::collect_instruction_stats(DVInst dv_temp, int i, int &combine_count, int offset){
 
	statistics.int_reg_file_read += dv_temp.insts[i].int_reg_read_count;
	statistics.int_reg_file_write += dv_temp.insts[i].int_reg_write_count;
	statistics.float_reg_file_read += dv_temp.insts[i].fp_reg_read_count;
	statistics.float_reg_file_write += dv_temp.insts[i].fp_reg_write_count;

	if(dv_temp.insts[i].is_simd){
		statistics.simd_count++;
	}
	if(dv_temp.insts[i].uop_type == SA_UOP_IDIV ||dv_temp.insts[i].uop_type == SA_UOP_FPDIV ||
		dv_temp.insts[i].uop_type == SA_UOP_AVX_FPDIVVEC || dv_temp.insts[i].uop_type == SA_UOP_SSE_FPDIVVEC ||
		dv_temp.insts[i].uop_type == SA_UOP_AVX_FPDIVSCAL || dv_temp.insts[i].uop_type == SA_UOP_SSE_FPDIVSCAL){
			statistics.div_access++;
	}
	else if(dv_temp.insts[i].uop_type == SA_UOP_IMUL ||dv_temp.insts[i].uop_type == SA_UOP_FPMUL ||
		dv_temp.insts[i].uop_type == SA_UOP_AVX_FPMULVEC || dv_temp.insts[i].uop_type == SA_UOP_SSE_FPMULVEC ||
		dv_temp.insts[i].uop_type == SA_UOP_AVX_FPMULSCAL || dv_temp.insts[i].uop_type == SA_UOP_SSE_FPMULSCAL){
			statistics.mul_access++;
	}
	
	else if(dv_temp.insts[i].is_fp){
		statistics.fpu_access++;
	}else{
		statistics.ialu_access++;
	}
				
	if(dv_temp.insts[i].load){
		int num_loads = 0;
		for(int j = 0; j < dv_temp.insts[i].rmem_num; j++){
			if(dv_temp.insts[i].rmem[j] > 0) 
				statistics.load_count[i+offset]++; 
				num_loads++;
		}
		dv_temp.load_count = num_loads;
	}
	if(dv_temp.insts[i].store){
		int num_stores = 0;
		for(int j = 0; j < dv_temp.insts[i].wmem_num; j++){
			if(dv_temp.insts[i].wmem[j] > 0)
				statistics.store_count[i+offset]++;	
				num_stores++;	
		}
		dv_temp.store_count = num_stores;
	}

	combine_count++;
	statistics.load_count_at_fetch[i+offset]+=threads[i+offset].prefetch_instruction_queue_buffer.front().rmem_num;
	statistics.store_count_at_fetch[i+offset]+=threads[i+offset].prefetch_instruction_queue_buffer.front().wmem_num;

	threads[i+offset].tracelength++;
	threads[i+offset].ins_count++;
}

/*************************************************************
	Fetch() helper function:	
	Refills the prefetch buffer if necessary	
*************************************************************/
void DataAnalyser::refill_prefetch_IQB(int i, int offset){

	pthread_mutex_lock( &threads[i+offset].iq_mutex ); 
	bool piq_empty = (threads[i+offset].prefetch_instruction_queue.size() < (uint)readahead_horizon);
	pthread_mutex_unlock( &threads[i+offset].iq_mutex );
						
	if(piq_empty && !threads[i+offset].finished){ 
		pthread_cond_wait(&threads[i+offset].prefetchq_full,&threads[i+offset].mutex_Var_full); 
	}

	pthread_mutex_lock( &threads[i+offset].iq_mutex );
	//cout<<"A "<<threads[i].prefetch_instruction_queue_buffer.size()<<" b "<<threads[i].prefetch_instruction_queue.size()<<endl;

	threads[i+offset].prefetch_instruction_queue_buffer.swap(threads[i+offset].prefetch_instruction_queue);
	pthread_cond_signal(&threads[i+offset].prefetchq_notfull);
	pthread_mutex_unlock( &threads[i+offset].iq_mutex );

#ifdef SAMPLING
	pthread_mutex_lock( &threads[i+offset].iq_mutex );
	piq_empty = (threads[i+offset].prefetch_instruction_queue_buffer.empty());
	pthread_mutex_unlock( &threads[i+offset].iq_mutex );
	if(piq_empty)
		continue;
#endif

	if(threads[i+offset].prefetch_instruction_queue_buffer.empty() && threads[i+offset].prefetch_instruction_queue.empty() ){ //set to prefetch traces into this queue (was swapped)
#ifdef DEBUGFETCH
	cout << "THREAD " << i+offset << " fetch_qempty." << endl;
#endif
		threads[i+offset].fetch_qempty = true;
		threads[i+offset].fetch_isactive = false;
		threads[i+offset].active_last_fetch = false; threads[i+offset].finished = true;
#ifdef DEBUGFETCH
	cout << ">THREAD " << i+offset << " marked as finished" << endl;
#endif
	
		erase_thread_from_path(i, offset, (int)offset/threads_per_warp);	
	}else{
		threads[i+offset].fetch_qempty = false; 
		/*if(!threads[i+offset].branch_stall && !threads[i].exception && !threads[i].ignore){
			threads[i+offset].fetch_isactive = false;
			threads[i+offset].active_last_fetch = false;
			threads[i+offset].finished = false; 
		}*/	
	}

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////											//////////////////						
/////////////////////				FETCH and associated functions				//////////////////
/////////////////////											//////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void DataAnalyser::gather_dv_stats(int combine_count, DVInst dv_temp, bool divergent, int x_smask[]){

	statistics.combine_count[combine_count]++;
	statistics.dv_inst_count++;

	if(dv_temp.has_load)
		statistics.dv_load+=dv_temp.load_count;
				
	if(dv_temp.has_store)
		statistics.dv_store+=dv_temp.store_count;
	
	if(dv_temp.is_fp){
		statistics.dv_fp_count++;
		statistics.fp_inst_count += count_inst_active(dv_temp.actual_mask);
	}else{
		statistics.dv_int_count++;
		statistics.int_inst_count += count_inst_active(dv_temp.actual_mask);
	}

	if(divergent)
		statistics.divergence_count++;

	//active thread occupance in DVInst
	int num = 0;
	for(int i = 0; i < threads_per_warp; i++){
		if(x_smask[i]){ 
			statistics.fetch_inst_count++;
			num++;
		}
	}
	if(num == 0) num = 1;
	statistics.thread_occupancy[num-1]++;

}


void DataAnalyser::update_warp_thread_tags(int offset, int tag, int x_smask[]){
	//int first = 0;
	for(int i=0 ; i < threads_per_warp; i++){			
		if(x_smask[i]){ //update all active threads of the warp with this tag
			threads[i+offset].tag = tag;
		}
	}
}


int DataAnalyser::get_last_tag(int smask[], int offset){ //<----------------
	for(int i = 0; i < threads_per_warp; i++){
		if(smask[i]){
			return 	threads[i+offset].tag;
		}
	} 
	return -1;
}

LONG DataAnalyser::return_pc(int x_smask[], int offset){
	for(int i = 0+offset; i < threads_per_warp+offset; i++){
		if(x_smask[i-offset] && !threads[i].fetch_qempty && !threads[i].ignore){
			return 	threads[i].fetch_pc;
		}
	} 
	return -1;
}

LONG DataAnalyser::return_mispr_pc(int x_smask[], int offset){
	for(int i = 0+offset; i < threads_per_warp+offset; i++){
		if(x_smask[i-offset] && !threads[i].fetch_qempty && !threads[i].ignore){
			threads[i].fetch_pc = threads[i].prefetch_instruction_queue_buffer.front().pc; //get next values
			threads[i].fetch_sp = threads[i].prefetch_instruction_queue_buffer.front().sp;			
			return 	threads[i].fetch_pc;
		}
	} 
	return -1;
}

LONG DataAnalyser::return_sp(int x_smask[], int offset){
	for(int i = offset; i < threads_per_warp + offset; i++){
		if(x_smask[i-offset] && !threads[i].fetch_qempty && !threads[i].ignore){
			return threads[i].fetch_sp;
		}
	}
	return -1;
}

/*int DataAnalyser::assign_mask_tags(int last_tag, DVInst &dv_temp){ //consult the IS table to determine the paths that will reconverge
								   //set PathT and PathNT accordingly (needed to generate c_uop)
	if(isTable.path.end() != find(isTable.path.begin(), isTable.path.end(), last_tag)){
		int found = find(isTable.path.begin(), isTable.path.end(), last_tag) - isTable.path.begin();
		dv_temp.pathT = last_tag; //T = 1st path to reconverge (re-used variables for branch instructions)
		
		LONG search_pc = isTable.pc[found];
		int iter = 0;
		for(std::vector<LONG>::iterator findpc = isTable.pc.begin(); findpc < isTable.pc.end(); findpc++){
			if(*findpc == search_pc && iter != found){
				dv_temp.pathNT = isTable.path[iter]; //NT = 2nd path to reconverge
				return 1;
			}else
				iter++;
		}		
	}
	return 0;
}*/

/*********************************************************
	Helper Functions for Fetch
	-updates to ISTable for new entry
	-create DVInsts and sedn to decode_rename buffer
**********************************************************/
void DataAnalyser::update_is_table(DVInst dv, int x_smask[], int offset, int tag, int insert, int warp_no){ //TODO deal with calls to this function
	//0 = cold case, 1 = divergent, 2 = reconvergent, 3 = eviction
	if(!insert){
		LONG pc_push;
		pc_push = return_pc(x_smask, offset);
		isTable[warp_no].pc.push_back(pc_push);
		LONG sp_push = return_sp(x_smask, offset);
		isTable[warp_no].call_depth.push_back(sp_push);
		isTable[warp_no].path.push_back(tag); 
		isTable[warp_no].barrier.push_back(0);
		pathtable[warp_no].ref_counter[tag] = 1;
		isTable[warp_no].time_stamp.push_back(number_cycles);
		//update thread tag
		update_warp_thread_tags(offset, tag, x_smask); 

	}else if(insert == 1 || insert == 2){ //find prev_tag in the istable. Replace
		int is_tag = find(isTable[warp_no].path.begin(), isTable[warp_no].path.end(), dv.pathT) - isTable[warp_no].path.begin(); //search for prev tag
#ifdef DEBUG
		cout << "searching for tag " << dv.pathT << " in the ISTable " << endl; 
#endif
		if(isTable[warp_no].path.end() != find(isTable[warp_no].path.begin(), isTable[warp_no].path.end(), dv.pathT)){
			isTable[warp_no].path[is_tag] = tag; //and replace with new info
			LONG pc_push = return_pc(x_smask, offset);
			LONG sp_push = return_sp(x_smask, offset);
			isTable[warp_no].pc[is_tag] = pc_push;
			isTable[warp_no].call_depth[is_tag] = sp_push;
		}

		//update pathtable reference counter
		if(pathtable[warp_no].ref_counter[dv.pathT] > 0) pathtable[warp_no].ref_counter[dv.pathT] -= 1; //dereferenced by IS table
		//update thread tag
		update_warp_thread_tags(offset, tag, x_smask);

	}else if(insert == 5 || insert == 6 || insert == 7){ //for branch mispredicts, make sure
			int is_tag;
			if(insert == 5 || insert == 7)
			is_tag = find(isTable[warp_no].path.begin(), isTable[warp_no].path.end(), dv.pathT) - isTable[warp_no].path.begin(); //search for prev tag
			else
			is_tag = find(isTable[warp_no].path.begin(), isTable[warp_no].path.end(), tag) - isTable[warp_no].path.begin(); //search for prev tag
#ifdef DEBUG
		cout << "searching for tag " << dv.pathT << " in the ISTable " << endl; 
#endif
			LONG pc_push;
			if(insert == 5)
			pc_push = return_mispr_pc(x_smask, offset); //fix the IStable
			else
			pc_push = return_pc(x_smask, offset); //fix the IStable

			if(insert == 7) isTable[warp_no].path[is_tag] = tag;

			LONG sp_push = return_sp(x_smask, offset);
			isTable[warp_no].pc[is_tag] = pc_push;
			isTable[warp_no].call_depth[is_tag] = sp_push;


	}else if(insert == 3 || insert == 4){ //eviction
		int is_tag = find(isTable[warp_no].path.begin(), isTable[warp_no].path.end(), tag) - isTable[warp_no].path.begin();
		if(insert == 3){ 
			if(isTable[warp_no].path.end() != find(isTable[warp_no].path.begin(), isTable[warp_no].path.end(), tag)){
				//erase the element
				//isTable[warp_no].is.erase(isTable[warp_no].is.begin() + is_tag);

				isTable[warp_no].path.erase(isTable[warp_no].path.begin() + is_tag);
				isTable[warp_no].pc.erase(isTable[warp_no].pc.begin() + is_tag);
				isTable[warp_no].call_depth.erase(isTable[warp_no].call_depth.begin() + is_tag);
				isTable[warp_no].barrier.erase(isTable[warp_no].barrier.begin() + is_tag);
				isTable[warp_no].time_stamp.erase(isTable[warp_no].time_stamp.begin() + is_tag);
			}

			//update reference counter, dereferenced by IS table
			pathtable[warp_no].ref_counter[tag] -= 1; 
		}else if(insert == 4){
			if(!isTable[warp_no].path.empty() && (isTable[warp_no].path.end() != find(isTable[warp_no].path.begin(), isTable[warp_no].path.end(), tag))){
				//erase the element
				//isTable[warp_no].is.erase(isTable[warp_no].is.begin() + is_tag);
				isTable[warp_no].path.erase(isTable[warp_no].path.begin() + is_tag);
				isTable[warp_no].pc.erase(isTable[warp_no].pc.begin() + is_tag);
				isTable[warp_no].call_depth.erase(isTable[warp_no].call_depth.begin() + is_tag);
				isTable[warp_no].barrier.erase(isTable[warp_no].barrier.begin() + is_tag);
				isTable[warp_no].time_stamp.erase(isTable[warp_no].time_stamp.begin() + is_tag);
			}
		}
	
	}

	if(insert == 2){ //if reconvergent also, must find the dv.pathNT and remove the entry from the IS table
		
		int is_tag = find(isTable[warp_no].path.begin(), isTable[warp_no].path.end(), dv.pathNT) - isTable[warp_no].path.begin();
		assert(isTable[warp_no].path.end() != find(isTable[warp_no].path.begin(), isTable[warp_no].path.end(), dv.pathNT));
		//erase the element
		//isTable[warp_no].is.erase(isTable[warp_no].is.begin() + is_tag);
		isTable[warp_no].path.erase(isTable[warp_no].path.begin() + is_tag);
		isTable[warp_no].pc.erase(isTable[warp_no].pc.begin() + is_tag);
		isTable[warp_no].call_depth.erase(isTable[warp_no].call_depth.begin() + is_tag);
		isTable[warp_no].barrier.erase(isTable[warp_no].barrier.begin() + is_tag);
		isTable[warp_no].time_stamp.erase(isTable[warp_no].time_stamp.begin() + is_tag);

		//update pathtable reference counter for this eviction
		pathtable[warp_no].ref_counter[dv.pathNT] -=1; //Evicting IS entry 
		
	}

	//reset the sp and pc of the warp for ext_minsp scheduling
	LONG max_sp = 0; int index = 0, count = 0;
	std::vector<LONG>::iterator max = isTable[warp_no].call_depth.begin();
	std::vector<LONG>::iterator pc_max = isTable[warp_no].pc.begin();

	for(; max != isTable[warp_no].call_depth.end(), pc_max != isTable[warp_no].pc.end(); max++, pc_max++){			
		if(*max > max_sp || (*max == max_sp && isTable[warp_no].pc[index] < *pc_max)){
			max_sp = *max;	
			index = count;
		}
		count++;
	}
	isTable[warp_no].sp = max_sp;
	isTable[warp_no].prog_counter = isTable[warp_no].pc[index];


#ifdef DEBUG
	print_is_table(warp_no); 
#endif
}

void DataAnalyser::print_pathtable(int warp_no){ 
	cout << endl << " ---------- Pathtable -------------- " << endl;
	cout << "Tag\t| V\t| Mask\t| RC " << endl;
	for(int i = 0; i < pathtable[warp_no].mask.size(); i++){
		cout << i << "\t" << pathtable[warp_no].rbit[i] << "\t" << pathtable[warp_no].mask[i] << "\t" << pathtable[warp_no].ref_counter[i] << endl;
	} 
}

void DataAnalyser::update_freePointer(int warp_no){ //for pathtable
	int counter = 0, entry;

	int last_free = pathtable[warp_no].free_ptr;
	entry = pathtable[warp_no].commit_ptr + 1;
	if(entry >= pathtable[warp_no].mask.size()) entry = 0;

	while(counter != (pathtable[warp_no].mask.size())){
		if(pathtable[warp_no].ref_counter[entry] <= -1 && entry != pathtable[warp_no].free_ptr){
			counter = 0; 
			pathtable[warp_no].free_ptr = entry;
			break;
		}
	
		counter++;
		if((entry + 1) == (pathtable[warp_no].mask.size())){
			entry = 0;
		}else{
			entry++;
		}		
	}
	assert(counter <= (pathtable[warp_no].mask.size()));
	if(last_free == pathtable[warp_no].free_ptr){ //currently no more entries
		pathtable[warp_no].free_ptr = -1;
	}
}

void DataAnalyser::newpathtable_entry(int mask, int valid, int actual_mask, int warp_no){
	int entry = pathtable[warp_no].free_ptr;
	if(pathtable[warp_no].ref_counter[entry] > -1) update_freePointer(warp_no);
	pathtable[warp_no].mask[entry] = mask;
	pathtable[warp_no].actual_mask[entry] = actual_mask; 
	pathtable[warp_no].ref_counter[entry] = 1; //reference counter
	pathtable[warp_no].rbit[entry] = valid;
	update_freePointer(warp_no);
	//cout << "**NEW FREE PTR = " << pathtable.free_ptr << endl;

	int count = 0;
	for(int i = 0; i < pathtable[current_warp].ref_counter.size(); i ++){
		if(pathtable[current_warp].ref_counter[i] > 0 && pathtable[current_warp].rbit[i] >= 0) count++;
	}

	if(count > statistics.max_paths[warp_no]) statistics.max_paths[warp_no] = count;  //////
}

void DataAnalyser::nextPC_error_check(int x_smask[], int offset){
	bool match = true;
	LONG last_pc = 0; int tmp_mask[threads_per_warp];
	for(int i = 0; i < threads_per_warp; i++) tmp_mask[i] = 0;	

	for(int i = 0; i < threads_per_warp; i++){
		
		if(x_smask[i]){
			tmp_mask[i] = 1;
			if(last_pc == 0){ 
				last_pc = return_pc(tmp_mask, offset);
				cout << "thread [" << i << "], PC = " << hex16(last_pc) << endl;
			}else{
				cout << "thread [" << i << "], PC = " << hex16(return_pc(tmp_mask, offset)) << endl;
				if(last_pc != return_pc(tmp_mask, offset)){
					cout << "ERROR" << endl;
				}
			}
		}
		tmp_mask[i] = 0;
	}	
}

void DataAnalyser::update_branchstall_path(int warp_no, int tag_count){
     //udpate time stamp for branch noop
	std::vector<int>::iterator is_tt;  
	int is_pc = find(isTable[warp_no].path.begin(), isTable[warp_no].path.end(), tag_count) - isTable[warp_no].path.begin();
	isTable[warp_no].time_stamp[is_pc] = number_cycles;

}


void DataAnalyser::update_path_and_is(int tag_count, int x_smask[], int offset, int warp_no){
					
	//udpate IS table pc - find tag and replace its pc to new pc
	std::vector<int>::iterator is_tt;  
	int is_pc = find(isTable[warp_no].path.begin(), isTable[warp_no].path.end(), tag_count) - isTable[warp_no].path.begin();

	int active_mask[threads_per_warp];
	convert_int_to_vectormask(pathtable[warp_no].actual_mask[tag_count], active_mask, threads_per_warp);


	//cout << ">>>>>[PC] Incrementing PST PC from " << hex16(isTable.pc[is_pc]) << " to ";
	isTable[warp_no].pc[is_pc] = return_pc(active_mask, offset);
	//cout << hex16(isTable.pc[is_pc]) << endl;
	isTable[warp_no].call_depth[is_pc] = return_sp(active_mask, offset);
	isTable[warp_no].time_stamp[is_pc] = number_cycles;

	isTable[warp_no].last_scheduled = number_cycles;
	LONG max_sp = 0; int index = 0, count = 0;
	for(std::vector<LONG>::iterator max = isTable[warp_no].call_depth.begin(); max != isTable[warp_no].call_depth.end(); max++){			
		if(*max > max_sp){
			max_sp = *max;	
			index = count;
		}
		count++;
	}

	isTable[warp_no].sp = max_sp;
	isTable[warp_no].prog_counter = isTable[warp_no].pc[index];

}

int DataAnalyser::generate_mask(int tag1, int tag2, int length, int warp_no){
	//get the two masks
	int mask1 = pathtable[warp_no].mask[tag1];
	int mask2 = pathtable[warp_no].mask[tag2];

	//convert to bits
	int m1[length], m2[length], mfinal[length];
	for(int i = 0; i < length; i++){
		m1[i] = mask1 & (1 << i) ? 1 : 0; //cout << m1[i];
		m2[i] = mask2 & (1 << i) ? 1 : 0;
	}
	
	//OR masks together
	for(int i = 0; i < length; i++){
		if(m1[i] == 1 || m2[i] == 1)
			mfinal[i] = 1;
		else
			mfinal[i] = 0;		
	}
	
	//convert back to int and return new mask
	int maskval = 0;
	for(int i = 0; i < length; i++)
		if(mfinal[i])
			maskval += pow(2, i);

	return maskval;
}

int DataAnalyser::return_branch_mask(DVInst dv_temp){
	int mask = 0;

	for(UINT k = 0; k < threads_per_warp; k++){
		if(!(dv_temp.insts[k].branch_taken))
			mask += pow(2, k);
	}

	return mask;

}

////////////////////////////////////////////////////////////
//  Two warps/IS were determined to converge/reconv
//  create a path table entry with v = 0, store this tag to DVInst
//  And generate/insert cuop into the pipeline		
////////////////////////////////////////////////////////////

void DataAnalyser::update_structures_reconverge(int x_smask[], int offset, int inst_count, int last_tag, int other_tag, int warp_no){ ////
	//get new tag, update instruction
	if(pathtable[warp_no].free_ptr == -1){ 
		update_freePointer(warp_no); 
		assert(pathtable[warp_no].free_ptr != -1);
	}

	int tag = pathtable[warp_no].free_ptr; 	//dv_temp.tag = tag; 
	int newmask = generate_mask(last_tag, other_tag, threads_per_warp, warp_no);  // "speculatively" compute masks
	convert_int_to_vectormask(newmask, x_smask, threads_per_warp);
#ifdef DEBUGFETCH
	print_pathtable(warp_no);
#endif
	//update path table
	newpathtable_entry(newmask, 0, ret_mask(x_smask, threads_per_warp), warp_no);  //TODO: r instead of newmask???
#ifdef DEBUGFETCH
	print_is_table(warp_no);			
	cout << "[Path Table(reconv)] Insert < Mask: " << newmask << " RC 1 " << endl;
	cout << "[TAG " << tag << "] created. Merging IS " << last_tag << " and IS " << other_tag <<  endl; 
	cout << "Inject c_uop" << endl;
#endif
	//create a cuop and place in decode_rename buffer
	DVInst cuop; 
	cuop.create_cuop(conf, warp_seqno[current_warp], inst_count, last_tag, other_tag);
	cuop.tag = tag;
	cuop.actual_mask = ret_mask(x_smask, threads_per_warp);
	cuop.warpid = warp_no;

	//update reference counters to paths
	pathtable[warp_no].ref_counter[last_tag] += 1; //by cuop
	pathtable[warp_no].ref_counter[other_tag] += 1; //by cuop

	decode_to_rename.push_back(cuop); 
	//decode_to_rename.push_back(dv_temp); 
	statistics.num_cuops++;

	//Update IS Table
	update_is_table(cuop, x_smask, offset, tag, 2, warp_no);
	update_warp_thread_tags(offset, tag, x_smask); //update threads
}

//updates div_counter, creates PST entry and Pathtable entry for both, -- reference counter of original path
int DataAnalyser::update_structure_mispredict(int x_smask[], int inv_mask[], DVInst &dv_temp, int offset, int &last_tag, int warp_no){  //new tag returned
	//create a path for NT, replace the PST entry
	if(pathtable[warp_no].free_ptr == -1){ 
		update_freePointer(warp_no);
		assert(pathtable[warp_no].free_ptr != -1);
	}
	int tag = pathtable[warp_no].free_ptr;
	newpathtable_entry(get_int_mask(x_smask, threads_per_warp), 1, get_int_mask(x_smask, threads_per_warp), warp_no);
	//update IS table with new tag	
	update_warp_thread_tags(offset, tag, x_smask);	
	dv_temp.pathT = last_tag;		
	update_is_table(dv_temp, x_smask, offset, tag, 7, warp_no);

	//create a path for T, add PST entry
	if(pathtable[warp_no].free_ptr == -1){ 
		update_freePointer(warp_no);
		assert(pathtable[warp_no].free_ptr != -1);
	}
	assert(pathtable[warp_no].free_ptr != -1);
	int new_tag = pathtable[warp_no].free_ptr;
#ifdef DEBUGFETCH
	cout << "NEW TAGS GENERATED = " << tag << " and " << new_tag << endl;
#endif	
	newpathtable_entry(get_int_mask(inv_mask, threads_per_warp), 1, get_int_mask(inv_mask, threads_per_warp), warp_no);
	//update IS table with new tag	
	//update_warp_thread_tags(offset, new_tag, inv_mask);			
	update_is_table(dv_temp, inv_mask, offset, new_tag, 0, warp_no);
	
	//increment FDC since branch transformed to buop
	div_counter++; 
#ifdef DEBUGFETCH
	cout << "Global {FDC}++ " << endl; //buop inject. Increment FDC
#endif
	//update reference counters of paths referenced by branch (i.e. last_tag)
	//pathtable[warp_no].ref_counter[last_tag] += 1; //by buop -- increment

	//last tag becomes other tag of new path, return
	last_tag = tag;
	return new_tag;
}

void DataAnalyser::update_structure_divergent(int x_smask[], DVInst &dv_temp, int offset, int last_tag, int warp_no, bool mispredict){ 
	//renew path table in case of divergence
	//get current mask, new tag, previous tag and copy to table
	if(pathtable[warp_no].free_ptr == -1){ 
		update_freePointer(warp_no);
		assert(pathtable[warp_no].free_ptr != -1);
	}

	int tag = pathtable[warp_no].free_ptr;
	int nt_mask = -1;
	if(mispredict) nt_mask = get_int_mask(x_smask, threads_per_warp); //actual mask of NT path (mispredict calculated)

	dv_temp.tag = tag; 
	std::vector<int>::iterator m = pathtable[warp_no].mask.begin();
	if(!mispredict){				
		std::advance(m, last_tag); //copy previous mask to this table entries mask
	#ifdef DEBUGFETCH
		cout << "Copying mask from location " << last_tag << endl;
		print_pathtable(warp_no);
	#endif
		
		newpathtable_entry(*m, 0, dv_temp.actual_mask, warp_no); 
	}else{
		newpathtable_entry(nt_mask, 1, nt_mask, warp_no); 

	}
	div_counter++; 
#ifdef DEBUGFETCH
	cout << "Global {FDC}++ " << endl; //buop inject. Increment FDC
	if(!mispredict)
		cout << "[Path Table(div)] Insert < Mask: " << *m << " RC 1 " << endl;
	else
		cout << "[Path Table(div)] Insert < Mask: " << nt_mask << " RC 1 " << endl;
#endif

	//update reference counters of paths referenced by branch (i.e. last_tag)
	pathtable[warp_no].ref_counter[last_tag] += 1; //by buop -- increment
	if(!mispredict){ //update dvInst and push onto queue
#ifdef DEBUGFETCH
	print_pathtable(warp_no);
	
		cout << "[actual_mask] = " << dv_temp.actual_mask << endl;
#endif
	decode_to_rename.push_back(dv_temp); 
}					
	//update threads
	update_warp_thread_tags(offset, tag, x_smask);	

#ifdef DEBUGFETCH	
	cout << "[TAG " << tag << "] created. Cont " << endl; 
#endif				
	//update IS table with new tag	
	if(!mispredict)
		update_is_table(dv_temp, x_smask, offset, tag, 1, warp_no);					
	else
		update_is_table(dv_temp, x_smask, offset, tag, 5, warp_no);
	
}
/************************************************************************
Assumed as boot up process:
- get actual mask, create new pathtable entry with v = 1 and place actual mask
- create the IStable entry with appropriate fields 
************************************************************************/
void DataAnalyser::update_structure_coldcase(int x_smask[], DVInst &dv_temp, int offset, int last_tag){
	pathtable[dv_temp.warpid].ref_counter[last_tag] -= 1;
	dv_temp.actual_mask = get_int_mask(x_smask, threads_per_warp);
	decode_to_rename.push_back(dv_temp); 		
#ifdef DEBUGFETCH	
	cout << "[Path Table(new)] Updated " << endl; 	
	print_pathtable(dv_temp.warpid);				
#endif
}

int max_mask(int threads_per_warp){//////////////
	int maskval = 0;
	for(int i = 0; i < threads_per_warp; i++)
		maskval += pow(2, i);

	return maskval;

}

void DataAnalyser::update_path_mask(int tag, int exception_mask[], DVInst &dv){
	int tmp_mask[threads_per_warp];
	convert_int_to_vectormask(pathtable[dv.warpid].actual_mask[tag], tmp_mask, threads_per_warp);
	bool change = false;
	
	//cout << "Comparing path mask " << pathtable.actual_mask[tag] << " to "; //<< get_int_mask(, threads_per_warp) << endl;
	for(int i = 0; i < threads_per_warp; i++){
		//cout << exception_mask[i];
		if(exception_mask[i] && tmp_mask[i]){
			tmp_mask[i] = 0;
			change = true;
		}
	}
	if(change){
	        pathtable[dv.warpid].actual_mask[tag] = get_int_mask(tmp_mask, threads_per_warp);	
	}
	
	//cout << " -----> " << get_int_mask(tmp_mask, threads_per_warp) << endl;

}

void DataAnalyser::fix_mask(int exception_mask[], int x_smask[], int offset){
	for(int i = 0+offset; i < threads_per_warp+offset; i++){
		if(exception_mask[i-offset]){
			x_smask[i-offset] = 1;
			threads[i].fetch_stall = true; //true, true, false
			threads[i].exception = true; 
			threads[i].fetch_isactive = false;
			
		}
	}

}

/*bool DataAnalyser::update_fetchq_empty_warps(int offset, int mask[], int path_tag){ //problem srad -- issue with tag assignment?
	bool finish = true;
	int current_mask = pathtable.actual_mask[path_tag];
	int emptyq_mask = get_int_mask(mask, threads_per_warp);
	int new_mask[threads_per_warp];
		
	if(finish){
		return true;
	}

	return false;

}*/

/******************************************************
 * Fetch Instructions and inserts into the DVIQ
 * @return number of instructions fetched
 ******************************************************/
UINT DataAnalyser::fetch(bool alter = false, int cyc = 0){
#ifdef DEBUG
	cout << "[Structure stats]: decode buff = " << decode_to_rename.size() << ", ROB = " << reorderB.data_valid.size() << ", issueQ size = " << get_issueQ_size() << endl;
#endif
	if(decode_to_rename.size() <= 64){ 
		int current_warp, table_num = fetch_warp, is_entry;  
		current_warp = thread_schedule_minsp_rr(table_num, is_entry, cyc);
		//thread_schedule_ext_minsp(table_num, is_entry, cyc); //thread_schedule_minsp(table_num, is_entry, cyc);//  //Fetch Heuristic   
  
		if(current_warp != -1){
			current_warp = table_num;
#ifdef DEBUGFETCH
		cout << "[schedule] IS Table entry " << is_entry << ", i.e. path = " << isTable[current_warp].path[is_entry] << ", i.e. actual mask = " << pathtable[current_warp].actual_mask[isTable[current_warp].path[is_entry]] <<  endl;
		cout << ">>>>current warp = " << current_warp << endl;
#endif
		}

		int barrier_count=0, fini = 0;
		int offset = current_warp * threads_per_warp; 

		//deal with barriers and other exceptional situations
		if(current_warp == -1){ 
			return -1;
		}

		assert((uint)current_warp <= num_warps);
 
		bool fetch = false;
		int x_smask[threads_per_warp], path_mask[threads_per_warp];
		convert_int_to_vectormask(pathtable[current_warp].actual_mask[isTable[current_warp].path[is_entry]], path_mask, threads_per_warp);  //getting mask from pathmask. Trace-based (need actual mask)
		for(int k = 0; k < threads_per_warp; k++){
			x_smask[k] = path_mask[k];
		}

		// check for any inactive threads or barriers, determine which threads of warp should be fetched
		int ret_fetch = to_fetch_or_not_to_fetch(offset, fetch, x_smask, is_entry, path_mask);  
		
		if(ret_fetch == -1){ //prefetch q empty
			cout << ", q is empty = to_fetch returning " << ret_fetch << endl;
			return -1;
		}else if(ret_fetch == 2){ //hit a barrier 
			cout << ", warp hit a barrier. Try again in next cycle " << ret_fetch << endl;
			return -1;
		}else if(ret_fetch == -2){
			cout << "No more PST entries to schedule" << endl;
			return -1;
		}

		if(fetch){ //thread is active and ready to fetch
			bool divergence = false, divergence_mispredict = false;
			int combine_count = 0;
			int dvqid;
			INSERT_TYPE type;
			bool almost_full = false;
		
			
			if(current_fetch_cycle < number_cycles){
				statistics.icache_access_cycles++;
				current_fetch_cycle = number_cycles;
			}

			bool first = true;
			DVInst dv_temp; 
			int inst_count = 0;
			LONG pc = 0;
			int ins_div_pt = 0;
		
			update_inst_count(inst_count, x_smask); //update instruction count (for SIMD insts that may take > 1c.c) 
			
			bool gen_exception = false;
			int exception_mask[threads_per_warp];
			for(int i = 0; i < threads_per_warp; i++){
				exception_mask[i] = 0;			
			}

			bool exception_generated = false, branch_stalled_warp = false;
			for(UINT i=0 ; i < threads_per_warp; i++){ //assign DV-Inst parameters their respective thread info
				gen_exception = false;
				if(x_smask[i]){ 
					if(pc == 0){	//Just a precaution for trace
						pc = threads[i+offset].prefetch_instruction_queue_buffer.front().pc;	
					}else{ 	
						if(pc != threads[i+offset].prefetch_instruction_queue_buffer.front().pc){ 
							gen_exception = true;
							exception_generated = true;
							x_smask[i] = 0;
						}
					}
							
					dv_temp.create(conf,warp_seqno[current_warp]++, inst_count, current_warp); 
					if(threads[i+offset].branch_stall)
						branch_stalled_warp = true;
						
					if(gen_exception){
						exception_mask[i] = 1;
						threads[i+offset].fetch_stall = true;
						threads[i+offset].exception = true;
						threads[i+offset].fetch_isactive = false;
#ifdef DEBUGFETCH
						cout << "EXCEPTION FOUND" << endl;
#endif
					}else{
						dv_temp.insts[i] = threads[i+offset].prefetch_instruction_queue_buffer.front();
						dv_temp.update(i,first);
						if(first){ //for 1st inst in warp only = true
							first= false;
						}
					}
				}
			}
			//update actual_mask for pathtable trace execution			
			current_thread = find_first_active(x_smask, threads_per_warp);			
			if(branch_stalled_warp){
				//will create a no-op. Branches do not have any other data
				for(int i = 0; i < threads_per_warp; i++){ 
					if(x_smask[i]){ 
						dv_temp.insts[i].is_branch = false;
					}
				}
				statistics.branch_nops++;
			}
	
			int active_inst_count = 0;
			if(dv_temp.insts[current_thread].is_branch){ 
				dv_temp.is_branch = true; 
				dv_temp.branch_taken = false;

				//1. get actual outcome bc of trace-based simulation
				for(int i = 0; i < threads_per_warp; i++){ 
					if(x_smask[i]){ 
						if(dv_temp.insts[i].branch_taken){ //NOTE: NT path (i.e. PC + 4) actually taken first in traces/simulator
							dv_temp.branch_taken = true; 
						}
						active_inst_count++;
					}
				}

				//2. get prediction
#ifndef PERFECT_BRANCH
				dv_temp.branch_taken_pred = branchpredictor->get_prediction(dv_temp.insts[current_thread].pc, current_thread+offset);
				//udpate threads since we don't know which thread will actually be the 1st active in warp once resolved
				for(int i = 0; i < threads_per_warp; i++){ 
					if(x_smask[i]){ 
						//get prediction per thread, false = NT, true = Taken. so we don't have to re-do TAGE sim definition
						dv_temp.insts[i].branch_taken_pred = branchpredictor->get_prediction(dv_temp.insts[i].pc, i+offset);


						if(dv_temp.branch_taken_pred != dv_temp.branch_taken){ //THIS SHOULD BE DONE AT EXE/COMMIT -- impl here to compare to in-order equivalent
							if(i+offset == current_thread){
								branchpredictor->update_predictor(true,dv_temp.insts[i].pc, dv_temp.insts[i].branch_taken, i+offset); 
							}else{
								branchpredictor->update_history(true,dv_temp.insts[i].pc, dv_temp.insts[i].branch_taken, i+offset); 
							}	
						}
						//Threads will stall for traces below (derived from divergence mask) 
					}
				}
#ifdef DEBUGBR
				cout << "[BRANCH} Predicted : " << dv_temp.branch_taken_pred << ", actually: " << dv_temp.branch_taken << endl;
#endif

#else
				dv_temp.branch_taken_pred = dv_temp.branch_taken;
				for(int i = 0; i < threads_per_warp; i++){ 
					if(x_smask[i]){ 
						dv_temp.insts[i].branch_taken_pred = dv_temp.insts[i].branch_taken;
					}
				}
#endif				
				
				if(exception_generated){
					exception_generated = false;
					fix_mask(exception_mask, x_smask, offset);
					
					
#ifdef DEBUGFETCH
					cout << "EXCEPTION OVERIDED" << endl;
#endif
				}
				assert(x_smask[current_thread]);

				//////////////////// 	DIVERGENCE PREDICTION 	///////////////////////////////////
				bool real_div;
#ifdef PERFECT_DIVERGENCE
				divergence = dviqm.branch_div_predictor(x_smask, dv_temp, offset, false); 
				real_div = divergence; 
#else
				if(active_inst_count > 1){
					divergence = dviqm.divergence_predictor(); 
					real_div = dviqm.branch_div_predictor(x_smask, dv_temp, offset, true); 
				}else{
					divergence = false; //can't diverge if there's only 1 thread
					real_div = false;		
				}
				
				if(real_div){
				dv_temp.new_path = true;
#ifdef DEBUGFETCH
				cout << "new path will be generated with mask " << get_int_mask(x_smask, threads_per_warp) << endl;					 
#endif
	
				}
				else dv_temp.new_path = false;
#ifdef DEBUGFETCH
				if(divergence) cout << "Predicted divergent" << endl; 
				else cout << "Predicted Non-divergent" << endl; 	
				if(divergence != real_div) cout << "<><><>DIVERGENCE MISPREDICTION" << endl;
#endif
#endif
				dv_temp.div_pred = divergence;
				dv_temp.div_actual = real_div;
				if(divergence != real_div){
					divergence_mispredict = true; ///////divergence = prediction, divergence_mispredict - whether prediction correct (T) or not (F) //////////////////////////
				}

				//actual_mask--if uniform, x_smask obtained from branch_div_predictor, if divergent, x_smask is NT branches from branch_div_predictor

				if(divergence) statistics.buop_count++;
				dv_temp.actual_mask = get_int_mask(x_smask, threads_per_warp); ///if uniform set mask, if branch then set below for NT/divergent
				statistics.dv_branch++;

			}else{ 	
				dv_temp.is_branch = false;
			}
			

			
			if(exception_generated){
				pathtable[current_warp].mask[isTable[current_warp].path[is_entry]] = ret_mask(x_smask, threads_per_warp); //update pathtable
				pathtable[current_warp].actual_mask[isTable[current_warp].path[is_entry]] = ret_mask(x_smask, threads_per_warp);
					
#ifdef DEBUGFETCH
					cout << "EXCEPTION CORRECTED IN PATH TABLE" << endl;
#endif
			}

			int tag_count = -1;	
			for(int i=0 ; i < threads_per_warp; i++){			
				if(x_smask[i]){ //find an active thread and get last known tag
					tag_count = threads[i+offset].tag;
					break;
				}
			}

			//if warp actually goes in uniform direction = branch direction is INCORRECT, stall threads 
			if(dv_temp.is_branch){ 

				if(!divergence && !divergence_mispredict){ //if uniform and no misprediction
					if(dv_temp.branch_taken != dv_temp.branch_taken_pred){ //and branch misprediction. nop front end until resolved in backend
						for(int j = 0; j < threads_per_warp; j++){
							if(x_smask[j]){
								threads[j+offset].branch_stall = true;					
							}
						}
#ifdef DEBUGFETCH
						cout << "<><>[uniform] branch mispredict, predicted " << dv_temp.branch_taken_pred << endl;
#endif
					}//else all is good
				}else if(divergence && divergence_mispredict){ //if divergent and mispredicted
					if(dv_temp.branch_taken != dv_temp.branch_taken_pred){ //and branch mispredicted (bc actually uniform)
						for(int j = 0; j < threads_per_warp; j++){
							if(x_smask[j]){
								threads[j+offset].branch_stall = true;					
							}
						}
#ifdef DEBUGFETCH
						cout << "<><>[uniform mispred] BUT branch mispredict " << endl;
#endif	
					}/*else{//else doesn't matter
						cout << "<><>[uniform mispred] BUT all is good. " << endl;

					}*/
				}else if(!divergence && divergence_mispredict){ //if predicted non-divergent but actually divergent
					        convert_int_to_vectormask(pathtable[current_warp].actual_mask[isTable[current_warp].path[is_entry]], x_smask, threads_per_warp);
						for(int j = 0; j < threads_per_warp; j++){
							if(x_smask[j]){
								threads[j+offset].branch_stall = true;					
							}
						}
#ifdef DEBUGFETCH
						cout << "<><>[full mispredict] divergence mispredict " << endl;
#endif	

				}else{ //if divergent, branch doesnt matter
#ifdef DEBUGFETCH
						cout << "<><>[divergent -- so branch doesn't matter] " << endl;
#endif	
					dv_temp.branch_taken_pred = dv_temp.branch_taken;
				}
			}

			if(divergence && !divergence_mispredict){ //if predicted divergent and actually divergent. ELSE uops are injected (above) until resolved in backend
				dv_temp.pathT = tag_count; //set previous tag as metadata
				dv_temp.actual_mask = get_int_mask(x_smask, threads_per_warp);	
				update_structure_divergent(x_smask, dv_temp, offset, tag_count, dv_temp.warpid, false);
#ifdef DEBUGFETCH
				cout << "MY TAG IS: " << dv_temp.tag << endl;				
#endif
		
			}else{ //proceeds as normal, and update fetch counter of path								
				assert(tag_count != -1); //shouldn't happen
				dv_temp.tag = tag_count;

				if(exception_generated){
					dv_temp.exception_bit = true;
					dv_temp.exception_mask = get_int_mask(exception_mask, threads_per_warp);
					update_path_mask(isTable[dv_temp.warpid].path[is_entry], exception_mask, dv_temp); 
				}
				decode_to_rename.push_back(dv_temp); //place in the queue for next pipeline stage 
				
#ifdef DEBUGFETCH
				print_smask(x_smask, threads_per_warp);					
#endif
			}	

			///////////////////////////////////////////////////////////////////
			int active_mask[threads_per_warp], mask2[threads_per_warp];
			convert_int_to_vectormask(pathtable[dv_temp.warpid].actual_mask[tag_count], active_mask, threads_per_warp);
			convert_int_to_vectormask(pathtable[dv_temp.warpid].mask[tag_count], mask2, threads_per_warp);
			
#ifdef DEBUGFETCH
			print_is_table(dv_temp.warpid); 
#endif
			//update active threads in the mask based on any structural obstructions or barriers
			int ref1 = -1;

			for (UINT i=0+offset; i<threads_per_warp+offset; i++){
				if(active_mask[i-offset]){
					if(threads[i].fetch_qempty){
					  active_mask[i-offset] = 0; mask2[i-offset] = 0;
					  pathtable[dv_temp.warpid].actual_mask[tag_count] = get_int_mask(active_mask, threads_per_warp);
					  pathtable[dv_temp.warpid].mask[tag_count] = get_int_mask(mask2, threads_per_warp);
#ifdef DEBUGFETCH	
					cout << "INACTIVE (Thread [" << i << "] PC = " << hex16(threads[i].pc) << ", tag = " << threads[i].tag << ")";
					cout << ", next pc = " << hex16(threads[i].fetch_pc) << "   emp = " << threads[i].fetch_qempty << ", finish = " << threads[i].finished << ", isactive = " << threads[i].fetch_isactive << ", branchstall = " << threads[i].branch_stall << ", exc = " << threads[i].exception << endl; 
#endif
					}else{
#ifdef DEBUGFETCH
					cout << "Thread [" << i << "] PC = " << hex16(threads[i].pc) << ", tag = " << threads[i].tag;
					cout << ", next pc = " << hex16(threads[i].fetch_pc) << "   emp = " << threads[i].fetch_qempty <<  ", finish = " << threads[i].finished << ", isactive = " << threads[i].fetch_isactive << ", branchstall = " << threads[i].branch_stall << ", exc = " << threads[i].exception << endl;  
#endif
					}
				}else{
#ifdef DEBUGFETCH
					cout << "INACTIVE (Thread [" << i << "] PC = " << hex16(threads[i].pc) << ", tag = " << threads[i].tag << ")";
					cout << ", next pc = " << hex16(threads[i].fetch_pc) << "   emp = " << threads[i].fetch_qempty <<  ", finish = " << threads[i].finished << ", isactive = " << threads[i].fetch_isactive << ", branchstall = " << threads[i].branch_stall << ", exc = " << threads[i].exception << endl; 
#endif	
				}
			}
		
			//////////////////////////////////////////////////////////////////////////////////	
			int first_thread = 0;
			for(UINT i=0 ; i < threads_per_warp; i++){
				if(active_mask[i] && !threads[i+offset].branch_stall){
					first_thread = i+offset;
					collect_instruction_stats(dv_temp, i, combine_count, offset);
 								
					threads[i+offset].prefetch_instruction_queue_buffer.pop_front(); //taken off prefetch IQ and placed into decoded_instructions
					if(threads[i+offset].prefetch_instruction_queue_buffer.empty()) //if prefetch queue is empty, try and refill
						refill_prefetch_IQB(i, offset);
					
					threads[i+offset].fetch_pc = threads[i+offset].prefetch_instruction_queue_buffer.front().pc; //get next values
					threads[i+offset].fetch_sp = threads[i+offset].prefetch_instruction_queue_buffer.front().sp;
				}
			} 
		
			//update path table and ISTable
			if(!threads[first_thread].branch_stall){ 
				update_path_and_is(tag_count, active_mask, offset, dv_temp.warpid); //x_smask

				gather_dv_stats(combine_count, dv_temp, divergence, active_mask); //x_smask
			}else{
				update_branchstall_path(dv_temp.warpid, tag_count);
			}
			return 1;
		}else
			return -1;
	}else{
		//gather stall stat
		statistics.decBuffer_stalls++;
		return -1;
		
	}
	return -1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////											//////////////////						
/////////////////////				Rename and associated functions				//////////////////
/////////////////////											//////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void DataAnalyser::print_decode_queue(){
	for(std::deque<DVInst>::iterator it = decode_to_rename.begin(); it != decode_to_rename.end(); it++){

	DVInst dv_temp = *it;
	#ifdef DEBUG
	cout << dv_temp.is_uop << endl;
	#endif
}

}

void DataAnalyser::setup_rename(){

	pathtable = new Path_Table[conf.num_warps];
	isTable = new IS_Table[conf.num_warps]; 
	pirat_int = new PIRAT_Table[conf.num_warps];
	pirat_fp = new PIRAT_Table[conf.num_warps];
	commit_pirat_int = new PIRAT_Table[conf.num_warps];
	commit_pirat_fp = new PIRAT_Table[conf.num_warps];

	//setup freelist
	for(int i = 0; i < NUM_PRF; i++){
		freelist_int.reg.push_back(i);
		freelist_int.taken.push_back(0);
		freelist_fp.reg.push_back(i);
		freelist_fp.taken.push_back(0);
	} 
	freelist_ptr = 0; freelist_ptr_fp = 0;


		//setup 2 PIRAT with default values (INT and FP). And PRF
	for(int j = 0; j < conf.num_warps; j++){ 
		for(int i = 0; i < INT_GPR; i++){
			int tmp = freelist_int.reg.front();
			assert(freelist_int.taken.front() == 0);
			pirat_int[j].default_reg.push_back(tmp);  
			pirat_int[j].r_bit.push_back(false); //valid bit for parital
			pirat_int[j].v_bit.push_back(true); //valid bit for default
			pirat_int[j].mt_bit.push_back(false); //mask tag bit
			pirat_int[j].partial_reg.push_back(-1);
			pirat_int[j].mask_tag.push_back(-1);
			prf.valid.push_back(true);
		
			freelist_int.reg.pop_front(); freelist_int.taken.pop_front();
			freelist_int.reg.push_back(tmp); freelist_int.taken.push_back(1);

			//commit PIRAT - copy initial contents
			commit_pirat_int[j].default_reg.push_back(tmp);  
			commit_pirat_int[j].r_bit.push_back(false); //valid bit for parital
			commit_pirat_int[j].v_bit.push_back(true); //valid bit for default
			commit_pirat_int[j].mt_bit.push_back(false); //mask tag bit
			commit_pirat_int[j].partial_reg.push_back(-1);
			commit_pirat_int[j].mask_tag.push_back(-1);

			//std::list<int> tmp_rec; //for register recycling
			//tmp_rec.clear();
			//recycle_registers.push_back(tmp_rec);
			freelist_ptr = freelist_int.reg.front();
		}
	}
	for(int i = INT_GPR; i < NUM_PRF; i++){
		prf.valid.push_back(true);
	}

	for(int j = 0; j < conf.num_warps; j++){ 
		for(int i = 0; i < SIMD_GPR; i++){
			int tmp = freelist_fp.reg.front();
			assert(freelist_fp.taken.front() == 0);
			pirat_fp[j].default_reg.push_back(tmp);
			pirat_fp[j].r_bit.push_back(false); //valid bit for partial
			pirat_fp[j].v_bit.push_back(true); //valid bit for default
			pirat_fp[j].mt_bit.push_back(false); //mask tag bit
			pirat_fp[j].partial_reg.push_back(-1);
			pirat_fp[j].mask_tag.push_back(-1);
			vrf.valid.push_back(true);
		
			freelist_fp.reg.pop_front(); freelist_fp.taken.pop_front();
			freelist_fp.reg.push_back(tmp); freelist_fp.taken.push_back(1);

			//commit PIRAT fp - copy initial contents
			commit_pirat_fp[j].default_reg.push_back(tmp);
			commit_pirat_fp[j].r_bit.push_back(false); //valid bit for partial
			commit_pirat_fp[j].v_bit.push_back(true); //valid bit for default
			commit_pirat_fp[j].mt_bit.push_back(false); //mask tag bit
			commit_pirat_fp[j].partial_reg.push_back(-1);
			commit_pirat_fp[j].mask_tag.push_back(-1);
			vrf.valid.push_back(true);
			freelist_ptr_fp = freelist_fp.reg.front();
			//std::list<int> tmp_rec; //for register recycling of FP
			//tmp_rec.clear();
			//recycle_registers_fp.push_back(tmp_rec);

		}
	}

	for(int i = SIMD_GPR; i < NUM_PRF; i++){
		vrf.valid.push_back(true);
	}

	//setup pathtable
	for(int j = 0; j < conf.num_warps; j++){
		pathtable[j].free_ptr = 0; pathtable[j].commit_ptr = -1;	
		isTable[j].prog_counter = 0; isTable[j].sp = 0; isTable[j].last_scheduled = 0;
		for(int i = 0; i < (threads_per_warp*EXTRA_PATHS); i++){ //+EXTRA_PATHS for leighway
			pathtable[j].rbit.push_back(-1);
			pathtable[j].mask.push_back(-1);
			pathtable[j].actual_mask.push_back(-1);
			pathtable[j].ref_counter.push_back(-1);
		}	
	}

	//setup variables in .hpp
	fetch_warp = 0;
	div_counter = 0; dv_count = 0; 
	current_warp = 0; minsp_current_warp = 0; pick_count = 0; //timeout_cycle = 0;
	pick_count = 0; //timeout_cycle = 0; 
	current_thread = 0; fetch_heur_cycle = 0;
	dviq_cycle = 0; current_fetch_cycle = 0; rrcounter = 0; fetch_started = false;

	cold = new bool[conf.num_warps];
	for(int i = 0; i < conf.num_warps; i++)
		cold[i] = true;

}

void DataAnalyser::update_rename_stats(std::string classification){
	if(!classification.compare("EQ")){
		statistics.r_equal_masks++;
	}else if(!classification.compare("SUPER")){
		statistics.r_strict_super++;
	}else if(!classification.compare("PARTIALLY")){
		statistics.r_other++;
	}else if(!classification.compare("NON_INTER")){
		statistics.r_non_inter_masks++; 
	}
	statistics.r_merge_event++;
}

/*****************************************************
   Creates a list of sources that the DVInst must
   fulfill during issueQ (must all be ready)		
*****************************************************/
void DataAnalyser::update_dv_source(DVInst &dv, int reg, int type, int ready){ //for issue purposes
	dv.sources++;
	dv.reads.push_back(reg);
	dv.pirat_type.push_back(type); // 0 = INT, 1 = FP
}


void DataAnalyser::update_dv_dest(DVInst &dv, int reg, int type, int orig){
	dv.dests++;
	dv.writes.push_back(reg);
	dv.pirat_wtype.push_back(type); //INT or FP
	dv.orig_dests.push_back(orig);
	if(type == 0){ //INT
		prf.valid[reg] = false;   ///<----------------------------
	}else{ //FP
		vrf.valid[reg] = false;
	}
}

int DataAnalyser::get_free_intreg(DVInst &dv, bool is_source){
	int tmp = -1;
	std::list<int>::iterator reg = freelist_int.reg.begin();  
	std::list<bool>::iterator free = freelist_int.taken.begin(); 

	if(freelist_ptr >= NUM_PRF || freelist_ptr < 0) freelist_ptr = 0;
	std::advance(reg, freelist_ptr); std::advance(free, freelist_ptr);
	assert(reg != freelist_int.reg.end());

	for(; free != freelist_int.taken.end(); free++){
		if(!(*free)){
			tmp = *reg;
			*free = true;
			break;
		}
		reg++;
	}

	if(tmp == -1){
		//search from the beginning just incase
		reg = freelist_int.reg.begin(); free = freelist_int.taken.begin();
		for(; free != freelist_int.taken.end(); free++){
			if(!(*free)){
				tmp = *reg;
				*free = true;
				break;
			}
			reg++; 
		}
	}

	if(tmp >= 0) freelist_ptr = tmp;
	return tmp;
}

int DataAnalyser::get_free_FPreg(DVInst &dv, bool is_source){
	int tmp = -1;
	std::list<int>::iterator reg = freelist_fp.reg.begin();
	std::list<bool>::iterator free = freelist_fp.taken.begin();

	if(freelist_ptr_fp >= NUM_PRF || freelist_ptr_fp < 0) freelist_ptr_fp = 0;
	std::advance(reg, freelist_ptr_fp); std::advance(free, freelist_ptr_fp);

	for(; free != freelist_fp.taken.end(); free++){
		if(!(*free)){
			tmp = *reg;
			*free = true;
			break;
		}
		reg++;
	}
	//search from the beginning just incase
	if(tmp == -1){
		reg = freelist_fp.reg.begin(); free = freelist_fp.taken.begin();
		for(; free != freelist_fp.taken.end(); free++){
			if(!(*free)){
				tmp = *reg;
				*free = true;
				break;
			}
			reg++; 
		}
	}
	
	if(tmp >= 0) freelist_ptr_fp = tmp;

	return tmp;
}


/********************************************************************
	Helper functions for STRICT superset and subset determination
*********************************************************************/
bool strict_subset(int r_mask[], int i_mask[], int num_threads){
	bool is_subset = true;
	for(unsigned int i = 0; i < num_threads; i++){
		if(r_mask[i] == 0){
			if(i_mask[i] != 0)
				is_subset = false;
		}
	}
	return is_subset;
}

bool strict_superset(int r_mask[], int i_mask[], int num_threads){
	bool is_supset = true;
	for(unsigned int i = 0; i < num_threads; i++){
		if(r_mask[i] == 1){
			if(i_mask[i] != 1)
				is_supset = false;
		}
	}
	return is_supset;
}

bool masks_equal(int r_mask[], int i_mask[], int num_threads){
	int eq = 0;
	for(unsigned int j = 0; j < num_threads; j++){//tally
		if(r_mask[j] == i_mask[j])
			eq++;
	}
	if(eq == num_threads) return true;
	else return false;
}

bool non_intersect(int r_mask[], int i_mask[], int num_threads){
	int eq = 0;
	for(unsigned int j = 0; j < num_threads; j++){//tally
		if(r_mask[j] == i_mask[j])
			eq++;
	}
	if(eq == 0) return true;
	else return false;
}

bool partial_dependence(int r_mask[], int i_mask[], int threads_per_warp){
	if(strict_subset(r_mask, i_mask, threads_per_warp))
		return false;
	if(strict_superset(r_mask, i_mask, threads_per_warp))
		return false;
	if(masks_equal(r_mask, i_mask, threads_per_warp))
		return false;
	if(non_intersect(r_mask, i_mask, threads_per_warp))
		return false;

	return true;

}

void invert_mask(int i_mask[], int inv_imask[], int length){
	for(int i = 0; i < length; i++){
		if(i_mask[i])
			inv_imask[i] = 0;
		else
			inv_imask[i] = 1;
	}
}

/////////////////////////////////////////////////////
// Source Rename with PIRAT /////////////////////////
/////////////////////////////////////////////////////
void DataAnalyser::rename_sources_pirat(std::vector<int> src, std::vector<int> type, int num_src, DVInst &dv, std::vector<int> &ow, std::vector<int> &owl){
	for(int i = 0; i < num_src; i++){
		int source = src[i]; //number of architectural register
		if(!type[i]){ //INT PIRAT//////////////////////////////////////////////////////////////////////
			statistics.pirat_rd_accesses++;
			assert(pirat_int[dv.warpid].mask_tag[source] == -1);//doesn't have a mask or path tag - use default
#ifdef DEBUG
			cout << "[INT S RENAME] no previous - use default [" << pirat_int[dv.warpid].default_reg[source] << "]" << endl;
#endif
			update_dv_source(dv, pirat_int[dv.warpid].default_reg[source], 0, pirat_int[dv.warpid].v_bit[source]); 
			statistics.pirat_default_rd++;	

		}else{//FP/SIMD PIRAT//////////////////////////////////////////////////////////////////////////
			statistics.pirat_fp_rd_accesses++;
			assert(pirat_fp[dv.warpid].mask_tag[source] == -1); //doesn't have a mask or path tag - use default
#ifdef DEBUG
			cout << "[FP S RENAME] no previous - use default [" << pirat_fp[dv.warpid].default_reg[source] << "]" << endl;
#endif
			update_dv_source(dv, pirat_fp[dv.warpid].default_reg[source], 1, pirat_fp[dv.warpid].v_bit[source]); 
			statistics.pirat_fp_default_rd++;
		}
	}
}

/********************************************************
 Used for renaming sources and dests
Phi instructions - is_partial = false -> default and partial merged to default
*********************************************************/
void DataAnalyser::update_PIRAT_entry(int new_reg, bool is_partial, bool mt_bit, int tag, int entry, bool is_int, int warp_no){
	if(is_int){ //its and int PIRAT entry

		//valid bits
		if(is_partial){
			pirat_int[warp_no].partial_reg[entry] = new_reg;
			pirat_int[warp_no].mt_bit[entry] = mt_bit; 
			pirat_int[warp_no].mask_tag[entry] = tag;
			pirat_int[warp_no].r_bit[entry] = false;
		}else{
			pirat_int[warp_no].default_reg[entry] = new_reg;
			pirat_int[warp_no].mt_bit[entry] = mt_bit; 
			pirat_int[warp_no].mask_tag[entry] = -1;
			pirat_int[warp_no].v_bit[entry] = false;
			//for phi uop - set partial fields to invalid
			pirat_int[warp_no].r_bit[entry] = false;
		}	
		
	}else{ //its a fp entry
		if(is_partial){
			pirat_fp[warp_no].partial_reg[entry] = new_reg;
			pirat_fp[warp_no].mt_bit[entry] = mt_bit; 
			pirat_fp[warp_no].mask_tag[entry] = tag;
			pirat_fp[warp_no].r_bit[entry] = false;
		}else{ 
			pirat_fp[warp_no].default_reg[entry] = new_reg;
			pirat_fp[warp_no].mt_bit[entry] = mt_bit; 
			pirat_fp[warp_no].mask_tag[entry] = -1;
			pirat_fp[warp_no].v_bit[entry] = false;
			//for phi uop - set partial fields to invalid
			pirat_fp[warp_no].r_bit[entry] = false;
		}
	}
}

/////////////////////////////////////////
//     DEST RENAME WITH PIRAT
/////////////////////////////////////////
void DataAnalyser::rename_dest_pirat(std::vector<int> dest, std::vector<int> type, int num_dest, DVInst &dv, std::vector<int> &ow, std::vector<int> &owl, 
			std::vector<int> &owt, int i){ //ow will have same num of elements as dest

	int destination = dest[i]; //number of architectural register
		
		if(!type[i]){ //INT PIRAT//////////////////////////////////////////////////////////////////////
			statistics.pirat_wr_accesses++;
			//get a free physical reg
			int tmp = get_free_intreg(dv, false); //get a free physical reg
			ow.push_back(pirat_int[dv.warpid].default_reg[destination]); //keep previous value for ROB
			owl.push_back(destination); owt.push_back(0);
			//Merge and update PIRAT
#ifdef DEBUG
			cout << "[INT D Rename] generating a PHI uop - phi reg"; // << endl; //and generate phi
			cout << "[" << tmp << "] dest = " << destination << " w = " << dv.warpid << endl;
			cout << "replace: " << pirat_int[dv.warpid].default_reg[destination] << endl;
#endif
			dv.is_phi = true; //TEMPORARY for readback to make phi instructions		
			dv.default_reg.push_back(pirat_int[dv.warpid].default_reg[destination]);						
			dv.partial_reg.push_back(tmp); //tmp <- default AND NOT mask, (+) tmp AND mask
			dv.phi_reg.push_back(tmp);
			dv.reg_type.push_back(0);
			dv.phi_dests.push_back(destination);
			statistics.write_merges++;

			update_PIRAT_entry(tmp, false, false, dv.tag, destination, true, dv.warpid);
			update_dv_dest(dv, tmp, 0, destination);	


		}else{//FP/SIMD PIRAT ////////////////////////////////////////////////////////
			statistics.pirat_fp_wr_accesses++;
			int tmp = get_free_FPreg(dv, false); //get a free physical reg
			ow.push_back(pirat_fp[dv.warpid].default_reg[destination]); //keep previous value for ROB
			owl.push_back(destination); owt.push_back(1);
#ifdef DEBUG
			cout << "[FP D Rename] generating a PHI uop = reg phi "; // << endl; //and generate phi
			cout << "[" << tmp << "] arch = " << destination << " w = " << dv.warpid << endl;
			cout << "replace: " << pirat_fp[dv.warpid].default_reg[destination] << endl;
#endif
			dv.is_phi = true; //TEMPORARY for readback to make phi instructions		
			dv.default_reg.push_back(pirat_fp[dv.warpid].default_reg[destination]);						
			dv.partial_reg.push_back(tmp); 
			dv.phi_reg.push_back(tmp);
			dv.reg_type.push_back(1);
			dv.phi_dests.push_back(destination);
			statistics.write_fp_merges++;

			//Merge and update PIRAT
			update_PIRAT_entry(tmp, false, false, dv.tag, destination, false, dv.warpid);
			update_dv_dest(dv, tmp, 1, destination);
		}
}



LONG DataAnalyser::generate_DVNum(){
	dv_count++;	

	return dv_count;
}

bool DataAnalyser::ROB_check(){
	bool insert;

#ifdef FREE_PHI
	LONG count = 0;
	for(std::list<DVInst>::iterator it = reorderB.inst.begin(); it != reorderB.inst.end(); it++){
		if(!(*it).is_phi) count++;
	}	
	if(count + 1 < conf.rob_size)
		insert = true;
	else
		insert = false;
#else
	if(reorderB.data_valid.size() + 1 < conf.rob_size)
		insert = true;
	else
		insert = false;
#endif

	return insert;
}

bool DataAnalyser::enough_registers(DVInst inst, bool &rename_stalled){
 	//search INT
	bool free_reg_int = false, free_reg_fp = false;
	int reg_free = 0;

	std::list<int>::iterator reg = freelist_int.reg.begin();
	for(std::list<bool>::iterator free = freelist_int.taken.begin(); free != freelist_int.taken.end(); free++){
		if(!(*free)){
			reg_free++;
			
		}
		reg++;
	}

	if(reg_free >= 4) free_reg_int = true;

	//search FP
	reg_free = 0;
	reg = freelist_fp.reg.begin();
	for(std::list<bool>::iterator free = freelist_fp.taken.begin(); free != freelist_fp.taken.end(); free++){
		if(!(*free)){
			reg_free++;
			
		}
		reg++;
	}
	
	if(reg_free >= 3) free_reg_fp = true;

	if(!(free_reg_int && free_reg_fp)){ 
		if(!rename_stalled && ROB_check() && !inst.is_uop){
			statistics.reg_rename_stalls++;
			rename_stalled = true;
		}
#ifdef DEBUG
		cout << "NOT ENOUGH REGISTERS" << endl;
#endif
	}

	return free_reg_int && free_reg_fp;
}

bool DataAnalyser::pirat_stall_check(std::vector<std::vector<int> > waw_dest, std::vector<std::vector<int> > waw_dest_type, std::vector<int> waw_warp){
	int waw_count = 0; //counts number of waw
	int row_counter = 1;
	bool stall = false;
	//nest these loops
	std::vector<std::vector<int> >::iterator it_dest = waw_dest.begin(), it_dest2 = waw_dest.begin(); //outer, inner
	std::vector<std::vector<int> >::iterator it_dest_type = waw_dest_type.begin(), it_dest_type2 = waw_dest_type.begin();
	std::vector<int> ::iterator it_warp = waw_warp.begin(), it_warp2 = waw_warp.begin();

	for( ; it_dest != waw_dest.end(), it_dest_type != waw_dest_type.end(), it_warp != waw_warp.end(); it_dest++, it_dest_type++, it_warp++){ //for each dest
		std::vector<int>::iterator it_d = (*it_dest).begin(); 
		std::vector<int>::iterator it_t = (*it_dest_type).begin(); 

		for(; it_d != (*it_dest).end(), it_t != (*it_dest_type).end(); it_d++, it_t++){ //destination to be sought for = *it_d
			it_dest2 = waw_dest.begin(); 
			it_dest_type2 = waw_dest_type.begin();
			it_warp2 = waw_warp.begin();

			if((*it_dest).size() > row_counter){ //advance the iterator to the next row and check for waw
				std::advance(it_dest2, row_counter);		
				std::advance(it_dest_type2, row_counter);
				std::advance(it_warp2, row_counter);
			}else break;

			for( ; it_dest2 != waw_dest.end(), it_dest_type2 != waw_dest_type.end(); it_dest2++, it_dest_type2++){ //check next row for WAW matches
				std::vector<int>::iterator it_d2 = (*it_dest2).begin(); 
				std::vector<int>::iterator it_t2 = (*it_dest_type2).begin(); 
				for(; it_d2 != (*it_dest2).end(), it_t2 != (*it_dest_type2).end(); it_d2++, it_t2++){
					if(*it_warp == *it_warp2 && *it_d == *it_d2 && *it_t == *it_t2){
						waw_count++;
#ifdef DEBUG
							cout << "FOUND A WAW for dest = " << *it_d << endl;
#endif
						if(waw_count >= 2){
#ifdef DEBUG
							cout << "[WAW] >> WAW STALL << [WAW]" << endl;
#endif
						 	stall = true;
						}
					}
				}
			}
		}
		if((*it_dest).size() <= row_counter) break;
		row_counter++;
	}		
	return stall;
}

/****************************************************************************************
   RENAME - goes through the decode_to_rename queue and renames the dv-inst, sources then dests
   -References the PIRAT for renaming
   -Generates PHI instructions for merging
   -All dv-inst are then put in the ROB which acts as a temporary queue until
    issue_push() is called which places the instructions in their respective
    issue queues
***************************************************************************************/
void DataAnalyser::rename(){ 
	int num_src = 0, num_dest = 0, count = -1;
	std::vector<int> src, type, dest, typed, ow, ows, owl, owls, owt;
	bool discard = true;
	int rename_count = 0;
	bool rename_stalled = false, structural_hazard = false;
	int num_phi_cycle_int = 0, num_phi_cycle_fp = 0;
	int phis_gen_line = 0;
	std::vector<std::vector<int> > waw_dest, waw_dest_type;
	std::vector<int> waw_warp;

	std::deque<DVInst>::iterator it = decode_to_rename.begin();
#ifndef FREE_PHI
	if(!stall_next_cycle){
#endif
		while(rename_count < conf.fetch_width  && it != decode_to_rename.end() && enough_registers(*it, rename_stalled)){  //enforce max rename width 
			if(ROB_check()){ 
				DVInst dv_temp = *it;	
				discard = true; bool is_nop = false;
				ow.clear(); ows.clear(); owl.clear(); owls.clear(); owt.clear(); dest.clear(); src.clear(); type.clear(); typed.clear();
				num_src = 0; num_dest = 0; phis_gen_line = 0;

				if(!dv_temp.is_uop){
					count++;
#ifdef DEBUG
					cout << "INST {" << count << "}" << endl;
					if(dv_temp.is_branch) cout << "renaming a branch" << endl;				
#endif 
					for(int j = 0; j < threads_per_warp; j++){
						if(dv_temp.smask[j]){
							//Get sources
							for(int i = 0; i < dv_temp.insts[j].num_roperands; i++){
							
								if(is_GPR(dv_temp.insts[j].roperands[i])){//is GPR
									if(return_num(dv_temp.insts[j].roperands[i]) != -1){ //if number
										src.push_back(return_num(dv_temp.insts[j].roperands[i]));
										type.push_back(0); //i.e. GPR type
										num_src++; discard = false;
								
									}else{	//else gpr eax, etc
										assert(return_named_gpr(dv_temp.insts[j].roperands[i]) != -1);
										src.push_back(return_named_gpr(dv_temp.insts[j].roperands[i]));
								
										type.push_back(0);
										num_src++; discard = false;
								
									}
								}else if(is_simd_fp(dv_temp.insts[j].roperands[i])){ 
									src.push_back(return_num(dv_temp.insts[j].roperands[i]));
									type.push_back(1); //simd/fp type
									
									num_src++; discard = false;

								}else{//else dw about it - dbg, and/or PIN related register inclusions etc	
									//is_nop = true; //possibly true
				
								}
							}
							
							//Get dests
							for(int i = 0; i < dv_temp.insts[j].num_woperands; i++){
								//cout << return_reg(dv_temp.insts[j].woperands[i]) << endl;
								if(is_GPR(dv_temp.insts[j].woperands[i])){//is GPR
									if(return_num(dv_temp.insts[j].woperands[i]) != -1){ //if number
										dest.push_back(return_num(dv_temp.insts[j].woperands[i]));
										typed.push_back(0); //i.e. GPR type
										
										num_dest++; discard = false;
								
									}else{	//else gpr eax, etc
										assert(return_named_gpr(dv_temp.insts[j].woperands[i]) != -1);
										dest.push_back(return_named_gpr(dv_temp.insts[j].woperands[i]));
										
										typed.push_back(0);
										num_dest++; discard = false;
								
									}
								}else if(is_simd_fp(dv_temp.insts[j].woperands[i])){ 
									dest.push_back(return_num(dv_temp.insts[j].woperands[i]));
									typed.push_back(1); //simd/fp type
									
									num_dest++; discard = false;

								}else{//else dw about it - dbg, and/or PIN related register  etc	
									
								}
							}
							waw_dest.push_back(dest);//for waw checks
							waw_dest_type.push_back(typed); 
							waw_warp.push_back(dv_temp.warpid);						
							break;	//only necessary for the first active thread of dv_temp warp					
						}		
					}

#ifdef DEBUG
					cout << "EBIT = " << (*it).exception_mask << endl;
#endif 

					//rename with PIRAT 
					if(!discard){
						//if(!dv_temp.is_nop)
						rename_sources_pirat(src, type, num_src, dv_temp, ows, owls);
										
						if(dv_temp.is_phi){ //generates phi instructions
							//create a phi instruction for every phi instruction generated by the instruction
							std::vector<int> tmp_dest, tmp_ptype; std::vector<char> tmp_dtype;	
			
							for(int k = 0; k < dv_temp.phi_reg.size(); k++){  
								DVInst phi_dv;
								phi_dv.create_phi(conf, dv_temp.default_reg[k], dv_temp.partial_reg[k], dv_temp.phi_reg[k], dv_temp.tag, dv_temp.reg_type[k], dv_temp.phi_dests[k]);
								phi_dv.warpid = dv_temp.warpid;
								statistics.phi_count++;
								//insert into ROB
								LONG robid = generate_DVNum();
								reorderB.id.push_back(robid); phi_dv.id = robid; 
#ifdef DEBUG
								cout << "ROB_ID = " << robid << endl;
#endif
								reorderB.exception_mask.push_back((*it).exception_mask);
								reorderB.inst.push_back(phi_dv);
								
								std::vector<int> ow_temp, ow_loc, ow_type;
								ow_temp.push_back(ows[k]); 
								ow_loc.push_back(owls[k]); ow_type.push_back(dv_temp.reg_type[k]);
								if(!dv_temp.reg_type[k]){
									num_phi_cycle_int++; phis_gen_line++;
								}else{
									num_phi_cycle_fp++;  phis_gen_line++;
								}

								reorderB.ow_register.push_back(ow_temp);
								reorderB.ow_location.push_back(ow_loc); reorderB.ow_type.push_back(ow_type);
								reorderB.entry_valid.push_back(true);
								reorderB.data_valid.push_back(false);
								reorderB.issued.push_back(false); 
								reorderB.done.push_back(false); 
								//reorderB.warpid.push_back(phi_dv.warpid);

								tmp_dest.push_back(dv_temp.phi_reg[k]);  
								tmp_ptype.push_back(dv_temp.reg_type[k]);
								tmp_dtype.push_back('s');

								//clear current inst - IS NOT a phi, just generated phi(s)					
								reorderB.dest.push_back(tmp_dest);
								reorderB.dest_type.push_back(tmp_dtype);
								reorderB.pirat_type.push_back(tmp_ptype); 
								tmp_dest.clear(); tmp_ptype.clear(); tmp_dtype.clear();
							}
							
							dv_temp.default_reg.clear(); dv_temp.partial_reg.clear(); dv_temp.phi_reg.clear();
							dv_temp.is_phi = false;
							
						} 
					    std::vector<int> tmp_dest, tmp_ptype; std::vector<char> tmp_dtype;
					    std::vector<int> new_ow, new_owl, new_owt; //std::vector<int> ow_temp, ow_loc, ow_type;
					
						//if num dests == 1, but generates a phi then first inst will replace commit PIRAT ONLY. actual inst == -1
						//if num dests > 1, go through each inst. But they may individually generate a phi 
						
					    if(num_dest > 1){
						if(dest[0] == dest[1]){
							num_dest = 1;
						}
					    }		
					    for(int h = 0; h < num_dest; h++){
						ow.clear(); owl.clear(); owt.clear(); dv_temp.is_phi = false;
						//if(!dv_temp.is_nop)
						rename_dest_pirat(dest, typed, num_dest, dv_temp, ow, owl, owt, h); 
						
						if(dv_temp.is_phi){ //generates phi instructions
							//create a phi instruction for every phi instruction generated by the instruction	
							int erase_num = 0;
							for(int k = 0; k < dv_temp.phi_reg.size(); k++){  //change this TODO
								DVInst phi_dv;
								phi_dv.create_phi(conf, dv_temp.default_reg[k], dv_temp.partial_reg[k], dv_temp.phi_reg[k], dv_temp.tag, dv_temp.reg_type[k], dv_temp.phi_dests[k]);
								phi_dv.warpid = dv_temp.warpid;
								statistics.phi_count++;
								//insert into ROB
								LONG robid = generate_DVNum();
								reorderB.id.push_back(robid); phi_dv.id = robid; 
#ifdef DEBUG
								cout << "PHI ROB_ID = " << robid << endl;
#endif
								reorderB.exception_mask.push_back((*it).exception_mask);
								reorderB.inst.push_back(phi_dv);
								//deal with ow's						
								std::vector<int> ow_temp2, ow_loc2, ow_type2;
								ow_temp2.push_back(ow[k]); //cout << "ow pushing back " << ow[k] << endl;
								ow_loc2.push_back(owl[k]); ow_type2.push_back(owt[k]); //for the PHI uop
								if(!owt[k]){
									num_phi_cycle_int++; phis_gen_line++;
								}else{
									num_phi_cycle_fp++;  phis_gen_line++;
								}

								new_ow.push_back(-1); new_owl.push_back(-1); new_owt.push_back(-1); //for actual inst. 
								reorderB.ow_register.push_back(ow_temp2); 
								reorderB.ow_location.push_back(ow_loc2); reorderB.ow_type.push_back(ow_type2);
								
								//deal with dest now (esp considering multiple dest per DV-inst)
								tmp_dest.push_back(dv_temp.phi_reg[k]); tmp_dtype.push_back('s');
								tmp_ptype.push_back(dv_temp.reg_type[k]);

								reorderB.entry_valid.push_back(true);
								reorderB.data_valid.push_back(false);
								reorderB.issued.push_back(false); 
								reorderB.done.push_back(false);
								//reorderB.warpid.push_back(phi_dv.warpid);

								std::vector<int> tmp_dest2, tmp_ptype2; std::vector<char> tmp_dtype2;
								tmp_dest2.push_back(-1); tmp_dest.push_back(dv_temp.phi_reg[k]);  
								tmp_ptype2.push_back(-1); tmp_ptype.push_back(dv_temp.reg_type[k]);
								tmp_dtype2.push_back('s'); tmp_dtype.push_back('s');

								//clear current inst - IS NOT a phi, just generated phi(s)					
								reorderB.dest.push_back(tmp_dest2);
								reorderB.dest_type.push_back(tmp_dtype2);
								reorderB.pirat_type.push_back(tmp_ptype2); 
								erase_num++;
							}
							ow.erase(ow.begin(), ow.begin() + erase_num); owl.erase(owl.begin(), owl.begin() + erase_num);
							owt.erase(owt.begin(), owt.begin() + erase_num);  dv_temp.reg_type.clear(); dv_temp.phi_dests.clear();
							dv_temp.default_reg.clear(); dv_temp.partial_reg.clear(); dv_temp.phi_reg.clear();
							dv_temp.is_phi = false;
								
						}else{ // if(!dv_temp.is_nop){
							//push back ow to ow_temp2
							new_ow.push_back(ow[0]); new_owt.push_back(owt[0]); new_owl.push_back(owl[0]);	
							
							//and push back dest values
							tmp_dest.push_back(dest[h]); tmp_dtype.push_back(typed[h]); tmp_ptype.push_back('s');
						}
					      }								

						//put instruction in ROB (also acts as temporary buffer for next pipeline stage -- issue)
						LONG robid = generate_DVNum();
						reorderB.id.push_back(robid); dv_temp.id = robid; 
#ifdef DEBUG
						cout << "ROB_ID = " << robid << endl;
#endif
						reorderB.exception_mask.push_back((*it).exception_mask);
						reorderB.inst.push_back(dv_temp);
						reorderB.entry_valid.push_back(true); 
						reorderB.data_valid.push_back(false); 
						reorderB.issued.push_back(false); 
						reorderB.done.push_back(false);
						//reorderB.warpid.push_back(dv_temp.warpid);
						if(dv_temp.is_branch){ //Pin generated an empty branch. Push back NULL data so that ROB structure lists correspond
							cout << "BRANCH????" << endl;
							std::vector<int> tmp_dest, ow_temp; tmp_dest.push_back(-1);  
							std::vector<int> tmp_ptype; tmp_ptype.push_back(-1);
							std::vector<char> tmp_dtype; tmp_dtype.push_back('p');
							reorderB.dest.push_back(tmp_dest);
							reorderB.pirat_type.push_back(tmp_ptype);
							reorderB.dest_type.push_back(tmp_dtype);
							ow_temp.push_back(-1);
							reorderB.ow_register.push_back(ow_temp); reorderB.ow_location.push_back(ow_temp);
							reorderB.ow_type.push_back(ow_temp);
						}else{
							if(new_ow.empty()){ //<---------------------
								new_ow.push_back(-1);
								reorderB.ow_register.push_back(new_ow); reorderB.ow_location.push_back(new_ow);
								reorderB.ow_type.push_back(new_ow);
							}else{
								reorderB.ow_register.push_back(new_ow); reorderB.ow_location.push_back(new_owl);
								reorderB.ow_type.push_back(new_owt);
							}
							if(!tmp_dest.empty()){ //// <-------------------------------------------------------
									
								reorderB.dest.push_back(tmp_dest); 
								reorderB.pirat_type.push_back(tmp_ptype);
								reorderB.dest_type.push_back(tmp_dtype);
					
							}else{ 
								std::vector<int> tmp_dest; tmp_dest.push_back(-1); 
								std::vector<int> tmp_ptype; tmp_ptype.push_back(-1);
								std::vector<char> tmp_dtype; tmp_dtype.push_back('s');
								reorderB.dest.push_back(tmp_dest);
								reorderB.pirat_type.push_back(tmp_ptype);
								reorderB.dest_type.push_back(tmp_dtype);
							}
						}
					  
					}else{
						//some branches have no sources for some reason (?). No dest
						if(dv_temp.is_branch){ 
#ifdef DEBUG
							cout << "Found empty branch" << endl;
#endif
							LONG robid = generate_DVNum();
							reorderB.id.push_back(robid); dv_temp.id = robid; 
#ifdef DEBUG
							cout << "ROB_ID = " << robid << ", tag = " << dv_temp.tag << endl;
#endif
							reorderB.exception_mask.push_back((*it).exception_mask);
							reorderB.inst.push_back(dv_temp);
							std::vector<int> ow_temp; ow_temp.push_back(-1);
							reorderB.ow_register.push_back(ow_temp); reorderB.ow_location.push_back(ow_temp);
							reorderB.ow_type.push_back(ow_temp);
							reorderB.entry_valid.push_back(true);
							reorderB.data_valid.push_back(false);
							reorderB.issued.push_back(false); 
							reorderB.done.push_back(false);
							//reorderB.warpid.push_back(dv_temp.warpid);
		
							std::vector<int> tmp_dest, tmp_ptype; tmp_dest.push_back(-1); tmp_ptype.push_back(-1); 
							std::vector<char> tmp_type; tmp_type.push_back('p');
					
							reorderB.dest.push_back(tmp_dest);
							reorderB.dest_type.push_back(tmp_type);
							reorderB.pirat_type.push_back(tmp_ptype);

						}else{
						 	//No-op type of instruction
							//if((*it).exception_mask != 0){
								LONG robid = generate_DVNum();
								reorderB.id.push_back(robid); dv_temp.id = robid; 
								//reorderB.warpid.push_back(dv_temp.warpid);
#ifdef DEBUG	
								cout << "ROB_ID = " << robid << endl; //
#endif
								reorderB.exception_mask.push_back((*it).exception_mask);
								reorderB.inst.push_back(dv_temp);
								reorderB.entry_valid.push_back(true); 
								reorderB.data_valid.push_back(false); 
								reorderB.issued.push_back(false); 
								reorderB.done.push_back(false);
								std::vector<int> tmp_dest; tmp_dest.push_back(-1); 
								std::vector<int> tmp_ptype; tmp_ptype.push_back(-1);
								std::vector<char> tmp_dtype; tmp_dtype.push_back('s');
								reorderB.ow_register.push_back(tmp_dest); reorderB.ow_location.push_back(tmp_dest);
								reorderB.ow_type.push_back(tmp_dest); 
								reorderB.dest.push_back(tmp_dest);
								reorderB.pirat_type.push_back(tmp_ptype);
								reorderB.dest_type.push_back(tmp_dtype);
						}
					}
		
				}else{ //is a uop. Simply place in ROB 
#ifdef DEBUG
					cout << "Found uop ";
#endif
					LONG robid = generate_DVNum();
					reorderB.id.push_back(robid); dv_temp.id = robid;
					//reorderB.warpid.push_back(dv_temp.warpid);
#ifdef DEBUG
					cout << "ROB_ID = " << robid << endl;
#endif
					reorderB.exception_mask.push_back((*it).exception_mask);
					reorderB.inst.push_back(dv_temp);
					reorderB.entry_valid.push_back(true);
					reorderB.data_valid.push_back(false);
					reorderB.issued.push_back(false); 
					reorderB.done.push_back(false);

					std::vector<int> tmp_dest, tmp_ptype; tmp_dest.push_back(-1); tmp_ptype.push_back(-1); 
					reorderB.ow_register.push_back(tmp_dest); reorderB.ow_location.push_back(tmp_dest);
					reorderB.ow_type.push_back(tmp_dest);
					std::vector<char> tmp_type; tmp_type.push_back('p');
					
					reorderB.dest.push_back(tmp_dest);
					reorderB.dest_type.push_back(tmp_type);
					reorderB.pirat_type.push_back(tmp_ptype);
				}

				//gather sources and dests for waw/war stall checks
				if(phis_gen_line > 1) structural_hazard = true;
				src.clear(); type.clear(); num_src = 0;
				dest.clear(); typed.clear(); num_dest = 0; ow.clear(); ows.clear(); owl.clear(); owls.clear(); owt.clear();

				rename_count++; 
				it++; 
			}else{ statistics.rob_stalls++; break; } //end if
		}
		//}//end for

		for(int i = 0; i < rename_count; i++){
			decode_to_rename.pop_front(); //emtpy out DV-inst in decode queue that have been renamed
		}

		//gather phi statistics here
		if(rename_count > 0){
#ifndef FREE_PHI
			//gather stats on instruction block to determine stalls
			if(structural_hazard){ 
				statistics.pirat_stalls++; //due to > 1 phi generated per line
				stall_next_cycle = true;
			}else{
				if(pirat_stall_check(waw_dest, waw_dest_type, waw_warp)){ //due to waw hazard
					statistics.waw_stalls++;
					stall_next_cycle = true;
				}
			}
#endif
			statistics.phi_frequency[num_phi_cycle_int]++; //calculations per fetch block
			statistics.phi_frequency_fp[num_phi_cycle_fp]++;
		}
#ifndef FREE_PHI
	}else{
		stall_next_cycle = false;
	}
#endif
	
}

////////////////////////////////////////////////////
//	Debug Helper Function for issueQ
//////////////////////////////////////////////////

void DataAnalyser::print_IQ(QueueTypes type){
	int count = 0;
#ifdef DEBUG
	cout << endl << "!!!!!!!!!!!!contents of issueQ[" << type << "]" << endl;
#endif
	if(!issueQ[type].sources.empty() ){ 
		std::vector<std::vector<int> >::iterator it_src, psource; 
		std::vector<int>::iterator in_src; 

		std::vector<std::vector<bool> >::iterator it; 
		std::vector<bool>::iterator in, exe = issueQ[type].execute.begin();

		std::vector<LONG>::iterator it_rob = issueQ[type].rob_id.begin();

		std::vector<DVInst>::iterator it_dv = issueQ[type].iq.begin();
		std::vector<int>::iterator it_dest = (*it_dv).writes.begin(), ptype = (*it_dv).pirat_wtype.begin();


		std::vector<int>::iterator pirat_src; psource = issueQ[type].pirat_src.begin();
		std::vector<char>::iterator srcpath = issueQ[type].dest_type.begin();

		for(it_src = issueQ[type].sources.begin(), it = issueQ[type].ready.begin();
				it_src != issueQ[type].sources.end(), it != issueQ[type].ready.end(); ++it_src, ++it){
#ifdef DEBUG
			 cout << "entry " << count << ":" << endl;  
#endif
			pirat_src = psource->begin();
			 


			 for(in_src = it_src->begin(), in = it->begin(); in_src != it_src->end(), in != it->end(); ++in_src, ++in){
#ifdef DEBUG
				cout << "Source " << (*in_src) << " (" << *pirat_src << ") Ready = " << (*in) << " | ";
#endif
				pirat_src++;						
			 } 
#ifdef DEBUG
			if(!(*it_dv).writes.empty()){ 
				for(; it_dest != (*it_dv).writes.end(); it_dest++ ){
					cout << " | (Dest = " << (*it_dest) << ", type = " << *srcpath << ", " << *ptype << ")  "; 
					//ptype++;
				}
			}
			cout << " |  ROB_ID = " << (*it_rob) << "{exe = " << *exe << "} ((WARP = " << (*it_dv).warpid << "))" << endl;
#endif
			 count++; ++it_rob; it_dv++; srcpath++; it_dest = (*it_dv).writes.begin(); ptype = (*it_dv).pirat_wtype.begin(); ++exe;	

			psource++;

		}
	}
#ifdef DEBUG
	cout << endl;
#endif
}

int DataAnalyser::get_issueQ_size(){
	int total = 0;

	for(int i = 0; i != NO_QUEUE_TYPES; i++){
#ifdef FREE_PHI
		if((QueueTypes)i != PHI){
			total += issueQ[i].rob_id.size();
		}
#else
		total += issueQ[i].rob_id.size();
#endif
	}
	return total;
}

/****************************************************************************************
    issue_push() responsible for: rename to issue stage
	-pushing new ROB contents (inserted @ rename) to their respective issue queue types
	-classification:
				IQ0 = Phi
				IQ1 = cuop
				IQ2 = FP
				IQ3 = INT
				IQ4 = MEM
				IQ5 = branch
	-NOTE: IQs share FUs - distributed just for coding organizational purposes
*****************************************************************************************/
void DataAnalyser::issue_push(){
#ifdef DEBUG
	cout << "Issuing Instruction:  " << endl;
#endif
	int count = -1;
		//check ROB entries for non-issued
		std::list<DVInst>::iterator dvi = reorderB.inst.begin();
		std::list<bool>::iterator it; std::list<LONG>::iterator it_id; 
	
		for(it = reorderB.issued.begin(), it_id = reorderB.id.begin(); it != reorderB.issued.end(), it_id != reorderB.id.end(); it++, it_id++){
			if(get_issueQ_size() > conf.iq_size){ statistics.issue_stalls++; break;}
			count++;

			if(!(*it)){ //place DV-Inst in respective issue queue
#ifdef DEBUG
			
				cout << count << " == ";
#endif
				//////////////////////////////////////////////////////////////////////////////////////////////////////
				//in case of a phi instruction, Pdefault and Ppartial may only be merged to Pphi if path is VALID
				//therefore, must snoop broadcast network for path (c_uop) and/or check path table for R=1 (source = path)	
				/////////////////////////////////////////////////////////////////////////////////////////////////////
				if(dvi->is_phi){ 
#ifdef DEBUG
					cout << "phi" << endl;
#endif
					issueQ[PHI].iq.push_back(*dvi);
					std::vector<int> tmp_source, pirat_src;
					tmp_source.push_back((*dvi).tag); //i.e. can execute when tag is executed
					pirat_src.push_back(2); //2 = not applicable

					//check path table for valid path, then set stat
					std::vector<bool> tmp_stat; std::vector<char> tmp_type;
					if(!(pathtable[(*dvi).warpid].rbit[(*dvi).tag])){ 
						tmp_stat.push_back(false);		
					}else{
						tmp_stat.push_back(true);		
					}
					tmp_type.push_back('p');

					//next get sources (default and partial register) with ready state
					int k = 0;
					for(std::vector<int>::iterator src = (*dvi).reads.begin(); src != (*dvi).reads.end(); src++){ 	
#ifdef DEBUG
						cout << "[PHI] generating src for " << *src << endl;
#endif
						tmp_source.push_back(*src); 
						tmp_type.push_back('s'); pirat_src.push_back((*dvi).pirat_type[k]);
						if((*dvi).pirat_type[k] == 0){//if int
							tmp_stat.push_back(prf.valid[*src]);
						}else{
							tmp_stat.push_back(vrf.valid[*src]);
						}
						k++;
					}
					issueQ[PHI].dest_type.push_back('s'); (*dvi).dest_type.push_back('s');
					issueQ[PHI].sources.push_back(tmp_source);
					issueQ[PHI].pirat_src.push_back(pirat_src);
					issueQ[PHI].ready.push_back(tmp_stat);		
					issueQ[PHI].execute.push_back(0); //these stats will be set in another function
					issueQ[PHI].src_type.push_back(tmp_type);
					issueQ[PHI].rob_id.push_back(*it_id);
					issueQ[PHI].warp_no.push_back((*dvi).warpid);
					(*it) = true; //set ROB status to DVInst has been sent to issue
	
				////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// incase of a c_uop, execute = the 2 paths == converged
				// Assume cuops may execute asap. 
				//////////////////////////////////////////////////////////////////////////////////////////////////////////
				}else if(dvi->is_uop){ 
#ifdef DEBUG
					cout << "uop" << endl;
#endif
					issueQ[CUOP].iq.push_back(*dvi);
					std::vector<int> tmp_source, pirat_src;  std::vector<char> tmp_type;	
					std::vector<bool> tmp_stat;
				
					tmp_stat.push_back(true); pirat_src.push_back(2);
					tmp_source.push_back(-1); 
					tmp_type.push_back('p');
					issueQ[CUOP].dest_type.push_back('p'); (*dvi).dest_type.push_back('p');
					issueQ[CUOP].sources.push_back(tmp_source);
					issueQ[CUOP].pirat_src.push_back(pirat_src);
					issueQ[CUOP].src_type.push_back(tmp_type);
					issueQ[CUOP].ready.push_back(tmp_stat);
					issueQ[CUOP].execute.push_back(0);
					issueQ[CUOP].rob_id.push_back(*it_id);
					issueQ[CUOP].warp_no.push_back((*dvi).warpid);
					(*it) = true; 

				/////////////////////////////////////////////////////////////////////////////////////////////////////
				// -in case of a branch inst, the register/condition must be ready, as well as the path valid
				// -branch must therefore snoop broadcast network for reg + path table R = 1
				////////////////////////////////////////////////////////////////////////////////////////////////////
				}else if(dvi->is_branch){ //BR DV_instruction  -- registers must be ready + path is valid
#ifdef DEBUG
					cout << "BRANCH, tag = " << (dvi->tag) <<  endl;
#endif
					issueQ[BR].iq.push_back(*dvi);
					std::vector<int> tmp_source, pirat_src; 
					std::vector<bool> tmp_stat;   std::vector<char> tmp_type;

					//if the branch generated a new entry in the pathtable
					if(dvi->new_path){
						tmp_source.push_back((*dvi).pathT); //tag for previous path -- needs to be ready first
						tmp_type.push_back('p'); pirat_src.push_back(2);
						//check path table for R, then set stat
						if(!(pathtable[(*dvi).warpid].rbit[(*dvi).pathT]))
							tmp_stat.push_back(false);
						else
							tmp_stat.push_back(true);	
					}
				
					int k = 0;
					for(std::vector<int>::iterator src = (*dvi).reads.begin(); src != (*dvi).reads.end(); src++){ 	
						tmp_source.push_back(*src); 
						tmp_type.push_back('s'); pirat_src.push_back((*dvi).pirat_type[k]);
						if((*dvi).pirat_type[k] == 0){
							tmp_stat.push_back(prf.valid[*src]);
						}else{
							tmp_stat.push_back(vrf.valid[*src]);
						}
						k++;
					}
					//for empty generated branches (should not be the case.. but just incase avoid a bug)
					if(tmp_source.empty()){
						tmp_stat.push_back(true); tmp_source.push_back(-1); tmp_type.push_back('p');
						pirat_src.push_back(2);
					}

					issueQ[BR].dest_type.push_back('p'); (*dvi).dest_type.push_back('p');
					issueQ[BR].sources.push_back(tmp_source); 
					issueQ[BR].pirat_src.push_back(pirat_src);
					issueQ[BR].src_type.push_back(tmp_type);
				
					issueQ[BR].ready.push_back(tmp_stat);	
					issueQ[BR].execute.push_back(0);	
					issueQ[BR].rob_id.push_back(*it_id);
					issueQ[BR].warp_no.push_back((*dvi).warpid);
					(*it) = true; //set ROB status to DVInst has been sent to issue

				////////////////////////////////////////////////////////////////////////////////////////////////////
				// Is a conventional pipeline instruction. Find type and place in respective queue 
				////////////////////////////////////////////////////////////////////////////////////////////////////
				}else{  
					int place;
					if(dvi->has_load || dvi->has_store){ //memory instruction
						place = MEM; //cout << " MEM ";
						if(dvi->has_load){ //cout << " (load) ";
							//place loads in the LQ
							int tmp_mask[threads_per_warp], count = 0;
							convert_int_to_vectormask((*dvi).actual_mask, tmp_mask, threads_per_warp); //get_mask_pathTable((*dvi).tag, (*dvi).warpid)
							
							for(std::vector<DVLoad>::iterator ll = dvi->loads.begin(); ll != dvi->loads.end(); ll++){
								for(std::vector<AddressLoadParams>::iterator addr = (*ll).address_params.begin(); addr != (*ll).address_params.end(); addr++){
									if(tmp_mask[count] && (*addr).address > 0){
										loadQ.address.push_back((*addr).address);
										loadQ.valid.push_back(false);
										loadQ.id.push_back(dvi->id);	
									}	
									count++;						
								}
							}
						}else{ //place stores in the SQ (early entry)
							//place loads in the LQ
							int tmp_mask[threads_per_warp], count = 0; //cout << " (store) ";
							convert_int_to_vectormask(get_mask_pathTable((*dvi).tag, (*dvi).warpid), tmp_mask, threads_per_warp); 
							
							//printSQ();

							for(std::vector<DVStore>::iterator ll = dvi->stores.begin(); ll != dvi->stores.end(); ll++){
								for(std::vector<AddressStoreParams>::iterator addr = ll->address_params.begin(); addr != ll->address_params.end(); addr++){
									if(tmp_mask[count] && (*addr).address > 0){
										storeQ.address.push_back((*addr).address);
										storeQ.valid.push_back(false);
										storeQ.id.push_back(dvi->id);
									}
									count++;
								}
							}
							//printSQ();
						}
					}else if(dvi->is_fp){			
						place = FP; //cout << " FP ";
					}else{ //is an INT based
						place = INT;  //cout << " INT ";
					}
					
				
					issueQ[place].iq.push_back(*dvi);
					//set source ready bits
					std::vector<int> tmp_source, pirat_src; std::vector<bool> tmp_stat;
					std::vector<char> tmp_type;
					std::vector<int>::iterator src = (*dvi).reads.begin(); 

					if((*dvi).sources == 0){ //no explicit sources. Just generates a dest
						issueQ[place].execute.push_back(1); 
						tmp_type.push_back('s');
						tmp_stat.push_back(true);
						tmp_source.push_back(-1);
						pirat_src.push_back(-1);
					}else{
						for(int k = 0; k < (*dvi).sources; k++){
							tmp_source.push_back(*src); 
							tmp_type.push_back('s');
							pirat_src.push_back((*dvi).pirat_type[k]);
							if((*dvi).pirat_type[k] == 0){ //pirat_type
								tmp_stat.push_back(prf.valid[*src]);
							}else{
								tmp_stat.push_back(vrf.valid[*src]);
							}
							src++;
						}
						issueQ[place].execute.push_back(0); 
					}
					issueQ[place].dest_type.push_back('s'); (*dvi).dest_type.push_back('s');
					issueQ[place].sources.push_back(tmp_source); 
					issueQ[place].pirat_src.push_back(pirat_src);
					issueQ[place].src_type.push_back(tmp_type);
					issueQ[place].ready.push_back(tmp_stat);			
					issueQ[place].rob_id.push_back(*it_id);
					issueQ[place].warp_no.push_back((*dvi).warpid);
				
					(*it) = true;
				}
#ifdef DEBUG
			cout << "rob ID [" << *it_id << "]" << endl;
#endif
			}
			dvi++;
		}
}

bool DataAnalyser::check_SQ(std::vector<LONG> addr, std::vector<bool> &valid){

	std::vector<LONG>::iterator find_addr = addr.begin(); std::vector<bool>::iterator val = valid.begin();

	for(; find_addr != addr.end(), val != valid.end(); find_addr++, val++){
		
		std::vector<LONG>::iterator aa = storeQ.address.begin();
		std::vector<bool>::iterator vv = storeQ.valid.begin();
		//if not in queue, will remain true (is ready)
		for(; aa != storeQ.address.end(), vv != storeQ.valid.end(); aa++, vv++){
			
			if((*find_addr) == (*aa)){ 
				(*val) = (*vv); //if in SQ, retrieves whatever the valid bit is		
			}
		}
	}
}

void DataAnalyser::printSQ(){
	std::vector<LONG>::iterator aa = storeQ.address.begin();
	std::vector<bool>::iterator vv = storeQ.valid.begin();
	std::vector<LONG>::iterator id = storeQ.id.begin();

	cout << "STOREQ CONTENTS" << endl;
	cout << "Valid |\t Address |\tROB ID" << endl;
	for(; aa != storeQ.address.end(), vv != storeQ.valid.end(), id != storeQ.id.end(); aa++, vv++, id++){
		cout << *vv << "\t" << hex16(*aa) << "\t" << *id <<endl;

	}

}

bool DataAnalyser::SQ_ready(LONG addr, int rob_id, int dep_id){
	std::vector<LONG>::iterator aa = storeQ.address.begin();
	std::vector<bool>::iterator vv = storeQ.valid.begin();
	std::vector<LONG>::iterator id = storeQ.id.begin();

	for(; aa != storeQ.address.end(), vv != storeQ.valid.end(), id != storeQ.id.end(); aa++, vv++, id++){
		if((addr) == (*aa) && (*id) < rob_id ){ 
			if(*vv) return true;
		}
	}

	//next check for the dep_id. If none present, this is done. if present, return false;
	aa = storeQ.address.begin(); vv = storeQ.valid.begin(); id = storeQ.id.begin(); 
	for(; aa != storeQ.address.end(), vv != storeQ.valid.end(), id != storeQ.id.end(); aa++, vv++, id++){
		if((addr) == (*aa) && (*id) == dep_id){ 
			if(*vv) return true;
			else return false;
		}
	}
	
	return true;
}

/*********************************************************************
	update_issueQ_entries() 
	  -Iterates through all entries of the issueQ, checks src valid bits
	  -If ALL operand sources valid, sets the instruction to ready for
	   execution (wakeup_dispatch helper function)
*********************************************************************/
void DataAnalyser::update_issueQ_entries(){
	for(int i = 0; i < NO_QUEUE_TYPES; i++){
		if(!issueQ[i].execute.empty() ){ 
			
			std::vector<bool>::iterator exe = issueQ[i].execute.begin();
			std::vector<LONG>::iterator rob = issueQ[i].rob_id.begin();

			

		
			for(std::vector<std::vector<bool> >::iterator it = issueQ[i].ready.begin(); it != issueQ[i].ready.end(); it++){
				bool max = true;
				for(std::vector<bool>::iterator in = it->begin(); in != it->end(); in++){
					if((*in) == false) max = false;  
				} 
				if(max){
					(*exe) = true; //is ready for execution 
				}
				if(*rob == reorderB.id.front() && (QueueTypes)i != MEM && !(*exe)){ //fixed bug?
					(*exe) = true;
				}
				
				exe++; rob++;
			} 

			if((QueueTypes)i == MEM){
				std::vector<bool>::iterator exe = issueQ[i].execute.begin(); //update "execution" bit if instruction ready to execute
				std::vector<std::vector<bool> >::iterator it = issueQ[i].ready.begin();
				std::vector<DVInst>::iterator inst = issueQ[i].iq.begin();

				for(; inst != issueQ[i].iq.end(), it != issueQ[i].ready.end(), exe != issueQ[i].execute.end(); inst++, it++, exe++){
					if(!(*exe)){
						if((*inst).has_store){
							//check sources	
							bool max = true;
							for(std::vector<bool>::iterator in = it->begin(); in != it->end(); in++){
								if((*in) == false) 
									max = false;  
							} 
							if(max){
								(*exe) = true;
							}

						}else if((*inst).has_load){ //same procedure, plus check SQ contents
							bool max = true;
							for(std::vector<bool>::iterator in = it->begin(); in != it->end(); in++){
								if((*in) == false) 
									max = false;  
							} 
					
							if(max){
								std::vector<bool> valid; 
								std::vector<LONG> addresses;
								int tmp_mask[threads_per_warp];
								convert_int_to_vectormask(get_mask_pathTable((*inst).tag, (*inst).warpid), tmp_mask, threads_per_warp); 
							
								int count = 0;
								for(std::vector<DVLoad>::iterator ll = (*inst).loads.begin(); ll != (*inst).loads.end(); ll++){
									std::vector<AddressLoadParams>::iterator aa = (*ll).address_params.begin();
									for(; aa != (*ll).address_params.end(); aa++){
										if(tmp_mask[count]){
											//check for duplicates
											bool found = false;
											for(std::vector<LONG>::iterator it_adr = addresses.begin(); it_adr != addresses.end(); it_adr++){
												if(*it_adr == (*aa).address){
													found = true;
													break;
												}
											}
											if(!found && (*aa).address != 0){
												addresses.push_back((*aa).address);
												valid.push_back(true);
											}
										}
										count++;
										(*aa).address_load_done = false; //load not started or finished at this point
									}			
								}
							
								check_SQ(addresses, valid);
					
								//go through valid, if any source false, can not dispatch yet. Waiting on SQ
								for(std::vector<bool>::iterator check = valid.begin(); check != valid.end(); check++){
									if(!(*check)){
										max = false;
										break;
									}
								}	
								if(max){
									(*exe) = true; 
#ifdef DEBUG
									cout << "[LOAD] ROB_ID = " << (*inst).id << endl; // my mask = " << get_mask_pathTable((*inst).tag) << endl;
#endif
								}
							}
						
						}
					}

				} //end for

			} //end if
			
		} //end if
	}//end for
}

/**********************************************************************
	removeIQ entry - used to remove IQ entry when sent off for exe
**********************************************************************/
void DataAnalyser::remove_IQ_entry(LONG rob_id, LONG id){

	std::vector<DVInst>::iterator it_dv = issueQ[id].iq.begin();
	std::vector<bool>::iterator it_exe = issueQ[id].execute.begin();
	std::vector<LONG>::iterator it_id = issueQ[id].rob_id.begin();
	std::vector<char>::iterator it_destType = issueQ[id].dest_type.begin();
	std::vector<int>::iterator it_warp = issueQ[id].warp_no.begin();

	std::vector<std::vector<int> >::iterator it_src = issueQ[id].sources.begin();
	std::vector<std::vector<bool> >::iterator it_rdy = issueQ[id].ready.begin();
	std::vector<std::vector<char> >::iterator it_type = issueQ[id].src_type.begin();
	std::vector<std::vector<int> >::iterator it_pirat = issueQ[id].pirat_src.begin();

	//find which element the ID is at
	int element = find(issueQ[id].rob_id.begin(), issueQ[id].rob_id.end(), rob_id) - issueQ[id].rob_id.begin();
	assert(issueQ[id].rob_id.end() != find(issueQ[id].rob_id.begin(), issueQ[id].rob_id.end(), rob_id));

	std::advance(it_dv, element); std::advance(it_exe, element);
	std::advance(it_src, element); std::advance(it_rdy, element);	
	std::advance(it_id, element); std::advance(it_type, element); 
	std::advance(it_pirat, element); std::advance(it_destType, element);
	std::advance(it_warp, element); 

	//it_src->clear(); it_rdy->clear();
	issueQ[id].iq.erase(it_dv); issueQ[id].execute.erase(it_exe);
	issueQ[id].sources.erase(it_src); issueQ[id].ready.erase(it_rdy);
	issueQ[id].rob_id.erase(it_id); issueQ[id].src_type.erase(it_type); 
	issueQ[id].pirat_src.erase(it_pirat); issueQ[id].dest_type.erase(it_destType);
	issueQ[id].warp_no.erase(it_warp);
}

void DataAnalyser::debug_cuop(LONG rob_id){
	//std::list<DVInst>::iterator dvi = reorderB.inst.begin()
	bool found = false;
	int count = 0; 

	std::list<LONG>::iterator r = reorderB.id.begin(); std::list<DVInst>::iterator inst = reorderB.inst.begin();
	for(; r != reorderB.id.end(), inst != reorderB.inst.end(); r++, inst++){
		if((*inst).is_uop) //cout << "Found a uop at ROB id " << *r << endl;
		if(*r != rob_id){
			//cout << "ENTRY WAS FOUND IN ROB" << endl;
			count++;
			//break;
		}
	}
	
	std::list<DVInst>::iterator ist = reorderB.inst.begin();
	std::advance(ist, count);

}

bool DataAnalyser::cache_request(std::vector<std::vector<int> > mask, int count, int length){
	std::vector<std::vector<int> >::iterator tt = mask.begin();
	std::advance(tt, count); 
	for(std::vector<int>::iterator it = (*tt).begin(); it != (*tt).end(); it++){
#ifdef DEBUG
		cout << (*it);
#endif
		if((*it)){
			return true;

		}
	}
	return false;
}

//1. stores are not reordered with other stores
//double check rob_id is oldest store in IQ.
bool DataAnalyser::tso_verify(LONG rob_id){
	std::vector<LONG>::iterator id = issueQ[MEM].rob_id.begin();
	std::vector<DVInst>::iterator inst = issueQ[MEM].iq.begin();
	std::vector<bool>::iterator exe = issueQ[MEM].execute.begin();

	for(; id != issueQ[MEM].rob_id.end(), inst != issueQ[MEM].iq.end(), exe != issueQ[MEM].execute.end(); id++, inst++, exe++){
		if((*id) >= rob_id || (*inst).has_load){
			continue; //finished checking all older entries
		}
		if((*inst).has_store && !(*exe) && (*id) < rob_id){ //search the older stores
			return false;
		}		
	}

	return true;
}

//2. Stores are NOT reordered w older loads
//double check (*it_dv).id is oldest id - compare to load ids in IQ
bool DataAnalyser::store_load_order(LONG rob_id){
	std::vector<LONG>::iterator id = issueQ[MEM].rob_id.begin();
	std::vector<DVInst>::iterator inst = issueQ[MEM].iq.begin();
	std::vector<bool>::iterator exe = issueQ[MEM].execute.begin();

	for(; id != issueQ[MEM].rob_id.end(), inst != issueQ[MEM].iq.end(), exe != issueQ[MEM].execute.end(); id++, inst++, exe++){
		if((*id) >= rob_id){
			continue; //finished checking all older entries
		}
		if((*inst).has_load && !(*exe) && rob_id > *id ){ 
			return false;
		}	

	}
	return true;
}
//3. Loads are NOT reorder w older stores to the same location
//check the rob ID against older stores. Check their address, if match return false
bool DataAnalyser::tso_ldstr(LONG rob_id){
	std::vector<LONG>::iterator id = issueQ[MEM].rob_id.begin();
	std::vector<DVInst>::iterator inst = issueQ[MEM].iq.begin();
	std::vector<bool>::iterator exe = issueQ[MEM].execute.begin();

	for(; id != issueQ[MEM].rob_id.end(), inst != issueQ[MEM].iq.end(), exe != issueQ[MEM].execute.end(); id++, inst++, exe++){
		if((*id) >= rob_id){
			continue; //finished checking all older entries
		}
		if((*inst).has_store && !(*exe) && rob_id > *id){ //theres an older store. Check addresses
			std::vector<LONG>addresses;
			get_addresses_for_tso(addresses, rob_id);

			for(std::vector<LONG>::iterator adr = addresses.begin(); adr != addresses.end(); adr++){
				for(std::vector<DVStore>::iterator str = (*inst).stores.begin(); str != (*inst).stores.end(); str++){
					for(std::vector<AddressStoreParams>::iterator addr = (*str).address_params.begin(); addr != (*str).address_params.end(); addr++){
						if((*addr).address == (*adr)){
							return false; //older ID and matching address
						}
					}
				}
			}
		}

	}
	return true;
}

void DataAnalyser::get_addresses_for_tso(std::vector<LONG> &addresses, LONG rob_id){
	std::vector<LONG>::iterator id = issueQ[MEM].rob_id.begin();
	std::vector<DVInst>::iterator inst = issueQ[MEM].iq.begin();

	for(; id != issueQ[MEM].rob_id.end(), inst != issueQ[MEM].iq.end(); id++, inst++){
		if((*id) == rob_id){
			int tmp_mask[threads_per_warp];	
			convert_int_to_vectormask(get_mask_pathTable((*inst).tag, (*inst).warpid), tmp_mask, threads_per_warp);		

			int count = 0;
			for(std::vector<DVLoad>::iterator ld = (*inst).loads.begin(); ld != (*inst).loads.end(); ld++){
				for(std::vector<AddressLoadParams>::iterator ad = ld->address_params.begin(); ad != ld->address_params.end(); ad++){
					if(tmp_mask[count] && (*ad).address > 0 && find(addresses.begin(), addresses.end(), (*ad).address) == addresses.end()){
						addresses.push_back((*ad).address);
					}
					count++;
				}
			}
		}
	}
}

/***********************************************************************
	wakeup_dispatch() responsible for:
	   -going through issueQs and searching for ready instructions
	   -if ready, is dispatched/scheduled to execute on its
	    respective FU for the next clock cycle
***********************************************************************/
void DataAnalyser::wakeup_dispatch(){
	int issue_count = 0;
	int thread_issue_count = 0; //update_issueQ_entries(); 
	bool no_more = false, no_ready[NO_QUEUE_TYPES];

	//check load updates()
	bool mem_scheduled = false;
	while(issue_count < conf.issue_width && !no_more){ 
		for(int i = 0; i < NO_QUEUE_TYPES; i++){		
			no_ready[i] = false;		
			if(!issueQ[i].execute.empty()){
				std::vector<DVInst>::iterator it_dv = issueQ[i].iq.begin();
				std::vector<LONG>::iterator it_id = issueQ[i].rob_id.begin();
				std::vector<bool>::iterator it_exe = issueQ[i].execute.begin();
				int count = 0;
				
				for(; it_exe != issueQ[i].execute.end(); ){			 
					if(*it_exe){ 		
						//assign instruction to a FU_DVInst
						FU_DVInst fu_dvi;
						fu_dvi.pc = (*it_dv).dv_pc;
						fu_dvi.is_fptype = (*it_dv).is_fp;
						fu_dvi.uop_type = (*it_dv).uop_type;
						fu_dvi.num_insts = (*it_dv).inst_count;
						fu_dvi.is_simd = (*it_dv).is_simd;
						fu_dvi.cycles_remaining = (*it_dv).cycles_remaining;
						fu_dvi.has_load = (*it_dv).has_load;
						fu_dvi.rob_id = (*it_id);
						fu_dvi.is_uop = (*it_dv).is_uop;
						fu_dvi.tag = (*it_dv).tag; fu_dvi.pathT = (*it_dv).pathT;
						fu_dvi.pathNT = (*it_dv).pathNT; 


#ifdef FREE_PHI
						if((QueueTypes)i == PHI){ //phi ready to execute in free_phi will exe without restrictions
							assert(fu.getfu(fu_dvi, (*it_dv).uop_type, i, (*it_id)));
							remove_IQ_entry((*it_id), i); //update the issueQ (remove inst)
						
						}else if((QueueTypes)i != MEM){
#else
						if((QueueTypes)i != MEM){	// IF NON-MEMORY instruction
#endif					
							if(fu.getfu(fu_dvi, (*it_dv).uop_type, i, (*it_id))){ //schedule on FU
#ifdef DEBUG
								if((*it_dv).is_uop){ 
									cout << "1) Executing a CUOP, masks are " << (*it_dv).pathT << ", " << (*it_dv).pathNT << ", tag = " << (*it_dv).tag;
									cout << " ROB_ID = " << *it_id  << endl;
								} else if((*it_dv).new_path){
									cout << "2) Executing a BUOP, prev tag is " << (*it_dv).pathT << ", tag = " << (*it_dv).tag << ", is branch = ";
									cout << (*it_dv).is_branch << ", ACTUAL MASK = " << (*it_dv).actual_mask << endl;
								}
#endif
								no_ready[i] = true;
#ifdef DEBUG
								cout << "Successfully scheduled the instruction, ROB ID = "<< *it_id << endl; 	
								if((*it_dv).is_branch) cout << "issued a branch" << endl;				
								
#endif													
								remove_IQ_entry((*it_id), i); //executing instruction -- update the issueQ (remove inst)
#ifdef DEBUG
								cout << "Entry removed. NEW IQ...." << endl; //print_IQ((QueueTypes)i);				
#endif
								issue_count++;
								break; //implements fairness among IQs
							}else{ ++it_exe; ++it_dv; ++it_id; }

						}else{ //is a <<Memory operation>>


//#ifdef DEBUGMEM
//							cout << "[Sched] Trying to sched ROB " << (*it_dv).id << ", and TSO_LDSTR = " << tso_ldstr((*it_dv).id) << endl;
//#endif
							if(!mem_scheduled){ //schedule on FU -  following section mimics LSU FU
								////////////////////////////////////	LOAD /////////////////////////
								//if its a load, scheduled on a FU which checks if there's an SQ match with DV = 1. Else send request to cache
								if((*it_dv).has_load){
									bool tso_load = true;
									tso_load = tso_ldstr((*it_dv).id);

									if(tso_load){
#ifdef DEBUGMEM
										cout << "[Sched] ---- load" << endl;
#endif
										(*it_dv).load_started = true;

										//get addresses to search for (from speculative mask) //////////////////////
										std::vector<LONG> addresses; std::vector<bool> found;
										int tmp_mask[threads_per_warp];
										convert_int_to_vectormask((*it_dv).actual_mask, tmp_mask, threads_per_warp); //get_mask_pathTable((*it_dv).tag, (*it_dv).warpid)
											

										//create a cache mask here, symbolizing which threads access SQ (0) and cache request (1)
										std::vector<std::vector<int> > cache_mask;
										int count = 0; 
										bool mark_valid = true;
										std::vector<int> push_mask;
										for(std::vector<DVLoad>::iterator ll = (*it_dv).loads.begin(); ll != (*it_dv).loads.end(); ll++){
											count = 0; 
											for(std::vector<AddressLoadParams>::iterator aa = (*ll).address_params.begin(); aa != (*ll).address_params.end(); aa++){
												if(tmp_mask[count] && (*aa).address != 0){
												
													bool inSQ = false; 
													std::vector<LONG>::iterator addr = storeQ.address.begin(); 
													std::vector<bool>::iterator valid = storeQ.valid.begin();
													std::vector<LONG>::iterator rid = storeQ.id.begin();
													for(; addr != storeQ.address.end(), valid != storeQ.valid.end(), rid != storeQ.id.end(); 
													    addr++, valid++, rid++){
														if((*aa).address == *addr && *rid < (*it_dv).id){ //address is present in storeQ. Check valid bit
															push_mask.push_back(0); inSQ = true; //no need to obtain from $ (.id eliminated duplicates)
															(*aa).storeQ = true;
															if(*valid){ //if valid, may obtain 
																(*aa).address_load_done = 2; //true; //valid store entry
#ifdef DEBUGMEM
																cout << "[LOAD] " << hex16((*aa).address) << " found in SQ, v = 1 " << endl;
#endif
															}else{ // if invalid
																(*aa).address_load_done = 2; //waiting on SQ
																(*aa).dependent_id = *rid;
#ifdef DEBUGMEN
																cout << "[LOAD] " << hex16((*aa).address) << " found in SQ, v = 0 " << endl;
#endif
																mark_valid = false; 
																(*aa).l1_miss = true;
																//(*aa).stat = true; statistics.l1_miss_r++;
																
															}
															break;
														}
													}
													if(!inSQ) push_mask.push_back(1); //not in SQ, must retrieve from cache
																			
												
												}else{	
													push_mask.push_back(0);
												}
												
											}
											cache_mask.push_back(push_mask);
											break; count++;
											
										}

										//check if any 0s present in cache mask
										int ld_count = 0; bool issuedTheLoad = false;
									
										for(std::vector<DVLoad>::iterator ll = (*it_dv).loads.begin(); ll != (*it_dv).loads.end(); ll++){

											if(!ll->load_issued){ //cache_request(cache_mask, ld_count, threads_per_warp)){
																	
												
												ll->load_issued = l1_cache.dv_load(&(*ll),number_cycles,1, &push_mask, (*it_dv).is_simd);
#ifdef DEBUGMEM
												if(ll->load_issued) cout << "[Cache] Issuing loads " << endl;
#endif
												mark_valid = false; 
											}else{
												ll->load_issued = true;
											}
											issuedTheLoad = ll->load_issued;
										//	ld_count++;
											break;
										}
										if(issuedTheLoad){ 
											(*it_dv).load_started = true;
											if(mark_valid){  //check to see if all loads completed (i.e. chance all in SQ)
												(*it_dv).load_done = true; 
											
											}else{
												(*it_dv).load_done = false;
											}
											//move load to their buffers (FUDVinst and DVInst type)										
											fu.getfu(fu_dvi, (*it_dv).uop_type, i, (*it_id)); //WIll exe once contents received from $
											waiting_on_loads.push_back(*it_dv);

											no_ready[i] = true;
	#ifdef DEBUGMEM
											cout << "Successfully scheduled the instruction, ROB ID = "<< *it_id << endl; 	//print_IQ((QueueTypes)i);
	#endif
								
											remove_IQ_entry((*it_id), i); //executing instruction -- update the issueQ (remove inst)
	#ifdef DEBUG
											cout << "Entry removed. NEW IQ...." << endl; //print_IQ((QueueTypes)i);				
	#endif																
										}
										mem_scheduled = true; //blocked
											//break; //implements fairness among IQs

									}else{++it_exe; ++it_dv; ++it_id; }

								}else{	//////////////////////// STORES //////////////// "calculates" effective addresses, and updates SQ 
									bool tso = true, store_load = true; 
									//Implement TSO (Total Store Ordering) 
									tso = tso_verify((*it_dv).id); 
									store_load = store_load_order((*it_dv).id);
									
									if(tso && store_load){
										if(fu.getfu(fu_dvi, (*it_dv).uop_type, i, (*it_id))){ //schedule on FU 
#ifdef DEBUGMEM
											cout << "[Sched] ---- store" << endl;
#endif
											//find addresses to store and mark as valid	
											std::vector<LONG> addresses;
											int tmp_mask[threads_per_warp]; int count = 0;
											convert_int_to_vectormask(get_mask_pathTable((*it_dv).tag, (*it_dv).warpid), tmp_mask, threads_per_warp); 
								
											for(std::vector<DVStore>::iterator ll = (*it_dv).stores.begin(); ll != (*it_dv).stores.end(); ll++){
												for(std::vector<AddressStoreParams>::iterator aa = (*ll).address_params.begin(); 
												    aa != (*ll).address_params.end(); aa++){

													if(tmp_mask[count]){//check for duplicates
														bool found = false;
														for(std::vector<LONG>::iterator it_adr = addresses.begin(); it_adr != addresses.end(); it_adr++){
															if(*it_adr == (*aa).address){
																found = true;
																break;
															}
														}
														if(!found){
															addresses.push_back((*aa).address);
														}
													} count++;
												}			
											}
					
											//mark as valid in SQ
											for(std::vector<LONG>::iterator addr = addresses.begin(); addr != addresses.end(); addr++){
												std::vector<LONG>::iterator find_addr = storeQ.address.begin();
												std::vector<bool>::iterator find_valid = storeQ.valid.begin();
												std::vector<LONG>::iterator find_id = storeQ.id.begin();
												for(; find_addr != storeQ.address.end(), find_valid != storeQ.valid.end(), find_id != storeQ.id.end(); 
												    find_addr++, find_valid++, find_id++){
													if((*addr) == (*find_addr) && (!(*find_valid)) && (*find_id) == (*it_dv).id){
#ifdef DEBUGMEM
														cout << "[StoreQ] Updating v = 1 for ADDRESS = " << hex16(*addr) << "[" << (*it_dv).id << "]" << endl;		
#endif
														(*find_valid) = true;	//computed but not stored to cache		
													}
												}
											}

											no_ready[i] = true;
#ifdef DEBUGMEM
											cout << "[STORE] Successfully scheduled the instruction, ROB ID = "<< *it_id << endl; 	//print_IQ((QueueTypes)i);
#endif
											remove_IQ_entry((*it_id), i); //executing instruction -- update the issueQ (remove inst)
#ifdef DEBUG
											cout << "[STORE] Entry removed. NEW IQ...." << endl; //print_IQ((QueueTypes)i);				
#endif
											mem_scheduled = true;
											//break; //implements fairness among IQs
										}
									}else {++it_exe; ++it_dv; ++it_id; }
								}
								
								

							}else { //cout << "couldn't schedule [MEM] ROB_ID = " << *it_id << endl; 
								++it_exe; ++it_dv; ++it_id; }
						}
					}else {++it_exe; ++it_dv; ++it_id;  }
				}
			}
		}
		//check if no more insts are ready -- also an exit criteria for wakeup	
		int count = 0;
		for(int i = 0; i < NO_QUEUE_TYPES; i++){ //do not consider memory for now
			if(!no_ready[i]) count++;	
		}
		if(count == (NO_QUEUE_TYPES)) no_more = true;			
	}
}


/*******************************************************************
  Searches the ROB for instructions to commit (oldest first)
********************************************************************/
void DataAnalyser::release_register(int reg, int pirat_type){
	if(!pirat_type){//int
		std::list<int>::iterator it = freelist_int.reg.begin(); std::list<bool>::iterator rr = freelist_int.taken.begin();	
		for(; it != freelist_int.reg.end(), rr != freelist_int.taken.end(); it++, rr++){

			if((*it) == reg){
				(*rr) = false;
				break;
			}
		}
	
	}else{ //FP
		std::list<int>::iterator it = freelist_fp.reg.begin(); std::list<bool>::iterator rr = freelist_fp.taken.begin();
		for(; it != freelist_fp.reg.end(), rr != freelist_fp.taken.end(); it++, rr++){

			if((*it) == reg){
				(*rr) = false;
				break;
			}
		}
	}
}

bool DataAnalyser::assert_ow_correct(int wmask, int rmask){
	int warp_mask[threads_per_warp], reg_mask[threads_per_warp];
	convert_int_to_vectormask(wmask, warp_mask, threads_per_warp);
	convert_int_to_vectormask(rmask, reg_mask, threads_per_warp);
	
	bool counter = false;
	if(div_counter == 0) counter = true; 
	
	//bool is_super = strict_superset(reg_mask, warp_mask, threads_per_warp);
	return strict_superset(reg_mask, warp_mask, threads_per_warp);
	//return (counter & is_super);
	
}

bool DataAnalyser::in_ISTable(int path, int wrp){
	for(int i = 0; i < isTable[wrp].path.size(); i++){
		if(isTable[wrp].path[i] == path)
			return true;
	}
	return false;

}

void DataAnalyser::evict_pathtable_entries(){//<-----------------------------------------------------
	//update the Pathtable - clear entries if path not required
	for(int j = 0; j < conf.num_warps; j++){
		for(int i = 0; i < pathtable[j].mask.size(); i++){
			if(pathtable[j].ref_counter[i] == 0 && !in_ISTable(i,j)){ //valid entry (-1 = invalid)
				//invalidate this entry
				pathtable[j].rbit[i] = -1;
				pathtable[j].mask[i] = -1;
				pathtable[j].ref_counter[i] = -1;
				pathtable[j].commit_ptr = i;
	#ifdef DEBUG
				cout << "[Pathtable [" << j << "]] Removing entry " << i << endl;
	#endif
			}

		}
	}
}

void DataAnalyser::omit_invalid_addresses(std::vector<int> &smask, DVInst dvi, int iteration){
	int count = 0;
	int count_iter = 0;

	for(std::vector<DVStore>::iterator it = dvi.stores.begin(); it != dvi.stores.end(); it++){
		if(iteration == count_iter){
			for(std::vector<AddressStoreParams>::iterator addr = (*it).address_params.begin(); addr != (*it).address_params.end(); addr++){
				if(smask[count]){
					if((*addr).address == 0){
						smask[count] = 0;
					}
				}
				count++;
			}
		}
		count_iter++;
	}
}


int DataAnalyser::count_inst_active(int mask){
	int num = 0;
	int tmp_mask[threads_per_warp];
	convert_int_to_vectormask(mask, tmp_mask, threads_per_warp); 
	for(int i = 0; i < threads_per_warp; i++){
		if(tmp_mask[i])
			num++;
	}

	return num;
}

void DataAnalyser::update_exception_threads(int mask[], int offset){
	for(int i = 0+offset; i < threads_per_warp+offset; i++){
		if(mask[i-offset]){
			threads[i].exception = false;
			if(!threads[i].fetch_qempty){
				threads[i].fetch_stall = false;
				threads[i].fetch_isactive = true;
			}	
		}
	}
}

void DataAnalyser::clear_pathtable_exceptions(int offset){
	std::vector<int> entries;
	int warp_no = offset/threads_per_warp;

	for(int i = 0; i < pathtable[warp_no].mask.size(); i++){
		if(pathtable[warp_no].mask[i] == 0){ //exception generated null mask (traces have not been fetched = OK!)
			entries.push_back(i);			
			pathtable[warp_no].rbit[i] = -1;
			pathtable[warp_no].mask[i] = -1;
			pathtable[warp_no].actual_mask[i] = -1;
			pathtable[warp_no].ref_counter[i] = -1;
		}
	}

	for(std::vector<int>::iterator it = entries.begin(); it != entries.end(); it++){
		//std::vector<int>::iterator is = isTable[warp_no].is.begin();
		std::vector<LONG>::iterator pc = isTable[warp_no].pc.begin(); 
		std::vector<LONG>::iterator sp = isTable[warp_no].call_depth.begin();
		std::vector<int>::iterator bar = isTable[warp_no].barrier.begin();
		std::vector<LONG>::iterator tstamp = isTable[warp_no].time_stamp.begin();
		for(std::vector<int>::iterator p = isTable[warp_no].path.begin(); p != isTable[warp_no].path.end(); ){
			if(*p == *it){ //matches path
				isTable[warp_no].path.erase(p);
				//isTable[warp_no].is.erase(is);
				isTable[warp_no].pc.erase(pc); isTable[warp_no].call_depth.erase(sp);
				isTable[warp_no].barrier.erase(bar);
				isTable[warp_no].time_stamp.erase(tstamp);
				break;
			}else{ pc++; sp++; p++; bar++; tstamp++;}
		}
	}
}

void DataAnalyser::clear_storeQ_entries(){

	//first update all pending loads.

	for(std::vector<DVInst>::iterator it_dv = waiting_on_loads.begin(); it_dv != waiting_on_loads.end(); it_dv++){ 
		int mask[threads_per_warp];
		convert_int_to_vectormask((*it_dv).actual_mask, mask, threads_per_warp); //get active mask get_mask_pathTable((*it_dv).tag, (*it_dv).warpid)
		std::vector<int> smask(mask, mask+threads_per_warp);
		int count = 0;

		for(std::vector<DVLoad>::iterator it = it_dv->loads.begin(); it != it_dv->loads.end(); it++){
			bool load_ready = true; count = 0;
			if(!(*it_dv).load_done){					
				for(std::vector<AddressLoadParams>::iterator addr = it->address_params.begin(); addr != it->address_params.end(); addr++){
					if((*addr).address_load_done == 2 && mask[count] && (*addr).address > 0){
						if(SQ_ready((*addr).address, (*it_dv).id, (*addr).dependent_id))
							(*addr).address_load_done = true; //ready
							
						else if(l1_cache.ready((*addr).address)){ 
							(*addr).address_load_done = true; //ready
							
						}
						
					}
					/*if(!(*addr).stat && (*addr).address_load_done){
							if((*addr).l1_miss) statistics.l1_miss_r++;
							else statistics.l1_hit_r++;
							(*addr).stat = true;
						}*/

					count++;
				}
			}
		}

	}
	std::vector<LONG>::iterator addr = storeQ.address.begin(); 
	std::vector<bool>::iterator valid = storeQ.valid.begin();
	std::vector<LONG>::iterator robid = storeQ.id.begin();
	//???here <-----------	
	//then clear
	addr = storeQ.address.begin(); valid = storeQ.valid.begin(); robid = storeQ.id.begin();
	for(; addr != storeQ.address.end(), valid != storeQ.valid.end(), robid != storeQ.id.end(); ){
		if( *robid < reorderB.id.front()){ //if valid and/or bypassed in previous c.c	 //*valid ||
			storeQ.address.erase(addr); //pop off the LQ 
			storeQ.valid.erase(valid);
			storeQ.id.erase(robid);
		}else{
			addr++; valid++; robid++;
		}
	}
}

bool DataAnalyser::is_check(int warpid){
	int mask_sum = 0;
	int max_mask = pow(2,threads_per_warp) - 1;	
	for(std::vector<int>::iterator path = isTable[warpid].path.begin(); path != isTable[warpid].path.end(); path++){
		mask_sum += pathtable[warpid].actual_mask[(*path)];
	}

	int add_offset = warpid * threads_per_warp;
	/*bool is_empty = false;
	for(int i = 0+add_offset; i < threads_per_warp+add_offset; i++){
		if(threads[i].prefetch_instruction_queue.empty() && threads[i].prefetch_instruction_queue_buffer.empty()) is_empty = true;
	}*/
	

	if(mask_sum == max_mask) return true;	// && !is_empty
	return false;
}


void DataAnalyser::commit_dv(){

	clear_storeQ_entries();
		

	int commit_count = 0;

#ifdef DEBUGEXE
		cout << "Trying to commit ROB entry " << reorderB.id.front() << ", but data_valid = " << reorderB.data_valid.front() << endl;
#endif	

	if(!reorderB.data_valid.empty()){
		while(reorderB.data_valid.front() && commit_count < conf.commit_width && !reorderB.data_valid.empty()){ //pop entry off the ROB
			reorderB.inst.front().actual_mask = pathtable[reorderB.inst.front().warpid].actual_mask[reorderB.inst.front().tag];
			//cout << "Trying to commit ROB entry " << reorderB.id.front() << ", but data_valid = " << reorderB.data_valid.front() << endl;
			bool commit_store = true;
			if(reorderB.inst.front().has_load){
#if defined(DEBUGEXE) || defined(DEBUGMEM)
				cout << "Commiting a load" << endl;
#endif
				std::vector<LONG> addresses; 
				int tmp_mask[threads_per_warp];
				convert_int_to_vectormask(reorderB.inst.front().actual_mask, tmp_mask, threads_per_warp); //get_mask_pathTable(reorderB.inst.front().tag, reorderB.inst.front().warpid)
				get_LOAD_addresses(reorderB.inst.front(), addresses, tmp_mask, true);  
			
				for(std::vector<LONG>::iterator it = addresses.begin(); it != addresses.end(); it++){
					std::vector<LONG>::iterator addr = loadQ.address.begin(); std::vector<bool>::iterator valid = loadQ.valid.begin(); 
					std::vector<LONG>::iterator rb = loadQ.id.begin();
					for(; addr != loadQ.address.end(), valid != loadQ.valid.end(); ){
						if( *rb == reorderB.id.front()){ //*addr == *it && *valid == true &&
							loadQ.address.erase(addr); //pop off the LQ 
							loadQ.valid.erase(valid);
							loadQ.id.erase(rb);
						}else{
							addr++; valid++; rb++;
						}
					}
				}

				
			}else if(reorderB.inst.front().has_store){
#if defined(DEBUGEXE) || defined(DEBUGMEM)
				cout << "Trying to commit a [store]" << endl;
#endif
				std::vector<LONG> addresses; 
				int tmp_mask[threads_per_warp];
				convert_int_to_vectormask(reorderB.inst.front().actual_mask, tmp_mask, threads_per_warp); 
				//convert_int_to_vectormask(pathtable[reorderB.inst.front().warpid].actual_mask[reorderB.inst.front().tag], tmp_mask, threads_per_warp); 


				//Request stores to cache
				bool store_done = true;
				//for assertion - ensures no zero addresses
				
				for(std::vector<DVStore>::iterator it = reorderB.inst.front().stores.begin(); it != reorderB.inst.front().stores.end(); it++){
					int cnt = 0;
					for(std::vector<AddressStoreParams>::iterator addr = it->address_params.begin(); addr != it->address_params.end(); addr++){
						if(tmp_mask[cnt] && (*addr).address == 0) tmp_mask[cnt] = 0;
						cnt++;
					}
				}


				int iteration = 0;
				for(std::vector<DVStore>::iterator it = reorderB.inst.front().stores.begin(); it != reorderB.inst.front().stores.end(); it++){
					if(!(*it).done){	
						std::vector<int> smask; //(tmp_mask, tmp_mask+threads_per_warp);
						//omit_invalid_addresses(smask, reorderB.inst.front(), iteration); //multiple stores per dvi?
						for(int j = 0; j < threads_per_warp; j++) smask.push_back(tmp_mask[j]);
						if(!it->store_issued){
							it->store_issued = l1_cache.dv_store(&(*it),number_cycles,1, &smask, reorderB.inst.front().is_simd);
#if defined(DEBUGEXE) || defined(DEBUGMEM)
							cout << "[STORE] issued store =  " << it->store_issued << "  --- " << endl;
#endif
							store_done = false; //will check the cache in the next cycle
						}else{
							//has been issued. Check if ready
							int count = 0; bool done = true;
							for(std::vector<AddressStoreParams>::iterator addr = it->address_params.begin(); addr != it->address_params.end(); addr++){
								if(tmp_mask[count] && (*addr).address > 0){//address_load_done = 0 cache  // <--------------------------
									if(!(*addr).address_store_done){
										if(l1_cache.ready((*addr).address)){
											(*addr).address_store_done = true; //stats tabulated later in commit
											/*if(!(*addr).stat){
												statistics.l1_hit_w++;	
												(*addr).stat = true;
											}*/
#if defined(DEBUGEXE) || defined(DEBUGMEM)
											cout << "Store Hit in $" << endl;
#endif
											
										}else{
											if(!(*addr).l1_miss && l1_cache.mshr->inMSHR((*addr).address)){
												(*addr).l1_miss = true;	
												/*if(!(*addr).stat){
													statistics.l1_miss_w++;
													(*addr).stat = true;
												}*/
#if defined(DEBUGEXE) || defined(DEBUGMEM)
											cout << "Store Miss in $" << endl;
#endif
											} 
											done = false;
										}
									}
#ifdef DEBUGMEM										
									else cout << "store already done?!" << endl;
#endif
								}
								count++;
							}
							(*it).done = done;
							if(!done) store_done = false;
						}
					}
					iteration++;
				}

				commit_store = store_done;
				//copy new parameter values back to ROB
				/*for(int j = 0; j < reorderB.inst.front().stores.size(); j++){
					reorderB.inst.front().stores[j] = inst[j];
				}*/				

				if(store_done){	////pop off the SQ and retire
					//Gather hit/miss stats for all stores
					for(std::vector<DVStore>::iterator it = reorderB.inst.front().stores.begin(); it != reorderB.inst.front().stores.end(); it++){
						int count = 0;
						for(std::vector<AddressStoreParams>::iterator addr = it->address_params.begin(); addr != it->address_params.end(); addr++){
							/*if(tmp_mask[count] && (*addr).address > 0 && !(*addr).stat){
								if((*addr).l1_miss) statistics.l1_miss_w++;
								else statistics.l1_hit_w++;
								(*addr).stat = true;
							}*/
							if(tmp_mask[count] && (*addr).address > 0) statistics.counting_stores++;
							count++;
						}
					}

					std::vector<LONG> addresses;
					get_STORE_addresses( addresses, tmp_mask); //reorderB.inst.front(),
					for(std::vector<LONG>::iterator it = addresses.begin(); it != addresses.end(); it++){
						std::vector<LONG>::iterator addr = storeQ.address.begin(); 
						std::vector<bool>::iterator valid = storeQ.valid.begin();
						std::vector<LONG>::iterator robid = storeQ.id.begin();
						for(; addr != storeQ.address.end(), valid != storeQ.valid.end(), robid != storeQ.id.end(); ){
							if(*robid == reorderB.id.front()){//*addr == *it && *valid == true){
								*valid = true;
							}
							addr++; valid++; robid++;
						}
					}
				}else{
					break;
				}
				


			}			

		
			//get the instructions tag and update pathtable reference counter 
			if(reorderB.inst.front().is_uop){ //its a convergent uop, decrement path dependencies
#ifdef DEBUGEXE
				cout << "Commiting cuop" << endl;
				print_pathtable(reorderB.inst.front().warpid);
				cout << "pathT = " << reorderB.inst.front().pathT << ", pathNT = " << reorderB.inst.front().pathNT << endl;
#endif
				if(reorderB.inst.front().pathT != -1 & pathtable[reorderB.inst.front().warpid].rbit[reorderB.inst.front().pathT] != -1){
#ifdef DEBUGEXE
					cout << "[TAG " << reorderB.inst.front().pathT << "] RC: " << pathtable[reorderB.inst.front().warpid].ref_counter[reorderB.inst.front().pathT] << " -----> ";
#endif
					pathtable[reorderB.inst.front().warpid].ref_counter[reorderB.inst.front().pathT] -=1;
#ifdef DEBUGEXE
					cout << pathtable[reorderB.inst.front().warpid].ref_counter[reorderB.inst.front().pathT] << endl;
#endif
				}
				if(reorderB.inst.front().pathNT != -1 & pathtable[reorderB.inst.front().warpid].rbit[reorderB.inst.front().pathNT] != -1){
#ifdef DEBUGEXE
					cout << "[TAG " << reorderB.inst.front().pathNT << "] RC: " << pathtable[reorderB.inst.front().warpid].ref_counter[reorderB.inst.front().pathNT] << " -----> ";
#endif
					pathtable[reorderB.inst.front().warpid].ref_counter[reorderB.inst.front().pathNT] -= 1;
#ifdef DEBUGEXE
					cout << pathtable[reorderB.inst.front().warpid].ref_counter[reorderB.inst.front().pathNT] << endl;
#endif
				}
#ifdef DEBUG
				print_pathtable(reorderB.inst.front().warpid);
#endif
			}else if(reorderB.inst.front().is_branch && reorderB.inst.front().new_path){ //divergent i.e. buop
#ifdef DEBUGEXE
				cout << "Buop commit" << endl;
				cout << "[TAG " << reorderB.inst.front().pathT << "] RC: " << pathtable[reorderB.inst.front().warpid].ref_counter[reorderB.inst.front().pathT] << " -----> ";
#endif
				pathtable[reorderB.inst.front().warpid].ref_counter[reorderB.inst.front().pathT] -=1;
#ifdef DEBUGEXE
				cout << pathtable[reorderB.inst.front().warpid].ref_counter[reorderB.inst.front().pathT] << endl;
#endif
				div_counter--; 
#ifdef DEBUGEXE
				cout << "Global {DC}--" << endl;
				print_pathtable(reorderB.inst.front().warpid);				
#endif
			}

			//check to see if generated an exception
			if(reorderB.exception_mask.front()){
#ifdef DEBUGEXE							
				cout << "EXCEPTIONAL INST - generate new path w mask = " << reorderB.exception_mask.front() << endl;
#endif	
				if(!is_check(reorderB.inst.front().warpid)){ //all threads present in ISTable. Taken care of.
				
					if(pathtable[reorderB.inst.front().warpid].free_ptr == -1){ 
						update_freePointer(reorderB.inst.front().warpid);  
						assert(pathtable[reorderB.inst.front().warpid].free_ptr != -1);
					}
					int new_tag = pathtable[reorderB.inst.front().warpid].free_ptr;
					

					//isTable new entry
					int new_mask[threads_per_warp];
					convert_int_to_vectormask(reorderB.exception_mask.front(), new_mask, threads_per_warp); //print_smask(new_mask, threads_per_warp);
				
					//update the rest of the pathtable
					std::vector<int> entries;
					generate_exception_entries(entries, pathtable[reorderB.inst.front().warpid].free_ptr, reorderB.inst.front().tag, reorderB.inst.front().warpid);		
	#ifdef DEBUGEXE
					cout << endl << "[Pathtable EXC] Updating invalid bits of other path masks ................." << endl;
					print_pathtable(reorderB.inst.front().warpid);
	#endif
					int tmp_mask[threads_per_warp];
					while(!entries.empty()){
						
						
						convert_int_to_vectormask(pathtable[reorderB.inst.front().warpid].mask[entries.front()], tmp_mask, threads_per_warp);
						for(int j = 0; j < threads_per_warp; j++){
							if(new_mask[j] == 1 && tmp_mask[j] == 1){
								tmp_mask[j] = 0;
							}
						}
						pathtable[reorderB.inst.front().warpid].mask[entries.front()] = get_int_mask(tmp_mask, threads_per_warp);
						entries.erase(entries.begin());
					}
	#ifdef DEBUGEXE	
					print_pathtable(reorderB.inst.front().warpid);
	#endif	
					newpathtable_entry(reorderB.exception_mask.front(), 1, reorderB.exception_mask.front(), reorderB.inst.front().warpid); 
					int add_offset = reorderB.inst.front().warpid * threads_per_warp;
					update_is_table(reorderB.inst.front(), new_mask, add_offset, new_tag, 0, reorderB.inst.front().warpid); 
					if(ret_mask(tmp_mask, threads_per_warp) > 0){
						update_is_table(reorderB.inst.front(), tmp_mask, add_offset, reorderB.inst.front().tag ,6, reorderB.inst.front().warpid); //previous tag may not be here bc of conv/div   //this was '1' not '6' before
#ifdef DEBUGEXE
						cout << "[][]update_is_table with mask " << ret_mask(tmp_mask, threads_per_warp) << endl;
#endif
					}
					update_exception_threads(new_mask, add_offset); 
					clear_pathtable_exceptions(add_offset); 
					
	#ifdef DEBUGEXE	
					print_pathtable(reorderB.inst.front().warpid);
	#endif	
				}else{
					int new_mask[threads_per_warp];
					convert_int_to_vectormask(reorderB.exception_mask.front(), new_mask, threads_per_warp); //print_smask(new_mask, threads_per_warp);
					int add_offset = reorderB.inst.front().warpid * threads_per_warp;
					update_exception_threads(new_mask, add_offset); 
					clear_pathtable_exceptions(add_offset); 

				}
			}


//////////////////	Register Commit -- Release registers //////////////////////////////////////////// 
			if(reorderB.inst.front().is_phi){
				//get PIRAT type
#ifdef DEBUG
				cout << "Committing a PHI " << endl;
#endif
				int warp_no = reorderB.inst.front().warpid;
				DVInst tmp_dv = reorderB.inst.front();
				std::vector<int> owr = reorderB.ow_register.front();
				std::vector<int> owl = reorderB.ow_location.front();	
				std::vector<int> owt = reorderB.ow_type.front();			

				for(int i = 0; i < owl.size(); i++){
					int entry = owl[i];

					if(owt[i] == 0){ //Integer						
						//find partial and default in freelist and mark as free
#ifdef DEBUG
cout << "PHI INT Trying to release " << commit_pirat_int[warp_no].default_reg[entry] <<" -> " << owr[i] << ", entry = " << entry << " w = " << warp_no << endl;
#endif
						assert(owr[i] == commit_pirat_int[warp_no].default_reg[entry]);  //partial?!?
						release_register(commit_pirat_int[warp_no].default_reg[entry], 0);
						//release_register(commit_pirat_int[warp_no].partial_reg[entry], 0);
#ifdef DEBUG
						cout << "{INT-phi} Releasing registers " << commit_pirat_int[warp_no].default_reg[entry];
						cout << ", entry = " << entry << " w = " << warp_no << endl;
						cout << "Replacing default with = " << tmp_dv.writes[i] << endl;
#endif
						//replace default entry and update partial 
						commit_pirat_int[warp_no].default_reg[entry] = tmp_dv.writes[i];
						commit_pirat_int[warp_no].partial_reg[entry] = -1;
						commit_pirat_int[warp_no].r_bit[entry] = false; //bc it's a phi
						commit_pirat_int[warp_no].mt_bit[entry] = false; 
						commit_pirat_int[warp_no].mask_tag[entry] = -1;
					
					}else if(owt[i] == 1){ //FP
						//find partial and default in freelist and mark as free
#ifdef DEBUG
		cout << "PHI FP Trying to release " << commit_pirat_fp[warp_no].default_reg[entry] << " -> " << owr[i] << ", entry = " << entry << " w = " << warp_no << endl;
#endif
						assert(owr[i] == commit_pirat_fp[warp_no].default_reg[entry]);
						release_register(commit_pirat_fp[warp_no].default_reg[entry], 1);
						//release_register(commit_pirat_fp[warp_no].partial_reg[entry], 1);
#ifdef DEBUG
						cout << "{FP-phi} Releasing registers " << commit_pirat_fp[warp_no].default_reg[entry];
						cout << ", entry = " << entry << " w = " << warp_no << endl;
						cout << "Replacing default with = " << tmp_dv.writes[i] << endl;
#endif
						//replace default entry and update partial
						commit_pirat_fp[warp_no].default_reg[entry] = tmp_dv.writes[i];
						commit_pirat_fp[warp_no].partial_reg[entry] = -1;
						commit_pirat_fp[warp_no].r_bit[entry] = false; //bc it's a phi
						commit_pirat_fp[warp_no].mt_bit[entry] = false; 
						commit_pirat_fp[warp_no].mask_tag[entry] = -1;
										
					}
				}

			}else if(!reorderB.inst.front().is_phi && !reorderB.inst.front().is_uop){ 
				DVInst tmp_dv = reorderB.inst.front();
				int warp_no = reorderB.inst.front().warpid;
				std::vector<int> owreg = reorderB.ow_register.front();
				std::vector<int> owl = reorderB.ow_location.front(); std::vector<int> owt = reorderB.ow_type.front();
				//cout << owreg.size() << " " << owl.size() << " "<< owt.size() << " " << tmp_dv.writes.size() << endl;
				for(int i = 0; i < tmp_dv.writes.size(); i++){
					int release_reg = -1; warp_no = reorderB.inst.front().warpid;
					//if(tmp_dv.writes.size() != 0) cout << owreg[i] << " " << owl[i] << " "<< owt[i] << " " << tmp_dv.writes[i] << endl;
					if(owt[i] == 0){ //Integer	
						int entry = owl[i];		
						if(commit_pirat_int[warp_no].partial_reg[entry] == -1){  //if no partial present. Write
							////!commit_pirat_int[warp_no].mt_bit[entry]){
							//check if previous inst was a phi-uop which already merged and renamed the entry
							if(commit_pirat_int[warp_no].default_reg[entry] == tmp_dv.writes[i]){ //owreg[i]
#ifdef DEBUG
								cout << "INT present in default = " << tmp_dv.writes[i] << endl;
#endif
								continue;
							}else{
								commit_pirat_int[warp_no].partial_reg[entry] = tmp_dv.writes[i];
								commit_pirat_int[warp_no].r_bit[entry] = true; 
								commit_pirat_int[warp_no].mt_bit[entry] = true;
								commit_pirat_int[warp_no].mask_tag[entry] = tmp_dv.actual_mask;//get_mask_pathTable(tmp_dv.tag); 
#ifdef DEBUG
								cout << "INT Writing Partial [-1] with " << commit_pirat_int[warp_no].partial_reg[entry] << " [default] = " << commit_pirat_int[warp_no].default_reg[entry] << " -> " << owreg[i] << " entry = " << entry << " w = " << warp_no << endl;
#endif 
							}				
	
						}else if(owreg[i] != -1){ //if(commit_pirat_int[warp_no].mt_bit[entry] ){ //default and partial register are present
							//discover whether the register is default or partial
							bool is_partial = false; bool is_default = false; 
							if(owreg[i] == commit_pirat_int[warp_no].partial_reg[entry]) is_partial = true;
							if(owreg[i] == commit_pirat_int[warp_no].default_reg[entry]) is_default = true;

							assert(is_partial != is_default);

							//assert(assert_ow_correct(get_mask_pathTable(tmp_dv.tag, tmp_dv.warpid), commit_pirat_int[warp_no].mask_tag[entry])); //assert criteria of partial overwrite is correct
							if(is_partial){
#ifdef DEBUG
								cout << "INT (P) Trying to release " << commit_pirat_int[warp_no].partial_reg[entry] << " -> " << owreg[i] << " entry = " << entry << " w = " << warp_no << endl;
#endif
								assert(owreg[i] == commit_pirat_int[warp_no].partial_reg[entry]);
								release_reg = commit_pirat_int[warp_no].partial_reg[entry];
				
								commit_pirat_int[warp_no].partial_reg[entry] = tmp_dv.writes[i];
								//commit_pirat_int[warp_no].r_bit[entry] = true; 
								commit_pirat_int[warp_no].mt_bit[entry] = true;
								//commit_pirat_int[warp_no].mask_tag[entry] = tmp_dv.actual_mask;//get_mask_pathTable(tmp_dv.tag, tmp_dv.warpid);    
								assert(release_reg != -1);
								//if(release_reg != -1){ // && div_counter == 0){ //release the partial register
								release_register(release_reg, 0);
	#ifdef DEBUG
								cout << "{INT} Releasing register " << release_reg << ", entry = " << entry << endl;
								cout << "Partial now = " << tmp_dv.writes[i] << endl;
	#endif
							}else{
#ifdef DEBUG
								cout << "INT (D) Trying to release " << commit_pirat_int[warp_no].default_reg[entry] << " -> " << owreg[i] << " entry = " << entry << " w = " << warp_no << endl;
#endif
								assert(owreg[i] == commit_pirat_int[warp_no].default_reg[entry]);
								release_reg = commit_pirat_int[warp_no].default_reg[entry];
				
								commit_pirat_int[warp_no].default_reg[entry] = tmp_dv.writes[i];
								commit_pirat_int[warp_no].r_bit[entry] = true; 
								//commit_pirat_int[warp_no].mt_bit[entry] = true;
								//commit_pirat_int[warp_no].mask_tag[entry] = tmp_dv.actual_mask;//get_mask_pathTable(tmp_dv.tag, tmp_dv.warpid);    
								assert(release_reg != -1);
								//if(release_reg != -1){ // && div_counter == 0){ //release the partial register
								release_register(release_reg, 0);
	#ifdef DEBUG
								cout << "{INT} Releasing register " << release_reg << ", entry = " << entry << endl;
								cout << "Default now = " << tmp_dv.writes[i] << endl;
	#endif
				
							}																	
						}
					}else if(owt[i] == 1){ //FP
						int entry = tmp_dv.orig_dests[i];		
						if(commit_pirat_fp[warp_no].partial_reg[entry] == -1){//!commit_pirat_fp[warp_no].mt_bit[entry]){ //No partial present
							if(commit_pirat_fp[warp_no].default_reg[entry] == tmp_dv.writes[i]){
#ifdef DEBUG
								cout << "FP present in default = " << tmp_dv.writes[i] << endl;
#endif
								continue;
							}else{
								commit_pirat_fp[warp_no].partial_reg[entry] = tmp_dv.writes[i];
								commit_pirat_fp[warp_no].r_bit[entry] = true; 
								commit_pirat_fp[warp_no].mt_bit[entry] = true;
								commit_pirat_fp[warp_no].mask_tag[entry] = tmp_dv.actual_mask; 
#ifdef DEBUG
								cout << "FP Writing Partial [-1] with " << commit_pirat_fp[warp_no].partial_reg[entry] << " [default] = " << commit_pirat_fp[warp_no].default_reg[entry] << " -> " << owreg[i] << " entry = " << entry << " w = " << warp_no << endl;
#endif 
							}						
	
						}else if(owreg[i] != -1){ // if(commit_pirat_fp[warp_no].mt_bit[entry]){ //default and partial register are present

							//discover whether the register is default or partial
							bool is_partial = false; bool is_default = false; 
							if(owreg[i] == commit_pirat_fp[warp_no].partial_reg[entry]) is_partial = true;
							if(owreg[i] == commit_pirat_fp[warp_no].default_reg[entry]) is_default = true;

							assert(is_partial != is_default);

							if(is_partial){
#ifdef DEBUG
								cout << "FP (P) Trying to release " << commit_pirat_fp[warp_no].partial_reg[entry] << " -> " << owreg[i] << " entry = " << entry << " w = " << warp_no << endl;
#endif
								assert(owreg[i] == commit_pirat_fp[warp_no].partial_reg[entry]);
								
								release_reg = commit_pirat_fp[warp_no].partial_reg[entry];					
								commit_pirat_fp[warp_no].partial_reg[entry] = tmp_dv.writes[i];
								//commit_pirat_fp[warp_no].r_bit[entry] = true; 
								commit_pirat_fp[warp_no].mt_bit[entry] = true;
								//commit_pirat_fp[warp_no].mask_tag[entry] =  tmp_dv.actual_mask; //get_mask_pathTable(tmp_dv.tag);

								assert(release_reg != -1);
								release_register(release_reg, 1);			
#ifdef DEBUG
								cout << "{FP} Releasing register " << release_reg << ", entry = " << entry << endl;
								cout << "Partial now = " << tmp_dv.writes[i] << endl;
#endif
							}else{
#ifdef DEBUG
								cout << "FP (D) Trying to release " << commit_pirat_fp[warp_no].default_reg[entry] << " -> " << owreg[i] << " entry = " << entry << " w = " << warp_no << endl;
#endif
								assert(owreg[i] == commit_pirat_fp[warp_no].default_reg[entry]);
								
								release_reg = commit_pirat_fp[warp_no].default_reg[entry];					
								commit_pirat_fp[warp_no].default_reg[entry] = tmp_dv.writes[i];
								commit_pirat_fp[warp_no].r_bit[entry] = true; 
								//commit_pirat_fp[warp_no].mt_bit[entry] = true;
								//commit_pirat_fp[warp_no].mask_tag[entry] =  tmp_dv.actual_mask; //get_mask_pathTable(tmp_dv.tag);

								assert(release_reg != -1);
								release_register(release_reg, 1);			
#ifdef DEBUG
								cout << "{FP} Releasing register " << release_reg << ", entry = " << entry << endl;
								cout << "Default now = " << tmp_dv.writes[i] << endl;
#endif

							}		
						}//else{ //cout << tmp_dv.writes[i] << endl; 
						//	assert(tmp_dv.writes[i] == -1);}
					}
				
				}
			} 
				///////////////////////////////////// end of register recycle ////////////////////////

				
			if((reorderB.inst.front().has_store && commit_store) || !(reorderB.inst.front().has_store)){ //if a store, must be done
				if(!reorderB.inst.front().is_phi && !reorderB.inst.front().is_uop){
					statistics.total_dv_committed++;
					statistics.total_inst_committed = count_inst_active(reorderB.inst.front().actual_mask);
				}
#ifdef FREE_PHI
				if(!reorderB.inst.front().is_phi) commit_count++;
#else
				commit_count++;	
#endif


				reorderB.inst.pop_front();
				reorderB.exception_mask.pop_front();
				reorderB.ow_register.pop_front();
				reorderB.ow_location.pop_front();
				reorderB.ow_type.pop_front();
#ifdef DEBUGEXE
				cout << "  \t   commiting ROB " << reorderB.id.front() << " (" << commit_count << ")" << endl;
#endif

				reorderB.id.pop_front();
				reorderB.entry_valid.pop_front();
				reorderB.issued.pop_front();
				reorderB.done.pop_front();
				reorderB.data_valid.pop_front();
					
				reorderB.dest.pop_front();			
				reorderB.dest_type.pop_front();
				reorderB.pirat_type.pop_front();			
			}
		}
	}
	
}

/*******************************************************************
   Searches the ready_to_commit Q for all threads w/in a given warp
   -> decoded_inst Q = in-order queue to "commit" insts
   -> ready_to_commit = executed insts Q	
********************************************************************/

/*	int warp;
	for(int warp_count = 0 ;warp_count < conf.num_warps; warp_count++){
		warp = (commit_current_warp + warp_count)% conf.num_warps; //starts rotating from last commit_current_warp + 1 (updated below), thru  all warps/threads

		int offset = threads_per_warp * warp;
		for(int i = 0+offset ;i< (int)threads_per_warp+offset;i++){
			do{//log(logDEBUG4)<<"Head of decoded q "<< threads[i].decoded_instructions.front().seq_no<<endl;
				if(threads[i].decoded_instructions.empty())  //???????????

	commit_current_warp = (commit_current_warp +1 )%conf.num_warps; //update commit_current_warp
*/

/********************************************************
	Used for periodic logging of active threads
********************************************************/
bool DataAnalyser::logger(){
	bool is_finished = false;
	bool check_cold = true; //true = no cold cases
	for(int i = 0; i < conf.num_warps; i++){
		if(cold[i]) check_cold = false; //still in cold case

	}

	/*if(check_cold && decode_to_rename.size() == 0 && reorderB.data_valid.size() == 0 && get_issueQ_size() == 0){
		for (UINT i=0; i<conf.num_threads; i++){ //update threads in the warp (see if finished)
			threads[i].fetch_isactive = false;
			//threads[i].finish_cycle = number_cycles; 
			//threads[i].set_finish_cycle = true;
			threads[i].finished = true;
		}
		is_finished = isFinished();
	}*/

	//Periodic log

	if((number_cycles - periodic_logger) > 4999999 ){
		periodic_logger = number_cycles; //loglevel = logDEBUG4;
		cout << "  \t   Trying to commit ROB " << reorderB.id.front() << endl;
		cout << ">>>> Total instruction count = " << statistics.total_ins_count << endl;
		cout<<"Status of active threads "<<endl; 
		for(int i = 0 ;i< conf.num_threads;i++){
			if(!threads[i].fetch_isactive){
				cout<<"Thread "<<i<< " is inactive "; 
			}else{
				cout<<"Thread "<<i<< " is active "; 
			}
			cout<<"fq_size "<<threads[i].prefetch_instruction_queue.size()  << ", fqb_size " << threads[i].prefetch_instruction_queue_buffer.size() << ", "; 
			cout << "exception = " << threads[i].exception << ", empty = " <<  threads[i].fetch_qempty	 << ", stall = " << threads[i].fetch_stall;
			cout << ", barrier = " << threads[i].ignore << endl;
		}
		
	}else{
		loglevel = logERROR;
	}
	return is_finished;
}

/***************************************************************
    Updates cache, and keeps tally of total instructions
    and cycles, memory conflicts etc
***************************************************************/
void DataAnalyser::increment_cycle(){
	int bank_conflict;
	int bank_conflict_hashed;
	int mem_op_count;

	l1_cache.update_cycle(number_cycles,bank_conflict,bank_conflict_hashed, mem_op_count); //update L1 data

	number_cycles++;
	statistics.cycle = number_cycles; 
	LONG tot=0;

	for(int i = 0 ;i< conf.num_threads;i++){
		tot+=threads[i].ins_count; //tally total instructions
		statistics.ins_count[i] = threads[i].ins_count; //+=???
	}

	statistics.total_ins_count = tot;

	totalinstructions = tot;

	fu.clear_fu();
	mem_operation_count+= mem_op_count;
	bank_conflict_count+=bank_conflict;
	bank_conflict_count_reduced+=bank_conflict_hashed;
	dviqm.clear();
}



bool DataAnalyser::issue_inst_with_loads(){
	fu.execute_inst_with_loads(reorderB);
	return true;
}

/*************************************************************************
	Helper functions for DataAnalyser's execute() function
*************************************************************************/
void DataAnalyser::check_valid_commit_threads(std::deque<DVInst>::iterator ii, int val_index){
	if(!ii->smask[val_index]){ //if this thread is divergent (not part of the instruction), find one that's valid and has exe in the warp
		for(UINT jj = 0 ; jj< threads_per_warp ; jj++){
			if(!ii->smask[jj]){ //mask element '0', look at next element
				continue;
			}
			val_index = jj; //element is 1, set as a valid inst that's ready to commit
			ii->valid_inst = jj;
			break;
		}
	}
}

void DataAnalyser::check_for_branch_mispr(std::deque<DVInst>::iterator ii){
	for(UINT i = 0 ;i<threads_per_warp; i++){
		if(ii->smask[i]){
			if(!ii->insts[i].is_branch) //if it's a branch, exit
				break;
			if(ii->insts[i].is_mispredicted){ //inst was mispredicted, stall the thread (stop fetching)
				threads[ii->tid[i]].fetch_stall = false;
				log(logDEBUG4)<<"Stall "<<ii->tid[i]<<" reset PC "<<hex16(ii->insts[i].pc)<<endl;
			}
		}
	}
}


//computes complement mask of path_mask
int compute_complement(int path_mask, int prev_mask, int length){
	int mask;
	int max_mask = pow(2,length) - 1;
	//mask = max_mask - path_mask;	
	mask = prev_mask - path_mask;
	return mask;
}

//determine if int masks are subsets
bool is_subset(int mask1, int mask2, int length){
	int nm1[length], nm2[length];
	convert_int_to_vectormask(mask1, nm1, length);
	convert_int_to_vectormask(mask2, nm2, length);
	return strict_subset(nm1, nm2, length); 
}

int compute_new_mask(int mask, int entry, int length){
	//entry element in mask needs to be invalidated, convert mask to vector, compute
	int m1[length];
	convert_int_to_vectormask(mask, m1, length);
	m1[entry] = 0;
	return get_int_mask(m1, length);
}

void DataAnalyser::print_is_table(int table){
	std::vector<int>::iterator path = isTable[table].path.begin(), bar = isTable[table].barrier.begin();
	std::vector<LONG>::iterator pc = isTable[table].pc.begin(), sp = isTable[table].call_depth.begin();
	std::vector<LONG>::iterator time = isTable[table].time_stamp.begin();
	

	cout << "---------------- Path Scheduling Table -----------------------" << endl;
	cout << "Path\t|ActualMask | Mask\t  | PC\t\t\t| SP\t\t\t| Barrier\t\t\t| Time sched" << endl;
	//if(!isTable[table].is.empty()){
		for(;path != isTable[table].path.end(), pc != isTable[table].pc.end(), sp != isTable[table].call_depth.end(), bar != isTable[table].barrier.end(), time != isTable[table].time_stamp.end(); 
				path++, pc++, sp++, bar++, time++){
			cout << (*path) << "\t" << pathtable[table].actual_mask[(*path)] << " | " << pathtable[table].mask[(*path)]  << "\t"<< hex16(*pc) << "\t" << hex16(*sp) << "\t " << *bar << "\t\t" << *time << endl;
		}
	//}
		cout << "-----------------------------------------------------" << endl;
}

// creates a list of younger Pathtable entries so that the incorrect bits 
// in their masks may be invalidated
void DataAnalyser::generate_entries(std::vector<int> &entries, int free, int tag, int warp_no){
	int i = tag+1;
	if(i >= pathtable[warp_no].ref_counter.size()) i = 0;
	
	while(i != free){ 
		if(pathtable[warp_no].ref_counter[i] > 0){
			entries.push_back(i);
		}
		i++;	
		if(i >= pathtable[warp_no].ref_counter.size())
			i = 0;
	}
}

void DataAnalyser::generate_exception_entries(std::vector<int> &entries, int free, int tag, int warp_no){
	//start from tag entry, until free (not including free)
	int i = tag;

	while(i != free){ 
		if(pathtable[warp_no].ref_counter[i] != -1){
			entries.push_back(i);
		}
		i++;	
		if(i >= pathtable[warp_no].ref_counter.size())
			i = 0;
	}

}

//generates a non-redundant list of LOAD addresses to look for
void DataAnalyser::get_LOAD_addresses(DVInst &it_inst, std::vector<LONG> &addresses, int tmp_mask[], bool is_commit){
	int count = 0; int lcount = 0;
	if(is_commit){
		for(std::vector<DVLoad>::iterator ll = reorderB.inst.front().loads.begin(); ll != reorderB.inst.front().loads.end(); ll++){
			count = 0;  lcount = 0;
			for(std::vector<AddressLoadParams>::iterator aa = (*ll).address_params.begin(); aa != (*ll).address_params.end(); aa++){
				bool found = false;
				if(tmp_mask[count]){
					for(std::vector<LONG>::iterator it = addresses.begin(); it != addresses.end(); it++){
						if((*aa).address == *it){ 
							found = true; 
							break;
						 }

					}
					if(!found){
						addresses.push_back((*aa).address); 
								
					}
				}
				//collect stats
					if((*aa).address > 0){ // && !(*aa).stat){
						if((*aa).l1_miss) statistics.l1_miss_r++;
						else statistics.l1_hit_r++;
						  statistics.counting_loads++; //(*aa).stat = true;
						lcount++;
#ifdef MEM_TRACE
						if(!(*aa).l1_miss){
							if(!(*aa).storeQ)  cout << "[LOAD] L1 HIT 0x" << hex16((*aa).address) << endl; //" ROB_ID = " <<  reorderB.inst.front().id << endl;			
							else    cout << "[LOAD] SQ HIT 0x" << hex16((*aa).address) << endl;// " ROB_ID = " <<   //reorderB.inst.front().id << endl;
						}else{
							if(!(*aa).storeQ)  cout << "[LOAD] L1 MISS 0x" << hex16((*aa).address) << endl; //" ROB_ID = " <<  reorderB.inst.front().id << endl;			
							else    cout << "[LOAD] SQ MISS? 0x" << hex16((*aa).address) << endl; //" ROB_ID = " <<  reorderB.inst.front().id << endl;
						}
#endif
					}
				count++;
			}
			/*if(reorderB.inst.front().load_count < lcount){ //<----------------------------------
				statistics.l1_hit_r += reorderB.inst.front().load_count - lcount;			
			}*/
			//break;
		}
		
	}else{	
		for(std::vector<DVLoad>::iterator ll = (it_inst).loads.begin(); ll != (it_inst).loads.end(); ll++){
			count = 0;  
			for(std::vector<AddressLoadParams>::iterator aa = (*ll).address_params.begin(); aa != (*ll).address_params.end(); aa++){
				bool found = false;
				if(tmp_mask[count]){
					for(std::vector<LONG>::iterator it = addresses.begin(); it != addresses.end(); it++){
						if((*aa).address == *it){ 
							found = true; 
							break;
						 }

					}
					if(!found){
						addresses.push_back((*aa).address); 
								
					}
					
				}
				count++;
			}
			//break;
		}
	}
}

//generates a non-redundant list of STORE addresses to look for
void DataAnalyser::get_STORE_addresses(std::vector<LONG> &addresses, int tmp_mask[]){
	int count = 0; int lcount;
	for(std::vector<DVStore>::iterator ll = reorderB.inst.front().stores.begin(); ll != reorderB.inst.front().stores.end(); ll++){
		count = 0;  lcount = 0;
		for(std::vector<AddressStoreParams>::iterator aa = (*ll).address_params.begin(); aa != (*ll).address_params.end(); aa++){
			bool found = false;
			if(tmp_mask[count]){
				for(std::vector<LONG>::iterator it = addresses.begin(); it != addresses.end(); it++){
					if((*aa).address == *it){ found = true; break; }

				}
				if(!found){
					addresses.push_back((*aa).address); 
				}
				//collect stats
#ifdef MEM_TRACE
				if((*aa).address > 0){
					if(!(*aa).l1_miss){
						if(!(*aa).storeQ)  cout << "[STORE] L1 HIT 0x" << hex16((*aa).address) << endl;			
						else    cout << "[STORE] SQ HIT 0x" << hex16((*aa).address) << endl;
					}else{
						if(!(*aa).storeQ)  cout << "[STORE] L1 MISS 0x" << hex16((*aa).address) << endl;			
						else    cout << "[STORE] SQ MISS? 0x" << hex16((*aa).address) << endl;
					}
				}
#endif
				if((*aa).address > 0){ // && !(*aa).stat){
					if((*aa).l1_miss) statistics.l1_miss_w++;
					else statistics.l1_hit_w++;
					(*aa).stat = true; lcount++; statistics.counting_stores++;
				}
			}
			count++;
		}
		/*if(reorderB.inst.front().store_count < lcount){
			statistics.l1_hit_w += reorderB.inst.front().load_count - lcount;			
		}*/	
		//break;
	}
}

//for mispredictions - clears the ROB of the given warp ID
/*void DataAnalyser::clear_rob(int warpid, int robid){

	std::list<DVInst>::iterator it_inst = reorderB.inst.begin();	
	std::list<int>::iterator it_exc = reorderB.exception_mask.begin();
	std::list<std::vector<int> >::iterator it_owr = reorderB.ow_register.begin(); 
	std::list<std::vector<int> >::iterator it_owl = reorderB.ow_location.begin();
	std::list<std::vector<int> >::iterator it_owt = reorderB.ow_type.begin();
	std::list<LONG>::iterator it_id = reorderB.id.begin(); 
	std::list<std::vector<int> >::iterator it_dest = reorderB.dest.begin();
	std::list<std::vector<int> >::iterator it_pt = reorderB.pirat_type.begin(); 
	std::list<std::vector<char> >::iterator it_destt = reorderB.dest_type.begin(); 
	std::list<bool>::iterator it_entry = reorderB.entry_valid.begin();
	std::list<bool>::iterator it_dvalid = reorderB.data_valid.begin();
	std::list<bool>::iterator it_issued = reorderB.issued.begin();
	std::list<bool>::iterator it_done = reorderB.done.begin();
	//std::list<int>::iterator it_warp = reorderB.warpid.begin();


	for(; it_done != reorderB.done.end(); ){	
		int it_warp = (*it_inst).warpid; 
		if(it_warp == warpid && *it_id != robid){
			//remove these elements
			//it_warp = reorderB.warpid.erase(it_warp);
			it_inst = reorderB.inst.erase(it_inst);
			it_exc = reorderB.exception_mask.erase(it_exc);
			it_owr = reorderB.ow_register.erase(it_owr);
			it_owl = reorderB.ow_location.erase(it_owl);
			it_owt = reorderB.ow_type.erase(it_owt);
			it_id = reorderB.id.erase(it_id);
			it_dest = reorderB.dest.erase(it_dest);
			it_pt = reorderB.pirat_type.erase(it_pt);
			it_destt = reorderB.dest_type.erase(it_destt);
			it_entry = reorderB.entry_valid.erase(it_entry);
			it_dvalid = reorderB.data_valid.erase(it_dvalid);
			it_issued =  reorderB.issued.erase(it_issued);
			it_done = reorderB.done.erase(it_done);
		}else{
			it_inst++; it_warp++;
			it_exc++; it_owr++; 
			it_owl++; it_owt++; 
			it_id++; it_dest++; 
			it_pt++; it_destt++; 
			it_entry++; it_dvalid++; 
			it_issued++; it_done++;
		}
	}


}*/

/* scenario for rolling back mispredicted divergence
				//pop prefetch queue front() so this warp may now continue executing from correct address

					//just issue noops which the simulator discards. No need for following code

					//clear any warp's noops from the decode_to_rename queue as well
					/*std::deque<DVInst>::iterator it = decode_to_rename.begin();
					std::vector<int> erase_elem; int counter = -1;
					DVInst dv_inst;
					for(; it != decode_to_rename.end(); it++){ //find the elements
						counter++;
						dv_inst = *it;
						if((*it).warpid == (*it_inst).warpid)
							erase_elem.push_back(counter);
					}

					for(std::vector<int>::iterator elem = erase_elem.begin(); elem != erase_elem.end(); elem++) //erase them
						decode_to_rename.erase(decode_to_rename.begin()+(*elem));
					

					//clear issue queue of these warp's instructions (noops)
					for(int i = 0; i < NO_QUEUE_TYPES; i++){ 	   
						std::vector<LONG>::iterator it_rb = issueQ[i].rob_id.begin();
						std::vector<int>::iterator it_wrp = issueQ[i].warp_no.begin();
						for(; it_wrp != issueQ[i].warp_no.end(); ){
							if((*it_wrp) == (*it_inst).warpid){ 
								remove_IQ_entry(*it_rb, i);
							}else{
								it_rb++; it_wrp++;
							}
						}
					}

					//clear out the ROB of this warp's instructions (noops)	
					clear_rob((*it_inst).warpid, *it_id);	*/	
					//continue; //next loop. This instruction has been evacuated from pipeline	



void DataAnalyser::unstall_branch_threads(int add_offset, int x_smask[], bool full_mispr){
	int warpid = add_offset/threads_per_warp;
#ifdef DEBUGFETCH
	cout << "unstalling branch mispredicted threads " << endl;
	cout << "MASK IS: " << get_int_mask(x_smask, threads_per_warp);
	print_is_table(warpid);
	
#endif
	int masked[threads_per_warp];
	int tag = -1;
	bool correct = false;

	//find tag
	/*for(int j= 0; j < threads_per_warp; j++){ 
		if(x_smask[j]){
			tag = threads[j+add_offset].tag;
			break;
		}
	}*/

	for(int j= 0; j < threads_per_warp; j++){  
		if(threads[j+add_offset].branch_stall && x_smask[j]){// && threads[j+add_offset].tag == tag){
			assert(threads[j+add_offset].branch_stall);
			threads[j+add_offset].branch_stall = false;
			/*masked[j] = 1;
		}else{
			masked[j] = 0;*/
		}
	}		
	
	for(UINT i=0 ; i < threads_per_warp; i++){
		if(x_smask[i]){ //masked[i]){ //
			threads[i+add_offset].prefetch_instruction_queue_buffer.pop_front(); //taken off prefetch IQ and placed into decoded_instructions
			if(threads[i+add_offset].prefetch_instruction_queue_buffer.empty()) //if prefetch queue is empty, try and refill
				refill_prefetch_IQB(i, add_offset);
			threads[i+add_offset].fetch_pc = threads[i+add_offset].prefetch_instruction_queue_buffer.front().pc; //get next values
			threads[i+add_offset].fetch_sp = threads[i+add_offset].prefetch_instruction_queue_buffer.front().sp;
			
			if(!full_mispr){
				if(!correct){
					correct = true; //flag to assert that all tags match
					tag = threads[i+add_offset].tag;
					//update the ISTable
					int is_tag = find(isTable[warpid].path.begin(), isTable[warpid].path.end(), tag) - isTable[warpid].path.begin(); //search for prev tag
					isTable[warpid].pc[is_tag] = threads[i+add_offset].fetch_pc;
					isTable[warpid].call_depth[is_tag] = threads[i+add_offset].fetch_sp;
				}else{
					assert(tag == threads[i+add_offset].tag);
				}
			}
		}
	} 
	
}

/*******************************************************************
  execute_bypass responsible for:
   -clear out the FU's with cycles_remaining = 0
   -simulate the bypass network, i.e. forward dest to PIRAT 
	(i.e. and "reg file") and the IQ	
*******************************************************************/
void DataAnalyser::execute_bypass(){

	std::vector<int> write_back, write_type, write_entry, write_warp; 
	std::vector<int> path_push, mask_push, warp_push;
	LONG inst_count = 0, dv_inst_count = 0;

	//execute
	fu.execute(reorderB, dv_inst_count, inst_count, threads_per_warp); //update FUs and insts which have finished executing go into ROB
	statistics.exe_dvinst_count += dv_inst_count; 
	statistics.exe_cycles++;
	statistics.exe_inst_count += inst_count;

#ifdef DEBUGEXE
	cout << "-------------------------	BYPASS     -----------------------------" << endl; //Broadcast to IQ
#endif
	std::list<bool>::iterator it_valid = reorderB.data_valid.begin(); std::list<bool>::iterator it_done = reorderB.done.begin(); 
	std::list<LONG>::iterator it_id = reorderB.id.begin(); std::list<DVInst>::iterator it_inst = reorderB.inst.begin(); 
	std::list<std::vector<char> >::iterator dtype = reorderB.dest_type.begin();  
	std::list<std::vector<int> >::iterator ptype = reorderB.pirat_type.begin();
	

	for(; it_valid != reorderB.data_valid.end(), it_done != reorderB.done.end(), it_id != reorderB.id.end(), it_inst != reorderB.inst.end(), 
			dtype != reorderB.dest_type.end(), ptype != reorderB.pirat_type.end(); 
			it_valid++, it_done++, it_id++, it_inst++, dtype++, ptype++){
		
		if((*it_valid) && !(*it_done)){	//get the destination value
			////////////////////////////////////////////////      BRANCHES       ///////////////////////////////////
			//   compute outcome for branch here, correct or mispredicted. Calculate outcome if predicted divergent
			////////////////////////////////////////////////////////////////////////////////////////////////////////
			if((*it_inst).is_branch){
				
#ifndef PERFECT_BRANCH
				//CHECK branch misprediction during uniform execution
				if((*it_inst).branch_taken != (*it_inst).branch_taken_pred){
					//correct the threads
					int x_smask[threads_per_warp];
					convert_int_to_vectormask((*it_inst).actual_mask, x_smask, threads_per_warp); 
					int add_offset = (*it_inst).warpid * threads_per_warp;

#ifdef DEBUGBR
					cout << "DETECTED: BRANCH MISPREDICT, pred = " << (*it_inst).branch_taken_pred << ", actual = " << (*it_inst).branch_taken;
					cout << ", div_actual = " << (*it_inst).div_actual << " && div_pred = " << (*it_inst).div_pred << endl;
					cout << ",  ROB_ID = " << *it_id << ", tag = " << (*it_inst).tag << endl; //CLEAN UP THE PIPELINE
#endif	

					if(!((*it_inst).div_actual == true && (*it_inst).div_pred == true) && !((*it_inst).div_actual == true && (*it_inst).div_pred == false)){ 
						//if this is not the case, and there was a branch mispr, unstall noops
						unstall_branch_threads(add_offset, x_smask, false);
					}

					statistics.branch_misprediction++;
				}
#ifdef DEBUGBR
				else{
					cout << "BRANCH predicted correctly, ROB_ID = " << *it_id << endl;	
				}
#endif	
#endif	
				////////////// update divergence predictor  /////////////////////
#ifndef PERFECT_DIVERGENCE
				bool correct = false; bool updown = true;
				if((*it_inst).div_actual == (*it_inst).div_pred)
					correct = true;
				if(!correct){
					if(!(*it_inst).div_actual) //was suppose to be non-divergent
						updown = false;
				}
				dviqm.divergence_predictor_udpate(correct, updown);		
#endif
			} 

			

			/////////////	Divergence prediction checks  /////////////////////////////
			if((*it_inst).is_branch){
				
#ifdef DEBUGBR
				cout << "TRYING TO BYPASS A BRANCH for tag " << (*it_inst).tag << " ROB_ID = " << *it_id << endl;
#endif
				int prev_mask = pathtable[(*it_inst).warpid].mask[(*it_inst).tag]; //computed mask = .actual_mask  

				//Predicted non-divergent -- correct (same path)  
				if((*it_inst).div_actual == false && (*it_inst).div_pred == false){
					statistics.correct_predict++;  
					//update pathtable as valid
					if(pathtable[(*it_inst).warpid].rbit[(*it_inst).tag] != -1){ 
						pathtable[(*it_inst).warpid].rbit[(*it_inst).tag] = 1;						
						path_push.push_back((*it_inst).tag); mask_push.push_back((*it_inst).actual_mask);
						warp_push.push_back((*it_inst).warpid);
#ifdef DEBUGBR
						cout << "predicted ND - correct " << endl;
						cout << "Updating TAG " << (*it_inst).tag << endl;
#endif
					}

				//Predicted non-divergent, incorrect -- misprediction
				}else if((*it_inst).div_actual == true && (*it_inst).div_pred == false){ //path was not re-newed. Must flush
#ifdef DEBUGBR
					cout << "+=+=+=+=+=+     WARNING - full mispredict     +=+=+=+=+=+=+=+" << endl; //"flush pipeline and roll back"
					cout << "Actual mask = " << (*it_inst).actual_mask << ", Pathtable mask = " << pathtable[(*it_inst).warpid].mask[(*it_inst).tag] << endl;
#endif
					statistics.full_misprediction++; 

					//if branch was correct, div mispredicted so need to recover from uops
					int x_smask[threads_per_warp], inv_mask[threads_per_warp];
					convert_int_to_vectormask((*it_inst).actual_mask, x_smask, threads_per_warp); 
					int add_offset = (*it_inst).warpid * threads_per_warp;	
					int taken_mask = pathtable[(*it_inst).warpid].mask[(*it_inst).tag] - (*it_inst).actual_mask;
					convert_int_to_vectormask(taken_mask, inv_mask, threads_per_warp); 

					//transform the branch to buop 
					(*it_inst).new_path = true; statistics.buop_count++;
					(*it_inst).pathT = (*it_inst).tag; //tag as metadata
					int offset = (*it_inst).warpid * threads_per_warp;
					

					//dviqm.compute_branch_mask(x_smask, inv_mask, (*it_inst)); 
					//cout << ">>>>>??? xmask = "; print_smask(x_smask, threads_per_warp);
					//cout << ">>>>> ???inv_mask = "; print_smask(inv_mask, threads_per_warp);			

					int branch_mask[threads_per_warp];
					for(int k = 0; k < threads_per_warp; k++){
						if(x_smask[k] == 1 || inv_mask[k] == 1){
							branch_mask[k] = 1;
						}else{
							branch_mask[k] = 0;
						}
					}
					unstall_branch_threads(add_offset, branch_mask, true);

					//updates div_counter, creates PST entry and Pathtable entry for both, -- reference counter of original path
					int new_tag = update_structure_mispredict(x_smask, inv_mask, (*it_inst), offset, (*it_inst).tag, (*it_inst).warpid);  //.newpath computed in here
#ifdef DEBUGBR
					print_is_table((*it_inst).warpid);
#endif		
					path_push.push_back((*it_inst).tag); //push back the path to be written on bypass network
					mask_push.push_back(get_int_mask(x_smask, threads_per_warp)); warp_push.push_back((*it_inst).warpid);			
					path_push.push_back(new_tag); //push back the path to be written on bypass network
					mask_push.push_back(get_int_mask(inv_mask, threads_per_warp)); warp_push.push_back((*it_inst).warpid);

				//Predicted divergent and correct (actually divergent)
				}else if((*it_inst).div_actual && (*it_inst).div_pred){
					pathtable[(*it_inst).warpid].rbit[(*it_inst).tag] = 1;
					pathtable[(*it_inst).warpid].mask[(*it_inst).tag] = (*it_inst).actual_mask;
					path_push.push_back((*it_inst).tag); //push back the path to be written on bypass network

					statistics.correct_predict++; 
					mask_push.push_back((*it_inst).actual_mask);
					warp_push.push_back((*it_inst).warpid);
#ifdef DEBUGBR
					cout << "Predicted D - correct. Updating TAG " << (*it_inst).tag << " to mask " << (*it_inst).actual_mask <<  endl;
#endif

				//else if predicted divergent, incorrect but still fully functional -- drain out nops
				}else{
					
					//created a useless entry -- still ok. Proceed
					pathtable[(*it_inst).warpid].rbit[(*it_inst).tag] = 1;
					pathtable[(*it_inst).warpid].mask[(*it_inst).tag] = (*it_inst).actual_mask;
					path_push.push_back((*it_inst).tag); //push back the path to be written on bypass network
					mask_push.push_back((*it_inst).actual_mask);
					warp_push.push_back((*it_inst).warpid);
#ifdef DEBUGBR
					cout << "[partial mispredict] detected. All is good " << endl;
					cout << "Updating TAG " << (*it_inst).tag << " to mask " << (*it_inst).actual_mask <<  endl;
#endif
					statistics.partial_misprediction++;  
				}
				


				if((*it_inst).actual_mask == 0){ //assert that the actual mask in not null, IF SO
					//evict the entry from the IS Table
					int new_mask[threads_per_warp];
					convert_int_to_vectormask((*it_inst).actual_mask, new_mask, threads_per_warp);
					int add_offset = (*it_inst).warpid * threads_per_warp;
					update_is_table((*it_inst), new_mask, add_offset, (*it_inst).tag, 3, (*it_inst).warpid); //evict
				}

				if((*it_inst).new_path && !((*it_inst).div_actual == true && (*it_inst).div_pred == false)){ //if new path that's not a full mispredict
					//re-obtain prev_mask (may have been adjusted in the previous conditions)
					prev_mask = pathtable[(*it_inst).warpid].mask[(*it_inst).pathT];//<----------------

					//compute complement mask 
					int not_mask = compute_complement(pathtable[(*it_inst).warpid].mask[(*it_inst).tag], prev_mask, threads_per_warp); 
#ifdef DEBUGBR
					cout << "COMPLEMENT MASK of " << pathtable[(*it_inst).warpid].mask[(*it_inst).tag] << " given  " << prev_mask << " --> " << not_mask << endl;
#endif

					//ONLY if predicted divergent, invalidate the active mask bits which were incorrectly copied
					//from pathtable.commit_ptr to pathtable.free_ptr-1 -> clear pathtable
					std::vector<int>::iterator it_entry = pathtable[(*it_inst).warpid].mask.begin();
					std::advance(it_entry, pathtable[(*it_inst).warpid].commit_ptr);

					int convert_mask[threads_per_warp];
  				   	convert_int_to_vectormask(not_mask, convert_mask, threads_per_warp);								

					//generate list of entries bw commit_ptr and free_ptr -- clear mask bits
					std::vector<int> entries; 
					if(pathtable[(*it_inst).warpid].free_ptr == -1){ 
						update_freePointer((*it_inst).warpid);
						assert(pathtable[(*it_inst).warpid].free_ptr != -1);
					}
					generate_entries(entries, pathtable[(*it_inst).warpid].free_ptr, (*it_inst).tag, (*it_inst).warpid); //free pointer	
#ifdef DEBUGBR
					cout << endl << "[Pathtable] Updating invalid bits of other path masks ................." << endl;
#endif
					while(!entries.empty()){
						int tmp_mask[threads_per_warp];
						convert_int_to_vectormask(pathtable[(*it_inst).warpid].mask[entries.front()], tmp_mask, threads_per_warp);
						for(int j = 0; j < threads_per_warp; j++){
							if(convert_mask[j] == 1 && tmp_mask[j] == 1){
								tmp_mask[j] = 0;
							}
						}
						pathtable[(*it_inst).warpid].mask[entries.front()] = get_int_mask(tmp_mask, threads_per_warp);
						entries.erase(entries.begin());
					}
	#ifdef DEBUGEXE	
					print_pathtable((*it_inst).warpid);
	#endif	
			
					//create new entry in pathtable and IS Table
					if(not_mask != 0){ //if mask is not null
						if(pathtable[(*it_inst).warpid].free_ptr == -1){ 
							update_freePointer((*it_inst).warpid);
							assert(pathtable[(*it_inst).warpid].free_ptr != -1);
						}
						int new_tag = pathtable[(*it_inst).warpid].free_ptr;
						newpathtable_entry(not_mask, 1, not_mask, (*it_inst).warpid);

						//isTable new entry
						int new_mask[threads_per_warp];
						convert_int_to_vectormask(not_mask, new_mask, threads_per_warp); 
	#ifdef DEBUGBR
						print_smask(new_mask, threads_per_warp);
						cout << "[BRANCHES] Created IS \"alternate\" entry (mask - " << not_mask << ")" << endl;
	#endif
						int add_offset = (*it_inst).warpid * threads_per_warp;
						update_is_table((*it_inst), new_mask, add_offset, new_tag, 0, (*it_inst).warpid); 
						path_push.push_back(new_tag); //push back the path to be written on bypass network
						mask_push.push_back(not_mask); warp_push.push_back((*it_inst).warpid);
					}

					
				}
			}//////////////////////////////////////////////////////////////////////////////////////////////////////////////
			if((*it_inst).has_load){ //update the contents in the LQ ---- STORES
				//get addresses	and path mask			
				std::vector<LONG> addresses; 
				int tmp_mask[threads_per_warp];
				convert_int_to_vectormask(get_mask_pathTable((*it_inst).tag, (*it_inst).warpid), tmp_mask, threads_per_warp); 
				
				get_LOAD_addresses(*it_inst, addresses, tmp_mask, false);
				//search LQ and update the valid bit
				for(std::vector<LONG>::iterator llsq = addresses.begin(); llsq != addresses.end(); llsq++){
					std::vector<LONG>::iterator aa = loadQ.address.begin(); std::vector<bool>::iterator vv = loadQ.valid.begin();
					
					for(; aa != loadQ.address.end(), vv != loadQ.valid.end(); aa++, vv++){
						if(*llsq == *aa && *vv == false && *aa > 0){ 
							(*vv) = true; 
							break;
						}
					}
				}
			} 


			if((*it_inst).is_uop){ //for uop operations -- compute mask ("considered" executed already)
#ifdef DEBUGEXE
				cout << "***Found a uop here with tag = " << (*it_inst).tag << ", pathT = " << (*it_inst).pathT << ", pathNT = " << (*it_inst).pathNT;
				cout << "ROB_ID = " << *it_id << endl; 
#endif
				path_push.push_back((*it_inst).tag); //push tag to validate, and compute new reconverged mask based on last tags
				mask_push.push_back((*it_inst).actual_mask);
				warp_push.push_back((*it_inst).warpid);
				pathtable[(*it_inst).warpid].rbit[(*it_inst).tag] = 1;

			}else{ 
				if(!((*it_inst).writes.empty())){ // && !((*it_inst).has_load || (*it_inst).has_store)){ //if not empty, there are dests to bypass
					
					std::vector<int>::iterator dest, wb, ifp_type; std::vector<char>::iterator it_dtype = (*it_inst).dest_type.begin();
					for(dest = (*it_inst).writes.begin(), wb = (*it_inst).orig_dests.begin(), ifp_type = (*it_inst).pirat_wtype.begin(); //instruction to look for
							dest != (*it_inst).writes.end(), wb != (*it_inst).orig_dests.end(), 
							ifp_type != (*it_inst).pirat_wtype.end(); 
							dest++, wb++, ifp_type++){
#ifdef DEBUGEXE
						cout << "searching for dest value = " << (*dest) << " (type = " << *it_dtype << ") ";
						if((*ifp_type)) cout << "[FP] ROB_ID = " << *it_id << endl;		
						else cout << "[INT] ROB_ID = " << *it_id << endl;
#endif

						if((*it_dtype) == 's'){ 
							write_back.push_back(*dest); 
							write_type.push_back((*ifp_type)); //int or floating pt
							write_entry.push_back(*wb); write_warp.push_back((*it_inst).warpid);
						}//else its a path
				
						//search the issueQ for this dest -> source value (double check type). Update issueQ
						//ISSUEQ BYPASS
						for(int i = 0; i < NO_QUEUE_TYPES; i++){ 	   
							if(!issueQ[i].sources.empty()){
						
								std::vector<std::vector<bool> >::iterator ii = issueQ[i].ready.begin(); //IQ to look into
								std::vector<std::vector<int> >::iterator it = issueQ[i].sources.begin(), pirat_source = issueQ[i].pirat_src.begin(); //int or fp
								std::vector<std::vector<char> >::iterator tt = issueQ[i].src_type.begin(); //s or p
								std::vector<DVInst>::iterator iq_inst = issueQ[i].iq.begin(); 	//dv-INST
							 
								std::vector<int>::iterator it_src, src_pirat; std::vector<bool>::iterator it_rdy; 
								std::vector<char>::iterator it_type; 

								//bypass source values
								for(; it != issueQ[i].sources.end(), ii != issueQ[i].ready.end(), tt != issueQ[i].src_type.end(), iq_inst != issueQ[i].iq.end();
									 it++, ii++, tt++, iq_inst++){ //if queue type == source type
					
									for(it_src = it->begin(), it_rdy = ii->begin(), it_type = tt->begin(), src_pirat = pirat_source->begin(); 
										it_src != it->end(), it_rdy != ii->end(), it_type != tt->end(), src_pirat != pirat_source->end(); 
										it_src++, it_rdy++, it_type++, src_pirat++){
										
										if(*src_pirat == 2){ //PIRAT type is not necessary for the instruction
											if(((*it_src) == (*dest)) && ((*it_type) == (*it_dtype))){  
												//matching source/dest and types (source or path)
												(*it_rdy) = true; //cout << "\t\tMarking as ready" << endl;
											}
										}else{
											if(((*it_src) == (*dest)) && ((*it_type) == (*it_dtype)) && (*src_pirat) == (*ifp_type)){  
												(*it_rdy) = true; //cout << "\t\tMarking as ready" << endl;
											}
										}
									}
								        pirat_source++;
								}
							}
						}
					} 

				}
			}
			//bypass path values to the IQ entries
			if(!path_push.empty()){
				for(int i = 0; i < NO_QUEUE_TYPES; i++){ //(-1 = don't go through MEM IQ)	
					std::vector<std::vector<bool> >::iterator ii = issueQ[i].ready.begin();
					std::vector<std::vector<int> >::iterator it = issueQ[i].sources.begin();
					std::vector<std::vector<char> >::iterator tt = issueQ[i].src_type.begin();	
			
					std::vector<int>::iterator it_src; std::vector<bool>::iterator it_rdy; 
					std::vector<char>::iterator it_type; 

					//bypass source values
					for(; it != issueQ[i].sources.end(), ii != issueQ[i].ready.end(), tt != issueQ[i].src_type.end(); it++, ii++, tt++){ //if queue type == source type
						for(it_src = it->begin(), it_rdy = ii->begin(), it_type = tt->begin(); 
							it_src != it->end(), it_rdy != ii->end(), it_type != tt->end(); it_src++, it_rdy++, it_type++){
							
							if((*it_type) == 'p' && !path_push.empty()){
						
								for(std::vector<int>::iterator pp = path_push.begin(); pp != path_push.end(); pp++){
									if((*it_src) == (*pp) && (!(*it_rdy))){
										(*it_rdy) = true;
										
									}
								}
							}
						}
					}
				}
			
			}
			
			(*it_done) = true; //update ROB can be committed
		
		}
	}

	update_issueQ_entries();
#ifdef DEBUG
	cout << "-------------------------	WRITEBACK     -----------------------------" << endl; 	
#endif
	//  writeback to PIRAT register validity ---------------------------------------------
	if(!write_back.empty()){
		std::vector<int>::iterator pp = write_back.begin(), ww = write_entry.begin(), wrp = write_warp.begin();
		for(std::vector<int>::iterator tt = write_type.begin(); tt != write_type.end(); tt++){	
#ifdef DEBUG
			cout << "WB to register " << *pp << " "; 	
#endif
			if(!(*tt)){ //integer PIRAT
#ifdef DEBUG			
				cout << " [int]" << endl;
#endif
				//update the PIRAT See if partial_ or deault_reg matched write_back renamed register. If so, updateto 1	
				if(pirat_int[*wrp].partial_reg[*ww] == *pp){
					pirat_int[*wrp].r_bit[*ww] = true; //cout << "[partial/int] found dest " << *pp << endl;
				}
				if(pirat_int[*wrp].default_reg[*ww] == *pp){
					pirat_int[*wrp].v_bit[*ww] = true; //cout << "[default/int] found dest " << *pp << endl;
				}


				//udpate the PRF valid bits
				prf.valid[*pp] = true;
				
	
			}else{ //FP PIRAT
#ifdef DEBUG
				cout << " [FP]" << endl;
#endif
				if(pirat_fp[*wrp].partial_reg[*ww] == *pp){
					pirat_fp[*wrp].r_bit[*ww] = true; //cout << "[partial/FP] found dest " << *pp << endl;
				}
				if(pirat_fp[*wrp].default_reg[*ww] == *pp){
					pirat_fp[*wrp].v_bit[*ww] = true; //cout << "[default/FP] found dest " << *pp << endl;
				}
						
				//update the VRF valid bits
				vrf.valid[*pp] = true;	
			}


			 pp++; ww++; wrp++;
		}
	}
	
	//writeback to pathtable and PIRAT mask.tags -----------------------------------
	if(!path_push.empty()){
		std::vector<int>::iterator pp, mask, wrp; int counter;
	
		for(pp = path_push.begin(), mask = mask_push.begin(), wrp = warp_push.begin(); 
					pp != path_push.end(), mask != mask_push.end(), wrp != warp_push.end(); pp++, mask++, wrp++){
			if(pathtable[*wrp].rbit[*pp] != -1){ //should not be the case (assert)
#ifdef DEBUGEXE
				cout << "{Path Table} Updating v = 1 for entry " << *pp << endl;
				print_pathtable(*wrp);
#endif
				pathtable[*wrp].rbit[*pp] = 1; //pathtable writeback
			}
			

			//PIRAT writeback: search PIRAT for tag and update to mask
			counter = 0;
			std::vector<int>::iterator mm; std::vector<bool>::iterator mt;
			for(mm = pirat_int[*wrp].mask_tag.begin(), mt = pirat_int[*wrp].mt_bit.begin(); 
			     mm != pirat_int[*wrp].mask_tag.end(), mt != pirat_int[*wrp].mt_bit.end(); mm++, mt++){						
				  if((*mm) == (*pp) && !(*mt)){
#ifdef DEBUGEXE
					cout << "{INT PIRAT} Updating tag " << *pp << " at entry " << counter << endl;
#endif
					(*mt) = true; 
					//and write mask <---------------
					(*mm) = (*mask);
				}
				counter++;			
			}
			counter = 0;
			for(mm = pirat_fp[*wrp].mask_tag.begin(), mt = pirat_fp[*wrp].mt_bit.begin(); 
			    mm != pirat_fp[*wrp].mask_tag.end(), mt != pirat_fp[*wrp].mt_bit.end(); mm++, mt++){	
				if((*mm) == (*pp) && !(*mt)){
#ifdef DEBUGEXE
					cout << "{FP PIRAT} Updating tag " << *pp << " at entry " << counter << endl;
#endif
					(*mt) = true; 
					//and write mask
					(*mm) = (*mask);
				}
				counter++ ;
			}	
		}
	}
}

void DataAnalyser::update_rob_inst(DVInst it_dv, LONG robid){
	bool found = false;
	std::list<DVInst>::iterator dvi = reorderB.inst.begin();
	std::list<LONG>::iterator it_id = reorderB.id.begin();
	for(; dvi != reorderB.inst.end(), it_id != reorderB.id.end(); dvi++, it_id++){

		if(robid == (*it_id)){ found = true;
			//cout << "updating miss and stat parameters: " << robid << " = " <<  (*it_id) << endl;
			int i = 0; int j = 0;
			for(std::vector<DVLoad>::iterator it = it_dv.loads.begin(); it != it_dv.loads.end(); it++){
				j = 0;
				for(std::vector<AddressLoadParams>::iterator addr = it->address_params.begin(); addr != it->address_params.end(); addr++){			
					(*dvi).loads[i].address_params[j].l1_miss = (*addr).l1_miss; //cout << "miss = " << (*addr).l1_miss << " ";
					(*dvi).loads[i].address_params[j].stat = (*addr).stat; //cout << "stat = " << (*addr).stat << endl;
					j++;
				}
				i++;
			}j++;
			//break; /////////
		}			
	}
	assert(found);
}


//////////////////////////////////////////////////////
//		Cache Functions
//////////////////////////////////////////////////////
void DataAnalyser::poll_cache(){
#ifdef DEBUGMEM
			cout << endl << "-----------------------------------------------" <<   endl;
#endif
		///////////////////////////////////////// LOADS  ////////////////////////////////////////////////		

		for(std::vector<DVInst>::iterator it_dv = waiting_on_loads.begin(); it_dv != waiting_on_loads.end(); ){ //<-----------
			int mask[threads_per_warp];
			convert_int_to_vectormask(get_mask_pathTable((*it_dv).tag, (*it_dv).warpid), mask, threads_per_warp); //get active mask  //get_mask_pathTable((*it_dv).tag, (*it_dv).warpid)
			//pathtable[(*it_dv).warpid].actual_mask[(*it_dv).tag]
			std::vector<int> smask(mask, mask+threads_per_warp);
			int count = 0;

			for(std::vector<DVLoad>::iterator it = it_dv->loads.begin(); it != it_dv->loads.end(); it++){
				bool load_ready = true; count = 0;

				//double check if load done				
				bool check = true;
				for(std::vector<AddressLoadParams>::iterator addr = it->address_params.begin(); addr != it->address_params.end(); addr++){
					if(((*addr).address_load_done == false || (*addr).address_load_done == 2) && mask[count] && (*addr).address > 0){ // check SQ (==2)
						if((*addr).address_load_done == 2) (*addr).storeQ = true;
						if(l1_cache.ready((*addr).address)){ 
							(*addr).address_load_done = true;
						}else check = false;

					} 
					count++;
				}
			
				if(check){
#ifdef DEBUGMEM
			printSQ();
#endif
					for(std::vector<AddressLoadParams>::iterator addr = it->address_params.begin(); addr != it->address_params.end(); addr++){
						if((l1_cache.ready((*addr).address))  && (*addr).address > 0) //|| (*addr).address_load_done > 0
							(*addr).address_load_done = true;
						else if(!(*addr).address_load_done && (*addr).address > 0){
							check = false;	
						}else if((*addr).address == 0)
							(*addr).address_load_done = true;
					
#ifdef DEBUGMEM
						cout << "\t[MEM] Checking for 0x" << hex16((*addr).address) <<  " [" << (*addr).address_load_done << "]" <<endl;
#endif	
					}
				}

				(*it_dv).load_done = check;

				count = 0;

				if(!(*it_dv).load_done){
#ifdef DEBUGMEM
					cout << "[ROB_ID = " << (*it_dv).id << "] !load_done " << endl;
#endif
					std::vector<int> cache_mask, sq_mask; count = 0; bool issuewcache = false;
						
					for(std::vector<AddressLoadParams>::iterator addr = it->address_params.begin(); addr != it->address_params.end(); addr++){
						if(((*addr).address_load_done == false )  && (*addr).address > 0){ //|| (*addr).address_load_done == 2 //&& mask[count]
							//create cache mask for requests (set in wakeup_dispatch)
							cache_mask.push_back(1); issuewcache = true;
						}else{
							cache_mask.push_back(0);
						}
						if((*addr).address_load_done == 2 && mask[count] && (*addr).address > 0){ //if in SQ, check if reached cache
							(*addr).storeQ = true;
							if(l1_cache.ready((*addr).address)){
								sq_mask.push_back(0); //no request necessary
								(*addr).address_load_done = true;
							}else{
								sq_mask.push_back(1); //request from SQ
								if(!(*addr).l1_miss) (*addr).l1_miss = true; //just in case
							}
						}else{
							sq_mask.push_back(0); //no sq
							//(*addr).storeQ = false;
						
						}						
						
						count++;
					}


					//DVInst may or may not have been issued to cache. Find out. Make a mask for not issued case
					if(!(*it).load_issued && issuewcache){
						(*it).load_issued = l1_cache.dv_load(&(*it),number_cycles,1, &cache_mask, (*it_dv).is_simd); 
						if(!(*it).load_issued) load_ready = false; 
#ifdef DEBUGMEM
						cout << "\tLoad issued = " << (*it).load_issued << ", requires issue ? " << issuewcache << endl;
#endif						
					}else if(issuewcache){ //check if ready
						count = 0;
						std::vector<AddressLoadParams>::iterator addr = it->address_params.begin(); std::vector<int>::iterator cash = cache_mask.begin();
						for(; addr != it->address_params.end(), cash != cache_mask.end(); addr++, cash++){
							if((*cash)){
#ifdef DEBUGMEM
								cout << "Thread: " << count << ", Addr: " << hex16((*addr).address) << endl;
#endif
								if(l1_cache.ready((*addr).address)){ 
									
									(*addr).address_load_done = true;  
#ifdef DEBUGMEM
									cout << " - and hit" << endl;
#endif
									
								}else{
									if(!(*addr).l1_miss && l1_cache.mshr->inMSHR((*addr).address)){ //else consider it a miss if in MSHR
										(*addr).l1_miss = true; 
										
									}
										
#ifdef DEBUGMEM
									if((*addr).l1_miss) cout << " - and miss" << endl;
									if(l1_cache.mshr->inMSHR((*addr).address)) cout << "In MSHR" << endl; 
									else cout << "Not in MSHR" << endl;
#endif

									load_ready = false;
								}
							}

							count++;
						}

					}
					//check SQ for any pending valid bits
					count = 0;
					std::vector<AddressLoadParams>::iterator addr = it->address_params.begin(); std::vector<int>::iterator sq = sq_mask.begin();
					for(; addr != it->address_params.end(), sq != sq_mask.end(); addr++, sq++){
						if(*sq){
							(*addr).storeQ = true;
#ifdef DEBUGMEM
							cout << "Thread: " << count << ", Addr: " << hex16((*addr).address) << " SQ status: " << endl;
#endif
							if(SQ_ready((*addr).address, (*it_dv).id, (*addr).dependent_id) || l1_cache.ready((*addr).address)){  //
#ifdef DEBUGMEM	
								cout << " ready " << endl;
#endif
								(*addr).address_load_done = true;  //if((*addr).l1_miss) (*addr).l1_miss = false; //<--------------

								//update the corresponding LQ's valid bit	
								std::vector<LONG>::iterator lqaddr = loadQ.address.begin(); 
								std::vector<bool>::iterator vv = loadQ.valid.begin();
								std::vector<LONG>::iterator rb = loadQ.id.begin();
								for(; lqaddr != loadQ.address.end(), vv != loadQ.valid.end(), rb != loadQ.id.end(); lqaddr++, vv++, rb++){
									if(*lqaddr == (*addr).address && *rb == (*it_dv).id){ 
										(*vv) = true;		
										break;
									}
								}
							}else{ 
#ifdef DEBUGMEM
								cout << "Not ready" << endl;
#endif								
								load_ready = false;
							}
						}
						count++;
					}
					
					if(load_ready){ //double check that
					for(std::vector<AddressLoadParams>::iterator addr = it->address_params.begin(); addr != it->address_params.end(); addr++){
						if((l1_cache.ready((*addr).address))  && (*addr).address > 0) //|| (*addr).address_load_done == 2
							(*addr).address_load_done = true;
						else if(!(*addr).address_load_done && (*addr).address > 0){
							load_ready = false;	
						}
					}
				}
					(*it_dv).load_done = load_ready;
			    }else{ 
#ifdef DEBUGMEM
					cout << "[ROB_ID = " << (*it_dv).id << "] Load IS Done " << endl;
#endif
					(*it_dv).load_done = load_ready;	
			    }
			}
	
			if((*it_dv).load_done){
				//update corresponding wait_load buffer for FU_DVInst
#ifdef DEBUGMEM
				cout << "Updating and erasing FU" << endl;
#endif

				update_rob_inst((*it_dv), (*it_dv).id);
				fu.update((*it_dv).id);	
				waiting_on_loads.erase(it_dv);
			
			}else{
				it_dv++;
			}
								
		}
#ifdef DEBUGMEM
			cout << "-----------------------------------------------" <<  endl <<  endl;
#endif
	//ls_current_warp = (ls_current_warp + 1)% conf.num_warps;*/
	//return true;
}


/////////////////////////////////////    End of Cache Functions    ////////////////////////////////////

