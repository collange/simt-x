
/////////////////////////////////////////
//     DEST RENAME WITH PIRAT
/////////////////////////////////////////
void DataAnalyser::rename_dest_pirat(std::vector<int> dest, std::vector<int> type, int num_dest, DVInst &dv, std::vector<int> &ow, std::vector<int> &owl, 
			std::vector<int> &owt, int i){ //ow will have same num of elements as dest

	int destination = dest[i]; //number of architectural register
		
		if(!type[i]){ //INT PIRAT//////////////////////////////////////////////////////////////////////
			statistics.pirat_wr_accesses++;
			//get a free physical reg
			int tmp = get_free_intreg(dv, false); //get a free physical reg
			ow.push_back(pirat_int[dv.warpid].default_reg[destination]); //keep previous value for ROB
			owl.push_back(destination); owt.push_back(0);
			//Merge and update PIRAT
#ifdef DEBUG
			cout << "[INT D Rename] generating a PHI uop - phi reg"; // << endl; //and generate phi
			cout << "[" << tmp << "] dest = " << destination << " w = " << dv.warpid << endl;
			cout << "replace: " << pirat_int[dv.warpid].default_reg[destination] << endl;
#endif
			dv.is_phi = true; //TEMPORARY for readback to make phi instructions		
			dv.default_reg.push_back(pirat_int[dv.warpid].default_reg[destination]);						
			dv.partial_reg.push_back(tmp); //tmp <- default AND NOT mask, (+) tmp AND mask
			dv.phi_reg.push_back(tmp);
			dv.reg_type.push_back(0);
			dv.phi_dests.push_back(destination);
			statistics.write_merges++;

			update_PIRAT_entry(tmp, false, false, dv.tag, destination, true, dv.warpid);
			update_dv_dest(dv, tmp, 0, destination);	


		}else{//FP/SIMD PIRAT ////////////////////////////////////////////////////////
			statistics.pirat_fp_wr_accesses++;
			int tmp = get_free_FPreg(dv, false); //get a free physical reg
			ow.push_back(pirat_fp[dv.warpid].default_reg[destination]); //keep previous value for ROB
			owl.push_back(destination); owt.push_back(1);
#ifdef DEBUG
			cout << "[FP D Rename] generating a PHI uop = reg phi "; // << endl; //and generate phi
			cout << "[" << tmp << "] arch = " << destination << " w = " << dv.warpid << endl;
			cout << "replace: " << pirat_fp[dv.warpid].default_reg[destination] << endl;
#endif
			dv.is_phi = true; //TEMPORARY for readback to make phi instructions		
			dv.default_reg.push_back(pirat_fp[dv.warpid].default_reg[destination]);						
			dv.partial_reg.push_back(tmp); 
			dv.phi_reg.push_back(tmp);
			dv.reg_type.push_back(1);
			dv.phi_dests.push_back(destination);
			statistics.write_fp_merges++;

			//Merge and update PIRAT
			update_PIRAT_entry(tmp, false, false, dv.tag, destination, false, dv.warpid);
			update_dv_dest(dv, tmp, 1, destination);
		}
}




/////////////////////////////////////////////////////
// Source Rename with PIRAT /////////////////////////
/////////////////////////////////////////////////////
void DataAnalyser::rename_sources_pirat(std::vector<int> src, std::vector<int> type, int num_src, DVInst &dv, std::vector<int> &ow, std::vector<int> &owl){
	for(int i = 0; i < num_src; i++){
		int source = src[i]; //number of architectural register
		if(!type[i]){ //INT PIRAT//////////////////////////////////////////////////////////////////////
			statistics.pirat_rd_accesses++;
			assert(pirat_int[dv.warpid].mask_tag[source] == -1);//doesn't have a mask or path tag - use default
#ifdef DEBUG
			cout << "[INT S RENAME] no previous - use default [" << pirat_int[dv.warpid].default_reg[source] << "]" << endl;
#endif
			update_dv_source(dv, pirat_int[dv.warpid].default_reg[source], 0, pirat_int[dv.warpid].v_bit[source]); 
			statistics.pirat_default_rd++;	

		}else{//FP/SIMD PIRAT//////////////////////////////////////////////////////////////////////////
			statistics.pirat_fp_rd_accesses++;
			assert(pirat_fp[dv.warpid].mask_tag[source] == -1); //doesn't have a mask or path tag - use default
#ifdef DEBUG
			cout << "[FP S RENAME] no previous - use default [" << pirat_fp[dv.warpid].default_reg[source] << "]" << endl;
#endif
			update_dv_source(dv, pirat_fp[dv.warpid].default_reg[source], 1, pirat_fp[dv.warpid].v_bit[source]); 
			statistics.pirat_fp_default_rd++;
		}
	}
}







//////////////////	Register Commit -- Release registers //////////////////////////////////////////// 
			if(reorderB.inst.front().is_phi){
				//get PIRAT type
#ifdef DEBUG
				cout << "Committing a PHI " << endl;
#endif
				int warp_no = reorderB.inst.front().warpid;
				DVInst tmp_dv = reorderB.inst.front();
				std::vector<int> owr = reorderB.ow_register.front();
				std::vector<int> owl = reorderB.ow_location.front();	
				std::vector<int> owt = reorderB.ow_type.front();			

				for(int i = 0; i < owl.size(); i++){
					int entry = owl[i];

					if(owt[i] == 0){ //Integer						
						//find partial and default in freelist and mark as free
#ifdef DEBUG
cout << "PHI INT Trying to release " << commit_pirat_int[warp_no].default_reg[entry] <<" -> " << owr[i] << ", entry = " << entry << " w = " << warp_no << endl;
#endif
						assert(owr[i] == commit_pirat_int[warp_no].default_reg[entry]);  //partial?!?
						release_register(commit_pirat_int[warp_no].default_reg[entry], 0);
						//release_register(commit_pirat_int[warp_no].partial_reg[entry], 0);
#ifdef DEBUG
						cout << "{INT-phi} Releasing registers " << commit_pirat_int[warp_no].default_reg[entry];
						cout << ", entry = " << entry << " w = " << warp_no << endl;
						cout << "Replacing default with = " << tmp_dv.writes[i] << endl;
#endif
						//replace default entry and update partial 
						commit_pirat_int[warp_no].default_reg[entry] = tmp_dv.writes[i];
						commit_pirat_int[warp_no].partial_reg[entry] = -1;
						commit_pirat_int[warp_no].r_bit[entry] = false; //bc it's a phi
						commit_pirat_int[warp_no].mt_bit[entry] = false; 
						commit_pirat_int[warp_no].mask_tag[entry] = -1;
					
					}else if(owt[i] == 1){ //FP
						//find partial and default in freelist and mark as free
#ifdef DEBUG
		cout << "PHI FP Trying to release " << commit_pirat_fp[warp_no].default_reg[entry] << " -> " << owr[i] << ", entry = " << entry << " w = " << warp_no << endl;
#endif
						assert(owr[i] == commit_pirat_fp[warp_no].default_reg[entry]);
						release_register(commit_pirat_fp[warp_no].default_reg[entry], 1);
						//release_register(commit_pirat_fp[warp_no].partial_reg[entry], 1);
#ifdef DEBUG
						cout << "{FP-phi} Releasing registers " << commit_pirat_fp[warp_no].default_reg[entry];
						cout << ", entry = " << entry << " w = " << warp_no << endl;
						cout << "Replacing default with = " << tmp_dv.writes[i] << endl;
#endif
						//replace default entry and update partial
						commit_pirat_fp[warp_no].default_reg[entry] = tmp_dv.writes[i];
						commit_pirat_fp[warp_no].partial_reg[entry] = -1;
						commit_pirat_fp[warp_no].r_bit[entry] = false; //bc it's a phi
						commit_pirat_fp[warp_no].mt_bit[entry] = false; 
						commit_pirat_fp[warp_no].mask_tag[entry] = -1;
										
					}
				}

			}else if(!reorderB.inst.front().is_phi && !reorderB.inst.front().is_uop){ 
				DVInst tmp_dv = reorderB.inst.front();
				int warp_no = reorderB.inst.front().warpid;
				std::vector<int> owreg = reorderB.ow_register.front();
				std::vector<int> owl = reorderB.ow_location.front(); std::vector<int> owt = reorderB.ow_type.front();
				//cout << owreg.size() << " " << owl.size() << " "<< owt.size() << " " << tmp_dv.writes.size() << endl;
				for(int i = 0; i < tmp_dv.writes.size(); i++){
					int release_reg = -1; warp_no = reorderB.inst.front().warpid;
					//if(tmp_dv.writes.size() != 0) cout << owreg[i] << " " << owl[i] << " "<< owt[i] << " " << tmp_dv.writes[i] << endl;
					if(owt[i] == 0){ //Integer	
						int entry = owl[i];		
						if(commit_pirat_int[warp_no].partial_reg[entry] == -1){  //if no partial present. Write
							////!commit_pirat_int[warp_no].mt_bit[entry]){
							//check if previous inst was a phi-uop which already merged and renamed the entry
							if(commit_pirat_int[warp_no].default_reg[entry] == tmp_dv.writes[i]){ //owreg[i]
#ifdef DEBUG
								cout << "INT present in default = " << tmp_dv.writes[i] << endl;
#endif
								continue;
							}else{
								commit_pirat_int[warp_no].partial_reg[entry] = tmp_dv.writes[i];
								commit_pirat_int[warp_no].r_bit[entry] = true; 
								commit_pirat_int[warp_no].mt_bit[entry] = true;
								commit_pirat_int[warp_no].mask_tag[entry] = tmp_dv.actual_mask;//get_mask_pathTable(tmp_dv.tag); 
#ifdef DEBUG
								cout << "INT Writing Partial [-1] with " << commit_pirat_int[warp_no].partial_reg[entry] << " [default] = " << commit_pirat_int[warp_no].default_reg[entry] << " -> " << owreg[i] << " entry = " << entry << " w = " << warp_no << endl;
#endif 
							}				
	
						}else if(owreg[i] != -1){ //if(commit_pirat_int[warp_no].mt_bit[entry] ){ //default and partial register are present
							//discover whether the register is default or partial
							bool is_partial = false; bool is_default = false; 
							if(owreg[i] == commit_pirat_int[warp_no].partial_reg[entry]) is_partial = true;
							if(owreg[i] == commit_pirat_int[warp_no].default_reg[entry]) is_default = true;

							assert(is_partial != is_default);

							//assert(assert_ow_correct(get_mask_pathTable(tmp_dv.tag, tmp_dv.warpid), commit_pirat_int[warp_no].mask_tag[entry])); //assert criteria of partial overwrite is correct
							if(is_partial){
#ifdef DEBUG
								cout << "INT (P) Trying to release " << commit_pirat_int[warp_no].partial_reg[entry] << " -> " << owreg[i] << " entry = " << entry << " w = " << warp_no << endl;
#endif
								assert(owreg[i] == commit_pirat_int[warp_no].partial_reg[entry]);
								release_reg = commit_pirat_int[warp_no].partial_reg[entry];
				
								commit_pirat_int[warp_no].partial_reg[entry] = tmp_dv.writes[i];
								//commit_pirat_int[warp_no].r_bit[entry] = true; 
								commit_pirat_int[warp_no].mt_bit[entry] = true;
								//commit_pirat_int[warp_no].mask_tag[entry] = tmp_dv.actual_mask;//get_mask_pathTable(tmp_dv.tag, tmp_dv.warpid);    
								assert(release_reg != -1);
								//if(release_reg != -1){ // && div_counter == 0){ //release the partial register
								release_register(release_reg, 0);
	#ifdef DEBUG
								cout << "{INT} Releasing register " << release_reg << ", entry = " << entry << endl;
								cout << "Partial now = " << tmp_dv.writes[i] << endl;
	#endif
							}else{
#ifdef DEBUG
								cout << "INT (D) Trying to release " << commit_pirat_int[warp_no].default_reg[entry] << " -> " << owreg[i] << " entry = " << entry << " w = " << warp_no << endl;
#endif
								assert(owreg[i] == commit_pirat_int[warp_no].default_reg[entry]);
								release_reg = commit_pirat_int[warp_no].default_reg[entry];
				
								commit_pirat_int[warp_no].default_reg[entry] = tmp_dv.writes[i];
								commit_pirat_int[warp_no].r_bit[entry] = true; 
								//commit_pirat_int[warp_no].mt_bit[entry] = true;
								//commit_pirat_int[warp_no].mask_tag[entry] = tmp_dv.actual_mask;//get_mask_pathTable(tmp_dv.tag, tmp_dv.warpid);    
								assert(release_reg != -1);
								//if(release_reg != -1){ // && div_counter == 0){ //release the partial register
								release_register(release_reg, 0);
	#ifdef DEBUG
								cout << "{INT} Releasing register " << release_reg << ", entry = " << entry << endl;
								cout << "Default now = " << tmp_dv.writes[i] << endl;
	#endif
				
							}																	
						}
					}else if(owt[i] == 1){ //FP
						int entry = tmp_dv.orig_dests[i];		
						if(commit_pirat_fp[warp_no].partial_reg[entry] == -1){//!commit_pirat_fp[warp_no].mt_bit[entry]){ //No partial present
							if(commit_pirat_fp[warp_no].default_reg[entry] == tmp_dv.writes[i]){
#ifdef DEBUG
								cout << "FP present in default = " << tmp_dv.writes[i] << endl;
#endif
								continue;
							}else{
								commit_pirat_fp[warp_no].partial_reg[entry] = tmp_dv.writes[i];
								commit_pirat_fp[warp_no].r_bit[entry] = true; 
								commit_pirat_fp[warp_no].mt_bit[entry] = true;
								commit_pirat_fp[warp_no].mask_tag[entry] = tmp_dv.actual_mask; 
#ifdef DEBUG
								cout << "FP Writing Partial [-1] with " << commit_pirat_fp[warp_no].partial_reg[entry] << " [default] = " << commit_pirat_fp[warp_no].default_reg[entry] << " -> " << owreg[i] << " entry = " << entry << " w = " << warp_no << endl;
#endif 
							}						
	
						}else if(owreg[i] != -1){ // if(commit_pirat_fp[warp_no].mt_bit[entry]){ //default and partial register are present

							//discover whether the register is default or partial
							bool is_partial = false; bool is_default = false; 
							if(owreg[i] == commit_pirat_fp[warp_no].partial_reg[entry]) is_partial = true;
							if(owreg[i] == commit_pirat_fp[warp_no].default_reg[entry]) is_default = true;

							assert(is_partial != is_default);

							if(is_partial){
#ifdef DEBUG
								cout << "FP (P) Trying to release " << commit_pirat_fp[warp_no].partial_reg[entry] << " -> " << owreg[i] << " entry = " << entry << " w = " << warp_no << endl;
#endif
								assert(owreg[i] == commit_pirat_fp[warp_no].partial_reg[entry]);
								
								release_reg = commit_pirat_fp[warp_no].partial_reg[entry];					
								commit_pirat_fp[warp_no].partial_reg[entry] = tmp_dv.writes[i];
								//commit_pirat_fp[warp_no].r_bit[entry] = true; 
								commit_pirat_fp[warp_no].mt_bit[entry] = true;
								//commit_pirat_fp[warp_no].mask_tag[entry] =  tmp_dv.actual_mask; //get_mask_pathTable(tmp_dv.tag);

								assert(release_reg != -1);
								release_register(release_reg, 1);			
#ifdef DEBUG
								cout << "{FP} Releasing register " << release_reg << ", entry = " << entry << endl;
								cout << "Partial now = " << tmp_dv.writes[i] << endl;
#endif
							}else{
#ifdef DEBUG
								cout << "FP (D) Trying to release " << commit_pirat_fp[warp_no].default_reg[entry] << " -> " << owreg[i] << " entry = " << entry << " w = " << warp_no << endl;
#endif
								assert(owreg[i] == commit_pirat_fp[warp_no].default_reg[entry]);
								
								release_reg = commit_pirat_fp[warp_no].default_reg[entry];					
								commit_pirat_fp[warp_no].default_reg[entry] = tmp_dv.writes[i];
								commit_pirat_fp[warp_no].r_bit[entry] = true; 
								//commit_pirat_fp[warp_no].mt_bit[entry] = true;
								//commit_pirat_fp[warp_no].mask_tag[entry] =  tmp_dv.actual_mask; //get_mask_pathTable(tmp_dv.tag);

								assert(release_reg != -1);
								release_register(release_reg, 1);			
#ifdef DEBUG
								cout << "{FP} Releasing register " << release_reg << ", entry = " << entry << endl;
								cout << "Default now = " << tmp_dv.writes[i] << endl;
#endif

							}		
						}//else{ //cout << tmp_dv.writes[i] << endl; 
						//	assert(tmp_dv.writes[i] == -1);}
					}
				
				}
			} 
				///////////////////////////////////// end of register recycle ////////////////////////
