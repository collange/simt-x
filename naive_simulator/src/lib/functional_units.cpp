#include "functional_units.hpp"

using namespace std;

void FunctionalUnits::remove_load_wait(LONG id){
	dvi_load_wait.remove(id);
}

//// called by DataAnalyser();
bool FunctionalUnits::fu_available(){
	for(int i = 0; i < MAX_FU_TYPES; i++){
		for(int k = 0; k < maxfu[i]; k++){
			if(!blocked_fu[i][k]){
				return true;
			}
		}
	}
	return false;
}


/***********************************************************************************
   setup_execution(....) for restricted ports -- mainly MUL/DIV SIMD and BRANCH
   - checks the FUs for availability by PORT
   - maintains dependencies and schedules accordingly to appropriate port
************************************************************************************/
bool FunctionalUnits::setup_execution(FU_DVInst dvi, int port, bool pipelined){ //if br_gen or mul_gen restricted
	if(blocked_port[port])
		return false; //port is already being used this clock cycle
		
	int k = 0; //dedicated FU at the port (for specialized exe - mul/div or branch)
	blocked_port[port] = true;
	if(port == mul_gen){ //only MUL/DIV SIMD-like insts go here
		nonpipelined_fu[port][k] = pipelined;
		if(dvi.is_simd){
			blocked_cycles[port][k] = dvi.num_insts - 1; //num_insts assigned at fetch (based on SSE or AVX)
			for(int l = 0; l < dvi.num_insts; l++){
				if(l == (dvi.num_insts -1)){
					dvi.lastinst = true; //keep track of last inst
				}else{
					dvi.lastinst = false;
				}
				if(!pipelined){
					fu_dvinst[port][k].push_back(dvi);
					blocked_cycles[port][k] = ((dvi.num_insts) * 20) - 1; //for FU - if not pipelined, will be blocked for num_insts*20 longer
					dvi.cycles_remaining+=20; //For FU_DVinst -- update structure
				}else{
					fu_dvinst[port][k].push_back(dvi);
					blocked_cycles[port][k] = (dvi.num_insts-1);
					dvi.cycles_remaining++;
				}
			}
			if(!pipelined){
				assert(fu_dvinst[port][k].back().cycles_remaining ==( dvi.cycles_remaining - 20));
			}else{
				assert(fu_dvinst[port][k].back().cycles_remaining ==( dvi.cycles_remaining - 1));
			}
		}else{	//if non-SIMD
			dvi.lastinst = true;
			if(!pipelined){
				blocked_cycles[port][k] = 20;
			}else{
				blocked_cycles[port][k] = 0;
			}
			fu_dvinst[port][k].push_back(dvi);
		}
		return true;
	}else{ //for branches
		dvi.lastinst = true;
		blocked_cycles[port][k] = 0;
		fu_dvinst[port][k].push_back(dvi);
		return true;
	}
			
	return false;
}

#ifdef FREE_PHI
bool FunctionalUnits::phi_execution(FU_DVInst dvi){
	phi_FU.push_back(dvi);
	return true;
}
#endif

/***********************************************************************************
   flex_execution(....) for flexible execution - not restricted to a certain port
   - checks the flex FUs for availability first
   - then goes to the other types of ports
************************************************************************************/
bool FunctionalUnits::flex_execution(FU_DVInst dvi){
	int k = 0; //FIRST check flex port for availability at given time	
	if(!blocked_port[flex]){ //if not blocked
		blocked_port[flex] = true;

		if(dvi.is_simd){
			int max = 100; k = 1;
			for(int i = 1; i < MAX_UNITS_PORT; i++){ //find port assignment - equal utilization ([0] = special unit)
				if(fu_dvinst[flex][i].size() < max){
					k = i;
					max = fu_dvinst[flex][i].size();
				} 
			}	
			blocked_cycles[flex][k] = dvi.num_insts -1;		
			for(int l = 0; l < dvi.num_insts;l++){
				if(l == (dvi.num_insts -1)){
					dvi.lastinst = true;
				}else{
					dvi.lastinst = false;
				}
				fu_dvinst[flex][k].push_back(dvi);
				dvi.cycles_remaining++;
			}
			assert(fu_dvinst[flex][k].back().cycles_remaining ==( dvi.cycles_remaining -1));			
		}else{ //use k = 0 port (non simd)
			dvi.lastinst = true;
			blocked_cycles[flex][k] = 0;
			fu_dvinst[flex][k].push_back(dvi);
		}
		return true;	
	}

	//else if flex was blocked, try the other ports
	bool schedulable = false;

	int port = 0;
	for(int j = 0; j < MAX_PORTS-1; j++){//else check the other ports
		if(!blocked_port[j]){
			blocked_port[j] = true;
			port = j; schedulable = true;
			break;
		}
	}
	if(schedulable){
		if(dvi.is_simd){		
			//find port assignment - equal utilization ([0] = special unit)
			int max = 100; k = 1;
			for(int i = 1; i < MAX_UNITS_PORT; i++){ 
				if(fu_dvinst[port][i].size() < max){
					k = i;
					max = fu_dvinst[port][i].size();
				} 
			}	
			blocked_cycles[port][k] = dvi.num_insts -1;		
			for(int l = 0; l < dvi.num_insts;l++){
				if(l == (dvi.num_insts -1)){
					dvi.lastinst = true;
				}else{
					dvi.lastinst = false;
				}
				fu_dvinst[port][k].push_back(dvi);
				dvi.cycles_remaining++;
			}
			assert(fu_dvinst[port][k].back().cycles_remaining ==( dvi.cycles_remaining -1));			
		}else{
			dvi.lastinst = true;
			blocked_cycles[port][k] = 0;
			fu_dvinst[port][k].push_back(dvi);
		}
		return true;	
	}

	return false;

}

bool FunctionalUnits::setup_memory(FU_DVInst dvi){ 
	if(!blocked_port[port_mem]) //if not blocked
		blocked_port[port_mem] = true; //calculate effective address
	else return false;
#ifdef DEBUG
	cout << "Issue/Exe [STORE] == rob ID [" << (dvi).rob_id << "]" << endl; 
#endif
	dvi.lastinst = true;
	blocked_cycles[port_mem][mem_unit] = 0;
	fu_dvinst[port_mem][0].push_back(dvi);
	return true;
}

/*********************************************
If schedulable, find an FU to execute the 
instruction on (dvi) based on dv_uop type
*********************************************/
bool FunctionalUnits::getfu(FU_DVInst dvi, sa_uoptype dv_uop, int iq_id, int rob_id){
	bool outcome = false;
	dvi.rob_id = rob_id;

	if((QueueTypes)iq_id == BR){ //can only be mapped to dvi port "br_gen"
		outcome = setup_execution(dvi, br_gen, false);	//if false, not scheduled. else scheduled. continue
	
	}else if((QueueTypes)iq_id == MEM){ 
		if(dvi.has_load){
			wait_on_load_buffer.push_back(dvi);
			outcome = true;
		}else{
			outcome = setup_memory(dvi);

		}
#ifdef FREE_PHI
	}else if((QueueTypes)iq_id == PHI){
		outcome = phi_execution(dvi);
#endif	

	}else if(dvi.uop_type==SA_UOP_AVX_FPMULVEC || dvi.uop_type==SA_UOP_SSE_FPMULVEC 
		 || dvi.uop_type==SA_UOP_AVX_FPMULSCAL || dvi.uop_type==SA_UOP_SSE_FPMULSCAL 
		 || dvi.uop_type==SA_UOP_FPMUL || dvi.uop_type==SA_UOP_AVX_IMUL 
		|| dvi.uop_type==SA_UOP_SSE_IMUL || dvi.uop_type==SA_UOP_IMUL ){

		outcome = setup_execution(dvi, mul_gen, true);
		
	}else if(dvi.uop_type==SA_UOP_AVX_FPDIVVEC || dvi.uop_type==SA_UOP_SSE_FPDIVVEC 	
		|| dvi.uop_type==SA_UOP_AVX_FPDIVSCAL || dvi.uop_type==SA_UOP_SSE_FPDIVSCAL ||
		dvi.uop_type==SA_UOP_FPDIV || dvi.uop_type==SA_UOP_AVX_IDIV || 
		dvi.uop_type==SA_UOP_SSE_IDIV ||dvi.uop_type==SA_UOP_IDIV){

		outcome = setup_execution(dvi, mul_gen, false);

	}else{//can be mapped to a "flex" port, SIMD or general
		
		outcome = flex_execution(dvi); 
			
	}
	
	return outcome;
}

/****************************************************************
   Searches the wait_on_load_buffer. If an inst is ready, then
   schedules the instruction on an available FU based on type	
/*****************************************************************/
void FunctionalUnits::execute_inst_with_loads(ROB &reorderB){
	
	for(std::vector<FU_DVInst>::iterator inst = wait_on_load_buffer.begin(); inst != wait_on_load_buffer.end(); ){ //<---- FU_DVInst
#ifdef DEBUG
		//cout << "[LOAD] Trying to schedule ROB_ID = " << (*inst).rob_id << endl;
#endif
		//schedule if load done
		if((*inst).load_done){  
			
			if(!blocked_port[port_mem]){ //if not blocked, schedule
				blocked_port[port_mem] = true; 

				if((*inst).is_simd){ 
					blocked_cycles[port_mem][mem_unit] = (*inst).num_insts -1;		
					for(int l = 0; l < (*inst).num_insts;l++){
						if(l == ((*inst).num_insts -1)){
							(*inst).lastinst = true;
						}else{
							(*inst).lastinst = false;
						}
						fu_dvinst[port_mem][mem_unit].push_back((*inst));
						(*inst).cycles_remaining++;
					}
					assert(fu_dvinst[port_mem][mem_unit].back().cycles_remaining ==( (*inst).cycles_remaining -1));			
				}else{ //use k = 0 port (non simd)
					(*inst).lastinst = true; 
					blocked_cycles[port_mem][mem_unit] = 0; 
					fu_dvinst[port_mem][mem_unit].push_back(*inst);
				}
#ifdef DEBUG
				cout << "Issue/Exe [LOAD] == rob ID [" << (*inst).rob_id << "]" << endl; //load has been scheduled to a FU_DVInst[][]
#endif											
				
				wait_on_load_buffer.erase(inst); //remove from buffer
				break;
			}else inst++;
		}else{
			inst++;
		}
	}
	
}


/*********************************************
  Updates load values if load_done = true
  called from cache_poll()
*********************************************/
void FunctionalUnits::update(LONG robid){
	for(std::vector<FU_DVInst>::iterator inst = wait_on_load_buffer.begin(); inst != wait_on_load_buffer.end(); inst++){
		if((*inst).rob_id == robid){
			(*inst).load_done = true;
			break;
		}
	}

}

int count_active(int threads_per_warp, int mask){
	int ret = 0;

	int rem; int new_mask[threads_per_warp];
	int count = 0; 
	while(count < threads_per_warp){
		rem = mask%2;
		mask = mask/2;
		new_mask[count] = rem; 
		count++;
	}

	for(int j = 0; j < threads_per_warp; j++){
		if(new_mask[j] == 1) ret++;
	}

	return ret;
}

bool FunctionalUnits::is_in_ROB(int robid, ROB reorderB){
	bool present = false;
	for(std::list<LONG>::iterator it_rob = reorderB.id.begin(); it_rob != reorderB.id.end(); it_rob++){
		if(*it_rob == robid){
			present = true;
			break;
		}
	}

	return present;
}

/********************************************************
  Iterates through all the FUs to see if any instruction has
  finished executing. Mark as data_valid in ROB
*********************************************************/
void FunctionalUnits::execute(ROB &reorderB, LONG &dv_ins_count, LONG &ins_count, int threads_per_warp){ //return ins_count, pass threads_per_warp
	//check memory
	for(std::vector<FU_DVInst>::iterator it = fu_dvinst[port_mem][mem_unit].begin(); it != fu_dvinst[port_mem][mem_unit].end(); ){
		if(it->is_simd)
			it->cycles_remaining--;
		if(it->cycles_remaining <= 0)
			it->lastinst = true;	
		
		if(it->lastinst){ //find DVinst in ROB and mark as data valid, and clear FU (will WB, bypass when function returns)
#ifdef DEBUG
			cout << "**** Finished [MEM] Executing inst ROB_ID = " << it->rob_id << endl;
#endif
			std::list<bool>::iterator valid, done; 
			std::list<LONG>::iterator it_rob; std::list<DVInst>::iterator inst;

			//check first to see if ROB_ID still in ROB - may have been speculative and DV-Inst flushed
			//if(is_in_ROB(it->rob_id, reorderB)){
				for(valid = reorderB.data_valid.begin(), it_rob = reorderB.id.begin(), done = reorderB.done.begin(), inst = reorderB.inst.begin(); 
				valid != reorderB.data_valid.end(), it_rob != reorderB.id.end(), done != reorderB.done.end(), inst != reorderB.inst.end(); 
				valid++, it_rob++, done++, inst++){
					if(it->rob_id == (*it_rob)){ 
	#ifdef DEBUG
						cout << "((ROB marked))"; 
	#endif
						dv_ins_count++;
						ins_count += count_active(threads_per_warp, (*inst).actual_mask);
						
						(*valid) = true; (*done) = false;
						it = fu_dvinst[port_mem][mem_unit].erase(it);
	#ifdef DEBUG
						cout << endl;
	#endif
						break;						
					}
				}
			/*}else{
				//dv_ins_count++;
				//ins_count += count_active(threads_per_warp, (*inst).actual_mask);		
				it = fu_dvinst[port_mem][mem_unit].erase(it);
			}*/
		}else{
			it++;
		}	
	}

	for(int port = 0; port < MAX_PORTS; port++){
		for(int unit = 0; unit < maxfu[port]; unit++){
			for(std::vector<FU_DVInst>::iterator it = fu_dvinst[port][unit].begin(); it != fu_dvinst[port][unit].end(); ){
		
				if(it->is_simd)
					it->cycles_remaining--;
				if(it->cycles_remaining <= 0)
					it->lastinst = true;	
				
				if(it->lastinst){ //find DVinst in ROB and mark as data valid, and clear FU (will WB, bypass when function returns)
#ifdef DEBUG
					cout << "**** Finished Executing inst ROB_ID = " << it->rob_id << endl;
#endif
					//check first to see if ROB_ID still in ROB - may have been speculative and DV-Inst flushed
					//if(is_in_ROB(it->rob_id, reorderB)){
						std::list<bool>::iterator valid, done; 
						std::list<LONG>::iterator it_rob; std::list<DVInst>::iterator inst;
						for(valid = reorderB.data_valid.begin(), it_rob = reorderB.id.begin(), done = reorderB.done.begin(), inst = reorderB.inst.begin(); 
						valid != reorderB.data_valid.end(), it_rob != reorderB.id.end(), done != reorderB.done.end(), inst != reorderB.inst.end(); 
						valid++, it_rob++, done++, inst++){
							if(it->rob_id == (*it_rob)){ 
								if(!(*inst).is_uop && !(*inst).is_phi){
									dv_ins_count++;
									ins_count += count_active(threads_per_warp, (*inst).actual_mask);
								}

	#ifdef DEBUG
								cout << "((ROB marked))"; 
	#endif
								if((*it).is_uop){ //secure microop 
									(*inst).is_uop = true; (*inst).tag = (*it).tag;
									(*inst).pathT = (*it).pathT; 
									(*inst).pathNT = (*it).pathNT;
								}
								
	#ifdef DEBUG
								if((*inst).is_branch && (*inst).new_path){ 
									cout << " [branch], tag = " << (*inst).tag << ", pathT = " << (*inst).pathT;
								}else if((*inst).is_branch){
									cout << " [branch], tag = " << (*inst).tag;
								}
	#endif
								(*valid) = true; (*done) = false;
								it = fu_dvinst[port][unit].erase(it);
	#ifdef DEBUG
								cout << endl;
	#endif
								break;						
							}
						}
					/*}else{
						//dv_ins_count++;
						//ins_count += count_active(threads_per_warp, (*inst).actual_mask);		
						it = fu_dvinst[port][unit].erase(it);
					}*/
				}else{
					it++;

				}	
			}
		}
	}
	//////////////////////////////////////////////////////////// for free tests only /////////////////////////////////
	#ifdef FREE_PHI
		for(std::vector<FU_DVInst>::iterator it = phi_FU.begin(); it != phi_FU.end(); ){
			
	#ifdef DEBUG
			cout << "**** Finished [PHI] Executing inst ROB_ID = " << it->rob_id << endl;
	#endif
			std::list<bool>::iterator valid, done; 
			std::list<LONG>::iterator it_rob; std::list<DVInst>::iterator inst;
			for(valid = reorderB.data_valid.begin(), it_rob = reorderB.id.begin(), done = reorderB.done.begin(), inst = reorderB.inst.begin(); 
				valid != reorderB.data_valid.end(), it_rob != reorderB.id.end(), done != reorderB.done.end(), inst != reorderB.inst.end(); 
				valid++, it_rob++, done++, inst++){
				if(it->rob_id == (*it_rob)){ 
	#ifdef DEBUG
					cout << "((ROB marked))" << endl; 
	#endif
					(*valid) = true; (*done) = false;
					it = phi_FU.erase(it);
					break;						
				}
			}
		}
	#endif
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

//we are implementing a fully pipelined fu so all the functional units will be available in next cycle
void FunctionalUnits::clear_fu(){
	//new clock cycle - none of the ports are blocked
	for(int i = 0; i < MAX_PORTS; i++)
		blocked_port[i] = false;	
	
	for(int i = 0; i < MAX_FU_TYPES; i++){
		for(int k = 0; k < maxfu[i]; k++){
			blocked_cycles[i][k]--;
			if(blocked_cycles[i][k] <= 0){
				blocked_fu[i][k] = false;
				/*for(std::vector<FU_DVInst>::iterator it = fu_dvinst[i][k].begin(); it != fu_dvinst[i][k].end(); it++){
					it->lastinst = true;

				}*/
				
			}
			
			
		}
	}
}

