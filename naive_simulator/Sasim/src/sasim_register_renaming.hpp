#ifndef SASIM_REGISTER_RENAMING_HPP
#define SASIM_REGISTER_RENAMING_HPP

#include <vector>

#include "sasim_uop_inst.hpp"

class sasim;

class register_renaming {
 public:
  vector<int64_t> regs;
  int64_t flags;

  void init();
  int64_t & rename(sa_reg r);
  void rename_uop(sasim & core, uop_ptr uptr);
  void update(uop_ptr uptr);
  void repair(register_renaming & rr);
};

#endif // End ifndef SASIM_REGISTER_RENAMING_HPP