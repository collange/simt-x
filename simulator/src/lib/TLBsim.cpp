/*
 * TLBsim.cpp
 *
 *  Created on: Sep 15, 2014
 *      Author: skalathi
 */

#include"TLBsim.hpp"



bool TLB::tlb_access(LONG address){
	//pthread_t w_thread;
	//pthread_t* w_threads = (pthread_t*)malloc(num_threads * sizeof(w_thread));
	unsigned long long page_number = address >> num_bits;
	/*if(!tlb_split){
		threadid = 0;

	}*/
	/*std::cout<<"Address "<<std::hex<<address<<std::endl;
	std::cout<<"Page "<<std::hex<<page_number<<std::endl;
	std::cout<<"Current tlb "<<std::endl;
	for(int i=0;i< tlb_size;i++){
		std::cout<<std::hex<< tlbarray[i]<<"("<<std::dec<<update_time[i]<<") ";
	}
	std::cout<<std::endl;
	//std::cout<<"Current time "<<std::endl;
*/
	//TLB walk
	access_count++;


	/*for(int i=0;i< tlb_size;i++){
		if(tlbarray[i] == page_number){
			//std::cout<<"HIT "<<std::endl;
			count++;
			update_time[i] = count;
			tlb_hit++;
			return;
		}
	}*/
	if(tlb_lru.exist(page_number)){
		tlb_lru.get(page_number);
		tlb_hit++;
		return true;
	}

	//std::cout<<"MISS "<<std::endl;
	//TLB miss. LRU
	/*int index = 0;
	//unsigned long high = 0UL;
	unsigned long low = update_time[index];
	for(int i=0;i< tlb_size;i++){
		if(update_time[i] <= low){
			low=update_time[i];
			index = i;
		}
	}
	//replace
	//std::cout<<"replace "<< index<<std::endl;
	tlbarray[index] = page_number;
	count++;
	update_time[index] = count;
	tlb_miss++;
	*/
	tlb_lru.put(page_number,0Ull);
	tlb_miss++;
	return false;
}

