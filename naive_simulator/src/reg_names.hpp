#ifndef REG_NAMES_HPP
#define REG_NAMES_HPP

#include <stdlib.h>
#include <string>

#define NUM_GPRS 85 
#define NUM_REGS 258
#define ALL_SIMD_GPR 48
#define SIMD_GPR 16
#define INT_GPR 16

string reg_name[NUM_REGS] ={    
    "REG_INVALID",
    "REG_RBASE",
    "REG_GR_BASE",  ///< rdi
    "REG_RDI",      ///< edi on a 32 bit machine, rdi on 64
    "REG_RSI",                ///< rsi
    "REG_RSI",      ///< esi on a 32 bit machine, rsi on 64
    "REG_RBP",                ///< rbp
    "REG_RBP",      ///< ebp on a 32 bit machine, rbp on 64
    "REG_RSP",                ///< rsp
    "REG_RSP",///< esp on a 32 bit machine, rsp on 64
    "REG_RBX",                ///< rbx
    "REG_RBX",      ///< ebx on a 32 bit machine, rbx on 64
    "REG_RDX",                ///< rdx
    "REG_RDX",      ///< edx on a 32 bit machine, rdx on 64
    "REG_RCX",                ///< rcx
    "REG_RCX",      ///< ecx on a 32 bit machine, rcx on 64
    "REG_RAX",                ///< rax
    "REG_RAX",      ///< eax on a 32 bit machine, rax on 64
    "REG_R8",
    "REG_R9",
    "REG_R10",
    "REG_R11",
    "REG_R12",
    "REG_R13",
    "REG_R14",
    "REG_R15",
    "REG_R15",
    "REG_SEG_BASE",
    "REG_SEG_BASE",
    "REG_SEG_SS",
    "REG_SEG_DS",
    "REG_SEG_ES",
    "REG_SEG_FS",
    "REG_SEG_GS",
    "REG_SEG_GS",
    "REG_RFLAGS",
    "REG_RFLAGS",
    "REG_RIP",
    "REG_RIP",
    "REG_AL",
    "REG_AH",
    "REG_AX",    
    "REG_CL",
    "REG_CH",
    "REG_CX",    
    "REG_DL",
    "REG_DH",
    "REG_DX",    
    "REG_BL",
    "REG_BH",
    "REG_BX",
    "REG_BP",
    "REG_SI",
    "REG_DI",
    "REG_SP",
    "REG_FLAGS",
    "REG_IP",
    "REG_EDI",
    "REG_DIL",
    "REG_ESI",
    "REG_SIL",
    "REG_EBP",
    "REG_BPL",
    "REG_ESP",
    "REG_SPL",
    "REG_EBX",
    "REG_EDX",
    "REG_ECX",
    "REG_EAX",
    "REG_EFLAGS",
    "REG_EIP",
    "REG_R8B",
    "REG_R8W",
    "REG_R8D",
    "REG_R9B",
    "REG_R9W",
    "REG_R9D",
    "REG_R10B",
    "REG_R10W",
    "REG_R10D",
    "REG_R11B",
    "REG_R11W",
    "REG_R11D",
    "REG_R12B",
    "REG_R12W",
    "REG_R12D",    
    "REG_R13B",
    "REG_R13W",
    "REG_R13D",
    "REG_R14B",
    "REG_R14W",
    "REG_R14D",
    "REG_R15B",
    "REG_R15W",
    "REG_R15D", 
    "REG_MM0",
    "REG_MM0",
    "REG_MM1",
    "REG_MM2",
    "REG_MM3",
    "REG_MM4",
    "REG_MM5",
    "REG_MM6",
    "REG_MM7",
    "REG_MM7",
    "REG_EMM0",
    "REG_EMM0",
    "REG_EMM1",
    "REG_EMM2",
    "REG_EMM3",
    "REG_EMM4",
    "REG_EMM5",
    "REG_EMM6",
    "REG_EMM7",
    "REG_EMM7",
    "REG_MXT",
    "REG_X87",
    "REG_XMM0",
    "REG_XMM0",
    "REG_XMM0",
    "REG_XMM1",
    "REG_XMM2",
    "REG_XMM3",
    "REG_XMM4",
    "REG_XMM5",
    "REG_XMM6",
    "REG_XMM7",
    "REG_XMM8",
    "REG_XMM9",
    "REG_XMM10",
    "REG_XMM11",
    "REG_XMM12",
    "REG_XMM13",
    "REG_XMM14",
    "REG_XMM15",
    "REG_XMM15",
    "REG_YMM0",
    "REG_YMM0",
    "REG_YMM1",
    "REG_YMM2",
    "REG_YMM3",
    "REG_YMM4",
    "REG_YMM5",
    "REG_YMM6",
    "REG_YMM7",
    "REG_YMM8",
    "REG_YMM9",
    "REG_YMM10",
    "REG_YMM11",
    "REG_YMM12",
    "REG_YMM13",
    "REG_YMM14",
    "REG_YMM15",
    "REG_YMM15",
    "REG_MXCSR",
    "REG_MXCSRMASK",
    "REG_RAX",
    "REG_RAX",
    "REG_DR0",
    "REG_DR0",
    "REG_DR1",
    "REG_DR2",
    "REG_DR3",
    "REG_DR4",
    "REG_DR5",
    "REG_DR6",
    "REG_DR7",
    "REG_DR7",
    "REG_CR0",
    "REG_CR0",
    "REG_CR1",
    "REG_CR2",
    "REG_CR3",
    "REG_CR4",
    "REG_CR4",  
    "REG_TSSR",    
    "REG_LDTR",
    "REG_TR_BASE",
    "REG_TR_BASE",
    "REG_TR3",
    "REG_TR4",
    "REG_TR5",
    "REG_TR6",
    "REG_TR7",
    "REG_TR7",
    "REG_FPST_BASE",
    "REG_FPST_BASE",
    "REG_FPCW",
    "REG_FPSW",
    "REG_FPTAG",          ///< Abridged 8-bit version of x87 tag register.
    "REG_FPIP_OFF",
    "REG_FPIP_SEL",
    "REG_FPOPCODE",
    "REG_FPDP_OFF",
    "REG_FPDP_SEL",
    "REG_FPDP_SEL",
    "REG_FPTAG_FULL",     ///< Full 16-bit version of x87 tag register.
    "REG_ST_BASE",
    "REG_ST0",
    "REG_ST1",
    "REG_ST2",
    "REG_ST3",
    "REG_ST4",
    "REG_ST5",
    "REG_ST6",
    "REG_ST7",
    "REG_ST7",
    "REG_ST7",
    "REG_SEG_GS_BASE", ///< Base address for GS segment
    "REG_SEG_FS_BASE", ///< Base address for FS segment
    "REG_INST_BASE",
    "REG_INST_G0",  ///< First available scratch register
    "REG_INST_G0",    ///< Scratch register used in pintools
    "REG_INST_G1",                            ///< Scratch register used in pintools
    "REG_INST_G2",                            ///< Scratch register used in pintools
    "REG_INST_G3",                            ///< Scratch register used in pintools
    "REG_INST_G4",                            ///< Scratch register used in pintools
    "REG_INST_G5",                            ///< Scratch register used in pintools
    "REG_INST_G6",                            ///< Scratch register used in pintools
    "REG_INST_G7",                            ///< Scratch register used in pintools
    "REG_INST_G8",                            ///< Scratch register used in pintools
    "REG_INST_G9",                            ///< Scratch register used in pintools
    "REG_INST_G10",                            ///< Scratch register used in pintools
    "REG_INST_G11",                            ///< Scratch register used in pintools
    "REG_INST_G12",                            ///< Scratch register used in pintools
    "REG_INST_G13",                            ///< Scratch register used in pintools
    "REG_INST_G14",                            ///< Scratch register used in pintools
    "REG_INST_G15",                            ///< Scratch register used in pintools
    "REG_INST_G16",                            ///< Scratch register used in pintools
    "REG_INST_G17",                            ///< Scratch register used in pintools
    "REG_INST_G18",                            ///< Scratch register used in pintools
    "REG_INST_G19",                            ///< Scratch register used in pintools
    "REG_INST_G0",     
    "REG_INST_G19",
    "REG_BUF_BASE0",
    "REG_BUF_BASE1",
    "REG_BUF_BASE2",
    "REG_BUF_BASE3",
    "REG_BUF_BASE4",
    "REG_BUF_BASE5",
    "REG_BUF_BASE6",
    "REG_BUF_BASE7",
    "REG_BUF_BASE8",
    "REG_BUF_BASE9",
    "REG_BUF_BASE9",
    "REG_BUF_END0",
    "REG_BUF_END1",
    "REG_BUF_END2",
    "REG_BUF_END3",
    "REG_BUF_END4",
    "REG_BUF_END5",
    "REG_BUF_END6",
    "REG_BUF_END7",
    "REG_BUF_END8",
    "REG_BUF_END9",
    "REG_BUF_END9",
    "REG_BUF_ENDLAST",
    "REG_LAST"
};

string GPRs[NUM_GPRS] = {
    "REG_RAX",                ///< rax
    "REG_EAX",
    "REG_AL",
    "REG_AH",
    "REG_AX",    
    "REG_RBX",                ///< rbx
    "REG_EBX",
    "REG_BL",
    "REG_BH",
    "REG_BX",
    "REG_RCX",                ///< rcx
    "REG_ECX",
    "REG_CL",
    "REG_CH",
    "REG_CX",    
    "REG_RDX",                ///< rdx
    "REG_EDX",
    "REG_DL",
    "REG_DH",
    "REG_DX",       
    "REG_RSP",
    "REG_ESP",
    "REG_SP",
    "REG_SPL",
    "REG_RBP",                ///< rbp
    "REG_EBP",
    "REG_BP",
    "REG_BPL",
    "REG_RSI",
    "REG_ESI",  
    "REG_SI", 
    "REG_SIL",
    "REG_RDI", 
    "REG_EDI",
    "REG_DI",
    "REG_DIL", 
    "REG_R8",
    "REG_R9",
    "REG_R10",
    "REG_R11",
    "REG_R12",
    "REG_R13",
    "REG_R14",
    "REG_R15",
    "REG_R15",
    "REG_R8B",
    "REG_R8W",
    "REG_R8D",
    "REG_R9B",
    "REG_R9W",
    "REG_R9D",
    "REG_R10B",
    "REG_R10W",
    "REG_R10D",
    "REG_R11B",
    "REG_R11W",
    "REG_R11D",
    "REG_R12B",
    "REG_R12W",
    "REG_R12D",    
    "REG_R13B",
    "REG_R13W",
    "REG_R13D",
    "REG_R14B",
    "REG_R14W",
    "REG_R14D",
    "REG_R15B",
    "REG_R15W",
    "REG_R15D"   
};

string gpr_reg_codes[INT_GPR] = {
    "REG_RAX",                ///< rax
    "REG_RBX",                ///< rbx
    "REG_RCX",                ///< rcx 
    "REG_RDX",                ///< rdx
    "REG_RSP",
    "REG_RBP",
    "REG_RSI",  
    "REG_RDI",
    "REG_R8",
    "REG_R9",
    "REG_R10",
    "REG_R11",
    "REG_R12",
    "REG_R13",
    "REG_R14",
    "REG_R15" 
};

string simd_reg_codes[SIMD_GPR] = {
    "REG_YMM0",
    "REG_YMM1",
    "REG_YMM2",
    "REG_YMM3",
    "REG_YMM4",
    "REG_YMM5",
    "REG_YMM6",
    "REG_YMM7",
    "REG_YMM8",
    "REG_YMM9",
    "REG_YMM10",
    "REG_YMM11",
    "REG_YMM12",
    "REG_YMM13",
    "REG_YMM14",
    "REG_YMM15"
};

string fp_simd_gpr[ALL_SIMD_GPR] = {     
    "REG_MM0",
    "REG_MM1",
    "REG_MM2",
    "REG_MM3",
    "REG_MM4",
    "REG_MM5",
    "REG_MM6",
    "REG_MM7",
    "REG_EMM0",
    "REG_EMM1",
    "REG_EMM2",
    "REG_EMM3",
    "REG_EMM4",
    "REG_EMM5",
    "REG_EMM6",
    "REG_EMM7",
    "REG_XMM0",
    "REG_XMM1",
    "REG_XMM2",
    "REG_XMM3",
    "REG_XMM4",
    "REG_XMM5",
    "REG_XMM6",
    "REG_XMM7",
    "REG_XMM8",
    "REG_XMM9",
    "REG_XMM10",
    "REG_XMM11",
    "REG_XMM12",
    "REG_XMM13",
    "REG_XMM14",
    "REG_XMM15",
    "REG_YMM0",
    "REG_YMM1",
    "REG_YMM2",
    "REG_YMM3",
    "REG_YMM4",
    "REG_YMM5",
    "REG_YMM6",
    "REG_YMM7",
    "REG_YMM8",
    "REG_YMM9",
    "REG_YMM10",
    "REG_YMM11",
    "REG_YMM12",
    "REG_YMM13",
    "REG_YMM14",
    "REG_YMM15"};

string return_reg(int offset){
	return reg_name[offset];
}

int return_num(int offset){
	string reg = reg_name[offset];
	std::string ret_num = "";
	int number = 0, flag = 0;

	for(int i = 0; i < reg.size(); i++){
		if(isdigit(reg[i])){
			flag = 1;
			for(int j = i; j < reg.size(); j++){
				if(isdigit(reg[j])) ret_num += reg[j];
			}
			break;
		}
	}

	if(flag){
		std::istringstream stream(ret_num);
		stream >> number;
	}else{
		number = -1;
	}
	return number;
}

int return_named_gpr(int offset){
	std::string reg = reg_name[offset];
	std::size_t found, found_alt;

	//RAX equivalents
	found = reg.find("AX");
	if(found != std::string::npos)
		return 0;

	//RBX equivalents
	found = reg.find("BX");
	if(found != std::string::npos)
		return 1;

	//RCX equivalents
	found = reg.find("CX");
	if(found != std::string::npos)
		return 2;

	//RDX equivalents
	found = reg.find("DX");
	if(found != std::string::npos)
		return 3;

	//RSP equivalents
	found = reg.find("SP");
	if(found != std::string::npos)
		return 4;

	//RBP equivalents
	found = reg.find("BP");
	if(found != std::string::npos)
		return 5;

	//RSI equivalents
	found = reg.find("SI");
	if(found != std::string::npos)
		return 6;

	//RDI equivalents
	found = reg.find("DI");
	if(found != std::string::npos)
		return 7;

	//double checks for AL, BL, CL, DL and AH, BH, CH and DH	
	found = reg.find("AL"); found_alt = reg.find("AH");
	if(found != std::string::npos || found_alt != std::string::npos)
		return 0;
	
	found = reg.find("BL"); found_alt = reg.find("BH");
	if(found != std::string::npos || found_alt != std::string::npos)
		return 1;

	found = reg.find("CL"); found_alt = reg.find("CH");
	if(found != std::string::npos || found_alt != std::string::npos)
		return 2;

	found = reg.find("DL"); found_alt = reg.find("DH");
	if(found != std::string::npos || found_alt != std::string::npos)
		return 3;


	return -1;
	
}

bool is_reg_ymm(int offset){
	string reg = reg_name[offset];

	std::size_t found = reg.find("YMM");
	if(found != std::string::npos)
		return true;
	
	return false;
}

bool is_reg_xmm(int offset){
	string reg = reg_name[offset];

	std::size_t found = reg.find("XMM");
	if(found != std::string::npos)
		return true;
	
	return false;
}

bool is_GPR(int offset){
	string reg = reg_name[offset];
	for(int i = 0; i < NUM_GPRS; i++){
		if(!reg.compare(GPRs[i])){
			return true; 
		}
	}
	return false;

}

bool is_simd_fp(int offset){
	string reg = reg_name[offset];
	for(int i = 0; i < ALL_SIMD_GPR; i++){
		if(!reg.compare(fp_simd_gpr[i]))
			return true;
	}
	return false;
}

/*inline BOOL REG_is_gr32(REG reg)
{
    switch (reg)
    {
      case REG_EDI:
      case REG_ESI:
      case REG_EBP:
      case REG_ESP:
      case REG_EBX:
      case REG_EDX:
      case REG_ECX:
      case REG_EAX:
#if defined(TARGET_IA32E)
      case REG_R8D:
      case REG_R9D:
      case REG_R10D:
      case REG_R11D:
      case REG_R12D:
      case REG_R13D:
      case REG_R14D:
      case REG_R15D:    
#endif        
        return TRUE;
      default:
        return FALSE;
    }
}

                                                                
inline BOOL REG_is_gr16(REG reg)
{
    switch (reg)
    {
      case REG_DI:
      case REG_SI:
      case REG_BP:
      case REG_SP:
      case REG_BX:
      case REG_DX:
      case REG_CX:
      case REG_AX:
#if defined(TARGET_IA32E)
      case REG_R8W:
      case REG_R9W:
      case REG_R10W:
      case REG_R11W:
      case REG_R12W:
      case REG_R13W:
      case REG_R14W:
      case REG_R15W:    
#endif        
        return TRUE;
      default:
        return FALSE;
    }
}

                                                               
inline BOOL REG_is_gr8(REG reg)
{
    switch (reg)
    {
      case REG_AL:
      case REG_AH:
      case REG_BL:
      case REG_BH:
      case REG_CL:
      case REG_CH:
      case REG_DL:
      case REG_DH:
#if defined(TARGET_IA32E)
      case REG_SIL:
      case REG_DIL:
      case REG_SPL:
      case REG_BPL:
      case REG_R8B:
      case REG_R9B:
      case REG_R10B:
      case REG_R11B:
      case REG_R12B:
      case REG_R13B:
      case REG_R14B:
      case REG_R15B:    
#endif        
        return TRUE;
      default:
        return FALSE;
    }
}*/

#endif

