/*
Copyright (C) 2009 Pierre Michaud
Copyright (C) 2009 INRIA

ATC is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

ATC is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with ATC; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/


/* Setting ENABLE_FORK allows to compress chunks in background when doing lossy compression. It should make lossy compression faster on multiprocessors. */

/* #define ENABLE_FORK */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#ifdef ENABLE_FORK
  #include <sys/types.h>
  #include <sys/wait.h>
  #include <signal.h>
#endif

#include "atc.h"


#define VERBOSE

#define INFO_FILE "INFO.xz"
#define INFO_COMPRESSOR "xz -2 -c"
#define INFO_DECOMPRESSOR "xz -dc"

#define FAST_COMPRESSOR "xz -2 -c"
#define FAST_DECOMPRESSOR "xz -dc"
#define TEMP_FILE_EXT "xz"

#define ASSERT(cond) if (!(cond)) {fprintf(stderr,"file %s assert line %d\n",__FILE__,__LINE__);exit(1);}
#define WARNING(cond) if (!(cond)) {fprintf(stderr,"file %s warning condition line %d\n",__FILE__,__LINE__);}

#define LL ATC_SOA
#define FNLEN (20+ATC_EXTLEN)


/*****************************************************************************
     phases
 *****************************************************************************/

#define MISMATCHVAL (3.)


static unsigned char bt[256]; /* for byte translation */
static unsigned char wb[256];
static int jb[256];


static void phase_init(Phase *p, int num)
{
  uint i,j;
  for (i=0; i<LL; i++) {
    for (j=0; j<256; j++) {
      p->h[i][j] = 0;
      p->t[i][j] = j;
    }
  }
  p->num = num;
  p->n = 0;
}


static Phase *phase_new(Phase *p)
{
  Phase *q;
  ASSERT(p);
  q = (Phase *) malloc(sizeof(Phase));
  ASSERT(q);
  *q = *p;
  return q;
}


static void phase_free(Phase *p)
{
  free(p);
}


static int phase_signature(Phase *p)
{
  uint j,s,ss;
  ASSERT((ATC_PSIZE & (ATC_PSIZE-1))==0);
  ASSERT(p->n>0);
  s = 0;
  for (j=0; j<LL; j++) {
    ss = (ATC_PSIZE-1) * (float) p->h[j][p->t[j][0]] / p->n;
    s ^= ss;
  }
  ASSERT(s>=0);
  ASSERT(s<ATC_PSIZE);
  return s;
}


static float sorted_histo_dist(Phase *p1, Phase *p2, int j)
{
  int i,d;
  float y;
  if (p1->n != p2->n) {
    return MISMATCHVAL;
  }
  d = 0;
  for (i=0; i<256; i++) {
    d += abs(p1->h[j][p1->t[j][i]] - p2->h[j][p2->t[j][i]]);
  }
  y = (float) d / p1->n;
  return y;
}


#if 0
static float histo_dist(Phase *p1, Phase *p2, int j)
{
  int i,d;
  float y;
  if (p1->n != p2->n) {
    return MISMATCHVAL;
  }
  d = 0;
  for (i=0; i<256; i++) {
    d += abs(p1->h[j][i] - p2->h[j][i]);
  }
  y = (float) d / p1->n;
  return y;
}
#endif

static float phase_match(Phase *p1, Phase *p2)
{
  uint i;
  float y,m;
  m = 0;
  for (i=0; i<LL; i++) {
    y = sorted_histo_dist(p1,p2,i);
    if (y>m) {
      m = y;
    }
  }
  return m;
}


static Phase * phase_search(struct atc *q, Phase *p)
{
  int i,s;
  float y,m;
  Phase *pm = NULL;
  s = phase_signature(p);
#ifdef VERBOSE
  fprintf(stderr,"signature: %d\n",s);
#endif
  m = MISMATCHVAL;
  for (i=0; i<ATC_PASSO; i++) {
    if (!q->p[i][s]) continue;
    y = phase_match(p,q->p[i][s]);
    if (y<m) {
      m = y;
      pm = q->p[i][s];
    }
  }
#ifdef VERBOSE
  fprintf(stderr,"diff = %f\n",m);
#endif
  if (m < ATC_MATCHVAL) {
    return pm;
  } else {
    return NULL;
  }
}


static void phase_store(struct atc *q, Phase *p)
{
  int i,s,mnum;
  Phase *pp = NULL;
  mnum = q->num;
  s = phase_signature(p);
  for (i=0; i<ATC_PASSO; i++) {
    if (q->p[i][s]==NULL) {
      q->p[i][s] = phase_new(p);
      return;
    }
    if (q->p[i][s]->num < mnum) {
      pp = q->p[i][s];
      mnum = pp->num;
    }
  }
  ASSERT(pp);
  *pp = *p;
}



static void mergesorted(unsigned char *tc, unsigned char ta[], int na, unsigned char tb[], int nb, int h[256])
{
  int i,ja,jb;
  i = ja = jb = 0;
  while ((ja<na) && (jb<nb)) {
    if (h[ta[ja]] < h[tb[jb]]) {
      tc[i++] = tb[jb++];
    } else {
      tc[i++] = ta[ja++];
    }
  }
  if (ja<na) {
    while (i<(na+nb)) {
      tc[i++] = ta[ja++];
    }
  } else {
    while (i<(na+nb)) {
      tc[i++] = tb[jb++];
    }
  }
}


static void stable_sort(unsigned char t[256], int n, int h[256])
{
  int m;
  unsigned char *ta,*tb;
  if (n==1) {
    return;
  }
  m = n/2;
  ta = &t[0];
  tb = &t[m];
  stable_sort(ta,m,h);
  stable_sort(tb,n-m,h);
  mergesorted(wb,ta,m,tb,n-m,h);
  memcpy(t,wb,n);
}


static void update_histograms(Phase *p, ATC_Addr a[], int nb)
{
  uint i;
  int j;
  unsigned char c;
  for (j=0; j<nb; j++) {
    for (i=0; i<LL; i++) {
      c = (a[j] >> (8*(LL-1-i))) & 255;
      p->h[i][c]++;
    }
  }
  /* sort histograms */
  for (i=0; i<LL; i++) {
    for (j=0; j<256; j++) {
      p->t[i][j] = j;
    }
    stable_sort(p->t[i],256,p->h[i]);
  }
  p->n += nb;
}



/*****************************************************************************
     compression / decompression
 *****************************************************************************/


static int open_file(struct atc *p, int num)
{
  char fn[FNLEN+1+ATC_DNLEN];
  char com[ATC_COMLEN+FNLEN+5];
  ASSERT(!p->fd);
  switch (p->mode) {
  case 'c':
    sprintf(fn,"%s/%d.%s",p->dirname,num,p->ext);
    if (strlen(p->command) > 0) {
      sprintf(com,"%s > %s",p->command,fn);
    } else {
      sprintf(com,"%s > %s",FAST_COMPRESSOR,fn);
    }
    p->fd = popen(com,"w");
    break;
  case 'k':
    sprintf(fn,"%s/%d.%s",p->dirname,num,TEMP_FILE_EXT);
    sprintf(com,"%s > %s",FAST_COMPRESSOR,fn);
    p->fd = popen(com,"w");
    break;
  case 'd':
    sprintf(fn,"%s/%d.%s",p->dirname,num,p->ext);
    if (access(fn,R_OK)!=0) {
      fprintf(stderr,"unable to read file %s\n",fn);
      return 0;
    }
    if (strlen(p->command) > 0) {
      sprintf(com,"cat %s | %s",fn,p->command);
    } else {
      sprintf(com,"cat %s | %s",fn,FAST_DECOMPRESSOR);
    }
    p->fd = popen(com,"r");
    break;
  default: 
    fprintf(stderr,"ATC modes are 'c','k','d'\n");
    ASSERT(0);
  }
  if (!p->fd) {
    fprintf(stderr,"command '%s' may not be valid\n",p->command);
    ASSERT(0);
  }
  return 1;
}


static void close_file(struct atc *p, int num, char deletefile)
{
  char fn1[FNLEN+1+ATC_DNLEN];
  char fn2[FNLEN+1+ATC_DNLEN];
  char com[ATC_COMLEN+FNLEN+5];
  ASSERT(p->fd);
  pclose(p->fd);
  ASSERT(!deletefile || (p->mode=='k'));
  if (p->mode=='k') {
    sprintf(fn1,"%s/%d.%s",p->dirname,num,TEMP_FILE_EXT);
    if (!deletefile) {
      if (strcmp(p->ext,TEMP_FILE_EXT) != 0) {
#ifdef ENABLE_FORK
	pid_t pid = fork();
	switch (pid) {
	case -1: 
	  fprintf(stderr,"forking failed\n");
	  ASSERT(0);
	case 0:
	  sprintf(fn2,"%s/%d.%s",p->dirname,num,p->ext);
	  sprintf(com,"cat %s | %s | %s > %s",fn1,FAST_DECOMPRESSOR,p->command,fn2);
	  system(com);
	  sprintf(com,"rm %s",fn1);
	  system(com);
	  exit(0);
	default:  /* parent process continues without waiting */
	  break;
	}
#else
	sprintf(fn2,"%s/%d.%s",p->dirname,num,p->ext);
	sprintf(com,"cat %s | %s | %s > %s",fn1,FAST_DECOMPRESSOR,p->command,fn2);
	WARNING(system(com) == 0);
	sprintf(com,"rm %s",fn1);
	WARNING(system(com) == 0);
#endif
      }
    } else {
      sprintf(com,"rm %s",fn1);
      WARNING(system(com) == 0);
    }
  }
  p->fd = NULL;
}


static void bb_init(bblock *bb)
{
  int i;
  for (i=0; i<256; i++) {
    bb->hb[i] = 0;
  }
  for (i=0; i<ATC_BUFFER; i++) {
    bb->b[i] = 0;
  }
}


static void set_string(char *d, const char *s, int length)
{
  int l;
  ASSERT(d);
  if (!s) {
    strcpy(d,"");
    return;
  }
  l = strlen(s);
  if (l > length) {
    fprintf(stderr,"'%s' is too long\n",s);
    ASSERT(0);
  }
  strcpy(d,s);
}



static void atc_init(struct atc *p, char mode, const char *dirname, const char *ext, const char *command)
{
  uint i,j;
  p->n = 0;
  p->nb = 0;
  p->read = 0;
  p->x = 0;
  p->mode = mode;;
  for (i=0; i<LL; i++) {
    bb_init(&p->bb[i]);
  }
  for (i=0; i<ATC_BUFFER; i++) {
    p->a[0][i] = 0;
    p->a[1][i] = 0;
  }
  for (i=0; i<ATC_PASSO; i++) {
    for (j=0; j<ATC_PSIZE; j++) {
      p->p[i][j] = NULL;
    }
  }
  p->num = 0;
  p->fd = NULL;
  p->infofd = NULL;
  set_string(p->dirname,dirname,ATC_DNLEN);
  set_string(p->ext,ext,ATC_EXTLEN);
  set_string(p->command,command,ATC_COMLEN);
  p->translate = 0;
  phase_init(&p->ph,0);
  p->DISABLE_TRANSLATE = 0;
}


#ifdef ENABLE_FORK

static void catch_child(int sig_num)
{
  /* child process has terminated (child created with fork() or system()) */
  /* get rid of zombies */
  waitpid(-1,NULL,WNOHANG);
}

#endif


void atc_open(struct atc *p, char mode, const char *dirname, const char *ext, const char *command)
{
  int e;
  char com[ATC_COMLEN+ATC_DNLEN+6];
  char fn[ATC_DNLEN+20];
  atc_init(p,mode,dirname,ext,command);
  if ((mode=='c') || (mode=='k')) {
    sprintf(com,"mkdir %s",dirname);
    e = system(com);
    if (e) {
      fprintf(stderr,"could not create directory '%s'\n",dirname);
      ASSERT(0);
    }
  }
  sprintf(fn,"%s/%s",dirname,INFO_FILE);
  if (mode=='d') {
    sprintf(com,"%s %s",INFO_DECOMPRESSOR,fn);
    p->infofd = popen(com,"r");
  } else {
    if (mode=='k') {
      if ((strcmp(ext,TEMP_FILE_EXT)==0) && (strcmp(command,FAST_COMPRESSOR)!=0)) {
	fprintf(stderr,"if you use suffix %s, the compression command must be \"%s\"\n",ext,FAST_COMPRESSOR);
	ASSERT(0);
      }
    }
    sprintf(com,"%s > %s",INFO_COMPRESSOR,fn);
    p->infofd = popen(com,"w");
  }
  if (!p->infofd) {
    fprintf(stderr,"could not open file '%s'\n",fn);
    ASSERT(0);
  }
#ifdef ENABLE_FORK
  signal(SIGCHLD,catch_child);
#endif
}


static void goto_interval(struct atc *p)
{
  int x,num,nn,j,t;
  uint i;
  ASSERT(p->infofd);
  ASSERT(p->mode=='d');
  ASSERT(p->num>0);
  do {
    x = fscanf(p->infofd,"%d %d",&num,&nn);
    if ((x!=2) || (num<=0)) {
      fprintf(stderr,"ATC unable to skip %d intervals\n",p->num);
      ASSERT(0);
    }
#ifdef VERBOSE
    fprintf(stderr,"skip interval %d\n",num);
#endif
    if (nn==0) {
      x = fscanf(p->infofd,"%d",&t);
      ASSERT(x==1);
      for (i=0; i<LL; i++) {
	for (j=0; j<256; j++) {
	  x = fscanf(p->infofd,"%X",&t);
	  ASSERT(x==1);
	}
      }
    }
  } while (num < p->num);
}


void atc_skip(struct atc *p, int num)
{
  /* skip 'num' intervals */
  if (p->mode!='d') {
    fprintf(stderr,"atc_skip called after atc_open and only for decompression\n");
    ASSERT(0);
  }
  if (num < 0) {
    fprintf(stderr,"atc_skip takes positive number as argument\n");
    ASSERT(0);
  }
  if (num==0) {
    ASSERT(p->num==0);
    return;
  }
  p->num = num;
  goto_interval(p);
}


static void finalize_interval(struct atc *p);


void atc_close(struct atc *p)
{
  int i,j;
  switch (p->mode) {
  case 'c': 
  case 'k':
    if (p->n > 0) {
      finalize_interval(p);
    }
    break;
  case 'd':
    if (p->fd) {
      while (fgetc(p->fd)!=EOF); /* get rid of 'broken pipe' message on Mac OS X */
      close_file(p,p->num,0);
    }
    break;
  default: ASSERT(0);
  }

  /* ENABLE_FORK: pclose waits till all child processes have terminated */
  pclose(p->infofd);

  for (i=0; i<ATC_PASSO; i++) {
    for (j=0; j<ATC_PSIZE; j++) {
      phase_free(p->p[i][j]);
    }
  }
}


/*****************************************************************************
     compression 
 *****************************************************************************/


static void unshuffle_bytes(struct atc *p, bblock *bb, ATC_Addr a[])
{
  int i;
  unsigned char c;
  for (i=0; i<256; i++) {
    bb->hb[i] = 0;
  }
  for (i=0; i<p->nb; i++) {
    c = (a[i] >> ((LL-1)*8)) & 255;
    bb->b[i] = c;
    bb->hb[c]++; /* compute the histogram of byte values */
  }
}


static void sort_bytes(struct atc *p, bblock *bb, ATC_Addr a[], ATC_Addr aa[])
{
  int i;
  unsigned char c;
  jb[0] = 0;
  for (i=1; i<256; i++) {
    jb[i] = jb[i-1] + bb->hb[i-1];
  }
  for (i=0; i<p->nb; i++) {
    c = bb->b[i];
    aa[jb[c]] = a[i] << 8;
    jb[c]++;
  }
}


#ifdef DISABLE_BYTESORT
static void do_not_sort_bytes(struct atc *p, bblock *bb, ATC_Addr a[], ATC_Addr aa[])
{
  int i;
  for (i=0; i<p->nb; i++) {
    aa[i] = a[i] << 8;
  }
}
#endif


static void output_block(struct atc *p, bblock *bb)
{
  fwrite(bb->b,1,p->nb,p->fd);
}


static void output_bytesorted_blocks(struct atc *p)
{
  uint i;
  /* from most significant to least significant bytes */
  ASSERT(p->nb>0);
  ASSERT(p->fd);
  fwrite(&p->nb,sizeof(int),1,p->fd);
  unshuffle_bytes(p,&p->bb[0],p->a[0]);
  output_block(p,&p->bb[0]);
  p->x = 0;
  for (i=1; i<LL; i++) {
#ifndef DISABLE_BYTESORT
    sort_bytes(p,&p->bb[i-1],p->a[p->x],p->a[p->x^1]);
#else
    do_not_sort_bytes(p,&p->bb[i-1],p->a[p->x],p->a[p->x^1]);
#endif
    p->x ^= 1;
    unshuffle_bytes(p,&p->bb[i],p->a[p->x]);
    output_block(p,&p->bb[i]);
  }
}


static void translate_bytes(struct atc *q, Phase *pp, Phase *ph)
{
  uint i,j;
  fprintf(q->infofd,"%d %d %d\n",q->num,0,ph->num);
  for (i=0; i<LL; i++) {
    if (i < (ATC_SOA-ATC_NOTRANSBYTES)) {
      /* translate bytes */
      for (j=0; j<256; j++) {
        bt[ph->t[i][j]] = pp->t[i][j];
      }
    } else {
      /* no byte translation for low-order bytes */
      for (j=0; j<256; j++) {
        bt[j] = j;
      }
    }
    for (j=0; j<256; j++) {
      fprintf(q->infofd,"%X ",bt[j]);
    }
    fprintf(q->infofd,"\n");
  }
  fflush(q->infofd);
}


static void finalize_interval(struct atc *p)
{
  /* close the file */
  if (p->nb > 0) {
    update_histograms(&p->ph,p->a[0],p->nb);
    output_bytesorted_blocks(p);
    p->nb = 0;
  }
  if (p->mode=='c') {
    fprintf(p->infofd,"%d %d\n",p->num,p->n); fflush(p->infofd);
    close_file(p,p->num,0);
  } else {
    ASSERT(p->mode=='k');
    Phase *ph = phase_search(p,&p->ph);
    if (ph==NULL) {
      /* no resembling interval, store chunk */
      fprintf(p->infofd,"%d %d\n",p->num,p->n); fflush(p->infofd);
      phase_store(p,&p->ph);
      close_file(p,p->num,0 /*keep the file*/);
    } else {
      /* resembling interval exists */
#ifdef VERBOSE
      fprintf(stderr,"looks like interval %d\n",ph->num);
#endif
      translate_bytes(p,&p->ph,ph);
      close_file(p,p->num,1 /*delete the file*/);
    }
  } 
}


void atc_code(struct atc *p, ATC_Addr v)
{
  if (p->n==0) {
    /* start new interval */
    p->num++;
#ifdef VERBOSE
    fprintf(stderr,"create interval %d\n",p->num);
#endif
    fprintf(stderr,"create interval %d\n",p->num);
    phase_init(&p->ph,p->num);
    open_file(p,p->num);
  }

  p->a[0][p->nb++] = v;
  p->n++;

  if (p->nb==ATC_BUFFER) {
    update_histograms(&p->ph,p->a[0],p->nb);
    output_bytesorted_blocks(p);
    p->nb = 0;
  }

  if (p->n==ATC_INTERVAL) {
    finalize_interval(p);
    p->n = 0;
  }
}


/*****************************************************************************
     decompression 
 *****************************************************************************/


static void read_bytes(struct atc *p, bblock *bb)
{
  int i, read_result;
  read_result=fread(bb->b,1,p->nb,p->fd);
  WARNING(read_result == p->nb);
  for (i=0; i<256; i++) {
    bb->hb[i] = 0;
  }
  for (i=0; i<p->nb; i++) {
    bb->hb[bb->b[i]]++;
  }
}


static void unsort_bytes(struct atc *p, int bi, ATC_Addr a[], ATC_Addr aa[], int pos)
{
  int i;
  bblock *bb;
  ASSERT((bi>=0)&&((uint)bi<LL));
  bb = &p->bb[bi];
  jb[0] = 0;
  for (i=1; i<256; i++) {
    jb[i] = jb[i-1] + bb->hb[i-1];
  }
  if (p->translate) {
    for (i=0; i<p->nb; i++) {
      aa[i] = a[jb[bb->b[i]]] | (((ATC_Addr) p->ph.t[bi][bb->b[i]]) << pos);
      jb[bb->b[i]]++;
    }
  } else {
    for (i=0; i<p->nb; i++) {
      aa[i] = a[jb[bb->b[i]]] | (((ATC_Addr) bb->b[i]) << pos);
      jb[bb->b[i]]++;
    }
  }
}


static void atc_unsort(struct atc *p)
{
  uint i;
  int ncz = 0;

  for (i=0; i<ATC_BUFFER; i++) {
    p->a[0][i] = 0;
    p->a[1][i] = 0;
  }

  if (p->translate) {
    for (i=0; i<LL; i++) {
      if ((p->ph.t[i][0]==0) && (p->bb[i].hb[0]==p->nb)) { 
	/* bytes are all zero */
	ncz++;
      } else {
	break;
      }
    }
  } else {
    for (i=0; i<LL; i++) {
      if (p->bb[i].hb[0]==p->nb) { 
	/* bytes are all zero */
	ncz++;
      } else {
	break;
      }
    }
  }


  if (p->translate) {
    for (i=0; (int)i<p->nb; i++) {
      p->a[0][i] = p->ph.t[LL-1][p->bb[LL-1].b[i]];
    }
  } else {
    for (i=0; (int)i<p->nb; i++) {
      p->a[0][i] = p->bb[LL-1].b[i];
    }
  }

  p->x = 0;
  for (i=0; i<(LL-1-ncz); i++) {
    unsort_bytes(p,LL-2-i,p->a[p->x],p->a[p->x^1],(i+1)*8);
    p->x ^= 1;
  }
}


static int fill_buffer(struct atc *p)
{
  uint i;
  int x;
  if (!p->fd) {
    return 0;
  }
  x = fread(&p->nb,sizeof(int),1,p->fd);
  if (x==0) {
    return 0;
  }
  if (p->nb > ATC_BUFFER) {
    fprintf(stderr,"Trace requires ATC buffer size of %d\n",p->nb);
    ASSERT(0);
  }
  for (i=0; i<LL; i++) {
    read_bytes(p,&p->bb[i]);
  }
  atc_unsort(p);
  return 1;
}


static void init_interval(struct atc *p, int n, int num)
{
  int j,ok,x;
  uint i;
  unsigned u;
  p->num++;
  ASSERT(num==p->num);
#ifdef VERBOSE
  fprintf(stderr,"get interval %d\n",p->num);
#endif

  if (n > 0) {
    /* chunk exists for this interval */
    ok = open_file(p,p->num);
    ASSERT(ok);
    p->translate = 0;
  } else {
    /* interval re-created from a previous chunk */
    x = fscanf(p->infofd,"%d",&num);
    ASSERT(x==1);
#ifdef VERBOSE
    fprintf(stderr,"create from chunk %d\n",num);
#endif
    ok = open_file(p,num);
    ASSERT(ok);
    /* read translation info */
    for (i=0; i<LL; i++) {
      for (j=0; j<256; j++) {
	x = fscanf(p->infofd,"%X",&u);
	ASSERT(x==1);
	ASSERT(u<256);
	p->ph.t[i][j] = u;
      }
    }
    p->translate = (p->DISABLE_TRANSLATE)? 0 : 1;
  }
}


int atc_decode(struct atc *p, ATC_Addr *v)
{
  int n,num,ok,x;

  if (p->read==p->nb) {

    ok = fill_buffer(p);

    if (!ok && p->fd) {
      /* end of interval */
      close_file(p,p->num,0);
      ASSERT(!p->fd);
    }

    if (!p->fd) {
      /* read new interval */
      x = fscanf(p->infofd,"%d %d",&num,&n);
      if (x<=0) {
	/* end of trace */
	return 0;
      }
      init_interval(p,n,num);
      ok = fill_buffer(p);
      ASSERT(ok);
    }

    ASSERT(p->nb>0);
    p->read = 0;
  }

  ASSERT(p->read < p->nb);
  *v = p->a[p->x][p->read];
  p->read++;
  return 1;
}

