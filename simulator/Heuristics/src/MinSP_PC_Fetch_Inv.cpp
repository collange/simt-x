#include "Heuristic.hpp"

class Sync : public Heuristic {
	ADDRINT pc;
	ADDRINT sp;
	std::vector< int >  mask;
	int invalid;

public:
	Sync(UINT nt, Thread* t, const char* param);
	~Sync();

	void choose_threads();
	int thread_schedule(LONG current_cycle){}
	virtual  bool lockstep(){
		return true;
	}
	int getCurrentWarp(){
		return -1;
	}
};

Heuristic* new_heuristic(UINT nt, Thread* t, const char* param){
	return new Sync(nt, t, param);
}
void delete_heuristic(Heuristic* h){
	delete h;
}

Sync::Sync(UINT nt, Thread* t, const char* param)
: Heuristic(nt, t)
{
	for( uint j = 0 ; j < num_threads ; j++){
		invalid = true;
		pc = 0ULL;
		sp = 0ULL;
		for (UINT i=0; i<num_threads; i++){
			mask.push_back(0);
		}
	}
}

Sync::~Sync(){

}


void Sync::choose_threads(){

	bool clear = true;
	bool referenced = false;
	for (UINT j=0; j<num_threads; j++){
		int ref1 = -1;
		if(t[j].fetch_divergent){
			for (UINT i=j; i<num_threads; i++){
				if (!t[i].fetch_qfull && !t[i].fetch_stall && !t[i].ignore && !t[i].fetch_qempty && t[i].fetch_divergent && !t[i].iq_full){
					if ( ref1 == -1 || t[i].fetch_sp < t[ref1].fetch_sp || ( t[i].fetch_sp == t[ref1].fetch_sp && t[i].fetch_pc < t[ref1].fetch_pc ) ){
						ref1 = i;
					}
				}
			}
			if(ref1 == -1){
				//invalid = true;
				sp = 0Ull;
				pc = 0Ull;
			}else{
				sp = t[ref1].sp;
				pc = t[ref1].pc;
				//invalid = false;
				for (UINT i=0; i<num_threads; i++){
					mask[i] = !t[i].fetch_qfull && t[i].fetch_pc == t[ref1].fetch_pc && !t[i].fetch_stall && !t[i].ignore && !t[i].fetch_qempty && !t[i].iq_full;
				}
			}
			break;
		}
		if(t[j].sp == sp){
			referenced = true;
		}
		if(!(t[j].forward_jump && mask[j])){
			clear = false;
		}
	}
	if (clear || !referenced){
		sp = 0Ull;
		pc = 0Ull;
		for (UINT i=0; i<num_threads; i++){
			mask[i] = 0;
		}
		invalid = true;
	}

	int ref = -1;
	for (UINT i=0; i<num_threads; i++){
		if (!t[i].fetch_qfull && !t[i].fetch_stall && !t[i].ignore && !t[i].fetch_qempty && !t[i].iq_full){
			if ( ref == -1 || t[i].fetch_sp < t[ref].fetch_sp || ( t[i].fetch_sp == t[ref].fetch_sp && t[i].fetch_pc < t[ref].fetch_pc ) ){
				if(t[i].fetch_sp == sp){
					if(!mask[i]){
						continue;
					}
				}
				ref = i;
			}
		}
	}
	if(ref == -1){
		for (UINT i=0; i<num_threads; i++){
			t[i].fetch_isactive = false;
		}
		return;
	}

	if(invalid){
		sp = t[ref].fetch_sp;
	}
	for (UINT i=0; i<num_threads; i++){
		t[i].fetch_isactive = !t[i].fetch_qfull && t[i].fetch_pc == t[ref].fetch_pc && !t[i].fetch_stall && !t[i].ignore && !t[i].fetch_qempty && !t[i].iq_full;
		t[i].active_last_fetch = t[i].fetch_isactive;
		if(t[i].fetch_sp == sp){
			mask[i] = t[i].fetch_isactive;
		}
		t[i].primary_heur_fetch = t[i].fetch_isactive;
	}

}

