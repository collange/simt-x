#ifndef __HEADER_HEURISTIC__
#define __HEADER_HEURISTIC__

#include "Types.hpp"
#include "Thread.hpp"
#include "InstructionFlags.hpp"

#define MAX_THREADS_HEUR 32
class Heuristic {
protected:
	UINT num_threads;
	UINT num_warps;
	Thread* t;
	int threads_in_warp;
	LONG heur_cycle[MAX_THREADS_HEUR];

public:
	Heuristic(UINT nt, Thread* threads)
: num_threads(nt), num_warps(0),t(threads)
{}
	Heuristic(UINT nt, UINT nw, Thread* threads)
	: num_threads(nt), num_warps(nw), t(threads)
	{}
	virtual ~Heuristic(){}

	virtual void choose_threads() = 0;
	virtual bool lockstep() = 0;
	virtual int thread_schedule(LONG current_cycle) = 0;
	virtual int getCurrentWarp() = 0;
	virtual void register_instruction(Instruction const & insn, UINT tid, uint64_t sequence) {}  // Default: just ignore

};

//extern "C" Heuristic* new_heuristic(UINT nt, Thread* t, const char* param);
extern "C" Heuristic* new_heuristic(UINT nt, UINT nw, Thread* t, const char* param);
extern "C" void delete_heuristic(Heuristic* h);

#endif
