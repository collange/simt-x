#include "sasim_uop_inst.hpp"
// The following file must be included here and not in sasim_uop.hpp due to declaration issues
//#include "sasim_engine.hpp"	

// ***************************************************************************
// Begin: sa_uop class

/*!
 * Resets all the variables that identify source and destination registers,
 * type of registers, type of uop, etc.
 */ 
void
sa_uop::clear()
{
	uoptype = SA_UOP_UNKNOWN;
	rd = NOREG;
	rdtype = SA_REG_TMP;
	for (int i=0; i<UOPNS; i++) {
		rs[i] = NOREG;
		rstype[i] = SA_REG_TMP;
	}
}

sa_uop::sa_uop()
{
	clear();
}

/*!
 * Writes uop information into the file specified
 * @param fd Pointer to a file descriptor
 */ 
void 
sa_uop::output(FILE *fd)
{
	WRITEFILE(fd,uoptype);
	WRITEFILE(fd,rd);
	WRITEFILE(fd,rdtype);
	for (int i=0; i<UOPNS; i++) {
		WRITEFILE(fd,rs[i]);
		WRITEFILE(fd,rstype[i]);
	}
	WRITEFILE(fd,write_flags);
	WRITEFILE(fd,read_flags);
}

/*!
 * Reads uop information from the file specified
 * @param fd Pointer to a file descriptor
 */ 
void 
sa_uop::input(FILE *fd)
{
	READFILE(fd,uoptype);
	READFILE(fd,rd);
	READFILE(fd,rdtype);
	for (int i=0; i<UOPNS; i++) {
		READFILE(fd,rs[i]);
		READFILE(fd,rstype[i]);
	}
	READFILE(fd,write_flags);
	READFILE(fd,read_flags);
}

/*!
 * Returns the string equivalent to the type of the current uop
 */ 
string
sa_uop::uoptype_string()
{
	switch (uoptype) {
	case SA_UOP_UNKNOWN: return "UNKNOWN"; break;
	case SA_UOP_NOP:     return "NOP"; break;
	case SA_UOP_ALU:     return "ALU"; break;
	case SA_UOP_MOV:     return "MOV"; break;
	case SA_UOP_BRANCH:  return "BRANCH"; break;
	case SA_UOP_SYSCALL: return "SYSCALL"; break;
	case SA_UOP_SHIFT:   return "SHIFT"; break;
	case SA_UOP_ADDR:    return "ADDR";  break;
	case SA_UOP_LOAD:    return "LOAD"; break;
	case SA_UOP_STORE:   return "STORE"; break;
	case SA_UOP_FP:      return "FP"; break;
	case SA_UOP_SSE:     return "SSE"; break;
	case SA_UOP_MMX:     return "MMX"; break;
	case SA_UOP_IMUL:    return "IMUL"; break;
	case SA_UOP_IDIV:    return "IDIV"; break;
	case SA_UOP_INTLONG: return "INTLONG"; break;
	case SA_UOP_FPDIV:   return "FPDIV"; break;
	case SA_UOP_FPLONG:  return "FPLONG"; break;
	case SA_UOP_MAGIC:   return "MAGIC"; break;
	default: SASSERT(0); break;
	}
}

/*!
 * Returns a string value that contains the destination register, write flags, source registers and read flags.
 * @param regtostring Pointer to a function that performs a conversion from integer to string (register)
 */ 
string
sa_uop::tostring(string (*regtostring)(sa_reg))
{
	ostringstream s;
	if ((rd != NOREG) || write_flags) {
		bool wd = false;
		if (rd != NOREG) {
			if (regtostring) {
				s << regtostring(rd);
			} else {
				s << rd;
			}
			wd = true;
		}
		if (write_flags) {
			if (wd) {
				s << ",";
			}
			s << "flags";
		}
		s << ":=";
	}

	s << uoptype_string() << "(";

	int ndep = 0;
	for (int i=0; i<UOPNS; i++) {
		if (rs[i] != NOREG) {
			if (ndep>0) {
				s << ",";
			}
			if (regtostring) {
				s << regtostring(rs[i]);
			} else {
				s << rs[i];
			}
			ndep++;
		}
	}
	if (read_flags) {
		if (ndep>0) {
			s << ",";
		}
		s << "flags";
	}
	s << ")";
	return s.str();
}

/*!
 * Returns true if the current uop is a branch or syscall instruction
 */ 
bool
sa_uop::is_branch()
{
	return (uoptype == SA_UOP_BRANCH) || (uoptype == SA_UOP_SYSCALL);
}

/*!
 * Returns true if the current uop corresponds to a Load or Store
 */ 
bool
sa_uop::is_mem()
{
	return (uoptype == SA_UOP_LOAD) || (uoptype == SA_UOP_STORE);
}

/*!
 * Returns true if the current uop corresponds to a FP, SSE, FPDIV or FPLONG instruction
 */ 
bool
sa_uop::is_fpsse()
{
	switch (uoptype) {
	case SA_UOP_FP:
	case SA_UOP_SSE:
	case SA_UOP_FPDIV:
	case SA_UOP_FPLONG:
		return true;
		break;
	default:
		return false;
		break;
	}
}

// End: sa_uop class
// ***************************************************************************

// ***************************************************************************
// Begin: sasim_uop class

/*!
 * Prepares the uop to be rescheduled by setting initial values to some of its internal variables
 * @param cycle Current cycle value
 */ 
void
sasim_uop::reschedule(int64_t cycle)
{
	scheduled = false;
	executed = false;
	memdone[0] = false;
	memdone[1] = false;
	memtranslated = false;
	forwardingstore = NOTHING;
}

/*!
 * Reschedules the current uop
 */ 
void
sasim_uop::reset_uop()
{
	reschedule();
	which_ldq = -1;
}

/*!
 * Checks if one of the source registers of the current uop references to a FP register.
 * If so, the uop is considered to work with FP values by setting an internal variable (is_fp).
 */ 
void
sasim_uop::set_isfp()
{
	// consider it is an FP uop if it uses an FP reg (dest or src)
	is_fp = (uop->rdtype == SA_REG_FP);
	if (is_fp) {
		return;
	}
	for (int i=0; i<UOPNS; i++) {
		switch (uop->rstype[i]) {
		case SA_REG_INT:
			break;
		case SA_REG_FP:
			is_fp = true;
			break;
		case SA_REG_TMP:
			// set_isfp must be used at rename
			if (uoprs[i]) {
				// uop has a dependent in same inst
				SASSERT(iptr==uoprs[i]->iptr);
				if (uoprs[i]->is_fp) {
					is_fp = true;
				}
			}
			break;
		default: SASSERT(0);
		}
	}
}

/*!
 * Establishes that the current uop has been executed and fixes the cycle value into the exec_cycle variable pointed by iptr. 
 * If the parameter issueq (issue queue) is passed, the current uop (sasim_uop) is also deleted from the queue issueq.
 * @param issueq Pointer to the issue queue
 */ 
void
sasim_uop::set_executed(int64_t cycle, saqueue<uop_ptr> * issueq)
{
	SASSERT(scheduled);
	executed = true;
	SASSERT(iptr);
	iptr->exec_cycle = cycle;
	if (issueq) {
		bool ok = issueq->search_delete(this);
		SASSERT(ok);
	}
}

/*!
 * Returns true if the flags and source registers for the current uop were already processed
 */ 
bool
sasim_uop::ready()
{
	if (uopflags && (uopflags->num <= num) && ! uopflags->executed) {
		return false;
	}
	for (int i=0; i<UOPNS; i++) {
		if (uoprs[i] && (uoprs[i]->num <= num) && ! uoprs[i]->executed) {
			return false;
		}
	}
	return true;
}

/*!
 * Returns true if the data to be stored by the current uop into the memory is ready
 */ 
bool
sasim_uop::store_data_ready()
{
	SASSERT(uop->uoptype == SA_UOP_STORE);
	if (uoprs[1] && (uoprs[1]->num <= num) && ! uoprs[1]->executed) {
		return false;
	}
	return true;
}

/*!
 * Returns true if the address of the data to be read/stored was already computed
 */ 
bool
sasim_uop::address_ready()
{
	SASSERT(uop->is_mem());
	// address is through first source
	if (uoprs[0] && (uoprs[0]->num <= num) && ! uoprs[0]->executed) {
		SASSERT(uoprs[0]->uop->uoptype == SA_UOP_ADDR);
		return false;
	}
	return true;
}

/*!
 * ??? It must be revised
 * Computes the value of variable dl1banks based on the configuration of DL1_BANK
 */ 
void
sasim_uop::set_dl1banks() 
{
	SASSERT(DL1_BANKS <= (8*sizeof(dl1banks)));
	int totalwidth = DL1_BANKS * DL1_BANK_WIDTH; // 64
	SASSERT(totalwidth >= MAXMEMACCWIDTH);
	SASSERT(ISPOW2(totalwidth));
	int first = addr % totalwidth;
	int last = (addr+nbytes-1) % totalwidth;
	int b = first / DL1_BANK_WIDTH;
	SASSERT(b < DL1_BANKS);
	int bb = last / DL1_BANK_WIDTH;
	SASSERT(bb < DL1_BANKS);
	SASSERT(nbytes <= totalwidth);
	if ((b==bb) && (nbytes > DL1_BANK_WIDTH)) {
		// access all banks
		dl1banks = ((uint64_t) 1 << DL1_BANKS)-1;
	} else {
		dl1banks = (1 << b);
		while (b != bb) {
			b = (b+1) % DL1_BANKS;
			dl1banks |= ((uint64_t) 1 << b);
		}
	}
}

// End: sasim_uop class
// ***************************************************************************

// ***************************************************************************
// Begin: sa_inst class


/*!
 * Returns true if the last uop of the current instruction corresponds to a branch uop.
 */ 
bool
sa_inst::is_branch()
{
	SASSERT(nuops > 0);
	bool isbranch = uop[nuops-1].is_branch();
	SASSERT(isbranch || ! (is_branch_cond || is_branch_ret || is_branch_ind));
	return isbranch;
}

/*!
 * Returns true if the current instruction has only one uop and it corresponds to a NOP.
 */ 
bool
sa_inst::is_nop()
{
	SASSERT(nuops > 0);
	return ((nuops == 1) && (uop[0].uoptype == SA_UOP_NOP));
}

/*!
 * Establishes that the current instruction has not been decoded yet, and resets all the
 * internal variables to zero/false
 */ 
void 
sa_inst::clear()
{
	decoded = false;
	inum = NOINSTNUM;
	pc = 0;
	fallthrough = 0;
	nuops = 0;
	nloads = 0;
	nstores = 0;
	mreadsize = 0;
	mwritesize = 0;
	is_branch_cond = false;
	is_branch_ret = false;
	is_branch_ind = false;
}

sa_inst::sa_inst()
{
	clear();
}

/*!
 * Writes current instruction information into the file specified
 * @param fd Pointer to a file descriptor
 */ 
void 
sa_inst::output(FILE *fd)
{
	SASSERT(inum>=0);
	SASSERT(decoded);
	WRITEFILE(fd,inum);
	WRITEFILE(fd,pc);
	WRITEFILE(fd,fallthrough);
	WRITEFILE(fd,is_branch_cond);
	WRITEFILE(fd,is_branch_ret);
	WRITEFILE(fd,is_branch_ind);
	WRITEFILE(fd,nuops);
	WRITEFILE(fd,nloads);
	WRITEFILE(fd,nstores);
	WRITEFILE(fd,mreadsize);
	WRITEFILE(fd,mwritesize);
	for (int i=0; i<nuops; i++) {
		uop[i].output(fd);
	}
	//DVSIM
	WRITEFILE(fd,idvsim.optype);
	WRITEFILE(fd,idvsim.flags);
	WRITEFILE(fd,idvsim.num_iregs);
	WRITEFILE(fd,idvsim.num_oregs);
	for(int i = 0; i < idvsim.num_iregs; i++){
		WRITEFILE(fd,idvsim.iregs[i]);
	}
	for(int i = 0; i < idvsim.num_oregs; i++){
		WRITEFILE(fd,idvsim.oregs[i]);
	}

	for(int i = 0; i < idvsim.num_iregs; i++){
		WRITEFILE(fd,idvsim.rstype[i]);
	}
	for(int i = 0; i < idvsim.num_oregs; i++){
		WRITEFILE(fd,idvsim.rdtype[i]);
	}
	//TODO:NOT REALLY NEEDED
	//WRITEFILE(fd,idvsim.assembly);

	WRITEFILE(fd,idvsim.is_branch);
}

/*!
 * Reads instruction information from the file specified
 * @param fd Pointer to a file descriptor
 */ 
bool
sa_inst::input(FILE *fd)
{
	clear();
	int n = fread(&inum,sizeof(inum),1,fd);
	if (n != 1) {
		SASSERT(n==0);
		return false;
	}
	READFILE(fd,pc);
	READFILE(fd,fallthrough);
	READFILE(fd,is_branch_cond);
	READFILE(fd,is_branch_ret);
	READFILE(fd,is_branch_ind);
	READFILE(fd,nuops);
	READFILE(fd,nloads);
	READFILE(fd,nstores);
	READFILE(fd,mreadsize);
	READFILE(fd,mwritesize);
	SASSERT(nuops <= MAXUOPS);
	for (int i=0; i<nuops; i++) {
		uop[i].input(fd);
	}
	//DVSIM
	READFILE(fd,idvsim.optype);
	READFILE(fd,idvsim.flags);
	READFILE(fd,idvsim.num_iregs);
	READFILE(fd,idvsim.num_oregs);
	for(int i = 0; i < idvsim.num_iregs; i++){
		READFILE(fd,idvsim.iregs[i]);
	}
	for(int i = 0; i < idvsim.num_oregs; i++){
		READFILE(fd,idvsim.oregs[i]);
	}
	//TODO:NOT REALLY NEEDED
	//READFILE(fd,idvsim.assembly);
	for(int i = 0; i < idvsim.num_iregs; i++){
		READFILE(fd,idvsim.rstype[i]);
	}
	for(int i = 0; i < idvsim.num_oregs; i++){
		READFILE(fd,idvsim.rdtype[i]);
	}

	//READFILE(fd,idvsim.assembly);

	READFILE(fd,idvsim.is_branch);
	decoded = true;
	return true;
}

/*!
 * Print debug information for the current instruction
 */ 
void 
sa_inst::debugprint()
{
	cout << hex << setw(16) << pc << " " << setw(16) << fallthrough << " ";
	cout << dec << is_branch_cond << is_branch_ret << is_branch_ind << " ";
	cout << nloads << " " << nstores << " ";
	cout << mreadsize << " " << mwritesize << " ";
	for (int i=0; i<nuops; i++) {
		cout << " ; " << uop[i].tostring();
	}
	cout << endl;
}

// End: sa_inst class
// ***************************************************************************

// ***************************************************************************
// Begin: sasim_inst class

void 
sasim_inst::reset_inst()
{
	block_end = false;
	mispredict = false;
	memtrap = false;
	pred_cycle = INFINITE_INTVAL;
	exec_cycle = INFINITE_INTVAL;
	for (int i=0; i<inst->nuops; i++) {
		simuop[i].reset_uop();
	}
	store_completed = false;
}

sasim_inst::sasim_inst()
{
	num = -1;
	inst = NULL;
}

// End: sasim_inst class
// ***************************************************************************
