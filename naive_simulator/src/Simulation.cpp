#include "Heuristic.hpp"
#include "Thread.hpp"
#include "DataAnalyser.hpp"
#include "Simulation.hpp"
#include <pthread.h>
#include <stdio.h>
#include <iostream>
#include <fstream>

#define DUMPSTREAM stderr


pthread_mutex_t shared_mutex = PTHREAD_MUTEX_INITIALIZER;

Loglevels loglevel = logDEBUG4;
TraceLevel tracelevel = pathTRACE;
std::ofstream logTrace;
std::ofstream logCSV;

//this is called 2x???
//#ifdef TAGE_PREDICTOR
//	PREDICTOR *branchpredictor = new PREDICTOR(nt); 
//#endif

/**********************************************************************
* Assigns various args to their parameters for the data analyzer	
* Sets up:
* 	- stats for memory and caches
*	- fetch heuristics
*	- DV structs and respective FUs
*	- And intialize other various DITVA stats
**********************************************************************/
Simulation::Simulation(UINT nt, UINT nf, const char *arqvs[], DataAnalyser::HeuristicData* fetch_h, Config c) :	horizon_sequence(nt){
	cache_warm = true;
	cache_warm_cycles = 0ULL;
	conf = c;
	thread_exit_count = 0;
	fetch_cycle = 0;
	num_threads = nt;
	orig_num_threads = nt;
	cache_warm_insts =  (LONG)conf.cache_warm * 1000000;
	threads = new Thread[num_threads];

	for (UINT i=0; i < num_threads; i++)
		threads[i].load(arqvs[i], i, (i/(num_threads/conf.num_warps)),i,0);
	
	cout<<"Load done"<<endl;
	analyser = new DataAnalyser();

	logTrace.open(c.outputfile); //output stats in file
	logCSV.open(c.csv);//output stats in a CSV
	wakeup_from_barrier = false;

	analyser->init(threads, num_threads, nf, c, readahead_horizon, fetch_h, NULL,0); //found in DataAnalyzer.cpp

	cout<<"Heuristic loaded"<<endl;
	finished = 0;
	reader_finished_count = 0;
	waiting_in_barrier = 0;
	reg_mask = (int **)malloc(num_threads * sizeof(int*));
	thread_instruction_count = new LONG[num_threads];

	for(UINT i = 0 ; i<num_threads;i++){
		reg_mask[i] = (int *)malloc(num_threads * sizeof(int));
		thread_instruction_count[i] = 0;
	}
	total_instruction_count = 0;
	simulation_instructions = conf.sim_insts* 1000000; //in million
}



Simulation::~Simulation(){
	logTrace.close();
	//	heur_data->delete_heuristic(heuristic);
	for(UINT i = 0; i < num_threads; i++ ){
		free(reg_mask[i]);
	}
	free(reg_mask);
	delete [] threads;
}

/***********************************************************************************
 * This function is used by the threads reading and filling the instruction queue. Once filled the simulator will start
 * and swap the full buffer with an empty one. From then onwards refilling is done by getins function
 * @param wa - Worker arguments
 ***********************************************************************************/
void* getinsfill(void* wa){
	Workerargs * worker = (Workerargs*)wa;
	Thread* thread = worker->thread;
	Instruction insn;

	thread->eof = false;
	//Fill the queue until the instruction queue is full.
	while(thread->prefetch_instruction_queue.size() < (UINT)worker->readahead_horizon){
		thread->eof = insn.read(&(thread->thread_arq),thread->threadid, thread->fetch_seq_no, thread->warpid, thread->coreid);  //in Threads.cpp - reads trace file based on specified format
		thread->fetch_seq_no++;
		if(thread->eof){
			cout<<"EOF "<<thread->threadid<<endl;
			break;
		}

		pthread_mutex_lock( &thread->iq_mutex );
		thread->prefetch_instruction_queue.push_back(insn);
		assert(thread->prefetch_instruction_queue.size() <= (UINT)worker->readahead_horizon);
		pthread_mutex_unlock( &thread->iq_mutex );
		//TODO:Decode should happen latter in the pipeline stages. Prefetch instruction queue should be replaced with sa_inst type.
	}

	//cout<<"Fill END "<<thread->threadid<<endl;
	return NULL;
}

pthread_t* Simulation::fill()
{

	pthread_t w_thread;
	pthread_t* w_threads = (pthread_t*)malloc(num_threads * sizeof(w_thread));
	Workerargs *wa = (Workerargs*)malloc(num_threads * sizeof(Workerargs));
	for (uint i=0; i<num_threads; i++){
		wa[i].thread = &threads[i];
		//wa[i].heuristic = heuristic;
		wa[i].horizon_sequence = &horizon_sequence;
		wa[i].readahead_horizon = readahead_horizon;
		wa[i].numthreads = &num_threads;
		wa[i].fin_count = &reader_finished_count;
		wa[i].thread_fin_count = &finished;
		wa[i].wait_in_barrier = &waiting_in_barrier;
#ifdef SAMPLING
		wa[i].sampling_size = &(conf.sampling_size);
#endif
		pthread_create ( &w_threads[i], NULL, getinsfill,(void*) &wa[i] ); //reads instructions from trace until queue is filled. Then returns

	}

	return w_threads;
}

/******************************************************************
   -Reads instructions from traces and places into
    prefetch_insructions_queue[]

   -Registers instruction and respective parameters
*******************************************************************/
void* getins(void* wa){
	Workerargs * worker = (Workerargs*)wa;
	Thread* thread = worker->thread;

#ifdef SAMPLING
	int sample_count = 0;
#endif
	bool marked_finished = false;
	do{

		thread->eof = false;
		do{
			Instruction insn;
			thread->eof = insn.read(&(thread->thread_arq), thread->threadid, thread->fetch_seq_no, thread->warpid, thread->coreid);

			if(thread->eof){
				thread->finished = true;
#ifdef SAMPLING
				thread->wait = false;
				pthread_mutex_lock( &thread->iq_mutex );
				pthread_cond_signal(&thread->prefetchq_full);
				pthread_mutex_unlock( &thread->iq_mutex );
#endif

				break;
			}
#ifdef SAMPLING
			thread->sampling_seq_no++;
			if((thread->sampling_seq_no % ((2)*1000000)) == 0){

				sample_count = (sample_count+1)%(*(worker->sampling_size));

			}
			if(sample_count != 0){
				thread->wait = true;
				continue;
			}
			thread->wait = false;
#endif
			thread->fetch_seq_no++;

			pthread_mutex_lock( &thread->iq_mutex );
			thread->prefetch_instruction_queue.push_back(insn);
			assert(thread->prefetch_instruction_queue.size() <= (UINT)worker->readahead_horizon);
			pthread_mutex_unlock( &thread->iq_mutex );
			bool full = (thread->prefetch_instruction_queue.size() >= (UINT)worker->readahead_horizon);

			if(full){
				pthread_cond_signal(&thread->prefetchq_full);
				usleep(500);
				pthread_mutex_lock(&thread->iq_mutex);
				bool piqempty=thread->prefetch_instruction_queue.empty();
				pthread_mutex_unlock( &thread->iq_mutex );
				if(!piqempty){
					pthread_cond_wait(&thread->prefetchq_notfull,&thread->mutex_Var);
				}
			}

		 //TODO: Maybe Dangerous we are not checking EOF
		}while(!(thread->end_of_simulation) && !thread->finished);
		if(thread->end_of_simulation && ! marked_finished){
			cout<<"Thread "<<thread->threadid <<" finished"<<endl;
			marked_finished = true;
			(*worker->thread_fin_count)++;
		}

	}while(!(thread->end_of_simulation) && !thread->finished );
		cout<<"fin count "<<(*worker->thread_fin_count)<<endl;
	return NULL;
}

/****************************************************
   Spawns threads to call getins() to fill the
   prefetch_instruction_queue()
****************************************************/
pthread_t* Simulation::refill(){
	pthread_t w_thread;
	pthread_t* w_threads = (pthread_t*)malloc(num_threads * sizeof(w_thread));
	Workerargs *wa = (Workerargs*)malloc(num_threads * sizeof(Workerargs));
	for (UINT i=0; i<num_threads; i++){
		wa[i].thread = &threads[i];
		//wa[i].heuristic = heuristic;
		wa[i].horizon_sequence = &horizon_sequence;
		wa[i].readahead_horizon = readahead_horizon;
		wa[i].thread_count = &thread_exit_count;
		wa[i].numthreads = &num_threads;
		//wa[i].orignumthreads = orig_num_threads;
		wa[i].fin_count = &reader_finished_count;
		wa[i].wait_in_barrier = &waiting_in_barrier;
		wa[i].wakeup = &wakeup_from_barrier;
		wa[i].thread_fin_count = &finished;
#ifdef SAMPLING
		wa[i].sampling_size = &(conf.sampling_size);
#endif
		pthread_create ( &w_threads[i], NULL, getins,(void*) &wa[i] );
	}
	return w_threads;
}



void Simulation::simulator(){
	finished = 0;
	waiting_in_barrier = 0;
	thread_exit_count = 0;

	//Fill thread buffers with instructions (read from its repsective trace file)
	pthread_t* pthreads_fill = fill();
	for (uint i = 0; i < num_threads; i++){
		pthread_join(pthreads_fill[i],NULL);
	}
	for(uint i = 0 ; i< num_threads; i++){
		threads[i].prefetch_instruction_queue_buffer.swap(threads[i].prefetch_instruction_queue);
		cout<<"PIQB "<<threads[i].prefetch_instruction_queue_buffer.size()<<" PIQ "<<threads[i].prefetch_instruction_queue.size()<<endl;
	}
	pthread_t* pthreads = refill();
	cout << "Finished refilling the pthreads...." <<endl;
	int fetch_count = 0;
	int temp_fc=0;
	bool early_exit = false;

	//First fetch
	for(UINT i = 0 ; i<num_threads; i++){
		threads[i].fetch_isactive = true;
		threads[i].fetch_pc = threads[i].prefetch_instruction_queue_buffer.front().pc;
	}

	early_exit = analyser->isFinished();
	analyser->setup_rename(); //setup PIRAT and Pathtable

	//LONG tmp_count = 0;
	while (!analyser->isFinished()) {
		bool finish_it = analyser->logger(); //shows status of prefetch buffers to stdout (debugging - know if simulator is working or not)
		if(finish_it) break;
#ifdef DEBUG
		cout << "-------------------------	Writeback      -----------------------------" << endl;
#endif
		analyser->poll_cache(); //checks DVInstStore DVInstLoad queue for their status and updates queues and instruction statuses 
#ifdef DEBUG
		cout << "-------------------------	Increment Cycle      -----------------------------" << endl;
#endif
		analyser->increment_cycle(); //updates the cache and gets its stats, "exe"/unblocks FUs, structures (& tallies). <-------- OoO 
#ifdef DEBUG			
		cout << "-------------------------	Commit      -----------------------------" << endl;
#endif
		analyser->commit_dv(); //finds instructions ready to commit that have been executed. Clears out ROB entry if committed
#ifdef DEBUG
		cout << "-------------------------	EXECUTE     -----------------------------" << endl; //need to allocate issueQ entries before bypass/WB -- eliminates overlap/deadlock
#endif
		analyser->execute_bypass();		
#ifdef DEBUG
		cout << "-------------------------   Dispatch    -----------------------------" << endl;
#endif
		analyser->wakeup_dispatch();	
		
#ifdef DEBUG
		cout << "-------------------------     Wakeup      -----------------------------" << endl;
#endif
		//analyser->update_issueQ_entries(); //set ready bits for next wakeup/dispatch stage
#ifdef DEBUG
		cout << "-------------------------	RENAME     -----------------------------" << endl;	
#endif		
		analyser->rename();  //Rename stage - PIRAT, and places DV-Inst in ROB
#ifdef DEBUG
		cout << "-------------------------	ISSUE      -----------------------------" << endl;
#endif
		
		analyser->issue_push(); //takes new entries off ROB (acts as a queue also) and places in issue queues
#ifdef DEBUG
		cout << "------------------------  DISPATCH MEM   -----------------------------" << endl;
#endif
		analyser->issue_inst_with_loads();

#ifdef DEBUG
		cout << "-------------------------	FETCH     -----------------------------" << endl;
#endif
		analyser->evict_pathtable_entries();
		while(true){
			
			temp_fc=analyser->fetch(false, fetch_count); //selects which thread to schedule using MinSP-PC

			if(temp_fc < 0 )
				break;

			fetch_count+=temp_fc;
			log(logDEBUG4)<<"Fetch count "<<fetch_count<<endl;

			if(fetch_count >= conf.fetch_width){
				break;
			}

		}
		fetch_count = 0;
		for(uint i = 0 ; i < num_threads; i++){
			threads[i].iq_full = false;
		}

		if((UINT)thread_exit_count == num_threads){
			cout<<"BREAKING"<<endl;

			break;
		}

	}

	if(!early_exit){
		for (uint i = 0; i < num_threads; i++){
			pthread_join(pthreads[i],NULL);
		}
	}

	LONG maxinst = 0;
	for(int i = 0 ;i< conf.num_threads;i++){
		if(maxinst < threads[i].ins_count)
		maxinst =threads[i].ins_count; //tally total instructions	
	}

	/////////////////// Print out stats for OoO DITVA ///////////////////////////////
	cout << endl << endl << ">>>>>>>>>>>>>>>>>>>>>>> OoO DITVA BENCHMARK STATS ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~" << endl;
	cout << "\tTotal DV-instructions Fetched:\t" << analyser->statistics.dv_inst_count << "\tTotal Instructions: " << analyser->statistics.total_ins_count << endl;
	//analyser->statistics.fetch_inst_count << endl; //TODO committed, fetched
	cout << "\tTotal DV committed:\t" << analyser->statistics.total_dv_committed << endl;
	cout << "\tTotal Cycles:\t" << analyser->statistics.exe_cycles << endl;
	cout << "\tTotal cuops:\t" << analyser->statistics.convergence_count << endl;
	//cout << "\tTotal Divergences:\t" << analyser->statistics.divergence_count << endl;
	cout << "\tTotal Branches:\t" << analyser->statistics.dv_branch << endl;
	cout << "\tTotal buops:\t" << analyser->statistics.buop_count << endl << endl;

	cout << "\tTotal loads:\t" << analyser->statistics.dv_load  << "    counting at commit = " << analyser->statistics.counting_loads << endl;
	cout << "\tTotal stores:\t" << analyser->statistics.dv_store << "    counting at commit = " << analyser->statistics.counting_stores << endl;
	cout << "\tTotal DV-Int:\t" << analyser->statistics.dv_int_count << endl;
	cout << "\tTotal DV-FP:\t" << analyser->statistics.dv_fp_count << endl;
	cout << "\tTotal SIMD:\t" << analyser->statistics.simd_count << endl;
	

	cout << endl << "-----------------------------------------------------------------------" << endl;
	cout << "PIRAT Statistics <<<<<<<<<<<<<<<<<<" << endl;
	cout << "\tTotal PIRAT INT writes:\t" << analyser->statistics.pirat_wr_accesses << endl;
	cout << "\tTotal PIRAT INT reads:\t" << analyser->statistics.pirat_rd_accesses << endl;
	LONG total_pirat_access = analyser->statistics.pirat_wr_accesses + analyser->statistics.pirat_rd_accesses;
	LONG total_fp_pirat_access = analyser->statistics.pirat_fp_wr_accesses + analyser->statistics.pirat_fp_rd_accesses;

	cout << "\tTotal PIRAT INT Accesses:\t" << total_pirat_access << endl << endl;
	
	cout << "\tTotal PIRAT FP writes:\t" << analyser->statistics.pirat_fp_wr_accesses << endl;
	cout << "\tTotal PIRAT FP reads:\t" << analyser->statistics.pirat_fp_rd_accesses << endl << endl;
	cout << "\tTotal PIRAT FP Accesses:\t" << total_fp_pirat_access << endl << endl;
	
	cout << "\tPIRAT INT Default Read Accesses:\t" << analyser->statistics.pirat_default_rd << endl;
	cout << "\tPIRAT INT Partial Read Accesses:\t" << analyser->statistics.pirat_partial_rd << endl;
	cout << "\tPIRAT FP Default Read Accesses:\t" << analyser->statistics.pirat_fp_default_rd << endl;
	cout << "\tPIRAT FP Partial Read Accesses:\t" << analyser->statistics.pirat_fp_partial_rd << endl << endl;

	cout << "\tPIRAT Total INT Partial Overwrites:\t" << analyser->statistics.pirat_wr_accesses - analyser->statistics.write_merges<< endl;
	cout << "\tPIRAT Total FP Partial Overwrites:\t" << analyser->statistics.pirat_fp_wr_accesses - analyser->statistics.write_fp_merges<< endl << endl;

	cout << "\tMerge Event Statistics <<<<<<<<<<<<<<<<<<" << endl;
	cout << "\t\t[Write] INT Phi-uop:\t" << analyser->statistics.write_merges << "\t[Read] INT Phi-uop:\t" << analyser->statistics.read_merges << endl;
	cout << "\t\t[Total INT Phi-uop]:\t" << analyser->statistics.write_merges + analyser->statistics.read_merges << endl << endl;

	cout << "\t\t[Write] FP phi-uop:\t" << analyser->statistics.write_fp_merges << "\t[Read] FP phi-uop:\t" << analyser->statistics.read_fp_merges << endl;
	cout << "\t\t[Total INT phi-uop]:\t" << analyser->statistics.write_fp_merges + analyser->statistics.read_fp_merges << endl;

	LONG total_phi = analyser->statistics.write_merges + analyser->statistics.read_merges + analyser->statistics.write_fp_merges + analyser->statistics.read_fp_merges;
	cout << "\t[Total Write phi-uops]:\t" << analyser->statistics.write_merges + analyser->statistics.write_fp_merges << endl;
	cout << "\t[Total Read phi-uops]:\t" << analyser->statistics.read_merges + analyser->statistics.read_fp_merges << endl;
	cout << "\t[Total phi-uops]:\t" << total_phi << endl;
	
	if(total_pirat_access > 0 || total_fp_pirat_access > 0){
		cout << "\t[Total Merges/total accesses]:\t" << std::scientific << (float)total_phi/((float)(total_pirat_access+total_fp_pirat_access))*(float)(100) << endl;
	}
	if(total_pirat_access > 0)
	cout << "\t[Total INT merges/total accesses]:\t" << ((((float)analyser->statistics.write_merges + (float)analyser->statistics.read_merges)/(float)(total_pirat_access))*100) << endl;
	if(total_fp_pirat_access > 0)
	cout << "\t[Total FP merges/total accesses]:\t" << (float)((((float)analyser->statistics.write_fp_merges + (float)analyser->statistics.read_fp_merges)/(float)(total_fp_pirat_access))*100) << endl << std::fixed << endl;
	


	cout << endl << "-----------------------------------------------------------------------" << endl;
	cout << "Stall Statistics <<<<<<<<<<<<<<<<<<" << endl;
	cout << "\tDecode stalls:\t" << analyser->statistics.decBuffer_stalls << endl;
	cout << "\tROB stalls:\t" << analyser->statistics.rob_stalls << endl;
	cout << "\tIssue stalls:\t" << analyser->statistics.issue_stalls << endl;
	cout << "\tRename stalls:\t" << analyser->statistics.reg_rename_stalls << endl << endl;

	cout << endl << "-----------------------------------------------------------------------" << endl;
	cout << "Memory Stats <<<<<<<<<<<<<<<<<<" << endl;
	cout << "\tL1 Miss Loads:\t" << analyser->statistics.l1_miss_r << endl;
	cout << "\tL1 Hit Loads:\t" << analyser->statistics.l1_hit_r << endl;
	cout << "\tL1 Miss Stores:\t" << analyser->statistics.l1_miss_w << endl;
	cout << "\tL1 Hit Stores:\t" << analyser->statistics.l1_hit_w << endl;
	cout << "\tTotal L1 Accesses: \t" << analyser->statistics.l1_miss_r + analyser->statistics.l1_hit_r + analyser->statistics.l1_miss_w + analyser->statistics.l1_hit_w << endl << endl;

	cout << endl << "-----------------------------------------------------------------------" << endl;
	cout << "Calculated Stats <<<<<<<<<<<<<<<<<<" << endl;
	cout << "\tThread Occupancy:\t" << endl;
	LONG total_thread_count = 0, thread_occup = 0;

	for(int i = 0; i < (int)(conf.num_threads/conf.num_warps); i++){
		total_thread_count += analyser->statistics.thread_occupancy[i];
		thread_occup += (i+1)*analyser->statistics.thread_occupancy[i];
	}
	
	for(int i = 0; i < (int)(conf.num_threads/conf.num_warps); i++){
		if(total_thread_count > 0){
			cout << i << " Thread: " << std::scientific << (((float)analyser->statistics.thread_occupancy[i]/(float)total_thread_count)*100) << "     i.e. " << std::fixed;
			cout << analyser->statistics.thread_occupancy[i] << ", ";
			cout << total_thread_count << endl;
		}
	}
	cout << endl;
	
	LONG total_predictions = analyser->statistics.correct_predict + analyser->statistics.partial_misprediction + analyser->statistics.full_misprediction;
	cout << " -------------------	Prediction Stats -------------------- " << endl;
	cout << "Correct Predictions:\t" << std::fixed << analyser->statistics.correct_predict << " = " <<  (float)(analyser->statistics.correct_predict/total_predictions)*100 << "%" << endl;	
	cout << "Branch mispredicitons:\t" << analyser->statistics.branch_misprediction <<  " = " << (float)(analyser->statistics.branch_misprediction/total_predictions)*100 << "%" << endl;
	cout << "Partial mispredictions:\t" << analyser->statistics.partial_misprediction <<  " = " << (float)(analyser->statistics.partial_misprediction/total_predictions)*100 << "%" << endl;
	cout << "Full mispredictions:\t" << analyser->statistics.full_misprediction <<  " = " << (float)(analyser->statistics.full_misprediction/total_predictions)*100 << "%" <<  endl;
	cout << "Total predictions:\t" << total_predictions << endl << endl;

	//cout << "\tRelative DV-Inst Count:\t" << std::scientific << (float)analyser->statistics.fetch_inst_count/(float)analyser->statistics.dv_inst_count <<  endl << endl;
	cout << "\tRelative DV-Inst Count:\t" << std::scientific << (float)thread_occup/(float)total_thread_count << endl;
	cout << "\tDV-Throughput:\t" << (float)analyser->statistics.exe_dvinst_count/(float)analyser->statistics.exe_cycles << endl;
	cout << "\tThroughput (no DV):\t" << ((float)analyser->statistics.exe_dvinst_count/(float)analyser->statistics.exe_cycles)*((float)thread_occup/(float)total_thread_count) << endl;
	cout << "\tIPC:\t" << ((float)analyser->statistics.total_ins_count/(float)analyser->statistics.cycle) << endl;
	cout << "\tDV-IPC:\t " << ((float)analyser->statistics.total_ins_count/((float)thread_occup/(float)total_thread_count))/(float)analyser->statistics.cycle << endl;
//(float)analyser->statistics.exe_inst_count/(float)analyser->statistics.exe_cycles << endl;
	cout << "\tTotal Cycles:\t" << std::fixed << analyser->statistics.exe_cycles << " / " <<  analyser->statistics.cycle << endl;

	
	cout << "Single-Thread IPC = \t" << (float)maxinst/(float)analyser->statistics.cycle << endl; 

	LONG max_paths = 0;
	for(int i = 0; i < conf.num_warps; i++){
		if(max_paths < analyser->statistics.max_paths[i])
			max_paths = analyser->statistics.max_paths[i];
	}

	cout << "\tMax Path Table Entries:\t" << max_paths << endl;

	float avg_phi_int = 0, avg_phi_fp = 0; 
	LONG phi_int = 0, phi_fp = 0;
	for(int i = 0; i < (conf.issue_width*3)+1; i++){
		phi_int += analyser->statistics.phi_frequency[i];  avg_phi_int += (i+1)*analyser->statistics.phi_frequency[i];
		phi_fp += analyser->statistics.phi_frequency_fp[i]; avg_phi_fp += (i+1)*analyser->statistics.phi_frequency_fp[i];
	}

	avg_phi_int = ((float)avg_phi_int/(float)phi_int) - 1;
	avg_phi_fp = ((float)avg_phi_fp/(float)phi_fp) - 1;	

	//generate csv and data
	ofstream csv;
	csv.open(conf.csv);
	csv << "#Benchmark,Total DV-Inst,Total Inst,Total Cycles,Total cuops,Total buops,Total Branches,Total Loads,Total Stores,Total Wr Phi,Total Rd Phi,Total Phi,Tot PIRAT Acc,Total Phi/Accesses,";
	csv << "RenameStalls,L1 Miss,L1 Hit,Avg Thread Occupancy,Warp Thru,Thru,Warp IPC,IPC,1T_IPC,Max Paths,branchnops,Bmispred,NoMispr,ParMispr,FullMispr,PIRATstalls,WAWstalls\n";
	//"AvgPhiINT,AvgPhiFP\n";
	csv <<  conf.benchmark << ",";
	csv << analyser->statistics.dv_inst_count << ","; //DV-instructions fetched
	csv << analyser->statistics.total_ins_count << ","; //analyser->statistics.fetch_inst_count << ",";
	csv << analyser->statistics.cycle << ",";
	csv << analyser->statistics.convergence_count << ",";
	csv << analyser->statistics.buop_count << ",";
	csv << analyser->statistics.dv_branch << ",";
	csv << analyser->statistics.dv_load << ","; //analyser->statistics.counting_loads << ","; 
	csv << analyser->statistics.dv_store << ","; //analyser->statistics.counting_stores << ","; //
	csv << analyser->statistics.write_merges + analyser->statistics.write_fp_merges << ",";
	csv << analyser->statistics.read_merges + analyser->statistics.read_fp_merges << ",";
	csv << total_phi << ",";
	csv << (total_pirat_access+total_fp_pirat_access)<< ",";
	csv << std::scientific << (float)total_phi/((float)(total_pirat_access+total_fp_pirat_access))*(float)(100) << "%,";
	csv << std::scientific << analyser->statistics.reg_rename_stalls <<"," << std::fixed;
	csv << analyser->statistics.l1_miss_r + analyser->statistics.l1_miss_w << ",";
	csv << analyser->statistics.l1_hit_r + analyser->statistics.l1_hit_w << ",";
	csv << std::scientific <<  std::scientific << std::scientific << (float)thread_occup/(float)total_thread_count << ",";
	//(float)analyser->statistics.fetch_inst_count/(float)analyser->statistics.dv_inst_count  << ",";
	csv << (float)analyser->statistics.exe_dvinst_count/(float)analyser->statistics.exe_cycles << ",";
	csv << ((float)analyser->statistics.exe_dvinst_count/(float)analyser->statistics.exe_cycles)*((float)thread_occup/(float)total_thread_count) << ",";
	csv << ((float)analyser->statistics.total_ins_count/(float)analyser->statistics.cycle) << ",";
	csv << ((float)analyser->statistics.total_ins_count/((float)thread_occup/(float)total_thread_count))/(float)analyser->statistics.cycle << ",";
	csv << (float)maxinst/(float)analyser->statistics.cycle << ",";
	csv << std::fixed << max_paths << "," << analyser->statistics.branch_nops << "," << (float)(analyser->statistics.branch_misprediction) << ",";
	csv << (float)(analyser->statistics.correct_predict) << "," << (float)(analyser->statistics.partial_misprediction) << ",";
	csv << (float)(analyser->statistics.full_misprediction) << ",";
	csv << analyser->statistics.pirat_stalls << "," << analyser->statistics.waw_stalls << "\n" << endl;
	//csv << avg_phi_int << "," << avg_phi_fp << "\n" << endl;

	//phi stat breakdowns - these are hard coded for now
/*	csv << "#Benchmark,INT_phi[0],INT_phi[1],INT_phi[2],INT_phi[3],INT_phi[4],INT_phi[5],INT_phi[6],INT_phi[7],INT_phi[8],INT_phi[9],INT_phi[10],INT_phi[11],INT_phi[12]\n";
	csv <<  conf.benchmark << ",";
	for(int i = 0; i < (conf.issue_width*3)+1; i++){
		csv << std::scientific << (((float)analyser->statistics.phi_frequency[i]/(float)phi_int)*100) << ",";
	}
	csv << "\n";
	csv << "#Benchmark,FP_phi[0],FP_phi[1],FP_phi[2],FP_phi[3],FP_phi[4],FP_phi[5],FP_phi[6],FP_phi[7],FP_phi[8],FP_phi[9],FP_phi[10],FP_phi[11],FP_phi[12]\n";
	csv <<  conf.benchmark << ",";
	for(int i = 0; i < (conf.issue_width*3)+1; i++){
		csv << std::scientific << (((float)analyser->statistics.phi_frequency_fp[i]/(float)phi_fp)*100) << ",";
	}
	csv << endl;*/
	csv.close();
	
	
	

	/*cout << endl << endl << ">>>>>>>>>>>>>>>>>> [PRELIM] OoO DITVA BENCHMARK STATS ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~" << endl;
	cout << "\tTotal DV-instructions:\t" << analyser->statistics.cnt_dvinsts << "\tTotal Instructions: " << analyser->statistics.cnt_insts << endl;
	cout << "\tConvergent Count:\t" << analyser->statistics.conv_cnt << endl;
	cout << "\tDivergent Count:\t" << analyser->statistics.div_cnt << endl;
	cout << "\tTotal Branches:\t" << analyser->statistics.branch_cnt << endl;
	cout << endl << "-----------------------------------------------------------------------" << endl;
	cout << "Register Read Mask Statistics<<<<<<<<<<<<" << endl;
	cout << "\tConv Equal: \t" << analyser->statistics.c_r_equal_masks << "\tDiv Equal:\t" << analyser->statistics.d_r_equal_masks << endl;
	cout << "\tNon Intersecting:  " << analyser->statistics.r_non_inter_masks << endl;
	cout << "\tStrict Subset:\t" << analyser->statistics.r_strict_sub << "\tStrict Superset:\t" << analyser->statistics.r_strict_super << endl;
	LONG tmp_tot = analyser->statistics.reg_reads - analyser->statistics.c_r_equal_masks - analyser->statistics.d_r_equal_masks - analyser->statistics.r_non_inter_masks - analyser->statistics.r_strict_sub - analyser->statistics.r_strict_super;
	cout << "\tTotal Reg Reads:\t" << analyser->statistics.reg_reads << "\tNo category:\t" << analyser->statistics.r_other << "(" << tmp_tot << ")" << endl;
	cout << endl;
	cout << endl << "-----------------------------------------------------------------------" << endl;
	cout << "Register Write Mask Statistics<<<<<<<<<<<<" << endl;
	cout << "\tConv Equal:\t" << analyser->statistics.c_w_equal_masks << "\tDiv Equal:\t" << analyser->statistics.d_w_equal_masks << endl;	
	cout << "\tNon Intersecting:  " << analyser->statistics.w_non_inter_masks << endl;
	cout << "\tStrict Subset:\t" << analyser->statistics.w_strict_sub << "\tStrict Superset:\t" << analyser->statistics.w_strict_super << endl;
	tmp_tot = analyser->statistics.reg_writes - analyser->statistics.c_w_equal_masks - analyser->statistics.d_w_equal_masks - analyser->statistics.w_non_inter_masks - analyser->statistics.w_strict_sub - analyser->statistics.w_strict_super;
	cout << "\tTotal Reg Writes:\t" << analyser->statistics.reg_writes << "\tNo category:\t" << analyser->statistics.w_other << "(" << tmp_tot << ")" << endl;
	cout << endl;
	cout << endl << "-----------------------------------------------------------------------" << endl;
	cout << "Merge Event Statistics <<<<<<<<<<<<<<<<<<" << endl;
	cout << "\t[Write] Merge:\t" << analyser->statistics.w_merge_event << "\t[Read] Merge:\t" << analyser->statistics.r_merge_event << endl;
	cout << "\t[Total Events]:\t" << analyser->statistics.merge_event << endl;*/
	//////////////////////////////////////////////////////////////////////////////////

	//log_ditva_stats();




}

void Simulation::log_ditva_stats(){
	logTrace<<"########################################################\n";
	logTrace<<"\t\tDITV-SIM STATS\n";
	logTrace<<"########################################################\n";



	logTrace<<"Memory_Operations " << analyser->mem_operation_count<<" Bank_conflict "<< analyser->bank_conflict_count << " Bank_conflict_reduced "<<  analyser->bank_conflict_count_reduced <<endl;
	//}
	logTrace<<"Combined stats: " ;
	for(UINT i = 1 ; i < num_threads + 1 ; i++){
		logTrace<<" ["<<i<<"] "<< analyser->statistics.combine_count[i];
	}
	//printf("Total exec %llu \n",analyser->statistics.total_exec_inst);
	logTrace<<endl;
	//printf("Same OP count %llu \n",analyser->sameOpCount());
	logTrace<<"Total Cycles: "<<analyser->number_cycles<<endl;
	LONG tot = 0;
	for(UINT i = 0 ; i< num_threads;i++){
		tot+=threads[i].ins_count;
	}

	logTrace<<"Issue stalled due to : ";
	logTrace<<"No_issuable_insts "<< analyser->statistics.no_issuable_instructions<<" iw_full "<<analyser->statistics.iw_full<<" fu_full "<<analyser->statistics.fu_full<<" Dep "<<analyser->statistics.dependant<<endl;
	logTrace<<"Outstanding loadstore: "<<analyser->statistics.outstandingls<<endl;
	logTrace<<"ICT: ";
	for(int i = 0 ; i < ((int)num_threads*conf.issue_width) + 1;i++){
		logTrace<<"["<<i<<"]-"<< analyser->statistics.thread_issue_count[i];
	}
	logTrace<<endl;
	logTrace<<"Issue count: ";
	for(int i = 0 ; i < conf.issue_width + 1;i++){
		logTrace<<"["<<i<<"]-"<< analyser->statistics.issue_count[i];
	}
	logTrace<<endl;
	logTrace<<"Total instructions: "<<tot<<" IPC "<< tot/(double)analyser->number_cycles<<endl<<endl;

	logTrace<<"--------------------------------------------------------\n";
	logTrace<<"Per-Thread Stats\n";
	logTrace<<"--------------------------------------------------------\n";
	logTrace<<"Finish cycle"<<endl;
	for(UINT i = 0 ; i< num_threads;i++){
		logTrace<<"T"<<i<<" "<<threads[i].finish_cycle<<" ";
	}
	logTrace<<endl;
	for(UINT i = 0; i < num_threads; i++){
		logTrace<<"Thread["<<i<<"]: Ins "<< threads[i].ins_count<<endl;
		logTrace<<" IPC "<<threads[i].ins_count/(double)analyser->number_cycles<<
				" branches "<<analyser->statistics.branches[i]<<
				" misprediction "<<analyser->statistics.branch_misprediction<<
				" ins[int "<<analyser->statistics.inst_int[i]<<
				" fp "<< analyser->statistics.inst_fp[i]<<
				"ld "<<analyser->statistics.inst_ld[i]<<
				" st "<<analyser->statistics.inst_st[i]<<
				" ] commit "<<analyser->statistics.ins_commit[i]<<
				" lsdivergence "<<analyser->statistics.lsdivergence[i]<<endl;

		//printf("Scalarized instruction count %llu \n",simulation.getScalarized_Thread(i));
		logTrace<<" loads "<<analyser->statistics.load_count[i] <<" stores "<< analyser->statistics.store_count[i] <<endl;
		//logTrace<<" loads_at_fetch %llu stores_at_fetch %llu\n", analyser->statistics.load_count_at_fetch[i],analyser->statistics.store_count_at_fetch[i]);
		//printf("Reg: read int %llu read float %llu write int %llu write float %llu \n", da->statistics.reg_read_int[i],
		//		analyser->statistics.reg_read_float[i],
		//		analyser->statistics.reg_write_int[i],
		//		analyser->statistics.reg_write_float[i]);
	}
	logTrace<<"Full Misprediction: "<<analyser->statistics.full_misprediction<<endl;
	logTrace<<"Partial Misprediction: "<<analyser->statistics.partial_misprediction<<endl;
	logTrace<<"--------------------------------------------------------"<<endl;
	logTrace<<"DVStats: "<<endl;
	logTrace<<"--------------------------------------------------------"<<endl;
	logTrace<<"DVInst: "<< analyser->statistics.dv_inst_count <<
			" DVLoad: "<<analyser->statistics.dv_load <<""
			" DVStore: "<< analyser->statistics.dv_store<<endl;
	logTrace<<"Divergence: "<< analyser->statistics.divergence_count <<
			" Convergence: "<<analyser->statistics.convergence_count<<endl;
	logTrace<<"DV-SIMD: "<<analyser->statistics.simd_count<<endl;

	logTrace<<endl;
	logTrace<<"--------------------------------------------------------"<<endl;
	logTrace<<"Cache Stats: "<<endl;
	logTrace<<"--------------------------------------------------------"<<endl;
	for(int i = 1 ;i < NUM_CACHE+1 ;i++){
		logTrace<<"l"<<i<<" miss "<< analyser->statistics.cache_miss_nonscalar[i]<<
				" read miss "<<analyser->statistics.cache_read_miss_nonscalar[i]<<
				" write miss "<<analyser->statistics.cache_write_miss_nonscalar[i]<<endl;
	}

	logTrace<<"Partial miss "<<analyser->statistics.partial_miss<<endl;

	LONG total_tlb_access_8=0Ull;
	LONG total_tlb_hit_8=0Ull;
	LONG total_tlb_miss_8=0Ull;
	/*LONG total_tlb_access_128=0Ull;
	LONG total_tlb_hit_128=0Ull;
	LONG total_tlb_miss_128 =0Ull;*/
	LONG total_tlb_access_16=0Ull;
	LONG total_tlb_hit_16=0Ull;
	LONG total_tlb_miss_16 =0Ull;
	/*LONG total_tlb_access_256=0Ull;
	LONG total_tlb_hit_256=0Ull;
	LONG total_tlb_miss_256 =0Ull;*/
	for(int i = 0; i< conf.threads_per_warp;i++){
		total_tlb_access_8+=analyser->l1_cache.split_tlb_8[i].get_access_count();
		total_tlb_access_16+=analyser->l1_cache.split_tlb_16[i].get_access_count();
		total_tlb_hit_8+=analyser->l1_cache.split_tlb_8[i].get_tlb_hit();
		total_tlb_hit_16+=analyser->l1_cache.split_tlb_16[i].get_tlb_hit();
		total_tlb_miss_8+=analyser->l1_cache.split_tlb_8[i].get_tlb_miss();
		total_tlb_miss_16+=analyser->l1_cache.split_tlb_16[i].get_tlb_miss();
		//logTrace<<"For thread "<<i<< " instruction count = " << thread_instruction_count[i] <<" TLB hit = "<<threads[i].tlb->get_tlb_hit()<<" TLB miss = "<< threads[i].tlb->get_tlb_miss()<<" Access count "<<threads[i].tlb->get_access_count() << endl;
	}
	logTrace<<"TLB8 "<<total_tlb_hit_8<<" "<<total_tlb_miss_8<<" "<<total_tlb_access_8<<endl;
	logTrace<<"TLB128 "<<analyser->l1_cache.unified_tlb_8->get_tlb_hit()<<" "<<analyser->l1_cache.unified_tlb_8->get_tlb_miss()<<" "<<analyser->l1_cache.unified_tlb_8->get_access_count()<<endl;
	logTrace<<"TLB16 "<<total_tlb_hit_16<<" "<<total_tlb_miss_16<<" "<<total_tlb_access_16<<endl;
	logTrace<<"TLB256 "<<analyser->l1_cache.unified_tlb_16->get_tlb_hit()<<" "<<analyser->l1_cache.unified_tlb_16->get_tlb_miss()<<" "<<analyser->l1_cache.unified_tlb_16->get_access_count()<<endl;
	logTrace<<endl;
	for(int i = 0; i< conf.num_warps;i++){
		logTrace<<" Warp : "<<i;
		logTrace<<" blocked "<<analyser->dviqm._stat_blocked[i]<<" inorder "<<analyser->dviqm._stat_inorder[i]<<" zero "<<analyser->dviqm._stat_zero[i]<<" picked "<<analyser->dviqm._stat_picked[i]<<endl;
		logTrace<<"Fetch M blocked "<<analyser->_stat_fetch_blocked[i]<<" Fetch M picked "<<analyser->_stat_fetch_picked[i]<<" Fetch R picked "<<analyser->_stat_fetch_picked[i]<<endl;
		logTrace<<"Issue FU blocked "<<analyser->issue_fu_blocked[i]<<" Issue dep blocked "<<analyser->issue_dep_blocked[i]<<endl;
	}
	logTrace<<endl;
	for(int i = 0; i< conf.num_warps;i++){
		logTrace<<"	_stat_active_thread_count[ref] "<< analyser->_stat_active_thread_count[i] <<"	_stat_fetch_stall[ref] "<< analyser->_stat_fetch_stall[i]<<"	_stat_ignore[ref] "<< analyser->_stat_ignore[i]<<"	_stat_fetch_qempty[ref] "<< analyser->_stat_fetch_qempty[i]<<"	_stat_iq_full[ref] "<< analyser->_stat_iq_full[i]<< " _stat_fetch_fair "<<analyser->_stat_fetch_fair[i]<<endl;
	}

	/*CSV File*/
	logCSV<<"total_instructions"<<","<<"total_dv_instructions"<<","<<"int_instructions"<<","<<"fp_instructions"<<","<<"branch_instructions"
			<<","<<"branch_mispredictions"<<","<<"load_instructions"<<","<<"store_instructions"<<","<<"committed_instructions"
			<<","<<"committed_int_instructions"<<","<<"committed_fp_instructions"<<","<<"int_regfile_reads"
			<<","<<"float_regfile_reads"<<","<<"int_regfile_writes"<<","<<"float_regfile_writes"
			<<","<<"ialu_accesses"<<","<<"fpu_accesses"<<","<<"mul_accesses"<<","<<"dtlb_total_accesses"
			<<","<<"dtlb_total_misses"<<","<<"l1_read_accesses"<<","<<"l1_write_accesses"
			<<","<<"l1_read_misses"<<","<<"l1_write_misses"<<","<<"l1_bank_conflicts"
			<<","<<"l2_read_accesses"<<","<<"l2_write_accesses"<<","<<"l2_read_misses"<<","<<"l2_write_misses"
			<<","<<"total_cycles"<<","<<"icache_access"<<endl;

	logCSV<<tot<<","<<analyser->statistics.dv_inst_count<<","<<analyser->statistics.dv_int_count<<","<<analyser->statistics.dv_fp_count<<","<<analyser->statistics.dv_branch
			<<","<<analyser->statistics.full_misprediction<<","<<analyser->statistics.dv_load<<","<<analyser->statistics.dv_store<<","<<analyser->statistics.dv_inst_count
			<<","<<analyser->statistics.dv_int_count<<","<<analyser->statistics.dv_fp_count<<","<<analyser->statistics.int_reg_file_read
			<<","<<analyser->statistics.float_reg_file_read<<","<<analyser->statistics.int_reg_file_write<<","<<analyser->statistics.float_reg_file_write
			<<","<<analyser->statistics.ialu_access<<","<<analyser->statistics.fpu_access<<","<<analyser->statistics.mul_access
			<<",";
	if(conf.base){
		logCSV<<analyser->l1_cache.unified_tlb_16->get_access_count()<<","<<analyser->l1_cache.unified_tlb_16->get_tlb_miss()<<",";
	}else{
		logCSV<<total_tlb_access_16<<","<<total_tlb_miss_16<<",";
	}
	logCSV<<analyser->statistics.cache_read_access[1]<<","<<analyser->statistics.cache_write_access[1]<<","<<analyser->statistics.cache_read_miss_nonscalar[1]
																																							<<","<<analyser->statistics.cache_write_miss_nonscalar[1]<<","<<analyser->bank_conflict_count_reduced
																																							<<","<<analyser->statistics.cache_read_access[2]<<","<<analyser->statistics.cache_write_access[2]<<","<<analyser->statistics.cache_read_miss_nonscalar[2]
																																																																												   <<","<<analyser->statistics.cache_write_miss_nonscalar[2]<<","<<analyser->number_cycles<<","<<analyser->statistics.icache_access_cycles<<endl;

	//REMOVE HISTOGRAM
	logTrace<<endl<<"L1 Histogram"<<endl;
	int count = sizeof(W64)*8;
	for(int i = 0; i < count; i++){
		logTrace<<std::dec<<analyser->l1_cache.histogram[i]<<",";
	}
	logTrace<<endl;
	//END

	for (UINT i=0; i<num_threads; i++){
		threads[i].thread_arq.close();
	}


}

