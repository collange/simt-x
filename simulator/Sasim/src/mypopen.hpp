/*
 * popen_noshell: A faster implementation of popen() and system() for Linux.
 * Copyright (c) 2009 Ivan Zahariev (famzah)
 * Version: 1.0
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; under version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses>.
 */

#ifndef POPEN_NOSHELL_H
#define POPEN_NOSHELL_H

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


//POpen impl
typedef struct pinfo {
	FILE         *file;
	pid_t         pid;
	struct pinfo *next;
} pinfo;

static pinfo *plist = NULL;
//End

/***************************
 * PUBLIC FUNCTIONS FOLLOW *
 ***************************/


FILE*
mypopen(const char *command, const char *mode);


int
mypclose(FILE *file);


#endif
