#include "Heuristic.hpp"
#include <bitset>

class Sync : public Heuristic {

	int invalid;
	int cwid;
	// Round robin among active threads

	int threads_in_warp;


public:
	Sync(UINT nt, UINT nw, Thread* t, const char* param);
	~Sync();

	int getCurrentWarp(){
		return cwid;
	}
	int thread_schedule(LONG current_cycle){}
	void choose_threads();
	virtual  bool lockstep(){
		return true;
	}
};

Heuristic* new_heuristic(UINT nt, UINT nw, Thread* t, const char* param){
	return new Sync(nt, nw ,t, param);
}
void delete_heuristic(Heuristic* h){
	delete h;
}

Sync::Sync(UINT nt, UINT nw, Thread* t, const char* param)
: Heuristic(nt, nw , t)
{
	threads_in_warp = num_threads/num_warps;

	invalid = true;

	cwid = -1;

}

Sync::~Sync(){

}

void Sync::choose_threads()
{
	//static int current_warp = 0;
	// Round robin among active threads
	static int rrcounter = 0;

	//RR GROUP
	static std::bitset<16> fetched_threads;
	//END
	int ref = -1;

	//uint warp_count = 0;
	//while(warp_count < num_warps)

	for (UINT i=0; i<num_threads; i++){
		t[i].fetch_isactive = false;
		t[i].active_last_fetch = false;
	}
	for (UINT ii=0; ii<num_threads; ii++){
		unsigned int i = (ii + rrcounter) % num_threads;
		/*if(t[i].active_last_fetch){
			continue;
		}*/
		//RR GROUP
		if(fetched_threads.test(i)){
			continue;
		}
		//END
		if (!t[i].fetch_qfull && !t[i].fetch_stall && !t[i].ignore &&  !t[i].fetch_qempty && !t[i].primary_heur_fetch ){
			ref = i;
			cwid = t[i].warpid;
			break;
		}
	}
	if(ref == -1){
		for (UINT ii=0; ii<num_threads; ii++){
			unsigned int i = (ii + rrcounter) % num_threads;
			/*if(t[i].active_last_fetch){
				continue;
			}*/

			//END
			if (!t[i].fetch_qfull && !t[i].fetch_stall && !t[i].ignore &&  !t[i].fetch_qempty && !t[i].primary_heur_fetch){
				ref = i;
				cwid = t[i].warpid;
				break;
			}
		}

	}

	if(ref == -1){
		for (UINT ii=0; ii<num_threads; ii++){
			unsigned int i = (ii + rrcounter) % num_threads;
			if (!t[i].fetch_qfull && !t[i].fetch_stall && !t[i].ignore &&  !t[i].fetch_qempty){
				ref = i;
				cwid = t[i].warpid;
				break;
			}
		}
		if(ref == -1){
			for (UINT i=0; i<num_threads; i++){
				t[i].fetch_isactive = false;
			}
			return;
		}
	}
	//assert(ref>=0);
	//bool isact = false;
	uint offset = cwid*threads_in_warp;
	//cwid = ref/threads_in_warp;
	//current_warp = cwid;
	for (UINT i=0+offset; i<threads_in_warp+offset; i++){
		t[i].fetch_isactive =  !t[i].fetch_qempty && !t[i].fetch_qfull && t[i].fetch_pc == t[ref].fetch_pc && !t[i].fetch_stall && !t[i].ignore;
		/*if(t[i].fetch_isactive && !t[i].fetch_stall && !t[i].ignore){
			isact = true;
		}*/
		//t[i].is_active = (i == ref);    // No fetch combining
		//RR GROUP
		if(t[i].fetch_isactive){
			fetched_threads.set(i);
		}
		//END
	}

	//RR GROUP
	bool rem = false;

	for (UINT i=0; i<num_threads; i++){
		if(!fetched_threads.test(i) && (!t[i].fetch_qfull && !t[i].fetch_stall && !t[i].ignore &&  !t[i].fetch_qempty)){
			rem = true;
			break;
		}
	}
	if(!rem){
		fetched_threads.reset();
	}
	//END

	/*bool problem = true;
	for (UINT i=0; i<num_threads; i++){
		if(t[i].is_active){
			problem = false;
		}
	}
	if (problem){
		for (UINT i=0; i<num_threads; i++){
			cout<<"Before Thread id "<<i<<" ";
			printf(" %.16lx \n",t[i].pc );
			if(t[i].ignore){
				cout<<" IG ";
			}else{
				cout<<" NIG ";
			}
			if(t[i].barrier){
				cout<<" BA ";
			}
			else{
				cout<<" NBA ";
			}
			if(t[i].is_finished){
				cout<<" FIN ";
			}else{
				cout<<" NFIN ";
			}
			cout<<endl;
		}
	}*/
	// Next
	rrcounter = (ref + 1) % num_threads;
	/*if(!isact){
		t[rrcounter].fetch_isactive = true;
	}*/
}

