#include "register.hpp"


using namespace std;

int Register::getReadRegIndex(string reg) {
	//string reg = reg1;
	//boost::to_upper(reg);
	transform(reg.begin(), reg.end(), reg.begin(), ::toupper);

	std::map<std::string, uint32_t>::iterator iter;
	iter = regname_to_index.find(reg);
	if(iter != regname_to_index.end())
		return iter->second;
	else
		return NOREG_DVSIM;
	//return 1;
}

int Register::getWriteRegIndex(string reg) {
	std::map<std::string, uint32_t>::iterator iter;
	//string reg = reg1;
	//boost::to_upper(reg);
	transform(reg.begin(), reg.end(), reg.begin(), ::toupper);

	iter = regname_to_index.find(reg);
	if(iter != regname_to_index.end()){
		return iter->second + write_reg_start_index;
	}
	else{
		assert(false);
		return NOREG_DVSIM;
	}
	//return 1;
}


string Register::getRegName(int index) {
	//return regname[index];
	//cout<<"INDEX "<<index<<endl;
	if(index == NOREG_DVSIM){
		return string("NOREG");
	}

	if(index >= write_reg_start_index)
		return index_to_regname.at(index-write_reg_start_index);
	else
		return index_to_regname.at(index);
}

bool Register::isnoreg(int index){
	if(index == NOREG_DVSIM){
		return true;
	}else{
		return false;
	}
	return false;
}
//int read =1  float read =2 int write =3 float write =4
int Register::getRegType(int index) {
	//return regname[index];
	//cout<<"INDEX "<<index<<endl;
	if(index == NOREG_DVSIM){
		return 0; //noreg
	}

	if(index >= write_reg_start_index){
		if (index - write_reg_start_index < 32){
			return 3;
		}else{
			return 4;
		}
	}else{
		//return 1; //int
		if(index < 32){
			return 1;
		}else{
			return 2;
		}
	}
	return 0;
}

