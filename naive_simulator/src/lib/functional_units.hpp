#ifndef FUNCTIONAL_UNITS_HPP
#define FUNCTIONAL_UNITS_HPP

#include <stdlib.h>
#include <map>
#include <vector>

#include "globals.hpp"

typedef enum {alu,simdu,mulu,memu, MAX_FU_TYPES} futype;

/************************************************
   OoO architecture considers ports for dispatching instructions
   flex    support general operations, with the following restrictions:
   mul_gen can only exe MUL/DIV based ops AND flex 
   br_gen  can only exe branch instructions AND flex
   Port_mem only supports MEM instructions
************************************************/
typedef enum {mul_gen, br_gen, flex, port_mem, MAX_PORTS} fu_ports; //port_mem,
typedef enum {special, unit1, unit2, mem_unit, MAX_UNITS_PORT} fu_units;


//Number of instructions of this fu type which can be combined and executed
static int fu_index_counter = 0;

struct FunctionalUnits{

		DVI_Load_Wait dvi_load_wait;
		std::map<sa_uoptype, futype> fu_mapping;
		std::map<sa_uoptype, futype> noncombined_mapping;
		int fu_count[MAX_UNITS_PORT];
		int fp_count[MAX_UNITS_PORT];
		bool blocked_port[MAX_UNITS_PORT];
		bool *blocked_fu[MAX_UNITS_PORT];
		int *blocked_cycles[MAX_UNITS_PORT];
		bool *nonpipelined_fu[MAX_UNITS_PORT];

		int *blocked_count[MAX_UNITS_PORT];
		int *max_blocked_count[MAX_UNITS_PORT];

		int maxfu[MAX_UNITS_PORT];

		typedef std::bitset<64> Bitset;
		Bitset uop_in_vector_fu;
		int fu_index;

		int num_threads;

		int max_fp;

		int *combined_count[MAX_UNITS_PORT];
		bool *dynvec[MAX_UNITS_PORT];
		ADDRINT *eip[MAX_UNITS_PORT];
		UINT *tid[MAX_UNITS_PORT];
		Config conf;

		std::vector<FU_DVInst> *fu_dvinst[MAX_UNITS_PORT];
#ifdef FREE_PHI
		std::vector<FU_DVInst> phi_FU;
#endif

		std::vector<FU_DVInst> wait_on_load_buffer;

		FunctionalUnits(){};
		FunctionalUnits(Config config){
			fu_index = (fu_index_counter*1000);
			fu_index_counter++;

			num_threads = config.num_threads;
			conf = config;
			
			maxfu[mul_gen] = 3; //Mul/div and general operation port
			maxfu[br_gen] = 3; //Branch port and general operations
			maxfu[flex] = 3; //flexible port - can be assigned to any general operation INT/FP, except branch, mul/div and MEM
			maxfu[port_mem] = MAX_UNITS_PORT; //memory port

			for(int i = 0; i < MAX_UNITS_PORT; i++){//MAX_FU_TYPES; i++){
				fu_count[i] = 0;
				fp_count[i] = 0;
			
				blocked_port[i] = false;
				assert(maxfu[i] >= 0);
				blocked_fu[i] = (bool*)malloc(sizeof(bool)*maxfu[i]);
				blocked_cycles[i] = (int*)malloc(sizeof(int)*maxfu[i]);
				nonpipelined_fu[i] = (bool*)malloc(sizeof(bool)*maxfu[i]);
			
				blocked_count[i] = (int*)malloc(sizeof(int)*maxfu[i]);
				max_blocked_count[i] = (int*)malloc(sizeof(int)*maxfu[i]);
				combined_count[i] = (int*)malloc(sizeof(int)*maxfu[i]);

				dynvec[i] = (bool*)malloc(sizeof(bool)*maxfu[i]);
				
				eip[i] = (ADDRINT*)malloc(sizeof(ADDRINT)*maxfu[i]);
				tid[i] = (UINT*)malloc(sizeof(UINT)*maxfu[i]);

#ifdef PIPELINED_FU
				fu_dvinst[i] = new std::vector<FU_DVInst>[maxfu[i]]();
#else
				fu_dvinst[i] = new FU_DVInst[maxfu[i]]();
#endif
				for(int id = 0 ;id < maxfu[i];id++){ //reset all values
					blocked_fu[i][id] = false;
					blocked_cycles[i][id] = 0;
					nonpipelined_fu[i][id] = false;

					blocked_count[i][id] = 0;
					max_blocked_count[i][id] = 0;
					combined_count[i][id] = 0;
					dynvec[i][id] = false;
					eip[i][id] = 0UL;
					tid[i][id] = 300;
					DVInst inst;
				}
			}

			uop_in_vector_fu.reset();

		}


		void insertoperands(int idx, ADDRINT op[MAX_OPERANDS][MAX_QWORD_SIZE], bool* valid, int *sz);
		void resetoperands(int idx);
		LONG getSameOpCount();
		bool addref(sa_uoptype uop, int idx);
		bool isdynvec(int idx);
		bool isvector(sa_uoptype uop, bool op_iswrite[], bool op_isint[]);
		bool isfp1(sa_uoptype uop);
		bool getfu(FU_DVInst dvi, sa_uoptype dv_uop ,int fuid, int rob_id);
		bool fu_available();

		void execute(ROB &reorderB, LONG &dv_ins_count, LONG &ins_count, int threads_per_warp);
		void execute_inst_with_loads(ROB &reorderB);
		void clear_fu();

		void remove_load_wait(LONG id);

		bool setup_execution(FU_DVInst dvi, int port, bool pipelined);
		bool flex_execution(FU_DVInst dvi);

		bool schedule_floating_type(std::vector<FU_DVInst>::iterator it);
		bool schedule_int_type(std::vector<FU_DVInst>::iterator it);
		bool setup_muldiv_fp_avxsse(FU_DVInst dvi, sa_uoptype dv_uop, int &fuid);
		bool setup_simd_fp(FU_DVInst dvi, sa_uoptype dv_uop, int &fuid);
		bool setup_muldiv_int_avxsse(FU_DVInst dvi, sa_uoptype dv_uop, int &fuid);
		bool setup_int_simd_alu(FU_DVInst dvi, sa_uoptype dv_uop, int &fuid);
		bool setup_memory(FU_DVInst dvi);
		void update(LONG robid);
		bool is_in_ROB(int robid, ROB reorderB);

#ifdef FREE_PHI
		bool phi_execution(FU_DVInst dvi);
#endif

		~FunctionalUnits(){

		}
	};
			/*if(config.base){ //REDO
				maxfu[alu] = 2;
				maxfu[mulu] = 1;
				maxfu[simdu] = 1;
			}else{
				maxfu[alu] = 2; // These are replicated alus
				maxfu[mulu] = 1; //muldiv unit is also a simdu
				maxfu[simdu] = 1;
			}*/




#endif
