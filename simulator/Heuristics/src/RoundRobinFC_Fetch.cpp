#include "SimpleHeuristic.hpp"
#include <bitset>

void Sync::choose_threads()
{
	// Round robin among active threads
	static int rrcounter = 0;

	//RR GROUP
	static std::bitset<16> fetched_threads;
	//END
	int ref = -1;

	for (UINT ii=0; ii<num_threads; ii++){
		unsigned int i = (ii + rrcounter) % num_threads;
		/*if(t[i].active_last_fetch){
			continue;
		}*/
		//RR GROUP
		if(fetched_threads.test(i)){
			continue;
		}
		//END
		if (!t[i].fetch_qfull && !t[i].fetch_stall && !t[i].ignore &&  !t[i].fetch_qempty && !t[i].primary_heur_fetch){
			ref = i;
			break;
		}
	}
	if(ref == -1){
		for (UINT ii=0; ii<num_threads; ii++){
			unsigned int i = (ii + rrcounter) % num_threads;
			/*if(t[i].active_last_fetch){
				continue;
			}*/

			//END
			if (!t[i].fetch_qfull && !t[i].fetch_stall && !t[i].ignore &&  !t[i].fetch_qempty && !t[i].primary_heur_fetch){
				ref = i;
				break;
			}
		}

	}

	if(ref == -1){
		for (UINT ii=0; ii<num_threads; ii++){
			unsigned int i = (ii + rrcounter) % num_threads;
			if (!t[i].fetch_qfull && !t[i].fetch_stall && !t[i].ignore &&  !t[i].fetch_qempty){
				ref = i;
				break;
			}
		}
		if(ref == -1){
			for (UINT i=0; i<num_threads; i++){
				t[i].fetch_isactive = false;
			}
			return;
		}
	}
	//assert(ref>=0);
	//bool isact = false;
	for (UINT i=0; i<num_threads; i++){
		t[i].fetch_isactive =  !t[i].fetch_qempty && !t[i].fetch_qfull && t[i].fetch_pc == t[ref].fetch_pc && !t[i].fetch_stall && !t[i].ignore;
		/*if(t[i].fetch_isactive && !t[i].fetch_stall && !t[i].ignore){
			isact = true;
		}*/
		//t[i].is_active = (i == ref);    // No fetch combining
		//RR GROUP
		if(t[i].fetch_isactive){
			fetched_threads.set(i);
		}
		//END
	}

	//RR GROUP
	bool rem = false;

	for (UINT i=0; i<num_threads; i++){
		if(!fetched_threads.test(i) && (!t[i].fetch_qfull && !t[i].fetch_stall && !t[i].ignore &&  !t[i].fetch_qempty)){
			rem = true;
			break;
		}
	}
	if(!rem){
		fetched_threads.reset();
	}
	//END

	/*bool problem = true;
	for (UINT i=0; i<num_threads; i++){
		if(t[i].is_active){
			problem = false;
		}
	}
	if (problem){
		for (UINT i=0; i<num_threads; i++){
			cout<<"Before Thread id "<<i<<" ";
			printf(" %.16lx \n",t[i].pc );
			if(t[i].ignore){
				cout<<" IG ";
			}else{
				cout<<" NIG ";
			}
			if(t[i].barrier){
				cout<<" BA ";
			}
			else{
				cout<<" NBA ";
			}
			if(t[i].is_finished){
				cout<<" FIN ";
			}else{
				cout<<" NFIN ";
			}
			cout<<endl;
		}
	}*/
	// Next
	rrcounter = (ref + 1) % num_threads;
	/*if(!isact){
		t[rrcounter].fetch_isactive = true;
	}*/
}

bool Sync::lockstep(){
	return true;
}
