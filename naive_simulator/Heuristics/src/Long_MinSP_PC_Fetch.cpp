#include "Heuristic.hpp"
#include <stdio.h>

class Sync : public Heuristic {
	UINT size_FHB;
	ADDRINT** table_FHB;
	UINT* last_pos;
	short ref;

public:
	Sync(UINT nt, Thread* t, const char* param);
	~Sync();

	void choose_threads();
	virtual  bool lockstep(){
		return true;
	}
	int getCurrentWarp(){
		return -1;
	}
	int thread_schedule(LONG current_cycle){}
};

Heuristic* new_heuristic(UINT nt, Thread* t, const char* param){
	return new Sync(nt, t, param);
}
void delete_heuristic(Heuristic* h){
	delete h;
}


Sync::Sync(UINT nt, Thread* t, const char* param)
: Heuristic(nt, t)
{
	sscanf(param, "%d", &size_FHB);
	ref = -1;

	//Criando a FHB de cada thread
	table_FHB = new ADDRINT*[num_threads];
	last_pos = new UINT[num_threads];
	for (UINT i=0; i<num_threads; i++){
		last_pos[i] = 0;
		table_FHB[i] = new ADDRINT[size_FHB];
		for (UINT j=0; j<size_FHB; j++){
			table_FHB[i][j] = 0;
		}
	}

}

Sync::~Sync(){
	for (UINT i=0; i<num_threads; i++){
		delete [] table_FHB[i];
	}
	delete [] table_FHB;
	delete [] last_pos;
}


void Sync::choose_threads(){
	if ( ref != -1 && !(t[ref].fetch_basic_block || t[ref].fetch_bb_priority > 0) && !t[ref].fetch_qfull &&  !t[ref].fetch_stall && !t[ref].ignore){
		for (UINT i=0; i<num_threads; i++){
			t[i].fetch_isactive = !t[i].fetch_qfull && t[i].fetch_pc == t[ref].fetch_pc &&  !t[ref].fetch_stall && !t[ref].ignore;
		}
		return;
	}

	//Procurar o PC de cada thread na FHB das outras e calcular a prioridade.
	UINT prioridade[num_threads];
	for (UINT k=0; k<num_threads; k++){//Para cada thread
		prioridade[k] = 0; //A prioridade de cada thread.
		for (UINT i=0; i<num_threads; i++){ //Para cada uma das demais threads
			if (i != k){
				for (UINT j=0; j<size_FHB; j++){ //Para cada posição da FHB de outra thread
					if (t[k].fetch_pc == table_FHB[i][j]){ //O PC está na tabela
						prioridade[k]++;
						break;
					}
				}
			}
		}
	}

	//Escolher thread de maior prioridade
	int primeiro = -1;
	for (UINT i=0; i<num_threads; i++){
		if (!t[i].fetch_qfull){
			if (primeiro == -1 || prioridade[i] > prioridade[primeiro]){
				primeiro = i;
			}
		}
	}


	bool para_executar[num_threads];

	//Ver quais threads tem a mesma prioridade da maior, e colocar para executar cada uma delas por vez.
	for (UINT i=0; i<num_threads; i++){
		para_executar[i] = false;
		if (!t[i].fetch_qfull){
			if (prioridade[i] == prioridade[primeiro]){
				para_executar[i] = true;
			}
		}
	}


	//Escolhendo thread de maior prioridade segundo sub-heurística
	ref = -1;
	for (UINT i=0; i<num_threads; i++){
		if (para_executar[i]){
			if ( ref == -1 || t[i].fetch_sp < t[ref].fetch_sp || (t[i].fetch_sp == t[ref].fetch_sp && t[i].fetch_pc < t[ref].fetch_pc ) ){
				ref = i;
			}
		}
	}

	if (ref == -1){
		throw Exception("Error in heuristic code: No thread was chosen to execute.");
	}

	//Marcando threads em execução
	for (UINT i=0; i<num_threads; i++){
		t[i].fetch_isactive = !t[i].fetch_qfull && t[i].fetch_pc ==  t[ref].fetch_pc && !t[ref].fetch_stall && !t[ref].ignore;

		if (t[i].fetch_isactive){

			//Adicionando instrução na tabela FHB
			table_FHB[i][ last_pos[i] ] = t[i].fetch_pc;
			last_pos[i]++;
			last_pos[i] %= size_FHB;
		}
	}

}
