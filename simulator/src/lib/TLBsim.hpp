/*
 * TLBsim.hpp
 *
 *  Created on: Sep 15, 2014
 *      Author: skalathi
 */

#include <stdio.h>
#include <iostream>
#include "../Types.hpp"
#include <map>
#include <unordered_map>
#include <list>
#include <assert.h>
//#include "globals.hpp"
#ifndef TLBSIM_HPP_
#define TLBSIM_HPP_

using namespace std;
#define CACHE_LINE_SZ 64

template <class KEY_T, class VAL_T>

class LRU_T{
private:
	list< pair<KEY_T,VAL_T> > item_list;
	unordered_map<KEY_T, decltype(item_list.begin()) > item_map;
	size_t cache_size;
private:
	void clean(void){
		while(item_map.size()>cache_size){
			auto last_it = item_list.end(); last_it --;
			item_map.erase(last_it->first);
			item_list.pop_back();
		}
	};
public:
	LRU_T(int cache_size_):cache_size(cache_size_){
		;
	};

	LRU_T(){};

	void put(const KEY_T &key, const VAL_T &val){
		auto it = item_map.find(key);
		if(it != item_map.end()){
			item_list.erase(it->second);
			item_map.erase(it);
		}
		item_list.push_front(make_pair(key,val));
		item_map.insert(make_pair(key, item_list.begin()));
		clean();
	};
	bool exist(const KEY_T &key){
		return (item_map.count(key)>0);
	};
	VAL_T get(const KEY_T &key){
		assert(exist(key));
		auto it = item_map.find(key);
		item_list.splice(item_list.begin(), item_list, it->second);
		return it->second->second;
	};

};


class TLB{
private:
	//int num_threads;
	int tlb_size;
	//bool tlb_split;
	int page_size;
	int num_bits;
	unsigned long long *tlbarray;
	unsigned long *update_time;
	unsigned long count;

	unsigned long tlb_hit;
	unsigned long tlb_miss;
	unsigned long access_count;
	LRU_T<LONG,LONG> tlb_lru;

public:
	TLB(){};
	void init(int size, int pg_sz){
		tlb_size = size;
		//tlb_split = split;
		//num_threads = nthreads;
		page_size = pg_sz;
		int pg_temp = page_size;
		num_bits = 0;
		count = 0UL;
		while(pg_temp > 0){
			num_bits++;
			pg_temp=pg_temp>>1;
		}
		num_bits--;
		//std::cout<<"Number of bits "<<num_bits<<std::endl;
		/*if(split){
			tlb_hit =(unsigned long*)malloc((num_threads)* sizeof(unsigned long));
			tlb_miss =(unsigned long*)malloc((num_threads)* sizeof(unsigned long));
			tlbarray = (unsigned long long **)malloc(num_threads * sizeof(unsigned long long*));
			update_time = (unsigned long **)malloc(num_threads * sizeof(unsigned long*));

			for(int i = 0; i< num_threads;i++){
				tlbarray[i] = (unsigned long long *)malloc(size * sizeof(unsigned long long));
				update_time[i] = (unsigned long *)malloc(size * sizeof(unsigned long));
				tlb_hit[i] = 0Ul;
				tlb_miss[i]= 0Ul;
			}
			for(int i = 0; i< num_threads;i++){
				for(int j = 0;j<size;j++){
					tlbarray[i][j] = 0ULL;
					update_time[i][j] = 0UL;
				}
			}
		}else{
			tlb_hit =(unsigned long*)malloc((1)* sizeof(unsigned long));
			tlb_miss =(unsigned long*)malloc((1)* sizeof(unsigned long));
			tlb_hit[0] = 0Ul;
			tlb_miss[0]= 0Ul;
			tlbarray = (unsigned long long **)malloc(1 * sizeof(unsigned long long*));
			update_time = (unsigned long **)malloc(1 * sizeof(unsigned long*));
			tlbarray[0] = (unsigned long long *)malloc(size * sizeof(unsigned long long));
			update_time[0] = (unsigned long *)malloc(size * sizeof(unsigned long));
			for(int j = 0;j<size;j++){
				tlbarray[0][j] = 0ULL;
				update_time[0][j] = 0UL;
			}
		}*/

//		tlb_hit =(unsigned long*)malloc(sizeof(unsigned long));
	//	tlb_miss =(unsigned long*)malloc(sizeof(unsigned long));
		tlb_hit = 0Ul;
		tlb_miss= 0Ul;
		access_count = 0UL;
		tlb_lru=LRU_T<LONG,LONG>(size);

		/*tlbarray = (unsigned long long *)malloc(size*sizeof(unsigned long long));
		update_time = (unsigned long *)malloc(size*sizeof(unsigned long));

		for(int j = 0;j<size;j++){
			tlbarray[j] = 0ULL;
			update_time[j] = 0UL;
		}*/
	}

	bool tlb_access(LONG address);
	unsigned long get_tlb_hit(){
		return tlb_hit;
	}
	unsigned long get_tlb_miss(){
		return tlb_miss;
	}
	unsigned long get_access_count(){
		return access_count;
	}
};


#endif /* TLBSIM_HPP_ */
