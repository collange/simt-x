#include "sasim_tlb_translation.hpp"
// The following files must be included here and not in sasim_tlb_translation.hpp due to declaration issues
#include "sasim_engine.hpp"	

// ***************************************************************************
// Begin: sasim_translation class

/*!
 * Computes the Address Line given a Virtual Page Number
 * 
 * @param vpage Virtual Page Number
 * @return Line Address
 */ 
sa_addr
get_pte_lineaddr(sa_addr vpage)
{
  sa_addr lineaddr = (PAGETABLE_ADDR + vpage * PTESIZE_BYTES) / CACHELINE_BYTES;
  return lineaddr;
}

void 
sasim_translation::init()
{
  count = 0;
  ptzhit = 0;
}

/*!
 * Returns the physical page associated to the virtual page (translation) specified
 * If the virtual page does not exist, a new page is granted for the virtual page
 * 
 * @param vpage Virtual Page Number
 * @return Physical page
 */ 
sa_addr 
sasim_translation::getpage(sa_addr vpage)
{
  map<sa_addr,sa_addr>::iterator it = trans.find(vpage);
  if (it != trans.end()) {
    return trans[vpage];
  }
  sa_addr newpage = count;
  trans[vpage] = newpage;
  count++;
  return newpage;
}

#ifdef STRIDE_PREFETCH
/*! 
 * Returns true if the respective virtual page number exists in the translator buffer and
 * set the parameter vpage to the value provided by the translator
 * 
 * @param vpage Virtual Page Number
 * @return Boolean value.
 */ 
bool
sasim_translation::lookup(sa_addr & vpage)
{
  map<sa_addr,sa_addr>::iterator it = trans.find(vpage);
  if (it != trans.end()) {
    vpage = trans[vpage];
    return true;
  }
  return false;
}
#endif

/*! 
 * Checks if the line address crosses the page table zone
 * 
 * @param lineaddr Line Address
 * @return void. This function does not return any data.
 */ 
void 
sasim_translation::check_pagetablezone(sa_addr lineaddr)
{
  sa_addr min_lineaddr = get_pte_lineaddr(0);
  sa_addr max_lineaddr = get_pte_lineaddr(count);
  if ((lineaddr>=min_lineaddr) && (lineaddr<max_lineaddr)) {
    ptzhit++;
    cerr << "memory accesses interfere with page table zone, change PAGETABLE_ADDR" << endl;
    SASSERT(0);
  }
}

// End: sasim_translation class
// ***************************************************************************

// ***************************************************************************
// Begin: sasim_tlb class

/*!
 * Initializes the different translation lookaside tables (ITLB1, DTLB1, TLB2) and the queue
 * representing the TLB Misses.
 * 
 * @return void. This function does not return any data.
 */ 
void 
sasim_tlb::init_tlb(sasim * proc)
{
  core = proc;
  itlb1.init(ITLB1_ENTRIES,1,ITLB1_ASSO,ITLB1_POL);
  dtlb1.init(DTLB1_ENTRIES,1,DTLB1_ASSO,DTLB1_POL);
  tlb2.init(TLB2_ENTRIES,1,TLB2_ASSO,TLB2_POL);
  tlbmq.init(1+MAX_DTLB_MISS,TLB2_LAT_CYCLES);
  mq.init(1+MAX_DTLB_MISS,TLB2_LAT_CYCLES);
  fq.init(1+MAX_DTLB_MISS);
  nacc = 0; // TLB2 accesses
  nmiss = 0;  // TLB2 misses
  name = "tlb";
  dtlb_miss = 0;
  itlb_miss = 0;
  itlbmiss_pending = false;
  itlbmiss_vpage = 0;
}

/*!
 * Flushes the different queues (miss queue, fill queue, etc.)
 * 
 * @return void. This function does not return any data.
 */ 
void 
sasim_tlb::flush()
{
  dtlbmiss_vpages.clear();
  tlbmq.flush();
  mq.flush();
  fq.flush();
  itlbmiss_pending = false;
}

void 
sasim_tlb::reschedule()
{
  for (int i=0; i<NUM_LDQ; i++) {
    if (core->ldq[i].empty()) {
      continue;
    }
    core->ldq[i].scan_forward();
    while (uop_ptr * p = core->ldq[i].read()) {
      uop_ptr uptr = *p;
      SASSERT(uptr);
      SASSERT(! uptr->executed);
      uptr->reschedule(core->cycle);
    }
  }
  core->prstq.scan_forward();
  while (uop_ptr * p = core->prstq.read()) {
    uop_ptr uptr = *p;
    SASSERT(uptr);
    if (! uptr->executed) {
      uptr->reschedule(core->cycle);
    }
  }
  core->dl1.flush();
}

bool
sasim_tlb::pendingmiss(sa_addr lineaddr, sa_addr & vpage)
{
  for (set<sa_addr>::iterator it = dtlbmiss_vpages.begin(); it != dtlbmiss_vpages.end(); it++) {
    if (lineaddr == get_pte_lineaddr(*it)) {
      vpage = *it;
      return true;
    }
  }
  return false;
}

bool
sasim_tlb::pendingmiss(sa_addr vpage)
{
  set<sa_addr>::iterator it = dtlbmiss_vpages.find(vpage);
  return (it !=  dtlbmiss_vpages.end());
}

void
sasim_tlb::dtranslate(uop_ptr uptr, int64_t cycle)
{
  SASSERT(! uptr->memtranslated);
#ifdef MAGIC_DTLB
  uptr->memtranslated = true;
  return;
#endif
  uptr->memtranslated = dtlb1.line_hit(uptr->vpage,true);
  if (uptr->memtranslated) {
    return;
  }
  // DTLB1 miss
  if (dtlbmiss_vpages.size() >= MAX_DTLB_MISS) {
    return;
  }
  dtlb_miss++;
  if (! pendingmiss(uptr->vpage)) {
    dtlbmiss_vpages.insert(uptr->vpage);
    // access TLB2
    SASSERT(!tlbmq.search(uptr->vpage));
    tlbmq.enqueue(sa_req(uptr->vpage,SA_REQ_DTLB),cycle);
  }
}

bool
sasim_tlb::itranslate(sa_addr vpage, int64_t cycle)
{
  if (itlbmiss_pending) {
    return false;
  }
  bool hit = itlb1.line_hit(vpage,true);
  if (hit) {
    return true;
  }
  // ITLB1 miss
  itlb_miss++;
  itlbmiss_pending = true;
  itlbmiss_vpage = vpage;
  // access TLB2
  SASSERT(!tlbmq.search(vpage));
  tlbmq.enqueue(sa_req(vpage,SA_REQ_ITLB),cycle);
  return false;
}

void 
sasim_tlb::tlb_miss_finished(sa_req req)
{
  sa_addr vpage = req.lineaddr; // actually a vpage num
  if (req.reqtype == SA_REQ_DTLB) {
    if (pendingmiss(vpage)) {
      dtlb1.insert(vpage);
      dtlbmiss_vpages.erase(vpage);
      SASSERT(!tlbmq.search(vpage));
      reschedule();
    }
  } else {
    SASSERT(req.reqtype == SA_REQ_ITLB);
    if (itlbmiss_pending && (itlbmiss_vpage == vpage)) {
      itlb1.insert(vpage);
      itlbmiss_pending = false;
      SASSERT(!tlbmq.search(vpage));
    }
  }
}

void 
sasim_tlb::tlb2_miss_finished(sa_req req)
{
  // NB: some data and inst PTEs can be mixed in the same cache line
  sa_addr vpage;
  bool match = false;
  while (pendingmiss(req.lineaddr,vpage)) {
    match = true;
    tlb2.insert(vpage);
    dtlb1.insert(vpage);
    dtlbmiss_vpages.erase(vpage);
    tlbmq.search_delete(vpage);
  }
  if (match) {
    reschedule();
  }
  if (itlbmiss_pending && (req.lineaddr == get_pte_lineaddr(itlbmiss_vpage))) {
    tlb2.insert(itlbmiss_vpage);
    itlb1.insert(itlbmiss_vpage);
    itlbmiss_pending = false;
    tlbmq.search_delete(itlbmiss_vpage);
  }
}

void 
sasim_tlb::onecycle(int64_t cycle)
{
  // fill TLB2 and TLB1
  if (fq.ready(cycle)) {
    sa_req req = fq.dequeue();
    tlb2_miss_finished(req);
  }

  // access TLB2
  if (tlbmq.ready(cycle)) {
    sa_req req = tlbmq.dequeue();
    sa_addr vpage = req.lineaddr; // actually a page num
    bool hit = tlb2.line_hit(vpage,true);
    nacc++;
    if (hit) {
      // TLB2 HIT
      tlb_miss_finished(req);
    } else {
      // TLB2 MISS, will access the L2 cache
      sa_addr pte_lineaddr = get_pte_lineaddr(vpage);
      mq.enqueue(sa_req(pte_lineaddr,req.reqtype),cycle);
      nmiss++;
    }
  }
}

void 
sasim_tlb::printstats(FILE * stream)
{
  fprintf(stream,"%s dtlb miss = %lld\n",name.c_str(),(long long) dtlb_miss);
  fprintf(stream,"%s itlb miss = %lld\n",name.c_str(),(long long) itlb_miss);
  fprintf(stream,"%s tlb2 acc = %lld\n",name.c_str(),(long long) nacc);
  fprintf(stream,"%s tlb2 miss = %lld\n",name.c_str(),(long long) nmiss);
}

void
sasim_tlb::debugprint_queues()
{
  cerr << name << endl;
  cerr << "pending itlb miss : " << itlbmiss_pending << endl;
  cerr << "pending dtlb misses : " << dtlbmiss_vpages.size() << endl;
  DBPRINTQ(tlbmq);
  DBPRINTQ(mq);
  DBPRINTQ(fq);
}

// End: sasim_tlb class
// ***************************************************************************
