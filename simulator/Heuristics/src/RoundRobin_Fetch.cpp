#include "Heuristic.hpp"


class Sync : public Heuristic {
	//ADDRINT pc;
	//ADDRINT sp;
	//std::vector< int >  mask;
	//int invalid;
	int cwid;
	// Round robin among active threads

	//int threads_in_warp;


public:
	Sync(UINT nt,Thread* t, const char* param);
	~Sync();

	virtual int getCurrentWarp(){
		return cwid;
	}
	void choose_threads();
	int thread_schedule(LONG current_cycle);
	virtual  bool lockstep(){
		return true;
	}
};

Heuristic* new_heuristic(UINT nt, UINT nw, Thread* t, const char* param){
	return new Sync(nt,t, param);
}
void delete_heuristic(Heuristic* h){
	delete h;
}

Sync::Sync(UINT nt,Thread* t, const char* param)
: Heuristic(nt,t)
{
	cwid = 0;
	//current_warp = 0;
	//threads_in_warp = (int)num_threads/num_warps;
	//for( uint j = 0 ; j < num_threads ; j++){
	//	invalid = true;
	//	pc = 0ULL;
	//	sp = 0ULL;
	//	cwid = -1;
	//	for (UINT i=0; i<num_threads; i++){
	//		mask.push_back(0);
	//	}
	//}
}

Sync::~Sync(){

}

void Sync::choose_threads()
{

}

int Sync::thread_schedule(LONG current_cycle)
{
	// Round robin among active threads
	static int rrcounter = 0;

	int count = 0;

	int ref = -1;
	for (UINT ii=0; ii<num_threads; ii++){
		unsigned int i = (ii + rrcounter) % num_threads;
		if (!t[i].ignore && !t[i].fetch_qfull && !t[i].fetch_stall && !t[i].fetch_qempty && !t[i].iq_full){
			ref = i;
			cwid = t[i].warpid;
			break;
		}
	}

	if(ref == -1){
		for (UINT i=0; i<num_threads; i++){
			t[i].fetch_isactive = false;
		}
		return -1;
	}

	for (UINT i=0; i<num_threads; i++){
		//t[i].is_active = !t[i].ignore && t[i].pc == t[ref].pc;
		t[i].fetch_isactive = (i == (uint)ref);    // No fetch combining
	}
	/*bool problem = true;
	for (UINT i=0; i<num_threads; i++){
		if(t[i].is_active){
			problem = false;
		}
	}
	if (problem){
		for (UINT i=0; i<num_threads; i++){
			cout<<"Before Thread id "<<i<<" ";
			printf(" %.16lx \n",t[i].pc );
			if(t[i].ignore){
				cout<<" IG ";
			}else{
				cout<<" NIG ";
			}
			if(t[i].barrier){
				cout<<" BA ";
			}
			else{
				cout<<" NBA ";
			}
			if(t[i].is_finished){
				cout<<" FIN ";
			}else{
				cout<<" NFIN ";
			}
			cout<<endl;
		}
	}*/
	// Next
	rrcounter = (ref + 1) % num_threads;
	return cwid;

}


