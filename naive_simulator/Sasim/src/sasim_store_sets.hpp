#ifndef SASIM_STORE_SETS_HPP
#define SASIM_STORE_SETS_HPP

#include "sasim_defs.hpp"

class ssit_entry {
 public:
  int ssid;
  bool valid;
};

class lfst_entry {
 public:
  bool valid;
  int ssid;
  int64_t num;
};

class store_sets {

 public:
  int CLEARPERIOD;
  int SSIT_SIZE;
  int LFST_SIZE;
  ssit_entry * ssit;
  lfst_entry * lfst;
  int64_t clear_cycle;

  store_sets();
  void clear_ssit();
  void clear_lfst();
  void init(int _ssit_size, int _lfst_size, int _clearperiod);
  ~store_sets();
  int ssit_ldindex(sa_addr pc);
  int ssit_stindex(sa_addr pc);
  int get_ssid(sa_addr pc);
  void periodic_clearing(int64_t cycle);
  void record_dep(sa_addr store_pc, sa_addr pc, bool twostores = false);
  void process_store(sa_addr store_pc, int64_t num);
  int search_lfst(int ssid);
  int64_t get_memdep(sa_addr load_pc);
  void clear_deps(sa_addr load_pc);
};

#endif // End ifndef SASIM_STORE_SETS_HPP