#ifndef SASIM_BRANCHPRED_HPP
#define SASIM_BRANCHPRED_HPP

#include "sasim_defs.hpp"
#include "sasim_uarch_defs.hpp"

#define SASIM_MAXPH 512

class path_history {
public:
  int ptr; 
  unsigned h[SASIM_MAXPH];
  bool valid[SASIM_MAXPH];

  path_history() ;
  void insert(unsigned val);
  unsigned & operator [] (int n);
  int checkpoint();
  void repair(int checkpoint);
};

class compressed_history {
public:
  unsigned comp;
  int clength;
  int olength;
  int nbits; 
  int outpoint;
  unsigned mask1;
  unsigned mask2;

  void init(int original_length, int compressed_length, int injected_bits);
  void rotateleft(unsigned & x, int m);
  void update(path_history & h);
  compressed_history & operator = (const compressed_history & c);
};

class tagehist {
 public:
  int n;
  compressed_history chg[MAX_NUMG];
  compressed_history cht[MAX_NUMG];
  compressed_history chtt[MAX_NUMG];

  void init(int numg, int minhist, double alpha, int logg, int tagbits, int pathbits) ;
  tagehist & operator = (const tagehist & c);
  void update(path_history & ph);
};

class sa_inst;
class sasim_inst;
typedef sasim_inst * inst_ptr;

class sasim_bpred {
 public:
  int64_t ninst;
  int64_t nbranch;
  int64_t ncond;
  int64_t nret;
  int64_t nind;
  int64_t nmisp;
  int64_t nmispcond;
  int64_t nmispind;

  virtual void init() {};
  virtual bool predict(inst_ptr iptr, sa_addr next_pc) {return true;}; 
  virtual void update(inst_ptr iptr, sa_addr next_pc) {};
  virtual void updatehist(inst_ptr iptr, sa_addr next_pc) {};

  void reset_stats();
  void update_stats(sa_inst & inst);
  void printstats(FILE * stream, const char * predname);
  void updctr(int & ctr, bool taken, int ctrbits, bool signedctr = true);
};

class ittage_entry {
 public:
  sa_addr target;
  int hyst;
  int tag;
  int u;

  ittage_entry();
};

class bpred_ittage : public sasim_bpred {
  static const int ubits = 2;
  static const int pwinbits = 4;
  static const int pathbits = 5;
  static const int bhystbits = 2;
  static const int ghystbits = 2;

 public:
  int numg;
  int bsize;
  int gsize;
  int minhist;
  double alpha;
  int tagbits; 
  int logg;
  ittage_entry * b;
  ittage_entry ** g;
  path_history ph;
  tagehist ch; 
  int bi;
  int * gi;
  int bankhit;
  sa_addr pred;
  sa_addr altpred;
  bool pseudonewalloc;
  int pwin;
  int seed;
  int tick;

  bpred_ittage(int numg, int bsize, int gsize, int minhist, double alpha, int tagbits);
  void init() ;
  int bindex(sa_addr pc);
  int gindex(sa_addr pc, tagehist & ch, int bank);
  int gtag(sa_addr pc, tagehist & ch, int bank);
  sa_addr get_prediction(inst_ptr iptr);
  bool predict(inst_ptr iptr, sa_addr next_pc);
  int rando();
  void update(inst_ptr iptr, sa_addr next_pc);
  void updatehist(inst_ptr iptr, sa_addr next_pc);
};

class bpred_tage : public sasim_bpred {
  static const int ubits = 2;
  static const int pwinbits = 4;
  static const int bctrbits = 2;
  static const int gctrbits = 3;
  static const int pathbits = 5;

 public:
  int numg;
  int bsize;
  int gsize;
  int minhist;
  double alpha;
  int tagbits; 
  int logg;
  int * b;
  int ** g;
  int ** t;
  int ** u;
  path_history ph;
  tagehist ch; 
  int bi;
  int * gi;
  int bankhit;
  bool predtaken;
  bool altpredtaken;
  bool pseudonewalloc;
  int pwin;
  int seed;
  int tick;

  bpred_tage(int numg, int bsize, int gsize, int minhist, double alpha, int tagbits);
  void init() ;
  int bindex(sa_addr pc);
  int gindex(sa_addr pc, tagehist & ch, int bank);
  int gtag(sa_addr pc, tagehist & ch, int bank);
  bool get_prediction(inst_ptr iptr);
  bool predict(inst_ptr iptr, sa_addr next_pc);
  int rando();
  void update(inst_ptr iptr, sa_addr next_pc);
  void updatehist(inst_ptr iptr, sa_addr next_pc);
};

class bpred_full : public sasim_bpred {
  static const int tage_numg = 4;
  static const int tage_minhist = 5;
  static const double tage_alpha = 2.5;
  static const int tage_tagbits = 9;

  static const int ittage_numg = 4;
  static const int ittage_minhist = 5;
  static const double ittage_alpha = 2.5;
  static const int ittage_tagbits = 10;

 public:
  bpred_tage tage;
  bpred_ittage ittage;
  path_history ph;

  bpred_full(int tage_bsize, int tage_gsize, int ittage_bsize, int ittage_gsize);
  void init();
  bool predict(inst_ptr iptr, sa_addr next_pc);
  void update(inst_ptr iptr, sa_addr next_pc);
  void updatehist(inst_ptr iptr, sa_addr next_pc);
  void checkpoint(inst_ptr iptr);
  void repair(inst_ptr iptr, sa_addr next_pc);
};

#endif // End ifndef SASIM_BRANCHPRED_HPP
