#include "Heuristic.hpp"

class Sync : public Heuristic {
	//ADDRINT pc;
	//ADDRINT sp;
	//std::vector< int >  mask;
	//int invalid;
	int cwid;
	// Round robin among active threads

	int threads_in_warp;


public:
	Sync(UINT nt, UINT nw, Thread* t, const char* param);
	~Sync();

	virtual int getCurrentWarp(){
		return cwid;
	}
	void choose_threads();
	int thread_schedule(LONG current_cycle){}
	virtual  bool lockstep(){
		return true;
	}
};

Heuristic* new_heuristic(UINT nt, UINT nw, Thread* t, const char* param){
	return new Sync(nt, nw ,t, param);
}
void delete_heuristic(Heuristic* h){
	delete h;
}

Sync::Sync(UINT nt, UINT nw, Thread* t, const char* param)
: Heuristic(nt,nw, t)
{
	//current_warp = 0;
	threads_in_warp = (int)num_threads/num_warps;
	//for( uint j = 0 ; j < num_threads ; j++){
	//	invalid = true;
	//	pc = 0ULL;
	//	sp = 0ULL;
	//	cwid = -1;
	//	for (UINT i=0; i<num_threads; i++){
	//		mask.push_back(0);
	//	}
	//}
}

Sync::~Sync(){

}


void Sync::choose_threads(){
	static int current_warp = 0;
	int ref = -1;
	uint warp_count = 0;

	for (UINT i=0; i<num_threads; i++){
		t[i].fetch_isactive = false;
		t[i].active_last_fetch = false;
	}
	while(warp_count < num_warps){
		uint offset = current_warp * threads_in_warp;
		for (UINT i=0+offset; i<threads_in_warp+offset; i++){
			//cout<<"i"<<i<<endl;
			if (!t[i].fetch_qfull && !t[i].fetch_stall && !t[i].ignore && !t[i].fetch_qempty && !t[i].iq_full){
				if ( ref == -1 || t[i].fetch_sp < t[ref].fetch_sp || ( t[i].fetch_sp == t[ref].fetch_sp && t[i].fetch_pc < t[ref].fetch_pc ) ){
					ref = i;
				}
			}
		}
		if(ref == -1){
			//cout<<"Ref -1"<<endl;
			warp_count++;
			cwid = current_warp;
			current_warp = (current_warp+1)%num_warps;
			continue;
		}
		warp_count++;
		cwid = current_warp;
		current_warp = (current_warp+1)%num_warps;

		for (UINT i=0+offset; i<threads_in_warp+offset; i++){
			t[i].fetch_isactive = !t[i].fetch_qfull && t[i].fetch_pc == t[ref].fetch_pc && !t[i].fetch_stall && !t[i].ignore && !t[i].fetch_qempty&& !t[i].iq_full;
			t[i].active_last_fetch = t[i].fetch_isactive;
			/*if(t[i].fetch_isactive){
				cout<<"I "<<i<<" is active ref "<<ref<<" pc "<<t[i].fetch_pc<<endl;
			}*/
		}
		//cout<<"Exit"<<endl;
		return;
	}
	if(ref == -1){
		for (UINT i=0; i<num_threads; i++){
			t[i].fetch_isactive = false;
		}
		return;
	}

}

