#include "globals.hpp"


#ifndef TAGE_PREDICTOR
bool BranchPredictor::predicted(W64 target, bool taken){
	int index = target % BRP_SIZE;
	int state = pred_table[index];
	bool pred;
	if(taken){
		switch(state){
		case 0:
		case 1:
			pred_table[index]++;
			pred = false;
			break;
		case 2:
			pred_table[index]++;
			pred = true;
			break;
		case 3:
			pred = true;
			break;

		}
	}else{
		switch(state){
		case 0:
			pred = true;
			break;
		case 1:
			pred = true;
			pred_table[index]--;
			break;
		case 2:
			pred_table[index]--;
			pred = false;
			break;
		case 3:
			pred_table[index]--;
			pred = false;
			break;

		}
	}
	return pred;
}
#endif

int  DVInstLatency::getInstructionLatency(sa_uoptype uop){
	std::map<sa_uoptype, uint32_t>::iterator iter;
	iter = instruction_latency.find(uop);
	if(iter != instruction_latency.end())
		return iter->second;
	else
		return 1;
}

#ifdef REAL_MULTI_CORE
int Cache::evict(int index, W64 current_cycle, W64 newtag, W64 address){ //We are implementing LRU
	Cacheline line;
	line.address = address;
	line.dat = 0ULL;
	line.dirty = false;
	cache_set[index].put(newtag,line);

	return 1;
}

W64 Cache::get_block(W64 address){
	return extract_value(address, byte_offset,63);
}
int Cache::evict_new(W64 address){ //We are implementing LRU
	W64 tag = extract_value(address, index_offset,63);
	int index = (int)extract_value(address, byte_offset,index_offset - 1) ;
	
	Cacheline line;
	line.address = address;
	line.dat = 0ULL;
	line.dirty = false;
	Cacheline evictedline = cache_set[index].put(tag,line);
	if(evictedline.address != 0ULL && evictedline.dirty){
		Request req;
		req.block_address =  extract_value(evictedline.address, index_offset,63); //TODO:This is actually tag
		req.avail_cycle = 0Ull;
		req.memreq.address = evictedline.address;
		req.coreid.push_back(coreid);
		next_level_buffer.push_back(req);
	}
	return 1;
}

void Cache::cache_warm_reset_cache_port(W64 current_cycle, int &bank_conflict ,int &bank_conflict_hashed){

	std::vector<mem_req>::iterator   x;
	for(x = mem_request_list.begin(); x != mem_request_list.end(); x++){
		//access(x->address,x->tid,current_cycle,x->operation,x->ins_count,0); //NO CACHE WARMING FOR NOW
	}
	mem_request_list.clear();
}
#ifdef REAL_MULTI_CORE
void Cache::clock_tick(){
	std::vector<Request>::iterator   x;

	log(logDEBUG4)<<"Level "<< cache_level <<" Cache hit size "<<cache_hit_list[0].size()<<" cache miss size "<<cache_miss_list[0].size()<<endl;
	for(x = cache_hit_list[0].begin(); x != cache_hit_list[0].end(); ){
		if(x->avail_cycle <= cache_cycle){
			//x->miss = false;

			//cout<<"CH ready "<<x->block_address<<" block "<<hex16(x->block_address)<<" level "<<cache_level<<endl;
			if(cache_level == NUM_CACHE){
				previous_level_buffer.push_back(*x);
			}else{
				l1_hit_list.push_back(x->block_address);
			}

			x = cache_hit_list[0].erase(x);
		}else{
			x++;
		}
	}
	if(cache_level == NUM_CACHE){
		for(x = cache_miss_list[0].begin();x != cache_miss_list[0].end();){
			if(cache_level == NUM_CACHE){
				if(x->avail_cycle <=  cache_cycle){
					previous_level_buffer.push_back(*x);
					evict_new(x->memreq.address);
					//	cout<<"l2 miss "<<hex16(x->memreq.address)<<" ready size"<<x->coreid.size()<<endl;
					x = cache_miss_list[0].erase(x);
				}else{
					x++;
				}
			}
		}
	}else{
		W64 block = 0;
		//Only one cache line per cycle
		if(!l2_cache->previous_level_buffer.empty()){
			block = l2_cache->previous_level_buffer.front().block_address;
			bool ready = false;
			for(x = cache_miss_list[0].begin();x != cache_miss_list[0].end();){

				if(x->block_address == block){
					x->miss = true;
					bool found = false;
					for(std::vector<int>::iterator coreiter = l2_cache->previous_level_buffer.front().coreid.begin(); coreiter !=  l2_cache->previous_level_buffer.front().coreid.end();){
						if(*coreiter == coreid){
							//cout<<"l1 miss "<<hex16(x->memreq.address)<<"ready for "<<coreid<<endl;
							evict_new(x->memreq.address); //l1 evict
							//l2_cache->evict_new(x->address); //l2 evict;
							//cout<<"L1miss "<<x->memreq.address<<" ready "<<x->block_address<<" block "<<hex16(x->block_address)<<" cycle "<<cache_cycle<<endl;
							coreiter =  l2_cache->previous_level_buffer.front().coreid.erase(coreiter);
							if( l2_cache->previous_level_buffer.front().coreid.empty()){
								//cout<<"Removing "<<hex16(x->memreq.address)<<endl;
								ready = true;
							}
							auto range = mshr_map.equal_range(x->block_address);
							for (auto iterator = range.first; iterator != range.second;){
								mshr->update(iterator->second);
								iterator = mshr_map.erase(iterator);
							}
							assert(mshr_map.size()<1000);

							found = true;
							//l2_cache->previous_level_buffer.pop_front();
							//previous_level_buffer.push_back(*x);
							x = cache_miss_list[0].erase(x);
							break;
						}else{
							coreiter++;
						}
					}
					if(found){
						break;
					}else{
						x++;
					}
				}else{
					x++;
				}
			}
			if(ready){
				statistics.l2_hit++;
				l2_cache->previous_level_buffer.pop_front();
			}
		}
	}
	if(!next_level_buffer.empty()){
		if(cache_level == NUM_CACHE){
			mem_req req;
			req.address = 0ull;
			req.operation =MEM_STORE;
			req.delay = MEM_ACCESS_DELAY;
			req.block_address = 0ull;
			assert(llc_miss_list.size()<1000);
			llc_miss_list.push_back(req);
			next_level_buffer.pop_front();
		}else{
			//l2_cache->mem_request_list.push_back(next_level_buffer.front().memreq);
			l2_cache->evict_new(next_level_buffer.front().memreq.address);
			next_level_buffer.pop_front();
		}
	}
}

void Cache::update_cycle(W64 current_cycle, int &bank_conflict ,int &bank_conflict_hashed, int &mem_op_count){
	//log(logDEBUG4)<<"Port reset cycle "<<current_cycle<<std::endl;
	//cout<<"#reset cache port"<<endl;
	mem_op_count = 0;
	bank_conflict=0;
	bank_conflict_hashed=0;

	if(current_cycle > cache_cycle){
		//serviced_request_list[0].clear();
		cache_cycle = current_cycle;
		l2_cache->cache_cycle = current_cycle;
	}
	l1_hit_list.clear();
	mshr->cleanmshr(current_cycle);

	assert(cache_level == 1);
	assert(l2_cache->cache_level == 2);
	l2_cache->clock_tick();
	clock_tick();


	if(!blocked){
		if(!mem_request_list.empty()){
			bank_conflict_hashed = bank_conflict_new();
			bank_conflict = bank_conflict_orig_new();
			
		}
	}

	used_port = 0;

	//L1 requests
	std::vector<mem_req>::iterator   x;
	for(x = mem_request_list.begin(); x != mem_request_list.end(); ){
		if(mshr->inMSHR(x->address)){
			log(logDEBUG4)<<"In MSHR "<<hex16(x->address)<<x->tid<<endl;
			x = mem_request_list.erase(x);
			continue;
		}
		if(mshr->mshrFULL()){
			if(x->delay > 0){
				x->delay--;
			}
			x++;
			continue;
		}
		if(x->delay == 0){
			mem_op_count++;
			if(!access_new(*x)){
				//cout<<"Miss for "<<hex16(x->address)<<" cycle "<<cache_cycle<<endl;
				mshr->insert(x->address);
				mshr_map.insert(std::make_pair(extract_value(x->address, byte_offset,63),x->address));
			}else{
				//cout<<"Hit for "<<hex16(x->address)<<" cycle "<<cache_cycle<<endl;
			}
			x = mem_request_list.erase(x);
		}else{
			x->delay--;
			x++;
		}
	}
	if(mem_request_list.size() == 0){
		blocked = false;
	}else{
		blocked = true;
	}

	//L2 requests. Multiple address in a cycle
	for(x = l2_cache->mem_request_list.begin(); x != l2_cache->mem_request_list.end(); ){

		if(x->delay == 0){
			l2_cache->access_new(*x);
			x = l2_cache->mem_request_list.erase(x);
		}else{
			x->delay--;
			x++;
		}
	}
	
	//Memory accesses
	if(!l2_cache->llc_miss_list.empty()){
		l2_cache->llc_miss_list.front().delay--;
		if(l2_cache->llc_miss_list.front().delay == 0){
			if(l2_cache->llc_miss_list.front().address == 0ULL){ //<------------------???
				//Write backs  
				l2_cache->llc_miss_list.pop_front();
			}else{
				l2_cache->memory_access(l2_cache->llc_miss_list.front());
				l2_cache->llc_miss_list.pop_front();
			}
		}
	}


	previous_level_buffer.clear();

}
#else
void Cache::clock_tick(){
	std::vector<Request>::iterator   x;

	log(logDEBUG4)<<"Level "<< cache_level <<" Cache hit size "<<cache_hit_list[0].size()<<" cache miss size "<<cache_miss_list[0].size()<<endl;
	for(x = cache_hit_list[0].begin(); x != cache_hit_list[0].end(); ){
		if(x->avail_cycle <= cache_cycle){
			//x->miss = false;

			//cout<<"CH ready "<<x->block_address<<" block "<<hex16(x->block_address)<<" level "<<cache_level<<endl;
			if(cache_level == NUM_CACHE){
				previous_level_buffer.push_back(*x);
			}else{
				l1_hit_list.push_back(x->block_address);
			}

			x = cache_hit_list[0].erase(x);
		}else{
			x++;
		}
	}
	if(cache_level == NUM_CACHE){
		for(x = cache_miss_list[0].begin();x != cache_miss_list[0].end();){
			if(cache_level == NUM_CACHE){
				if(x->avail_cycle <=  cache_cycle){
					previous_level_buffer.push_back(*x);
					evict_new(x->memreq.address);
					x = cache_miss_list[0].erase(x);
				}else{
					x++;
				}
			}
		}
	}else{
		W64 block = 0;
		//Only one cache line per cycle
		if(!l2_cache->previous_level_buffer.empty()){
			block = l2_cache->previous_level_buffer.front().block_address;
			bool ready = false;
			for(x = cache_miss_list[0].begin();x != cache_miss_list[0].end();){

				if(x->block_address == block){
					evict_new(x->memreq.address); //l1 evict
					//l2_cache->evict_new(x->address); //l2 evict;
					//cout<<"L1miss "<<x->memreq.address<<" ready "<<x->block_address<<" block "<<hex16(x->block_address)<<" cycle "<<cache_cycle<<endl;
					x->miss = true;
					ready = true;
					auto range = mshr_map.equal_range(x->block_address);
					for (auto iterator = range.first; iterator != range.second;){
						mshr->update(iterator->second);
						iterator = mshr_map.erase(iterator);
					}
					assert(mshr_map.size()<1000);
					//l2_cache->previous_level_buffer.pop_front();
					//previous_level_buffer.push_back(*x);
					x = cache_miss_list[0].erase(x);
					break;
				}else{
					x++;
				}
			}
			if(ready){
				l2_cache->previous_level_buffer.pop_front();
			}
		}
	}
	if(!next_level_buffer.empty()){
		if(cache_level == NUM_CACHE){
			mem_req req;
			req.address = 0ull;
			req.operation =MEM_STORE;
			req.delay = 64;
			assert(llc_miss_list.size()<1000);
			llc_miss_list.push_back(req);
			next_level_buffer.pop_front();
		}else{
			//l2_cache->mem_request_list.push_back(next_level_buffer.front().memreq);
			l2_cache->evict_new(next_level_buffer.front().memreq.address);
			next_level_buffer.pop_front();
		}
	}
}

void Cache::update_cycle(W64 current_cycle, int &bank_conflict ,int &bank_conflict_hashed, int &mem_op_count){
	//log(logDEBUG4)<<"Port reset cycle "<<current_cycle<<std::endl;
	//cout<<"#reset cache port"<<endl;
	mem_op_count = 0;
	bank_conflict=0;
	bank_conflict_hashed=0;

	if(current_cycle > cache_cycle){
		//serviced_request_list[0].clear();
		cache_cycle = current_cycle;
		l2_cache->cache_cycle = current_cycle;
	}
	l1_hit_list.clear();
	mshr->cleanmshr(current_cycle);

	l2_cache->clock_tick();
	clock_tick();


	if(!blocked){
		if(!mem_request_list.empty()){
			bank_conflict_hashed = bank_conflict_new();
			bank_conflict = bank_conflict_orig_new();
			
		}
	}

	used_port = 0;

	//L1 requests
	std::vector<mem_req>::iterator   x;
	for(x = mem_request_list.begin(); x != mem_request_list.end(); ){
		if(mshr->inMSHR(x->address)){
			log(logDEBUG4)<<"In MSHR "<<hex16(x->address)<<x->tid<<endl;
			//cout<<"In MSHR "<<hex16(x->address)<<" cycle "<<cache_cycle<<endl;
			//x++;
			x = mem_request_list.erase(x);
			/*if(mem_request_list.size() == 0){
				blocked = false;
			}*/
			continue;
		}
		if(mshr->mshrFULL()){
			if(x->delay > 0){
				x->delay--;
			}
			x++;
			continue;
		}
		if(x->delay == 0){
			mem_op_count++;
			if(!access_new(*x)){
				//cout<<"Miss for "<<hex16(x->address)<<" cycle "<<cache_cycle<<endl;
				//l1_miss.push_back(x->address);
				mshr->insert(x->address);
				mshr_map.insert(std::make_pair(extract_value(x->address, byte_offset,63),x->address));
			}else{
				//cout<<"Hit for "<<hex16(x->address)<<" cycle "<<cache_cycle<<endl;
			}
			x = mem_request_list.erase(x);
			/*if(mem_request_list.size() == 0){
				blocked = false;
			}*/
		}else{
			x->delay--;
			x++;
		}
	}
	if(mem_request_list.size() == 0){
		blocked = false;
	}else{
		blocked = true;
	}

	//L2 requests. Multiple address in a cycle
	for(x = l2_cache->mem_request_list.begin(); x != l2_cache->mem_request_list.end(); ){

		if(x->delay == 0){
			l2_cache->access_new(*x);
			x = l2_cache->mem_request_list.erase(x);
		}else{
			x->delay--;
			x++;
		}
	}

	//Memory accesses
	if(!l2_cache->llc_miss_list.empty()){ //cout << "[L2 Miss] delay = " << l2_cache->llc_miss_list.front().delay << endl;
		l2_cache->llc_miss_list.front().delay--;
		if(l2_cache->llc_miss_list.front().delay == 0){
			if(l2_cache->llc_miss_list.front().address == 0ULL){
				//Write backs
				l2_cache->llc_miss_list.pop_front();
			}else{
				l2_cache->memory_access(l2_cache->llc_miss_list.front());
				l2_cache->llc_miss_list.pop_front();
			}
		}
	}
	
	previous_level_buffer.clear();

}


#endif


bool Cache::cache_warm_dv_load(DVLoad *dvl, W64 current_cycle, int ins_count,std::vector<int> *smask){
	mem_request_add_dv(dvl,MEM_LOAD, current_cycle, ins_count, smask);
	return true;
}

bool Cache::cache_warm_dv_store(DVStore *dvl, W64 current_cycle, int ins_count,std::vector<int> *smask){
	mem_request_add_dv_st(dvl,MEM_STORE, current_cycle, ins_count, smask);
	return true;
}

bool Cache::dv_load(DVLoad *dvl, W64 current_cycle, int ins_count, std::vector<int> *smask){

	//Port is blocked for this cycle
	if(blocked){
		return false;
	}
	if(used_port >= num_ports){
		//cout<<"DVL port"<<endl;
		return false;
	}
	/*if(delayed_load->size() > 0){

	}*/

	log(logDEBUG4)<<"DV LOAD "<<endl;
	mem_request_add_dv(dvl,MEM_LOAD,current_cycle, ins_count,smask);
	used_port++;
	return true;

}

bool Cache::dv_store(DVStore *dvl, W64 current_cycle, int ins_count, std::vector<int> *smask){

	//Port is blocked for this cycle
	
	if(blocked){
		return false;
	}
	if(used_port >= num_ports){
		//cout<<"DVS port"<<endl;
		return false;
	}
	log(logDEBUG4)<<"DV STORE "<<endl;
	mem_request_add_dv_st(dvl,MEM_STORE, current_cycle, ins_count, smask);
	used_port++;
	return true;

}

int Cache::intbitXor(int x, int y)
{
	return x ^ y;
}


W64 Cache::bitXor(W64 x, W64 y)
{
	W64 a = x & y;
	W64 b = ~x & ~y;
	W64 z = ~a & ~b;
	return z;
}

void Cache::print_histogram(){
	int count = sizeof(W64)*8;
	for(int i = 0; i < count; i++){
		cout<<"i["<<std::dec<<i<< "]="<<std::dec<<histogram[i]<<" ";
	}
}
void Cache::print_histogram_csv(){
	int count = sizeof(W64)*8;
	for(int i = 0; i < count; i++){
		cout<<std::dec<<histogram[i]<<",";
	}
}

void Cache::mem_request_add_dv(DVLoad *inst, Memoperation operation, W64 current_cycle, int ins_count,std::vector<int> *smask){

	for(int i = 0; i< inst->length ; i++){
		
		if(!(*smask)[i]){
			continue;
		}
	

		//assert(mem_request_list[clusterid].size() <= 8);
		log(logDEBUG4)<<"Address to load "<<hex16(inst->address_params[i].address)<<endl;
#ifdef MEMDEBUG
		cout<<"\t>>>> Address to load "<<hex16(inst->address_params[i].address)<<endl;
#endif

		mem_req req;
		req.coreid.push_back(coreid);
		req.address= inst->address_params[i].address;
		req.block_address = extract_value(req.address, byte_offset,63);
		req.size = inst->address_params[i].address_size;
		assert(req.size > 0);
		//req.tid = i;
		req.operation = operation;
		req.delay = 0;
		req.ins_count = ins_count;
		req.tid = inst->address_params[i].tid;
		req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;


		split_tlb_8[req.tlb_index].tlb_access(req.address);
		bool split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
		unified_tlb_8->tlb_access(req.address);
		bool unified_hit = unified_tlb_16->tlb_access(req.address);

		//TLB miss penalty
		if(conf.base){
			if(!unified_hit){
				req.delay = 10;
			}
		}else{
			if(!split_hit){
				req.delay = 10;
			}
		}

		mem_request_list.push_back(req);
	}
}


void Cache::mem_request_add_dv_st(DVStore *inst, Memoperation operation, W64 current_cycle, int ins_count,std::vector<int> *smask){

	for(int i = 0; i< inst->length ; i++){
	
		if(!smask[0][i]){
			continue;
		}
#ifdef DEBUGMEM
		cout<<"Address to store "<<hex16(inst->address_params[i].address)<<endl;
#endif
		mem_req req;
		req.coreid.push_back(coreid);
		req.address= inst->address_params[i].address;
		req.block_address = extract_value(req.address, byte_offset,63);
		req.size = inst->address_params[i].address_size;
		assert(req.size > 0);
		//req.tid = i;
		req.delay = 0;
		req.tid = inst->address_params[i].tid;
		req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;
		req.operation = operation;
		req.ins_count = ins_count;

		split_tlb_8[req.tlb_index].tlb_access(req.address);
		int split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
		unified_tlb_8->tlb_access(req.address);
		int unified_hit = unified_tlb_16->tlb_access(req.address);

		if(conf.base){
			if(!unified_hit){
				req.delay = 10;
			}
		}else{
			if(!split_hit){
				req.delay = 10;
			}
		}
		mem_request_list.push_back(req);
	}
}


/*Data could be across multiple banks b1 and b2 gives the first bank. Now we check if any of the banks actually overlap*/
bool Cache::isOverlappingBanks(int b1, int num_b1, int b2, int num_b2, int num_banks){
	//num_b1 = num_b1 - 1;
	//num_b2 = num_b2 - 1;
	//cout<<"B1 "<<b1<<" numb1 "<<num_b1<<" B2 "<<b2<<" numb2 "<<num_b2<<endl;
	if((num_b1 == 0) && (num_b2 == 0)){
		if(b1 == b2){
			return true;
		}else{
			return false;
		}
	}else{
		int rot1 = (b1 + num_b1)/num_banks;
		int rot2 = (b2 + num_b2)/num_banks;
		if((rot1 == 1) && ( rot2 == 1)){
			return true;
		}else if(rot1 == 1 || rot2 == 1){
			if(b1 < b2){
				if(((b1 + num_b1) >= b2) || (b1 <=  (b2 + num_b2) % num_banks)){
					return true;
				}
				return false;
			}else{
				if(((b2 + num_b2) >= b1) || (b2 <=  (b1 + num_b1) % num_banks)){
					return true;
				}
				return false;
			}
		}else{
			if(b1 < b2){
				if((b1 + num_b1) >= b2 ){
					return true;
				}
				return false;
			}else{
				if((b2 + num_b2) >= b1 ){
					return true;
				}
				return false;
			}
		}
	}
}

bool Cache::bank_conflict(LONG address, int tid, W64 current_cycle, int fuid, int refcount, W64 issue_cycle, int size){
	//int bank = extract_value(address,4,5);
	//int bank2;
	//cout<<"Bank "<<bank<<" Address "<<hex16(address)<<" Cycle "<<current_cycle<<" cluster "<<clusterid<<endl;
	std::vector<mem_req>::iterator   x;

	if(size > 256){// Seeing some prefetch instructions 512 bits
		return false;
	}
	int bank_width = 32; //Bits 64byte cache line with 16 banks is 4byte per bank (32 bit)
	int num_banks = 16; //4 bits so 16 banks
	//int req_banks = size/bank_width;
	//log(logDEBUG4)<<" gatherscatter_ "<<endl;
	for(x = mem_request_list.begin(); x != mem_request_list.end(); x++){
		LONG mem = x->address;
		//if(extract_value(address,4,5) == (extract_value(mem,4,5))){
		//if(intbitXor(intbitXor(extract_value(address,12,13),extract_value(address,24,25)),intbitXor(extract_value(address,14,15), extract_value(address,26,27))) == intbitXor(intbitXor(extract_value(mem,12,13),extract_value(mem,24,25)),intbitXor(extract_value(mem,14,15), extract_value(mem,26,27)))){
		//if(intbitXor(extract_value(address,12,15),extract_value(address,24,27)) == (intbitXor(extract_value(mem,12,15),extract_value(mem,24,27)))){
		//cout<<"SZ "<<size<<endl;
		if(isOverlappingBanks(extract_value(address,3,6),(size - 1)/bank_width, extract_value(mem,3,6),(x->size - 1)/bank_width,num_banks)){ //Lower order bits
			//if(isOverlappingBanks(intbitXor(extract_value(address,12,15),extract_value(address,24,27)),((size - 1)/bank_width),intbitXor(extract_value(mem,12,15),extract_value(mem,24,27)),(x->size - 1)/bank_width, num_banks)){
			//cout<<"Conflict Bank "<<bank2<<" Address "<<hex16(*x)<<" Cycle "<<current_cycle<<endl;
			return true;
		}

	}
	return false;
}

//Returns number of bank conflicts
int Cache::bank_conflict_new(){
#ifdef MULTI_CORE
	int bank_width = 128; //Bits 64byte cache line with 16 banks is 4byte per bank (32 bit)
	int num_banks = 4;
#else
	int bank_width = 32; //Bits 64byte cache line with 16 banks is 4byte per bank (32 bit)
	int num_banks = 16;
#endif
	int ls_count = 0;
	int ls_bank[num_banks];
	for(int i = 0 ; i < num_banks; i++){
		ls_bank[i] = 0;
	}

	std::vector<mem_req>::iterator   x;
	for(x = mem_request_list.begin(); x != mem_request_list.end(); x++){
		ls_count++;
		LONG mem = x->address;
		//x->delay = 0;
		int delay = 0;
		x->bankindexhashed = intbitXor(extract_value(mem,12,15),extract_value(mem,24,27));
		for(int i = 0 ;i < ((x->size -1)/bank_width) + 1; i++ ){

			ls_bank[(x->bankindexhashed+i) % num_banks]++; //Banks occupied by a memory request
			if(!conf.base){
				if(ls_bank[(x->bankindexhashed+i) % num_banks] > delay){
					delay = ls_bank[(x->bankindexhashed+i) % num_banks];
				}
			}
		}
		x->delay += delay;
	}
	int max = 0;
	for(int i = 0 ;i< num_banks;i++){
		if(max < ls_bank[i]){
			max = ls_bank[i];
		}
	}
	
	int bankconflictpenalty = max - 1;
	assert(bankconflictpenalty >= 0);
	return bankconflictpenalty;
	//printf("\n Cycles %d \n",(num + 2 - 1) / 2);
	//return (2.0*inter) - 1.0;
}

int Cache::bank_conflict_orig_new(){
	int bank_width = 32; //Bits 64byte cache line with 16 banks is 4byte per bank (32 bit)
	int num_banks = 16;
	int ls_count = 0;
	int ls_bank[num_banks];
	for(int i = 0 ; i < num_banks; i++){
		ls_bank[i] = 0;
	}
	std::vector<mem_req>::iterator   x;
	for(x = mem_request_list.begin(); x != mem_request_list.end(); x++){
		ls_count++;
		LONG mem = x->address;
		int delay = 0;
		x->bankindexlow = extract_value(mem,3,6);
		for(int i = 0 ;i < ((x->size -1)/bank_width) + 1; i++ ){
			ls_bank[(x->bankindexlow+i) % num_banks]++; //Banks occupied by a memory request
			if(conf.base){
				if(ls_bank[(x->bankindexlow+i) % num_banks] > delay){
					delay = ls_bank[(x->bankindexlow+i) % num_banks];
				}
			}
		}
		x->delay += delay;
	}
	int max = 0;
	for(int i = 0 ;i< num_banks;i++){
		if(max < ls_bank[i]){
			max = ls_bank[i];
		}
	}
	
	int bankconflictpenalty = max - 1;
	assert(bankconflictpenalty >= 0);
	return bankconflictpenalty;
}

bool Cache::bank_conflict_reduced(LONG address, int tid, W64 current_cycle, int fuid, int refcount, W64 issue_cycle, int size){
	//int bank = extract_value(address,4,5);
	//int bank2;
	//cout<<"Bank "<<bank<<" Address "<<hex16(address)<<" Cycle "<<current_cycle<<" cluster "<<clusterid<<endl;
	std::vector<mem_req>::iterator   x;

	if(size > 256){// Seeing some prefetch instructions 512 bits
		return false;
	}
	int bank_width = 32; //Bits 64byte cache line with 16 banks is 4byte per bank (32 bit)
	int num_banks = 16; //4 bits so 16 banks
	//int req_banks = size/bank_width;
	//log(logDEBUG4)<<" gatherscatter_ "<<endl;
	for(x = mem_request_list.begin(); x != mem_request_list.end(); x++){
		LONG mem = x->address;
		//if(extract_value(address,4,5) == (extract_value(mem,4,5))){
		//if(intbitXor(intbitXor(extract_value(address,12,13),extract_value(address,24,25)),intbitXor(extract_value(address,14,15), extract_value(address,26,27))) == intbitXor(intbitXor(extract_value(mem,12,13),extract_value(mem,24,25)),intbitXor(extract_value(mem,14,15), extract_value(mem,26,27)))){
		//if(intbitXor(extract_value(address,12,15),extract_value(address,24,27)) == (intbitXor(extract_value(mem,12,15),extract_value(mem,24,27)))){
		//cout<<"SZ "<<size<<endl;
		//if(isOverlappingBanks(extract_value(address,3,6),(size - 1)/bank_width, extract_value(mem,3,6),(x->size - 1)/bank_width,num_banks)){ //Lower order bits
		if(isOverlappingBanks(intbitXor(extract_value(address,12,15),extract_value(address,24,27)),((size - 1)/bank_width),intbitXor(extract_value(mem,12,15),extract_value(mem,24,27)),(x->size - 1)/bank_width, num_banks)){
			//cout<<"Conflict Bank "<<bank2<<" Address "<<hex16(*x)<<" Cycle "<<current_cycle<<endl;
			return true;
		}

	}
	return false;
}

bool Cache::tag_conflict(LONG address, int tid, W64 current_cycle, int fuid, int refcount, W64 issue_cycle){
	int tag_bank = extract_value(address,9,10);
	int tag_bank2;
	//cout<<"Tag Bank "<<tag_bank<<"Address "<<hex16(address)<<"Cycle "<<current_cycle<<" cluster "<<clusterid<<endl;
	std::vector<mem_req>::iterator   x;
	//log(logDEBUG4)<<" gatherscatter_ "<<endl;
	for(x = mem_request_list.begin(); x != mem_request_list.end(); x++){
		LONG mem = x->address;
		if(tag_bank == (tag_bank2 = extract_value(mem,9,10))){
			//cout<<"Conflict Bank "<<tag_bank2<<" Address "<<hex16(*x)<<" Cycle "<<current_cycle<<endl;
			return true;
		}

	}
	return false;
}


void Cache::memory_access(mem_req memreq){
	int tid = 0; //Virtual Address can be shared
	W64 block = extract_value(memreq.address, byte_offset,63);
	cache_miss_list[tid].push_back(Request(block,cache_cycle + MEM_LATENCY_DVSIM, memreq));

}
bool Cache::access_new(mem_req memreq){
	int tid = 0; //Virtual Address can be shared
	W64 tag = extract_value(memreq.address, index_offset,63);
	W64 block_address = extract_value(memreq.address, byte_offset,63);
	int index = (int)extract_value(memreq.address, byte_offset,index_offset - 1) ;
	assert(index < cache_line_numbers/cache_ways);
	if(memreq.operation == MEM_STORE){
		stats->cache_write_access[cache_level]++;
	}else{
		stats->cache_read_access[cache_level]++;
	}
	std::vector<Request>::iterator it_hit = std::find(cache_hit_list[tid].begin(), cache_hit_list[tid].end(), Request(block_address,0, memreq));
	if(it_hit != cache_hit_list[tid].end()){
		//cout<<"Was in  Hit "<<hex16(memreq.address)<<" level "<<cache_level<<" cycle "<<cache_cycle<<" block "<<block_address<<endl;

		return true;
	}else{
		std::vector<Request>::iterator it_miss = std::find(cache_miss_list[tid].begin(), cache_miss_list[tid].end(), Request(block_address,0, memreq));
		if(it_miss != cache_miss_list[tid].end()){

			//cout<<"Was in  miss "<<hex16(memreq.address)<<" level "<<cache_level<<" cycle "<<cache_cycle<<" block "<<block_address<<endl;
			return false;
		}

		if(cache_set[index].exist(tag)){

			if(memreq.operation == MEM_STORE){
				//line.dirty= true;
				cache_set[index].get(tag,true);
			}else{
				cache_set[index].get(tag,false);
				//line.dirty = false;
			}
#ifdef DEBUG
			cout<<"Hit "<<hex16(memreq.address)<<" level "<<cache_level<<" cycle "<<cache_cycle<<" block "<<block_address<<endl;
#endif
			//cache_set[index].put(tag,line); //LRU No return we are just updating the existing line
			//cache_set[index].get(tag);
			assert(cache_hit_list[tid].size()<1000);
			cache_hit_list[tid].push_back(Request(block_address, cache_cycle + cache_latency, memreq));
			return true;
		}else{
			if(cache_level != NUM_CACHE){
				memreq.delay = 1; // 1 cycle delay to send address
				cache_miss_list[tid].push_back(Request(block_address, 0,memreq));
#ifdef DEBUG
				cout<<"L1miss "<<hex16(memreq.address)<<" level "<<cache_level<<" cycle "<<cache_cycle<<" block "<<block_address<<endl;
#endif
				assert(l2_cache->mem_request_list.size()<1000);
				//HERE
				//	std::vector<memreq>::const_iterator req = std::find(l2_cache->mem_request_list.begin(), l2_cache->mem_request_list.end(), memreq);
				l2_cache->mem_request_list.push_back(memreq);
				stats->cache_miss_nonscalar[cache_level]++;
				if(memreq.operation == MEM_STORE){
					stats->cache_write_miss_nonscalar[cache_level]++;
				}else{
					stats->cache_read_miss_nonscalar[cache_level]++;
				}
				return false;
			}else{
				stats->cache_miss_nonscalar[cache_level]++;
				if(memreq.operation == MEM_STORE){
					stats->cache_write_miss_nonscalar[cache_level]++;
				}else{
					stats->cache_read_miss_nonscalar[cache_level]++;
				}
				//LLC and Cachemisslist

				memreq.delay = MEM_ACCESS_DELAY;
				//cout<<"L2miss "<<hex16(memreq.address)<<" level "<<cache_level<<" cycle "<<cache_cycle<<" block "<<block_address<<endl;
				assert(llc_miss_list.size()<1000);
				llc_miss_list.push_back(memreq);
				return false;
			}
		}
	}
}

#else
int Cache::evict(int index, W64 current_cycle, W64 newtag, W64 address){ //We are implementing LRU
	
	Cacheline line;
	line.address = address;
	line.dat = 0ULL;
	line.dirty = false;
	cache_set[index].put(newtag,line);

	return 1;
}

W64 Cache::get_block(W64 address){
	return extract_value(address, byte_offset,63);
}
int Cache::evict_new(W64 address){ //We are implementing LRU
	W64 tag = extract_value(address, index_offset,63);
	int index = (int)extract_value(address, byte_offset,index_offset - 1) ;
	
	Cacheline line;
	line.address = address;
	line.dat = 0ULL;
	line.dirty = false;
	Cacheline evictedline = cache_set[index].put(tag,line);
	if(evictedline.address != 0ULL && evictedline.dirty){
		Request req;
		req.block_address =  extract_value(evictedline.address, index_offset,63); //TODO:This is actually tag
		req.avail_cycle = 0Ull;
		req.memreq.address = evictedline.address;
		next_level_buffer.push_back(req);
	}
	return 1;
}

void Cache::cache_warm_reset_cache_port(W64 current_cycle, int &bank_conflict ,int &bank_conflict_hashed){

	std::vector<mem_req>::iterator   x;
	for(x = mem_request_list.begin(); x != mem_request_list.end(); x++){
		//access(x->address,x->tid,current_cycle,x->operation,x->ins_count,0); //NO CACHE WARMING FOR NOW
	}
	mem_request_list.clear();
}

void Cache::clock_tick(){
	std::vector<Request>::iterator   x;

	log(logDEBUG4)<<"Level "<< cache_level <<" Cache hit size "<<cache_hit_list[0].size()<<" cache miss size "<<cache_miss_list[0].size()<<endl;
	for(x = cache_hit_list[0].begin(); x != cache_hit_list[0].end(); ){
		if(x->avail_cycle <= cache_cycle){
			//x->miss = false;

			//cout<<"CH ready "<<x->block_address<<" block "<<hex16(x->block_address)<<" level "<<cache_level<<endl;
			if(cache_level == NUM_CACHE){
				previous_level_buffer.push_back(*x);
			}else{
				l1_hit_list.push_back(x->block_address);
			}

			x = cache_hit_list[0].erase(x);
		}else{
			x++;
		}
	}
	if(cache_level == NUM_CACHE){
		for(x = cache_miss_list[0].begin();x != cache_miss_list[0].end();){
			if(cache_level == NUM_CACHE){
				if(x->avail_cycle <=  cache_cycle){
					previous_level_buffer.push_back(*x);
					evict_new(x->memreq.address);
					x = cache_miss_list[0].erase(x);
				}else{
					x++;
				}
			}
		}
	}else{
		W64 block = 0;
		//Only one cache line per cycle
		if(!l2_cache->previous_level_buffer.empty()){
			block = l2_cache->previous_level_buffer.front().block_address;
			bool ready = false;
			for(x = cache_miss_list[0].begin();x != cache_miss_list[0].end();){

				if(x->block_address == block){
					evict_new(x->memreq.address); //l1 evict
					//l2_cache->evict_new(x->address); //l2 evict;
					//cout<<"L1miss "<<x->memreq.address<<" ready "<<x->block_address<<" block "<<hex16(x->block_address)<<" cycle "<<cache_cycle<<endl;
					x->miss = true;
					ready = true;
					
					mshr->update(x->memreq.address);
		
					x = cache_miss_list[0].erase(x);
					break;
				}else{
					x++;
				}
			}
			if(ready){
				l2_cache->previous_level_buffer.pop_front();
			}
		}
	}
	if(!next_level_buffer.empty()){
		if(cache_level == NUM_CACHE){
			mem_req req;
			req.address = 0ull;
			req.operation =MEM_STORE;
			req.delay = MEM_ACCESS_DELAY;
			assert(llc_miss_list.size()<1000);
			llc_miss_list.push_back(req);
			next_level_buffer.pop_front();
		}else{
			//l2_cache->mem_request_list.push_back(next_level_buffer.front().memreq);
			l2_cache->evict_new(next_level_buffer.front().memreq.address);
			next_level_buffer.pop_front();
		}
	}
}

void Cache::update_cycle(W64 current_cycle, int &bank_conflict ,int &bank_conflict_hashed, int &mem_op_count){
	//log(logDEBUG4)<<"Port reset cycle "<<current_cycle<<std::endl;
	//cout<<"#reset cache port"<<endl;
	mem_op_count = 0;
	bank_conflict=0;
	bank_conflict_hashed=0;

	if(current_cycle > cache_cycle){
		//serviced_request_list[0].clear();
		cache_cycle = current_cycle;
		l2_cache->cache_cycle = current_cycle;
	}
	l1_hit_list.clear();
	mshr->cleanmshr(current_cycle);

	l2_cache->clock_tick();
	clock_tick();

	if(!mem_request_list.empty()){
		bank_conflict_hashed = bank_conflict_new();
		bank_conflict = bank_conflict_orig_new();

	}

	used_port = 0;

	//L1 requests
	std::vector<mem_req>::iterator   x;
	for(x = mem_request_list.begin(); x != mem_request_list.end(); ){
		if(!x->issued){
#ifdef DEBUG
			cout<<"[Cache] Load not issued for " << hex16(x->address)<<" "<<endl;
#endif
			x++;
			continue;
		}
		if(mshr->inMSHR(x->address)){
#ifdef DEBUG
			cout<<"[Cache] In MSHR "<<hex16(x->address)<<" "<<endl;
#endif
			x = mem_request_list.erase(x);
			continue;
		}
		if(mshr->mshrFULL()){
			if(x->delay > 0){
				x->delay--;
			}
			x++;
			continue;
		}
		if(x->delay == 0){
			mem_op_count++;
			if(!access_new(*x)){
#ifdef DEBUG
				cout<<"[Cache] Miss for "<<hex16(x->address)<<" cycle "<<cache_cycle<<endl;
#endif
				//l1_miss.push_back(x->address);
				mshr->insert(x->address);
				//mshr_map.insert(std::make_pair(extract_value(x->address, byte_offset,63),x->address));
			}else{
#ifdef DEBUG
				cout<<"[Cache] Hit for "<<hex16(x->address)<<" cycle "<<cache_cycle<<endl;
#endif
			}
			x = mem_request_list.erase(x);
	
		}else{
			
			x->delay--;
			x++;
		}
	}
	if(mem_request_list.size() == 0){
		blocked = false;
	}else{
		blocked = true;
	}

	//L2 requests. Multiple address in a cycle
	for(x = l2_cache->mem_request_list.begin(); x != l2_cache->mem_request_list.end(); ){

		if(x->delay == 0){
			l2_cache->access_new(*x);
			x = l2_cache->mem_request_list.erase(x);
		}else{
			x->delay--;
			x++;
		}
	}

	//Memory accesses
	if(!l2_cache->llc_miss_list.empty()){
		l2_cache->llc_miss_list.front().delay--; //cout << "l2_cache delay == " << l2_cache->llc_miss_list.front().delay << " address = : " << hex16(l2_cache->llc_miss_list.front().address) << endl;
		if(l2_cache->llc_miss_list.front().delay == 0){
			if(l2_cache->llc_miss_list.front().address == 0ULL){
				//Write backs
				l2_cache->llc_miss_list.pop_front();
			}else{
				l2_cache->memory_access(l2_cache->llc_miss_list.front());
				l2_cache->llc_miss_list.pop_front();
			}
		}
	}

	previous_level_buffer.clear();
}


bool Cache::cache_warm_dv_load(DVLoad *dvl, W64 current_cycle, int ins_count,std::vector<int> *smask, bool _scalarise){
	mem_request_add_dv(dvl,MEM_LOAD, current_cycle, ins_count, smask, _scalarise);
	return true;
}

bool Cache::cache_warm_dv_store(DVStore *dvl, W64 current_cycle, int ins_count,std::vector<int> *smask, bool _scalarise){
	mem_request_add_dv_st(dvl,MEM_STORE, current_cycle, ins_count, smask, _scalarise);
	return true;
}

bool Cache::dv_load(DVLoad *dvl, W64 current_cycle, int ins_count, std::vector<int> *smask, bool _scalarise){

	if(blocked){
		return false;
	}
	if(used_port >= num_ports){
		//cout<<"DVL port"<<endl;
		return false;
	}
#ifdef DEBUGMEM
	for(int i = 0; i < smask->size(); i++)
		cout << smask[0][i];// << endl;

	cout << endl;
#endif	
	log(logDEBUG4)<<"DV LOAD "<<endl;
	mem_request_add_dv(dvl,MEM_LOAD,current_cycle, ins_count,smask, _scalarise);
	used_port++;
	return true;

}

bool Cache::dv_store(DVStore *dvl, W64 current_cycle, int ins_count, std::vector<int> *smask,bool _scalarise){

	if(blocked){
		return false;
	}
	if(used_port >= num_ports){
		//cout<<"DVS port"<<endl;
		return false;
	}
#ifdef DEBUGMEM
	for(int i = 0; i < smask->size(); i++)
		cout << smask[0][i];// << endl;

	cout << endl;
#endif	
	log(logDEBUG4)<<"DV STORE "<<endl;
	mem_request_add_dv_st(dvl,MEM_STORE, current_cycle, ins_count, smask,_scalarise);
	used_port++;
	return true;

}


int Cache::intbitXor(int x, int y)
{
	return x ^ y;
}

/*	int nBitXor(int y, int y, int size){

	}
 */
W64 Cache::bitXor(W64 x, W64 y)
{
	W64 a = x & y;
	W64 b = ~x & ~y;
	W64 z = ~a & ~b;
	return z;
}

void Cache::print_histogram(){
	int count = sizeof(W64)*8;
	for(int i = 0; i < count; i++){
		cout<<"i["<<std::dec<<i<< "]="<<std::dec<<histogram[i]<<" ";
	}
}
void Cache::print_histogram_csv(){
	int count = sizeof(W64)*8;
	for(int i = 0; i < count; i++){
		cout<<std::dec<<histogram[i]<<",";
	}
}
#ifdef V_SCALARISE
void Cache::mem_request_add_dv(DVLoad *inst, Memoperation operation, W64 current_cycle, int ins_count,std::vector<int> *smask, bool _scalarise){

	if(inst->is_sse && conf.threads_per_warp > 2){
		bool del=0;
		bool active = false;
		for(int i = 0; i< inst->length ; i= i++){
			/*if(!inst.mask[i]){
					continue;
				}*/
			if(!(*smask)[i]){
				continue;
			}
			log(logDEBUG4)<<"Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
#ifdef DEBUGMEM
			cout<<"\t>>>>Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
#endif
			active = true;
			mem_req req;
			req.inst_delay = del++;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.operation = operation;
			req.delay = 0;
			//req.issued = false;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;


			split_tlb_8[req.tlb_index].tlb_access(req.address);
			bool split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			bool unified_hit = unified_tlb_16->tlb_access(req.address);

			//TLB miss penalty
			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}

	}else if(inst->is_avx){
		int del = 0;
		for(int i = 0; i< inst->length ; i++){
			/*if(!inst.mask[i]){
							continue;
						}*/
			if(!(*smask)[i]){
				continue;
			}

#ifdef DEBUGMEM
			cout<<"\t>>>>Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
#endif
			log(logDEBUG4)<<"Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
			mem_req req;
			req.inst_delay = del++;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.operation = operation;
			req.delay = 0;
			//req.issued = false;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;


			split_tlb_8[req.tlb_index].tlb_access(req.address);
			bool split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			bool unified_hit = unified_tlb_16->tlb_access(req.address);

			//TLB miss penalty
			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}
	}else{
		for(int i = 0; i< inst->length ; i++){
			/*if(!inst.mask[i]){
			continue;
		}*/
			if(!(*smask)[i]){
				continue;
			}

#ifdef DEBUGMEM
			cout<<"\t>>>>Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
#endif
			
			log(logDEBUG4)<<"Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;

			mem_req req;
			req.inst_delay = 0;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.operation = operation;
			req.delay = 0;
			//req.issued = false;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;


			split_tlb_8[req.tlb_index].tlb_access(req.address);
			bool split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			bool unified_hit = unified_tlb_16->tlb_access(req.address);

			//TLB miss penalty
			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}
	}
}


void Cache::mem_request_add_dv_st(DVStore *inst, Memoperation operation, W64 current_cycle, int ins_count,std::vector<int> *smask, bool _scalarise){

	if(inst->is_sse && conf.threads_per_warp > 2){
		bool active = false;
		int del = 0;
		for(int i = 0; i< inst->length ; i = i++){
			if(!smask[0][i]){
				continue;
			}
			active = true;
#ifdef DEBUGMEM
			cout<<"Address to store "<<hex16(inst->address_params[i].address)<<endl;
#endif
			mem_req req;
			req.inst_delay = del++;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.delay = 0;
			//req.issued = false;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;
			req.operation = operation;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			split_tlb_8[req.tlb_index].tlb_access(req.address);
			int split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			int unified_hit = unified_tlb_16->tlb_access(req.address);

			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}

	}else if(inst->is_avx){
		int del = 0;
		for(int i = 0; i< inst->length ; i++){
			/*if(!inst.mask[i]){
				continue;
			}*/
			if(!smask[0][i]){
				continue;
			}
#ifdef DEBUGMEM
			cout<<"Address to store "<<hex16(inst->address_params[i].address)<<endl;
#endif
			mem_req req;
			req.inst_delay = del++;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.delay = 0;
			//req.issued = false;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;
			req.operation = operation;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			split_tlb_8[req.tlb_index].tlb_access(req.address);
			int split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			int unified_hit = unified_tlb_16->tlb_access(req.address);

			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}

	}else{
		for(int i = 0; i< inst->length ; i++){
			/*if(!inst.mask[i]){
			continue;
		}*/
			if(!smask[0][i]){
				continue;
			}
#ifdef DEBUGMEM			
			cout<<"Address to store "<<hex16(inst->address_params[i].address)<<endl;
#endif
			mem_req req;
			req.inst_delay = 0;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.delay = 0;
			//req.issued = false;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;
			req.operation = operation;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			split_tlb_8[req.tlb_index].tlb_access(req.address);
			int split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			int unified_hit = unified_tlb_16->tlb_access(req.address);

			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}
		
			mem_request_list.push_back(req);
		}
	}
}

#else
#ifdef AVX_512
void Cache::mem_request_add_dv(DVLoad *inst, Memoperation operation, W64 current_cycle, int ins_count,std::vector<int> *smask, bool _scalarise){

	if(inst->is_sse && conf.threads_per_warp > 2){
		bool del=0;
		bool active = false;
		for(int i = 0; i< inst->length ; i= i+2){
			/*if(!inst.mask[i]){
					continue;
				}*/
			if(!(*smask)[i]){
				continue;
			}

			log(logDEBUG4)<<"Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
			active = true;
			mem_req req;
			req.inst_delay = del;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.operation = operation;
			req.delay = 0;
			//req.issued = false;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;


			split_tlb_8[req.tlb_index].tlb_access(req.address);
			bool split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			bool unified_hit = unified_tlb_16->tlb_access(req.address);

			//TLB miss penalty
			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}
		if(active){
			del++;
		}
		for(int i = 1; i< inst->length ; i= i+2){
			/*if(!inst.mask[i]){
					continue;
				}*/
			if(!(*smask)[i]){
				continue;
			}

			log(logDEBUG4)<<"Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
			active = true;
			mem_req req;
			req.inst_delay = del;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.operation = operation;
			req.delay = 0;
			//req.issued = false;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;


			split_tlb_8[req.tlb_index].tlb_access(req.address);
			bool split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			bool unified_hit = unified_tlb_16->tlb_access(req.address);

			//TLB miss penalty
			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}
	}else if(inst->is_avx){
		int del = 0;
		bool active = false;
		for(int i = 0; i< inst->length ; i = i+4){
			/*if(!inst.mask[i]){
							continue;
						}*/
			if(!(*smask)[i]){
				continue;
			}
			active = true;
			log(logDEBUG4)<<"Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
			mem_req req;
			req.inst_delay = del++;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.operation = operation;
			req.delay = 0;
			//req.issued = false;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;


			split_tlb_8[req.tlb_index].tlb_access(req.address);
			bool split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			bool unified_hit = unified_tlb_16->tlb_access(req.address);

			//TLB miss penalty
			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}

		if(active){
			active = false;
			del++;
		}

		for(int i = 1; i< inst->length ; i=i+4){
			/*if(!inst.mask[i]){
									continue;
								}*/
			if(!(*smask)[i]){
				continue;
			}
			active = true;
			log(logDEBUG4)<<"Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
			mem_req req;
			req.inst_delay = del++;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.operation = operation;
			req.delay = 0;
			//req.issued = false;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;


			split_tlb_8[req.tlb_index].tlb_access(req.address);
			bool split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			bool unified_hit = unified_tlb_16->tlb_access(req.address);

			//TLB miss penalty
			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}
		if(active){
			active = false;
			del++;
		}

		for(int i = 2; i< inst->length ; i=i+4){
			/*if(!inst.mask[i]){
									continue;
								}*/
			if(!(*smask)[i]){
				continue;
			}
			active = true;
			log(logDEBUG4)<<"Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
			mem_req req;
			req.inst_delay = del++;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.operation = operation;
			req.delay = 0;
			//req.issued = false;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;


			split_tlb_8[req.tlb_index].tlb_access(req.address);
			bool split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			bool unified_hit = unified_tlb_16->tlb_access(req.address);

			//TLB miss penalty
			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}

		if(active){
			active = false;
			del++;
		}

		for(int i = 3; i< inst->length ; i=i+4){
			/*if(!inst.mask[i]){
									continue;
								}*/
			if(!(*smask)[i]){
				continue;
			}

			active = true;

			log(logDEBUG4)<<"Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
			mem_req req;
			req.inst_delay = del++;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.operation = operation;
			req.delay = 0;
			//req.issued = false;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;


			split_tlb_8[req.tlb_index].tlb_access(req.address);
			bool split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			bool unified_hit = unified_tlb_16->tlb_access(req.address);

			//TLB miss penalty
			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}

	}else{
		for(int i = 0; i< inst->length ; i++){
		
			if(!(*smask)[i]){
				continue;
			}
			
			log(logDEBUG4)<<"Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;

			mem_req req;
			req.inst_delay = 0;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.operation = operation;
			req.delay = 0;
			//req.issued = false;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;


			split_tlb_8[req.tlb_index].tlb_access(req.address);
			bool split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			bool unified_hit = unified_tlb_16->tlb_access(req.address);

			//TLB miss penalty
			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}
	}
}


void Cache::mem_request_add_dv_st(DVStore *inst, Memoperation operation, W64 current_cycle, int ins_count,std::vector<int> *smask, bool _scalarise){

	if(inst->is_sse && conf.threads_per_warp > 2){
		bool active = false;
		int del = 0;
		for(int i = 0; i< inst->length ; i = i+2){
			if(!smask[0][i]){
				continue;
			}
			active = true;
			log(logDEBUG4)<<"Address to store "<<hex16(inst->address_params[i].address)<<endl;

#ifdef DEBUGMEM
			cout<<"\t>>>>Address to store "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
#endif

			mem_req req;
			req.inst_delay = del;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.delay = 0;
			//req.issued = false;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;
			req.operation = operation;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			split_tlb_8[req.tlb_index].tlb_access(req.address);
			int split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			int unified_hit = unified_tlb_16->tlb_access(req.address);

			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}
			
			mem_request_list.push_back(req);
		}
		if(active){
			del++;
		}
		for(int i = 1; i< inst->length ; i=i+2){

			if(!smask[0][i]){
				continue;
			}

			//assert(mem_request_list[clusterid].size() <= 8);
			log(logDEBUG4)<<"Address to store "<<hex16(inst->address_params[i].address)<<endl;
#ifdef DEBUGMEM
			cout<<"\t>>>>Address to store "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
#endif
			mem_req req;
			req.inst_delay =  del;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.delay = 0;
			//req.issued = false;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;
			req.operation = operation;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			split_tlb_8[req.tlb_index].tlb_access(req.address);
			int split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			int unified_hit = unified_tlb_16->tlb_access(req.address);

			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}

	}else if(inst->is_avx){
		int del = 0;
		for(int i = 0; i< inst->length ; i++){
			/*if(!inst.mask[i]){
				continue;
			}*/
			if(!smask[0][i]){
				continue;
			}
#ifdef DEBUGMEM
			cout<<"\t>>>>Address to store "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
#endif
			log(logDEBUG4)<<"Address to store "<<hex16(inst->address_params[i].address)<<endl;

			mem_req req;
			req.inst_delay = del++;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.delay = 0;
			//req.issued = false;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;
			req.operation = operation;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			split_tlb_8[req.tlb_index].tlb_access(req.address);
			int split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			int unified_hit = unified_tlb_16->tlb_access(req.address);

			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}
			
			mem_request_list.push_back(req);
		}

	}else{
		for(int i = 0; i< inst->length ; i++){
			/*if(!inst.mask[i]){
			continue;
		}*/
			if(!smask[0][i]){
				continue;
			}
			
			log(logDEBUG4)<<"Address to store "<<hex16(inst->address_params[i].address)<<endl;

			mem_req req;
			req.inst_delay = 0;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.delay = 0;
			//req.issued = false;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;
			req.operation = operation;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			split_tlb_8[req.tlb_index].tlb_access(req.address);
			int split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			int unified_hit = unified_tlb_16->tlb_access(req.address);

			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}
			mem_request_list.push_back(req);
		}
	}
}
#else
void Cache::mem_request_add_dv(DVLoad *inst, Memoperation operation, W64 current_cycle, int ins_count,std::vector<int> *smask, bool _scalarise){

	if(inst->is_sse && conf.threads_per_warp > 2){
		bool del=0;
		bool active = false;
		for(int i = 0; i< inst->length ; i= i+2){
			/*if(!inst.mask[i]){
					continue;
				}*/
			if(!smask[0][i]){
				continue;
			}
#ifdef DEBUGMEM
			cout<<"\t>>>>Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
#endif
			log(logDEBUG4)<<"Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
			active = true;
			mem_req req;
			req.inst_delay = del;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.operation = operation;
			req.delay = 0;
			//req.issued = false;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;


			split_tlb_8[req.tlb_index].tlb_access(req.address);
			bool split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			bool unified_hit = unified_tlb_16->tlb_access(req.address);

			//TLB miss penalty
			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}
		if(active){
			del++;
		}
		for(int i = 1; i< inst->length ; i= i+2){
			/*if(!inst.mask[i]){
					continue;
				}*/
			if(!smask[0][i]){
				continue;
			}
#ifdef DEBUGMEM
			cout<<"\t>>>>Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
#endif
			log(logDEBUG4)<<"Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
			active = true;
			mem_req req;
			req.inst_delay = del;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.operation = operation;
			req.delay = 0;
			//req.issued = false;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;


			split_tlb_8[req.tlb_index].tlb_access(req.address);
			bool split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			bool unified_hit = unified_tlb_16->tlb_access(req.address);

			//TLB miss penalty
			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}
	}else if(inst->is_avx){
		int del = 0;
		for(int i = 0; i< inst->length ; i++){
			/*if(!inst.mask[i]){
							continue;
						}*/
			if(!smask[0][i]){
				continue;
			}
#ifdef DEBUGMEM
			cout<<"\t>>>>Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
#endif
			log(logDEBUG4)<<"Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
			mem_req req;
			req.inst_delay = del++;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.operation = operation;
			req.delay = 0;
			//req.issued = false;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;


			split_tlb_8[req.tlb_index].tlb_access(req.address);
			bool split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			bool unified_hit = unified_tlb_16->tlb_access(req.address);

			//TLB miss penalty
			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}
	}else{
		for(int i = 0; i< inst->length ; i++){
			/*if(!inst.mask[i]){
			continue;
		}*/
			if(!smask[0][i]){
				continue;
			}
#ifdef DEBUGMEM
			cout<<"\t>>>>Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;
#endif
			log(logDEBUG4)<<"Address to load "<<hex16(inst->address_params[i].address)<<" scalarise "<< _scalarise <<endl;

			mem_req req;
			req.inst_delay = 0;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.operation = operation;
			req.delay = 0;
			//req.issued = false;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;


			split_tlb_8[req.tlb_index].tlb_access(req.address);
			bool split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			bool unified_hit = unified_tlb_16->tlb_access(req.address);

			//TLB miss penalty
			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}
			mem_request_list.push_back(req);
		}
	}
}


void Cache::mem_request_add_dv_st(DVStore *inst, Memoperation operation, W64 current_cycle, int ins_count,std::vector<int> *smask, bool _scalarise){
	if(inst->is_sse && conf.threads_per_warp > 2){
		bool active = false;
		int del = 0;
		for(int i = 0; i< inst->length ; i = i+2){
			if(!smask[0][i]){
				continue;
			}
			active = true;
			log(logDEBUG4)<<"Address to store "<<hex16(inst->address_params[i].address)<<endl;

			mem_req req;
			req.inst_delay = del;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.delay = 0;
			//req.issued = false;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;
			req.operation = operation;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			split_tlb_8[req.tlb_index].tlb_access(req.address);
			int split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			int unified_hit = unified_tlb_16->tlb_access(req.address);

			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}
		if(active){
			del++;
		}
		for(int i = 1; i< inst->length ; i=i+2){

			if(!smask[0][i]){
				continue;
			}

			//assert(mem_request_list[clusterid].size() <= 8);
			log(logDEBUG4)<<"Address to store "<<hex16(inst->address_params[i].address)<<endl;

			mem_req req;
			req.inst_delay =  del;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.delay = 0;
			//req.issued = false;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;
			req.operation = operation;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			split_tlb_8[req.tlb_index].tlb_access(req.address);
			int split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			int unified_hit = unified_tlb_16->tlb_access(req.address);

			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}

	}else if(inst->is_avx){
		int del = 0;
		for(int i = 0; i< inst->length ; i++){
			/*if(!inst.mask[i]){
				continue;
			}*/
			if(!smask[0][i]){
				continue;
			}

			log(logDEBUG4)<<"Address to store "<<hex16(inst->address_params[i].address)<<endl;

			mem_req req;
			req.inst_delay = del++;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
			assert(req.size > 0);
			//req.tid = i;
			req.delay = 0;
			//req.issued = false;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;
			req.operation = operation;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			split_tlb_8[req.tlb_index].tlb_access(req.address);
			int split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			int unified_hit = unified_tlb_16->tlb_access(req.address);

			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}
			mem_request_list.push_back(req);
		}

	}else{
		for(int i = 0; i< inst->length ; i++){
			if(!smask[0][i]){
				continue;
			}
#ifdef DEBUGMEM			
			cout<<"Address to store "<<hex16(inst->address_params[i].address)<<endl;
#endif
			mem_req req;
			req.inst_delay = 0;
			req.issued = false;
			req.scalarise = _scalarise;
			req.address= inst->address_params[i].address;
			req.size = inst->address_params[i].address_size;
#ifdef DEBUG
			cout << "Address = " << hex16(req.address) << ", size = " << req.size << endl;
#endif
			assert(req.size > 0);
			//req.tid = i;
			req.delay = 0;
			//req.issued = false;
			req.block_address = extract_value(req.address, byte_offset,63);
			req.tid = inst->address_params[i].tid;
			req.tlb_index = inst->address_params[i].tid % conf.threads_per_warp;
			req.operation = operation;
#ifdef V_SCALARISE
			req.ins_count = inst->inst_remaining;
#else
			req.ins_count = ins_count;
#endif

			split_tlb_8[req.tlb_index].tlb_access(req.address);
			int split_hit = split_tlb_16[req.tlb_index].tlb_access(req.address);
			unified_tlb_8->tlb_access(req.address);
			int unified_hit = unified_tlb_16->tlb_access(req.address);

			if(conf.base){
				if(!unified_hit){
					req.delay = 10;
				}
			}else{
				if(!split_hit){
					req.delay = 10;
				}
			}

			mem_request_list.push_back(req);
		}
	}
}
#endif


 
/*Data could be across multiple banks b1 and b2 gives the first bank. Now we check if any of the banks actually overlap*/
bool Cache::isOverlappingBanks(int b1, int num_b1, int b2, int num_b2, int num_banks){
	//num_b1 = num_b1 - 1;
	//num_b2 = num_b2 - 1;
	//cout<<"B1 "<<b1<<" numb1 "<<num_b1<<" B2 "<<b2<<" numb2 "<<num_b2<<endl;
	if((num_b1 == 0) && (num_b2 == 0)){
		if(b1 == b2){
			return true;
		}else{
			return false;
		}
	}else{
		int rot1 = (b1 + num_b1)/num_banks;
		int rot2 = (b2 + num_b2)/num_banks;
		if((rot1 == 1) && ( rot2 == 1)){
			return true;
		}else if(rot1 == 1 || rot2 == 1){
			if(b1 < b2){
				if(((b1 + num_b1) >= b2) || (b1 <=  (b2 + num_b2) % num_banks)){
					return true;
				}
				return false;
			}else{
				if(((b2 + num_b2) >= b1) || (b2 <=  (b1 + num_b1) % num_banks)){
					return true;
				}
				return false;
			}
		}else{
			if(b1 < b2){
				if((b1 + num_b1) >= b2 ){
					return true;
				}
				return false;
			}else{
				if((b2 + num_b2) >= b1 ){
					return true;
				}
				return false;
			}
		}
	}
}

bool Cache::bank_conflict(LONG address, int tid, W64 current_cycle, int fuid, int refcount, W64 issue_cycle, int size){
	//int bank = extract_value(address,4,5);
	//int bank2;
	//cout<<"Bank "<<bank<<" Address "<<hex16(address)<<" Cycle "<<current_cycle<<" cluster "<<clusterid<<endl;
	std::vector<mem_req>::iterator   x;

	if(size > 256){// Seeing some prefetch instructions 512 bits
		assert(false);
		return false;
	}
	int bank_width = 32; //Bits 64byte cache line with 16 banks is 4byte per bank (32 bit)
	int num_banks = 16; //4 bits so 16 banks
	//int req_banks = size/bank_width;
	//log(logDEBUG4)<<" gatherscatter_ "<<endl;
	for(x = mem_request_list.begin(); x != mem_request_list.end(); x++){
		LONG mem = x->address;
		//if(extract_value(address,4,5) == (extract_value(mem,4,5))){
		//if(intbitXor(intbitXor(extract_value(address,12,13),extract_value(address,24,25)),intbitXor(extract_value(address,14,15), extract_value(address,26,27))) == intbitXor(intbitXor(extract_value(mem,12,13),extract_value(mem,24,25)),intbitXor(extract_value(mem,14,15), extract_value(mem,26,27)))){
		//if(intbitXor(extract_value(address,12,15),extract_value(address,24,27)) == (intbitXor(extract_value(mem,12,15),extract_value(mem,24,27)))){
		//cout<<"SZ "<<size<<endl;
		if(isOverlappingBanks(extract_value(address,3,6),(size - 1)/bank_width, extract_value(mem,3,6),(x->size - 1)/bank_width,num_banks)){ //Lower order bits
			//if(isOverlappingBanks(intbitXor(extract_value(address,12,15),extract_value(address,24,27)),((size - 1)/bank_width),intbitXor(extract_value(mem,12,15),extract_value(mem,24,27)),(x->size - 1)/bank_width, num_banks)){
			//cout<<"Conflict Bank "<<bank2<<" Address "<<hex16(*x)<<" Cycle "<<current_cycle<<endl;
			return true;
		}

	}
	return false;
}

//Returns number of bank conflicts
int Cache::bank_conflict_new(){
#ifdef MULTI_CORE
	int bank_width = 128; //Bits 64byte cache line with 16 banks is 4byte per bank (32 bit)
	int num_banks = 4;
#else
	int bank_width = 32; //Bits 64byte cache line with 16 banks is 4byte per bank (32 bit)
	int num_banks = 16;
#endif
	int ls_count = 0;
	int ls_bank[num_banks];
	for(int i = 0 ; i < num_banks; i++){
		ls_bank[i] = 0;
	}

	bool issue = false;
	int inst_count = 0;
	std::vector<mem_req>::iterator   x;
#ifdef V_SCALARISE
	for(x = mem_request_list.begin(); x != mem_request_list.end(); x++){
		if(x->issued){
			continue;
		}
		issue = true;
		ls_count++;
		LONG mem = x->address;
		//x->delay = 0;
		int delay = 0;
		x->bankindexhashed = intbitXor(extract_value(mem,12,15),extract_value(mem,24,27));
		if(conf.base){
			for(int i = 0 ;i < ((x->size -1)/bank_width) + 1; i++ ){

				ls_bank[(x->bankindexhashed+i) % num_banks]++; //Banks occupied by a memory request
				if(!conf.base){
					if(ls_bank[(x->bankindexhashed+i) % num_banks] > delay){
						delay = ls_bank[(x->bankindexhashed+i) % num_banks];
					}
				}
			}
		}else if(x->is_sse){
			for(int i = 0 ;i < ((x->size -1)/bank_width) + 1; i++ ){
				ls_bank[(x->bankindexhashed+i) % num_banks]++; //Banks occupied by a memory request
				if(!conf.base){
					if(ls_bank[(x->bankindexhashed+i) % num_banks] > delay){
						delay = ls_bank[(x->bankindexhashed+i) % num_banks];
					}
				}
			}

		}else if(x->is_avx){ //Scalarise the loads 2 xmm
			for(int i = 0 ;i < (((x->size/2) -1)/bank_width) + 1; i++ ){
				ls_bank[(x->bankindexhashed+((x->word*x->size/2)+i)) % num_banks]++; //Banks occupied by a memory request
				if(!conf.base){
					if(ls_bank[(x->bankindexhashed+i) % num_banks] > delay){
						delay = ls_bank[(x->bankindexhashed+i) % num_banks];
					}
				}
			}
			x->word++;
		}else{
			for(int i = 0 ;i < ((x->size -1)/bank_width) + 1; i++ ){

				ls_bank[(x->bankindexhashed+i) % num_banks]++; //Banks occupied by a memory request
				if(!conf.base){
					if(ls_bank[(x->bankindexhashed+i) % num_banks] > delay){
						delay = ls_bank[(x->bankindexhashed+i) % num_banks];
					}
				}
			}
		}
		if(!conf.base){
			x->delay += (delay-1);
			assert(x->delay >=0);
		}

		if(x->is_sse || x->is_avx){
			inst_count++;
			if(inst_count == 2){ // Load Max 2 inst in a cycle
				break;
			}
		}

		//x->issued  = true; //We are setting this after the call to hashed bank
		/*if(x->scalarise){
			break;
		}*/
	}
#else

	for(x = mem_request_list.begin(); x != mem_request_list.end(); x++){
		if(x->issued){
			continue;
		}
		if(x->inst_delay > 0){
			continue;
		}
		issue = true;
		ls_count++;
		LONG mem = x->address;
		//x->delay = 0;
		int delay = 0;
		x->bankindexhashed = intbitXor(extract_value(mem,12,15),extract_value(mem,24,27));
		for(int i = 0 ;i < ((x->size -1)/bank_width) + 1; i++ ){

			ls_bank[(x->bankindexhashed+i) % num_banks]++; //Banks occupied by a memory request
			if(!conf.base){
				if(ls_bank[(x->bankindexhashed+i) % num_banks] > delay){
					delay = ls_bank[(x->bankindexhashed+i) % num_banks];
				}
			}
		}
		if(!conf.base){
			x->delay += (delay-1);
			assert(x->delay >=0);
		}
		//x->issued  = true; //We are setting this after the call to hashed bank
		/*if(x->scalarise){
			break;
		}*/
	}
#endif
	int max = 0;
	for(int i = 0 ;i< num_banks;i++){
		if(max < ls_bank[i]){
			max = ls_bank[i];
		}
	}
	if(!conf.base){
		for(x = mem_request_list.begin(); x != mem_request_list.end(); x++){
			if(x->inst_delay > 0){
				x->inst_delay += max;
			}
		}
	}

	if(issue){
		int bankconflictpenalty = max - 1;
		assert(bankconflictpenalty >= 0);
		return bankconflictpenalty;
	}else{
		return 0;
	}
	//printf("\n Cycles %d \n",(num + 2 - 1) / 2);
	//return (2.0*inter) - 1.0;
}


int Cache::bank_conflict_orig_new(){
	int bank_width = 32; //Bits 64byte cache line with 16 banks is 4byte per bank (32 bit)
	int num_banks = 16;
	int ls_count = 0;
	int ls_bank[num_banks];
	for(int i = 0 ; i < num_banks; i++){
		ls_bank[i] = 0;
	}
	bool issue = false;
	int inst_count = 0;
	std::vector<mem_req>::iterator   x;

	for(x = mem_request_list.begin(); x != mem_request_list.end(); x++){
		if(x->issued){
			continue;
		}
		if(x->inst_delay > 0){
			x->inst_delay--;
			continue;
		}

		ls_count++;
		issue = true;
		LONG mem = x->address;
		int delay = 0;
		x->bankindexlow = extract_value(mem,3,6);
		for(int i = 0 ;i < ((x->size -1)/bank_width) + 1; i++ ){
			ls_bank[(x->bankindexlow+i) % num_banks]++; //Banks occupied by a memory request
			if(conf.base){
				if(ls_bank[(x->bankindexlow+i) % num_banks] > delay){
					delay = ls_bank[(x->bankindexlow+i) % num_banks];
#ifdef DEBUG
					cout << "[Cache] Calculated delay for access  = " << delay << endl;
#endif
				}
			}
		}
		if(conf.base){
			x->delay += (delay-1);

			assert(x->delay >=0);
		}
		x->issued  = true;
		
	}

	int max = 0;
	for(int i = 0 ;i< num_banks;i++){
		if(max < ls_bank[i]){
			max = ls_bank[i];
		}
	}
	if(conf.base){
		for(x = mem_request_list.begin(); x != mem_request_list.end(); x++){
			if(x->inst_delay > 0){
				x->inst_delay += max;
			}
		}
	}
	
	if(issue){
		int bankconflictpenalty = max - 1;
		assert(bankconflictpenalty >= 0);
		return bankconflictpenalty;
	}else{
		return 0;
	}

}

bool Cache::bank_conflict_reduced(LONG address, int tid, W64 current_cycle, int fuid, int refcount, W64 issue_cycle, int size){
	//int bank = extract_value(address,4,5);
	//int bank2;
	//cout<<"Bank "<<bank<<" Address "<<hex16(address)<<" Cycle "<<current_cycle<<" cluster "<<clusterid<<endl;
	std::vector<mem_req>::iterator   x;

	if(size > 256){// Seeing some prefetch instructions 512 bits
#ifdef DEBUG
		cout<<"Address size "<<size<<endl;
#endif
		return false;
	}
	int bank_width = 32; //Bits 64byte cache line with 16 banks is 4byte per bank (32 bit)
	int num_banks = 16; //4 bits so 16 banks
	//int req_banks = size/bank_width;
	//log(logDEBUG4)<<" gatherscatter_ "<<endl;
	for(x = mem_request_list.begin(); x != mem_request_list.end(); x++){
		LONG mem = x->address;
		//if(extract_value(address,4,5) == (extract_value(mem,4,5))){
		//if(intbitXor(intbitXor(extract_value(address,12,13),extract_value(address,24,25)),intbitXor(extract_value(address,14,15), extract_value(address,26,27))) == intbitXor(intbitXor(extract_value(mem,12,13),extract_value(mem,24,25)),intbitXor(extract_value(mem,14,15), extract_value(mem,26,27)))){
		//if(intbitXor(extract_value(address,12,15),extract_value(address,24,27)) == (intbitXor(extract_value(mem,12,15),extract_value(mem,24,27)))){
		//cout<<"SZ "<<size<<endl;
		//if(isOverlappingBanks(extract_value(address,3,6),(size - 1)/bank_width, extract_value(mem,3,6),(x->size - 1)/bank_width,num_banks)){ //Lower order bits
		if(isOverlappingBanks(intbitXor(extract_value(address,12,15),extract_value(address,24,27)),((size - 1)/bank_width),intbitXor(extract_value(mem,12,15),extract_value(mem,24,27)),(x->size - 1)/bank_width, num_banks)){
			//cout<<"Conflict Bank "<<bank2<<" Address "<<hex16(*x)<<" Cycle "<<current_cycle<<endl;
			return true;
		}

	}
	return false;
}

bool Cache::tag_conflict(LONG address, int tid, W64 current_cycle, int fuid, int refcount, W64 issue_cycle){
	int tag_bank = extract_value(address,9,10);
	int tag_bank2;
	//cout<<"Tag Bank "<<tag_bank<<"Address "<<hex16(address)<<"Cycle "<<current_cycle<<" cluster "<<clusterid<<endl;
	std::vector<mem_req>::iterator   x;
	//log(logDEBUG4)<<" gatherscatter_ "<<endl;
	for(x = mem_request_list.begin(); x != mem_request_list.end(); x++){
		LONG mem = x->address;
		if(tag_bank == (tag_bank2 = extract_value(mem,9,10))){
			//cout<<"Conflict Bank "<<tag_bank2<<" Address "<<hex16(*x)<<" Cycle "<<current_cycle<<endl;
			return true;
		}

	}
	return false;
}


/*bool Cache::poll_cache_new(LONG address, int tid, W64 current_cycle, Memoperation operation, int &cache_level, LONG &avail_cycle){
	W64 tag = extract_value(address, index_offset,63);
	int index = (int)extract_value(address, byte_offset,index_offset - 1) ;

}*/
void Cache::memory_access(mem_req memreq){
	int tid = 0; //Virtual Address can be shared
	W64 block = extract_value(memreq.address, byte_offset,63);
	//int index = (int)extract_value(memreq.address, byte_offset,index_offset - 1) ;

	cache_miss_list[tid].push_back(Request(block,cache_cycle + MEM_LATENCY_DVSIM + ((memreq.ins_count -1)*2), memreq));
}
bool Cache::access_new(mem_req memreq){
	int tid = 0; //Virtual Address can be shared
	W64 tag = extract_value(memreq.address, index_offset,63);
	W64 block_address = extract_value(memreq.address, byte_offset,63);
	int index = (int)extract_value(memreq.address, byte_offset,index_offset - 1) ;
	assert(index < cache_line_numbers/cache_ways);

	if(memreq.operation == MEM_STORE){
		stats->cache_write_access[cache_level]++;
	}else{
		stats->cache_read_access[cache_level]++;
	}

	std::vector<Request>::const_iterator it_hit = std::find(cache_hit_list[tid].begin(), cache_hit_list[tid].end(), Request(block_address,0, memreq)); //If HIT
	if(it_hit != cache_hit_list[tid].end()){
#ifdef DEBUG
		cout<<"Was in  Hit "<<hex16(memreq.address)<<" level "<<cache_level<<" cycle "<<cache_cycle<<" block "<<block_address<<endl;
#endif
		//return true;
		//return true;
		/*if(cache_level == NUM_CACHE){
			assert(false);
		}*/
		return true;
	}else{
		std::vector<Request>::const_iterator it_miss = std::find(cache_miss_list[tid].begin(), cache_miss_list[tid].end(), Request(block_address,0, memreq)); //if prior MISS
		if(it_miss != cache_miss_list[tid].end()){
#ifdef DEBUG
			cout<<"Was in  miss "<<hex16(memreq.address)<<" level "<<cache_level<<" cycle "<<cache_cycle<<" block "<<block_address<<endl;
#endif
			return false;
		}

		if(cache_set[index].exist(tag)){ //if tag exists in L1
		
			if(memreq.operation == MEM_STORE){
				//line.dirty= true;
				cache_set[index].get(tag,true); //cache line is dirty
			}else{
				cache_set[index].get(tag,false); //cache line is clean, retrieve block
				//line.dirty = false;
			}
		
			assert(cache_hit_list[tid].size()<1000);
#ifdef SCALARISE
			cache_hit_list[tid].push_back(Request(block_address, cache_cycle + cache_latency+((memreq.ins_count-1)*2), memreq)); ////<--- cache_hit_list
#else
			cache_hit_list[tid].push_back(Request(block_address, cache_cycle + cache_latency, memreq));
#endif
			return true;
		}else{ //MISS with no prior miss
			if(cache_level != NUM_CACHE){ //L1 miss only
				memreq.delay = 1; // 1 cycle delay to send address
				cache_miss_list[tid].push_back(Request(block_address, 0,memreq)); //<---- cache_miss_list
#ifdef DEBUG
				cout<<"L1miss "<<hex16(memreq.address)<<" level "<<cache_level<<" cycle "<<cache_cycle<<" block "<<block_address<<endl;
#endif
				assert(l2_cache->mem_request_list.size()<1000);
				l2_cache->mem_request_list.push_back(memreq);
				stats->cache_miss_nonscalar[cache_level]++;
				if(memreq.operation == MEM_STORE){
					stats->cache_write_miss_nonscalar[cache_level]++;
				}else{
					stats->cache_read_miss_nonscalar[cache_level]++;
				}
				return false;
			}else{ //L2 miss
				stats->cache_miss_nonscalar[cache_level]++;
				if(memreq.operation == MEM_STORE){
					stats->cache_write_miss_nonscalar[cache_level]++;
				}else{
					stats->cache_read_miss_nonscalar[cache_level]++;
				}
				memreq.delay = MEM_ACCESS_DELAY;
#ifdef DEBUG
				cout<<"L2miss "<<hex16(memreq.address)<<" level "<<cache_level<<" cycle "<<cache_cycle<<" block "<<block_address<<endl;
#endif
				//analyser->statistics.l2_miss++;
				assert(llc_miss_list.size()<1000);
				llc_miss_list.push_back(memreq);
				return false;
			}
		}
	}
}

#endif
#endif




