#ifndef SASIM_ENGINE_HPP
#define SASIM_ENGINE_HPP

#include <set>
#include <math.h>

#include "sasim_uarch_defs.hpp"
#include "sasim_uop_inst.hpp"
#include "sasim_defs.hpp"
#include "sasim_register_renaming.hpp"
#include "sasim_steering.hpp"
#include "sasim_branchpred.hpp"
#include "sasim_store_sets.hpp"
//#include "sasim_tlb_translation.hpp"
//#include "sasim_memory.hpp"

// ***************************************************************************
// Begin: sasim class

class sasim {
 public:
  int64_t ninst;
  int64_t nuops;
  int64_t cycle;

  register_renaming rr;
  register_renaming ret_rr; // updated at retirement, for repairing 

  sasim_inst storage[MAXWINDOW];
  saqueue<inst_ptr> window;
  saqueue<inst_ptr> freepool;

  sa_addr currentline;
  saqueue<inst_ptr> predq;
  saqueue<inst_ptr> feq; // artificial queue for simulating front-end pipe stages

  saqueue<uop_ptr> rob;
  int rob_loads; // number of loads in the ROB

  saqueue<uop_ptr> beq; // artificial queue for simulating back-end pipe stages
  
  saqueue<uop_ptr> ldq[NUM_LDQ]; 
  saqueue<uop_ptr> stq;
  saqueue<uop_ptr> prstq; // post-retirement store queue
  
#ifdef STEERING_ROUNDROBIN
  steering_rr steer;
#else
  steering_dep steer;
#endif
  
  saqueue<uop_ptr> intq[NUM_ALU]; 
  saqueue<uop_ptr> fpq[NUM_FPADDMUL]; 
  saqueue<uop_ptr> alu[NUM_ALU];
  saqueue<uop_ptr> fpaddmul[NUM_FPADDMUL]; // assume all fp ops are add or mul (FIXME)
  saqueue<uop_ptr> fpmov[NUM_FPADDMUL];
  saqueue<uop_ptr> imul;
  saqueue<uop_ptr> idiv;
  saqueue<uop_ptr> fpdiv;

  bpred_full bp;
  inst_ptr pendingmisp;

  store_sets ss;

  sasim_translation pagerename;
  sasim_tlb tlb;

  sasim_cache_il1 il1;
  sasim_cache_dl1 dl1;
  sasim_cache_l2 l2;
#ifdef ENABLE_L3
  sasim_cache l3;
#endif
  sasim_memory mem;

#ifdef STRIDE_PREFETCH
  stride_prefetcher spf;
#endif

  int64_t prefetchinst;
  int64_t last_retire_cycle;

  int64_t numloads;
  int64_t numstores;
  int64_t fpwriteint;
  int64_t fpreadint;
  int64_t fpsseonint;
  int64_t nfpuops;
  int64_t artificialdelay;
  int64_t nnops;
  int64_t fpmovs;
  int64_t nfpsse;
  int64_t fpother;
  int64_t nshift;
  int64_t nimul;
  int64_t nidiv;
  int64_t nfpdiv;
  int64_t nintlong;
  int64_t nfplong;
  int64_t nbranchmisp;
  int64_t minexecdelay;
  int64_t minretiredelay;
  int64_t nmemtrap;
  int64_t nbytes_problem;

  sasim();
  void init();
  void debugprint(sa_inst & inst, sa_addr raddr[], sa_addr waddr[]);
  uop_ptr find_uop_in_rob(int64_t uopnum);
  uop_ptr find_uop_in_prstq(int64_t uopnum);
  void input(sa_inst & inst, sa_addr raddr[], sa_addr waddr[], sa_addr sp_addr, bool is_branch_taken);
  void printstats(FILE * stream);
  void printconfig(FILE * stream);
  void drain();
  void schedule_load(uop_ptr dl1_uptr[DL1_PORTS]);
  void schedule_store(uop_ptr dl1_uptr[DL1_PORTS]);
  bool check_memdepready(uop_ptr lduop);
  int64_t check_forwarding(uop_ptr lduop);
  void check_memtrap(uop_ptr stuop);
  void flush(bool memtrap);
  void onecycle();
  void onecycle_pred();
  void onecycle_ifetch();
  void onecycle_decode();
  void onecycle_dispatch();
  void onecycle_schedule();
  void onecycle_execute_mem();
  void onecycle_execute_int();
  void onecycle_execute_fp();
  void onecycle_execute();
  void onecycle_retire();
  void onecycle_post_retirement();
};

// End: sasim class
// ***************************************************************************

#endif // End ifndef SASIM_ENGINE_HPP
