#ifndef SASIM_ATC_HPP
#define SASIM_ATC_HPP

#include "sasim_defs.hpp"

#ifdef USE_ATC
#include "./atc.h"

class astream {
 public:
  struct atc fileaddr;

  void open(char atcmode, const char filename[], const char ext[], const char command[]);
  void close();
  void write(sa_addr addr) ;
  bool read(sa_addr & addr);
};
#endif

#endif // End ifndef SASIM_ATC_HPP
