#include "SimpleHeuristic.hpp"

void Sync::choose_threads(){

	int ref = -1;
	for (UINT i=0; i<num_threads; i++){
		if (!t[i].fetch_qfull && !t[i].fetch_stall && !t[i].ignore && !t[i].fetch_qempty){
			if ( ref == -1 || t[i].fetch_sp < t[ref].fetch_sp || ( t[i].fetch_sp == t[ref].fetch_sp && t[i].fetch_pc < t[ref].fetch_pc ) ){
				ref = i;
			}
		}
	}
	if(ref == -1){
		for (UINT i=0; i<num_threads; i++){
			t[i].fetch_isactive = false;
		}
		return;
	}

	for (UINT i=0; i<num_threads; i++){
		t[i].fetch_isactive = !t[i].fetch_qfull && t[i].fetch_pc == t[ref].fetch_pc && !t[i].fetch_stall && !t[i].ignore && !t[i].fetch_qempty;
		t[i].active_last_fetch = t[i].fetch_isactive;
	}

}

bool Sync::lockstep(){
	return true;
}
