#ifndef SASIM_DEFS_HPP
#define SASIM_DEFS_HPP

#include <cstdio>	// fprintf() and related functions
#include <cstdlib>	// abort() and related functions
#include <stdint.h>	// Data types such as int64_t, uint64_t, and so on

// ***************************************************************************
// Begin: Simulator Definitions

using namespace std;

#define USE_ATC

// End: Simulator Definitions
// ***************************************************************************

// ***************************************************************************
// Begin: Internals Definitions

// The following preprocessor macro is passed in the makefile if the ASSERT must be disabled
#ifndef NO_ASSERT
 #define SASSERT(cond) if (!(cond)) {fprintf(stderr,"\nSasim Execution Aborted.\nFile %s assert at line %d\n",__FILE__,__LINE__); abort();}
 // #warning Assert has been enabled
#else
 #define SASSERT(cond)
 // #warning Assert has been disabled
#endif

#define VERBOSE(x)
#define ISPOW2(n) (((n)&((n)-1))==0)
#define ALIGNED(addr,nbytes) (((addr) & ((nbytes)-1))==0)
#define OVERLAP(a1,n1,a2,n2) ((((a1)+(n1))>(a2)) && (((a2)+(n2))>(a1)))

#define INCPTR(ptr,size)			\
{						\
  ptr++;					\
  if (ptr==size) {				\
    ptr = 0;					\
  }						\
}

#define DECPTR(ptr,size)			\
{						\
  ptr--;					\
  if (ptr==(-1)) {				\
    ptr = size-1;				\
  }						\
}

#define INCSAT(ctr,max)				\
{						\
  ctr++;					\
  if (ctr > (max)) {				\
    ctr = max;					\
  }						\
}

#define DECSAT(ctr,min)				\
{						\
  ctr--;					\
  if (ctr < (min)) {				\
    ctr = min;					\
  }						\
}

#define SAMAX(a,b) 	(((a)>(b))? (a):(b))
#define SAMIN(a,b) 	(((a)<(b))? (a):(b))
#define IS_EVEN(x) 	(((x) & 1)==0)
#define LROT32(x) 	(((x) << 1) ^ ((x) >> 31))
#define RROT32(x) 	(((x) >> 1) ^ ((x) << 31))
#define NOTHING 	((int64_t)(-1))

typedef uint64_t sa_addr;
typedef int sa_reg;


#define CROSSLINE(addr,nbytes) ((((addr) & (CACHELINE_BYTES-1))+(nbytes)) > CACHELINE_BYTES)
#define CROSSPAGE(lineaddr,step) ((((lineaddr) & (PAGESIZE_LINES-1))+(step)) >= PAGESIZE_LINES)
#define SAMEPAGE(lineaddr1,lineaddr2) ((lineaddr1/PAGESIZE_LINES)==(lineaddr2/PAGESIZE_LINES))

#define MAXFNAMELEN 256

#define WRITEFILE(fd,data)			\
{						\
  int n = fwrite(&(data),sizeof(data),1,fd);	\
  if (n!=1) SASSERT(0);				\
}

#define READFILE(fd,data)			\
{						\
  int n = fread(&(data),sizeof(data),1,fd);	\
  if (n!=1) SASSERT(0);				\
}

#define DBPRINTQ(qname)				\
{						\
  if (qname.full()) {				\
    cerr << "full " << #qname << endl;		\
  }						\
  if (qname.empty()) {				\
    cerr << "empty " << #qname << endl;		\
  }						\
}

// End: Internals Definitions
// ***************************************************************************

#endif // End ifndef SASIM_DEFS_HPP
