/*
 * globals.hpp
 *
 *  Created on: Dec 3, 2013
 *      Author: skalathi

    New version created Oct 28, 2016
	Author: atino	
 */

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <algorithm>
#include <assert.h>
#include <bitset>
#include <stdlib.h>
#include <iterator>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <limits.h>
#include <deque>
#include "../Types.hpp"
#include <list>
#include <unordered_map>
#include <fstream> 
#include "TLBsim.hpp"
#include <boost/array.hpp>
#include "dviq.hpp"
#include "functional_units.hpp"
#include "register.hpp"

//#define DEBUG 1 //<----------------- uncomment for debug statements
//#define DEBUGFETCH 1
//#define DEBUGEXE 1
//#define DEBUGBR 1
//#define DEBUGMEM
//#define FREE_PHI 1 //also uncomment if running SMT configurations
//#define MEM_TRACE

//#define REAL_MULTI_CORE
//#define MULTI_CORE
//#define SAMPLING
#define PIPELINED_FU
#define SCALARISE  ////////////////////////????????????
//#define SECOND_CHANCE

#define ALTER

#define SA_TRACER
//#define SA_TRACER_UOP

#ifdef SA_TRACER
#include "../Sasim/src/sasim_uop_inst.hpp"
#include "../Sasim/src/sasim_tracer.hpp"
#endif


#ifndef GLOBALS_HPP_
#define GLOBALS_HPP_


#define MAX_QWORD_SIZE 6
#define BRP_SIZE 1024 //Branch predictor size

#define TAGE_PREDICTOR  //keep regardless of prediction mechanism
//#define PERFECT_BRANCH  //comment if not perfect branch
//#define PERFECT_DIVERGENCE //comment if not perfect divergence
//#define REV_BIMODAL 	//uncomment if using revised bimodal for div predictor



//#define PAUSE_THREAD

#ifdef TAGE_PREDICTOR
#include "predictor.h"
#endif

#define MSHR_SIZE 128

//#define V_SCALARISE //NO RF bank skewing
//#define AVX_512
//#define MEM_ACCESS_DELAY 256
#define MEM_ACCESS_DELAY 64 //(2GB/s) <------------------------------------
//#define MEM_ACCESS_DELAY  4 //(16GB/s)

using namespace std;
using std::vector;
//using std::deque;
using std::string;

#ifndef _LOGGER_HPP_
#define _LOGGER_HPP_

enum Loglevels
{logERROR, logWARNING, logINFO, logDEBUG, logDEBUG1, logDEBUG2, logDEBUG3, logDEBUG4, logDEBUG5};

enum TraceLevel
{NoTRACE, pathTRACE};

enum QueueTypes{BR, PHI, CUOP, FP, INT, MEM, NO_QUEUE_TYPES};

enum INSERT_TYPE {SAME,RECONV,NEW, DIV}; //SAME means normal exe

#define MAX_OPERANDS 6
//#define ISSUE_WIDTH 4

//#define ISSUE_COMBINE
#define NO_COMBINE_WITHIN_THREAD
#define NON_BLOCKING_CACHE

#define NUM_CACHE 2 //L1 & L2
#define MEM_LATENCY_DVSIM 210 //<------------------------- 8

#define FU_ID 100

#define ROB_ENTRIES 256

//#define LONG_ASSERTS


//#define ZERO_LATENCY
//#define ENABLE_EXECUTION_PATH_TRACE


enum inst_type {UNKNOWN,DYN_VEC,NO_VEC};

typedef enum{
	MEM_LOAD,
	MEM_STORE,
	MEM_NONE
}Memoperation;

#ifndef SA_TRACER
typedef enum {
	SA_UOP_UNKNOWN,
	SA_UOP_SYSCALL,
	SA_UOP_MAGIC,
	SA_UOP_NOP,
	SA_UOP_ALU,
	SA_UOP_MOV,
	SA_UOP_BRANCH,
	SA_UOP_SHIFT,
	SA_UOP_ADDR,
	SA_UOP_LOAD,
	SA_UOP_STORE,
	SA_UOP_FP,
	SA_UOP_SSE,
	SA_UOP_AVX,
	SA_UOP_SSE2,
	SA_UOP_MMX,
	SA_UOP_IMUL,
	SA_UOP_IDIV,
	SA_UOP_INTLONG,
	SA_UOP_FPDIV,
	SA_UOP_FPMUL,
	SA_UOP_FPLONG,
	SA_UOP_LAST,
	SA_UOP_POP,
	SA_UOP_PUSH,
	SA_UOP_SEMAPHORE,
	SA_UOP_SYSTEM,
	//SA_UOP_COMBINED,  //New uop
	SA_UOP_FREE
} uoptype;

#endif
struct Config{

	int issue_width;
	int issue_entries;
	int num_fp_fus;
	int num_int_fus;
	int num_mem_fus;
	int commit_width;
	int rob_size;
	int iq_size;

	bool issue_combine;
	//int num_clusters;
	int sim_insts;
	bool scalarize;
	int alu;
	int vector_fu;
	int fp_count;
	int num_ports;
	int num_threads;
	int fetch_width;
	int fetch_q_size;
	int cache_warm;
	int dviq_size;
	bool base;
	bool pause_thread;
	string outputfile;
	string csv;
	string benchmark;
	string periodicfile;
	string primary_heuristic;
	string secondary_heuristic;
	bool use_secondary_heuristic;
	bool split_on_block;
	bool icount;
	bool primary_priority_iqfull;
	int  num_warps;
	int threads_per_warp;
	//#ifdef REAL_MULTI_CORE
	int threads_per_core;
	//#endif
#ifdef SAMPLING
	int sampling_size;
#endif
#ifdef MULTI_CORE
	int coreid;
#endif
public:
	Config(){
		//cout<<"CONFIG constructor"<<endl;
		outputfile = "abc";
		csv = "abcde";
		pause_thread = false;
	};
	Config(const Config& rhs ) {
#ifdef SAMPLING
		this->sampling_size = rhs.sampling_size;
#endif

#if defined(MULTI_CORE)
		this->coreid = rhs.coreid;
#else
		//#elif defined (REAL_MULTI_CORE)
		this->threads_per_core = rhs.threads_per_core;
#endif
		this->pause_thread = rhs.pause_thread;
		this->outputfile = rhs.outputfile;
		this->csv = rhs.csv;
		this->benchmark = rhs.benchmark;
		this->periodicfile = rhs.periodicfile;
		this->num_threads = rhs.num_threads;
		this->num_ports = rhs.num_ports;
		this->alu = rhs.alu;
		this->vector_fu = rhs.vector_fu;
		this->fp_count = rhs.fp_count;
		this->issue_width = rhs.issue_width;
		this->commit_width = rhs.commit_width;
		this->issue_entries = rhs.issue_entries;
		this->issue_combine = rhs.issue_combine;
		this->num_fp_fus = rhs.num_fp_fus;
		this->num_int_fus = rhs.num_int_fus;
		this->num_mem_fus = rhs.num_mem_fus;
		this->rob_size = rhs.rob_size;
		this->iq_size = rhs.iq_size;

		//this->num_clusters = rhs.num_clusters;
		this->sim_insts = rhs.sim_insts;
		this->scalarize = rhs.scalarize;
		this->fetch_width = rhs.fetch_width;
		this->fetch_q_size = rhs.fetch_q_size;
		this->cache_warm = rhs.cache_warm;
		this->dviq_size = rhs.dviq_size;
		this->base = rhs.base;
		this->primary_heuristic = rhs.primary_heuristic;
		this->secondary_heuristic = rhs.secondary_heuristic;
		this->use_secondary_heuristic = rhs.use_secondary_heuristic;
		this->split_on_block = rhs.split_on_block;
		this->icount = rhs.icount;
		this->primary_priority_iqfull = rhs.primary_priority_iqfull;
		this->num_warps = rhs.num_warps;
		this->threads_per_warp = rhs.threads_per_warp;
		//return *this;
	}
	~Config(){};
	Config& operator= (const Config& rhs){
#ifdef SAMPLING
		this->sampling_size = rhs.sampling_size;
#endif

#if defined(MULTI_CORE)
		this->coreid = rhs.coreid;
#else
		//#elif defined (REAL_MULTI_CORE)
		this->threads_per_core = rhs.threads_per_core;
#endif

		this->pause_thread = rhs.pause_thread;
		this->outputfile =  rhs.outputfile;
		this->csv = rhs.csv;
		this->benchmark = rhs.benchmark;
		this->periodicfile = rhs.periodicfile;
		this->num_threads = rhs.num_threads;
		this->num_ports = rhs.num_ports;
		this->alu = rhs.alu;
		this->vector_fu = rhs.vector_fu;
		this->issue_width = rhs.issue_width;
		this->commit_width = rhs.commit_width;
		this->issue_entries = rhs.issue_entries;
		this->num_fp_fus = rhs.num_fp_fus;
		this->num_int_fus = rhs.num_int_fus;
		this->num_mem_fus = rhs.num_mem_fus;
		this->rob_size = rhs.rob_size;
		this->iq_size = rhs.iq_size;

		this->issue_combine = rhs.issue_combine;
		//this->num_clusters = rhs.num_clusters;
		this->sim_insts = rhs.sim_insts;
		this->scalarize = rhs.scalarize;
		this->fetch_width = rhs.fetch_width;
		this->fetch_q_size = rhs.fetch_q_size;
		this->cache_warm = rhs.cache_warm;
		this->fp_count = rhs.fp_count;
		this->dviq_size = rhs.dviq_size;
		this->base = rhs.base;
		this->primary_heuristic = rhs.primary_heuristic;
		this->secondary_heuristic = rhs.secondary_heuristic;
		this->use_secondary_heuristic = rhs.use_secondary_heuristic;
		this->split_on_block = rhs.split_on_block;
		this->icount = rhs.icount;
		this->primary_priority_iqfull = rhs.primary_priority_iqfull;
		this->num_warps = rhs.num_warps;
		this->threads_per_warp = rhs.threads_per_warp;
		return *this;
	}
};


struct CacheParams{
public:
	CacheParams(){};
	int index_offset;
	int byte_offset;

	CacheParams& operator=(const CacheParams& rhs) {
		index_offset = rhs.index_offset;
		byte_offset = rhs.byte_offset;
		return *this;
	}

	CacheParams(const CacheParams& rhs){
		index_offset = rhs.index_offset;
		byte_offset = rhs.byte_offset;
	}
};

struct DecodedInstruction { 
	LONG seq_no;
	bool store;
};


struct Instruction {
	ADDRINT pc;
	ADDRINT sp;
	UINT32 opc;
	UINT32 uop;

	LONG seq_no;

	int wmem_num;
	int rmem_num;
	LONG wmem[MAX_OPERANDS];
	LONG rmem[MAX_OPERANDS];

	int rmem_size[MAX_OPERANDS];
	int wmem_size[MAX_OPERANDS];

	int rmem_done;
	int wmem_done;

	sa_uoptype uop_type;

	int cycles_remaining;
	bool is_simd;
	bool is_scalar;
	bool is_sse;
	bool is_avx;
	bool is_fp;
#ifndef SA_TRACER
	UINT32 operands[MAX_OPERANDS];
	bool reg_value_valid[MAX_OPERANDS];
#else
	sa_reg roperands[MAX_OPERANDS];
	sa_reg woperands[MAX_OPERANDS];
	int num_roperands;
	int num_woperands;
#endif
	bool store;
	bool load;
	bool store_start;
	bool load_start;
	int fuid;
	int combine_count;

	int clusterid;
	int threadid;
	int warpid;

	int fp_reg_read_count;
	int int_reg_read_count;

	int fp_reg_write_count;
	int int_reg_write_count;

	
	bool barrier;
	bool basic_block;

	bool is_branch;
	bool is_float_reg;
	bool is_int_reg;
	bool branch_taken, branch_taken_pred;
	ADDRINT branch_target;

	bool is_mispredicted;

#ifndef SA_TRACER
	bool read(FILE * file);
#else
	bool read(sa_tracer *tracer, int threadid, LONG seqno, int warp, int coreid);
	//bool read_mem()
#endif
	Instruction(){
		seq_no = 0;
		this->num_roperands = 0;
		this->num_woperands = 0;
		this->rmem_num = 0;
		this->wmem_num = 0;
		pc=0;
		sp=0;


	}
	Instruction(const Instruction& ins){
		this->is_scalar = ins.is_scalar;
		this->is_sse = ins.is_sse;
		this->is_avx = ins.is_avx;
		this->is_int_reg = ins.is_int_reg;
		this->is_float_reg = ins.is_float_reg;
		this->is_simd = ins.is_simd;
		this->is_fp = ins.is_fp;
		//this->word_size = ins.word_size;
		this->fp_reg_read_count = ins.fp_reg_read_count;
		this->int_reg_read_count = ins.int_reg_read_count;
		this->fp_reg_write_count = ins.fp_reg_write_count;
		this->int_reg_write_count = ins.int_reg_write_count;
		this->seq_no = ins.seq_no;
		this->uop = ins.uop;
		this->is_mispredicted = ins.is_mispredicted;
		this->is_branch = ins.is_branch; 
		this->branch_taken = ins.branch_taken;  this->branch_taken_pred = ins.branch_taken_pred; 
		this->branch_target = ins.branch_target;

		this->fuid = ins.fuid;
		//	this->scalarized_store_completed = ins.scalarized_store_completed;
		this->combine_count = ins.combine_count;
		//this->store_count = ins.store_count;
		this->store = ins.store;
		this->load = ins.load;
		this->load_start = ins.load_start;
		//		this->load_end = ins.load_end;
		this->store_start = ins.store_start;

		this->pc = ins.pc;
		this->sp= ins.sp;
		this->opc = ins.opc;
		this->uop_type = ins.uop_type;
		//this->combined = ins.combined;
		//this->orig_uop = ins.orig_uop;
		this->cycles_remaining = ins.cycles_remaining;
		this->wmem_num = ins.wmem_num;
		this->rmem_num = ins.rmem_num;
		this->rmem_done = ins.rmem_done;
		this->wmem_done = ins.wmem_done;
		this->threadid = ins.threadid;
		this->warpid = ins.warpid;
		this->clusterid = ins.clusterid;
		this->num_roperands = ins.num_roperands;
		this->num_woperands = ins.num_woperands;
		//int large =  ((num_roperands > num_woperands) ? num_roperands : num_woperands);
		for(int i = 0 ; i< num_roperands ;i++){
			this->roperands[i] = ins.roperands[i];
		}
		for(int i = 0 ; i< num_woperands ;i++){
			this->woperands[i] = ins.woperands[i];
		}
		for(int i=0; i < wmem_num;i++){

#ifndef SA_TRACER
			this->reg_value_valid[i] = 0;
			this->operands[i] =0;
#else

			//this->woperands[i] = ins.woperands[i];

#endif

			this->wmem[i] = ins.wmem[i];
			this->wmem_size[i] = ins.wmem_size[i];

		}
		for(int i=0; i < rmem_num;i++){
			this->rmem[i] = ins.rmem[i];
			this->rmem_size[i] = ins.rmem_size[i];
		}

		this->barrier = ins.barrier;
		this->basic_block = ins.basic_block;

	}

	Instruction& operator=(const Instruction &ins){

		this->is_scalar = ins.is_scalar;
		this->is_sse = ins.is_sse;
		this->is_avx = ins.is_avx;
		this->is_int_reg = ins.is_int_reg;
		this->is_float_reg = ins.is_float_reg;

		this->is_simd = ins.is_simd;
		this->is_fp = ins.is_fp;
		//this->word_size = ins.word_size;
		this->fp_reg_read_count = ins.fp_reg_read_count;
		this->int_reg_read_count = ins.int_reg_read_count;
		this->fp_reg_write_count = ins.fp_reg_write_count;
		this->int_reg_write_count = ins.int_reg_write_count;
		this->seq_no = ins.seq_no;
		this->is_mispredicted = ins.is_mispredicted;
		this->is_branch = ins.is_branch;
		this->branch_taken = ins.branch_taken; this->branch_taken_pred = ins.branch_taken_pred; 
		this->branch_target = ins.branch_target;
		
		this->fuid = ins.fuid;
		this->combine_count = ins.combine_count;
		//this->store_count = ins.store_count;
		this->store = ins.store;
		this->load = ins.load;
		this->load_start = ins.load_start;
		//		this->load_end = ins.load_end;
		this->store_start = ins.store_start;
		//this->assembly = ins.assembly;
		this->pc = ins.pc;
		this->sp= ins.sp;
		this->opc = ins.opc;
		this->uop_type = ins.uop_type;
		//this->combined = ins.combined;
		//this->orig_uop = ins.orig_uop;
		this->cycles_remaining = ins.cycles_remaining;
		this->wmem_num = ins.wmem_num;
		this->rmem_num = ins.rmem_num;
		this->rmem_done = ins.rmem_done;
		this->wmem_done = ins.wmem_done;
		this->threadid = ins.threadid;
		this->warpid = ins.warpid;
		this->clusterid = ins.clusterid;
		this->num_roperands = ins.num_roperands;
		this->num_woperands = ins.num_woperands;
		//int large =  ((num_roperands > num_woperands) ? num_roperands : num_woperands);
		for(int i=0; i < num_roperands;i++){
			this->roperands[i] = ins.roperands[i];
		}
		for(int i=0; i < num_woperands;i++){
			this->woperands[i] = ins.woperands[i];
		}

		for(int i=0; i < wmem_num;i++){
			if(ins.wmem_size[i] > 0){
				this->wmem[i] = ins.wmem[i];
				this->wmem_size[i] = ins.wmem_size[i];
			}
			//assert(this->wmem_size[i] > 0);  //<-
		}

		for(int i=0; i < rmem_num;i++){
			if(ins.rmem_size[i] > 0){
#ifndef SA_TRACER
				this->reg_value_valid[i] = 0;
				this->operands[i] =0;
#endif
				this->rmem[i] = ins.rmem[i];

				this->rmem_size[i] = ins.rmem_size[i];
			}
			//assert(this->rmem_size[i] > 0);
			//this->op_isint[i] = ins.op_isint[i];
			//this->op_iswrite[i] = ins.op_iswrite[i];
		}

		this->barrier = ins.barrier;
		this->basic_block = ins.basic_block;
#ifdef COMPILER_SUPPORT
		/*	this->pragma_fun_call = ins.pragma_fun_call;
		this->pragma_fun_ret  = ins.pragma_fun_ret;
		 */
#endif
		return *this;
	}


	bool operator==(const Instruction &rhs) const {
		//return this->pc == rhs.pc;
		return this->seq_no == rhs.seq_no;
	}

	~Instruction();
};

struct AddressLoadParams{
	LONG  address;
	int address_load_done;
	int address_size;
	int dependent_id;
	int tid;
	int warpid;
	bool l1_miss; ///
	bool stat;
	bool storeQ;
};

class DVLoad{

public:
	bool done;
	std::vector<AddressLoadParams> address_params;
	
	bool load_issued;
	int inst_remaining;

	int length;
	bool load_diverged;
	bool is_sse;
	bool is_avx;

	//int dvqid;
	//int length;

	DVLoad(){
		is_sse = false;
		is_avx = false;
		inst_remaining = 0;
		done = false;
		length = 0;
		load_issued = false;
	}

	void create(int num_threads, int _inst_remaining = 0, bool _is_sse = false, bool _is_avx = false){
		done = false;
		is_sse = _is_sse;
		is_avx = _is_avx;
		//l1_params = _l1_params;
		//length = len;
		//dvqid = -1;
		load_issued= false;
		load_diverged = false;
		length = num_threads;
		
		inst_remaining = _inst_remaining;
		address_params.resize(length);
	}

	void reset(int index){
		//mask[index] = false;
		//seqno[index] = 0;
	}


	void update(int index, LONG addr,int size, LONG seq, int tid, int warpid){
		//address_params[index].l1_miss = false;
		address_params[index].address_load_done = false;
		address_params[index].address = addr;
		address_params[index].address_size = size;
		assert(address_params[index].address_size > 0);
		address_params[index].tid = tid;
		address_params[index].warpid = warpid;
		address_params[index].stat = false;
		//address_params[index].storeQ = false;
	}

	DVLoad& operator=(const DVLoad rhs){
		done = rhs.done;
		is_sse = rhs.is_sse;
		is_avx = rhs.is_avx;
		inst_remaining = rhs.inst_remaining;
		load_issued = rhs.load_issued;
		load_diverged = rhs.load_diverged;
		//length = rhs.length;
		//dvqid = rhs.dvqid;
		length = rhs.length;

		address_params.resize(length);

		for(int i= 0 ;i<length;i++){
			address_params[i].l1_miss = rhs.address_params[i].l1_miss;
			//address_params[i].cache_level = rhs.address_params[i].cache_level;
			address_params[i].address_load_done = rhs.address_params[i].address_load_done;
			address_params[i].address = rhs.address_params[i].address;
			address_params[i].tid = rhs.address_params[i].tid;
			address_params[i].warpid = rhs.address_params[i].warpid;

			address_params[i].address_size = rhs.address_params[i].address_size;
			address_params[i].stat = rhs.address_params[i].stat;
			address_params[i].storeQ = rhs.address_params[i].storeQ;

		}
		return *this;
	}
	DVLoad(const DVLoad& rhs){
		done = rhs.done;
		is_sse = rhs.is_sse;
		is_avx = rhs.is_avx;
		inst_remaining = rhs.inst_remaining;
		//length = rhs.length;
		load_diverged = rhs.load_diverged;
		load_issued = rhs.load_issued;
		//dvqid = rhs.dvqid;
		length = rhs.length;

		address_params.resize(length);
		for(int i= 0 ;i<length;i++){
			address_params[i].l1_miss = rhs.address_params[i].l1_miss;
			address_params[i].address_load_done = rhs.address_params[i].address_load_done;
			address_params[i].address= rhs.address_params[i].address;
			address_params[i].tid = rhs.address_params[i].tid;
			address_params[i].warpid = rhs.address_params[i].warpid;
			address_params[i].address_size = rhs.address_params[i].address_size;
			address_params[i].stat = rhs.address_params[i].stat;
			address_params[i].storeQ = rhs.address_params[i].storeQ;

		}
	}
};

struct AddressStoreParams{
	LONG address;
	//std::vector< LONG > tag;
	//std::vector< int > cache_index;
	LONG  seqno;
	//std::vector< int > mask;
	int address_size;
	int  address_store_done;
	//int  cache_level;
	//LONG avail_cycle;
	bool l1_miss;
	int tid;
	int warpid;
	bool stat;
	bool storeQ;
	//LONG l1_tag;
};
class DVStore{
	
public:
	bool done;

	std::vector<AddressStoreParams> address_params;

	bool store_issued;
	int length;
	int inst_remaining;
	bool is_sse;
	bool is_avx;
	//CacheParams l1_params;


	//int dvqid;
	DVStore(){
		is_sse = false;
		is_avx = false;
		inst_remaining = 0;
		done = false;
		length = 0;
		//numthreads = 0;
		store_issued = false;
	}


	void update(int index, LONG addr,int size, LONG seq, int tid, int warpid){
		//address_params[index].address_store_done = false;
		address_params[index].l1_miss = false;
		address_params[index].address = addr;
		address_params[index].address_size = size;
		assert(address_params[index].address_size > 0);
		//address_params[index].cache_level = -1;
		address_params[index].seqno = seq;
		//address_params[index].avail_cycle = 0ULL;
		address_params[index].tid = tid;
		address_params[index].warpid = warpid;
		address_params[index].stat = address_params[index].stat;
		address_params[index].storeQ = address_params[index].storeQ;
		//address_params[index].l1_tag = extract_value(addr,l1_params.index_offset,63);
	}

	void create(int _length, int _inst_remaining = 0, bool _is_sse = false, bool _is_avx = false){
		done = false;
		is_sse = _is_sse;
		is_avx = _is_avx;
		store_issued = false;
		length = _length;
		inst_remaining = _inst_remaining;

		address_params.resize(length);

	}

	bool store_done(){
		if(done){
			return true;
		}else{

		}
	}
	DVStore& operator=(const DVStore& rhs) {
		done = rhs.done;
		is_sse = rhs.is_sse;
		is_avx = rhs.is_avx;
		inst_remaining = rhs.inst_remaining;
		store_issued = rhs.store_issued;
		length = rhs.length;

		address_params.resize(length);
		//address_params = new AddressStoreParams[length];
		for(int i= 0 ;i<length;i++){
			address_params[i].l1_miss = rhs.address_params[i].l1_miss;
			address_params[i].address_store_done = rhs.address_params[i].address_store_done;
			//address_params[i].cache_level = rhs.address_params[i].cache_level;
			address_params[i].address = rhs.address_params[i].address;
			address_params[i].tid = rhs.address_params[i].tid;
			address_params[i].warpid = rhs.address_params[i].warpid;
			address_params[i].seqno = rhs.address_params[i].seqno;
			address_params[i].address_size = rhs.address_params[i].address_size;
			address_params[i].stat = rhs.address_params[i].stat;
			address_params[i].storeQ = rhs.address_params[i].storeQ;
		
		}
		return *this;
	}

	DVStore(const DVStore& rhs){
		done = rhs.done;
		is_sse = rhs.is_sse;
		is_avx = rhs.is_avx;
		inst_remaining = rhs.inst_remaining;
		store_issued = rhs.store_issued;
		//length = rhs.length;
		//dvqid = rhs.dvqid
		length = rhs.length;

		address_params.resize(length);
		//address_params = new AddressStoreParams[length];
		for(int i= 0 ;i < length;i++){
			address_params[i].l1_miss = rhs.address_params[i].l1_miss;
			//address_params[i].cache_level = rhs.address_params[i].cache_level;
			address_params[i].address_store_done = rhs.address_params[i].address_store_done;
			address_params[i].address = rhs.address_params[i].address;
			address_params[i].tid = rhs.address_params[i].tid;
			address_params[i].warpid = rhs.address_params[i].warpid;
			address_params[i].seqno = rhs.address_params[i].seqno;
			address_params[i].address_size = rhs.address_params[i].address_size;
			address_params[i].stat = rhs.address_params[i].stat;
			address_params[i].storeQ = rhs.address_params[i].storeQ;
		}
	}
};


struct DVInstLoad{
	int dvqid;
	std::vector<DVLoad> loads;
	int loadcount;
	std::vector<int> smask;
	std::vector<int> tid;
	std::vector<LONG> seqno; //NO LONGER NEEDED
	LONG pc;
	int valid_inst;
	bool load_started;
	LONG id;
	int warpid;
	bool scalarise;

	int length;
	bool valid;

	DVInstLoad(){
		scalarise = false;
		dvqid = -1;
		pc =0ULL;
		loadcount = 0;
		valid_inst = -1;
		load_started = false;
		length = 0;
		valid = false;

	}


	void create(int len, int dviq, LONG dv_pc, int ldcount, int valid_in, std::vector<int> &mask, std::vector<LONG> &seq, std::vector<int> _tid, LONG dvinstid, int _warpid, bool _scalarise){

		scalarise = _scalarise;
		length = len;
		dvqid = dviq;
		pc = dv_pc;
		loadcount = ldcount;
		valid_inst = valid_in;
		loads.resize(ldcount);
		smask.resize(len);
		seqno.resize(len);
		tid.resize(len);
		load_started = false;
		id = dvinstid;
		warpid = _warpid;

		valid = false;
		for(int i = 0 ;i <len; i++){
			smask[i]= mask[i];
			seqno[i] = seq[i];
			tid[i] = _tid[i];
		}
	}
	DVInstLoad& operator=(const DVInstLoad& rhs) {
		scalarise = rhs.scalarise;
		id = rhs.id;
		dvqid = rhs.dvqid;
		pc = rhs.pc;
		warpid = rhs.warpid;
		loadcount = rhs.loadcount;
		valid_inst = rhs.valid_inst;
		load_started = rhs.load_started;
		length = rhs.length;
		loads.resize(loadcount);
		smask.resize(length);
		seqno.resize(length);
		tid.resize(length);

		for(int i = 0 ;i < length; i++){
			smask[i]= rhs.smask[i];
			seqno[i]= rhs.seqno[i];
			tid[i] = rhs.tid[i];
		}

		for(int i = 0 ; i< loadcount ;i++){
			loads[i] = rhs.loads[i];
		}
		valid = rhs.valid;
		return *this;
	}

	DVInstLoad(const DVInstLoad& rhs){
		scalarise = rhs.scalarise;
		id = rhs.id;
		dvqid = rhs.dvqid;
		pc = rhs.pc;
		warpid = rhs.warpid;
		loadcount = rhs.loadcount;
		valid_inst = rhs.valid_inst;
		load_started = rhs.load_started;
		length = rhs.length;
		valid = rhs.valid;
		loads.resize(loadcount);
		smask.resize(length);
		seqno.resize(length);
		tid.resize(length);

		for(int i = 0 ; i< loadcount ;i++){
			loads[i] = rhs.loads[i];
		}

		for(int i = 0 ;i < length; i++){
			smask[i]= rhs.smask[i];
			seqno[i]= rhs.seqno[i];
			tid[i] = rhs.tid[i];
		}
	}

};

struct DVInstStore{
	int dvqid;
	std::vector<DVStore> stores;
	int storecount;
	std::vector<int> smask;
	LONG pc;
	int valid_inst;
	bool store_started;
	std::vector<LONG> seqno;
	std::vector<int> tid;
	std::vector<int> commit_started;
	LONG id;
	int warpid;
	int length;
	bool scalarise;
	bool valid;

	DVInstStore(){
		id = 0;
		dvqid = -1;
		pc =0ULL;
		storecount = 0;
		valid_inst = -1;
		store_started = false;
		length = 0;
		valid = false;

	}

#ifdef SCALARISE
	void create(int len, int dviq, LONG dv_pc, int ldcount, int _inst_count, int valid_in, std::vector<int> &mask, std::vector<LONG> &seq, std::vector<int> &_tid, LONG dvinstid, int _warpid, bool _scalarise ){
#else
		void create(int len, int dviq, LONG dv_pc, int ldcount, int valid_in, std::vector<int> &mask, std::vector<LONG> &seq, std::vector<int> &_tid, LONG dvinstid, int _warpid ){
#endif

			scalarise = _scalarise;
			length = len;
			dvqid = dviq;
			pc = dv_pc;
			storecount = ldcount;
			valid_inst = valid_in;
			stores.resize(ldcount);
			smask.resize(len);
			seqno.resize(len);
			tid.resize(len);
			commit_started.resize(len);
			store_started = false;
			valid = false;
			id = dvinstid;
			warpid = _warpid;

			for(int i = 0 ;i <len; i++){
				smask[i]= mask[i];
				seqno[i] = seq[i];
				commit_started[i] = 0;
				tid[i] = _tid[i];
			}
		}
		DVInstStore& operator=(const DVInstStore& rhs) {
#ifdef SCALARISE
			scalarise = rhs.scalarise;
#endif

			id = rhs.id;
			dvqid = rhs.dvqid;
			pc = rhs.pc;
			warpid = rhs.warpid;
			storecount = rhs.storecount;
			valid_inst = rhs.valid_inst;
			store_started = rhs.store_started;
			length = rhs.length;
			stores.resize(storecount);
			smask.resize(length);
			seqno.resize(length);
			commit_started.resize(length);
			tid.resize(length);
			for(int i = 0 ;i < length; i++){
				smask[i]= rhs.smask[i];
				seqno[i] = rhs.seqno[i];
				commit_started[i] = rhs.commit_started[i];
				tid[i] = rhs.tid[i];
			}

			for(int i = 0 ; i< storecount ;i++){
				stores[i] = rhs.stores[i];
			}
			valid = rhs.valid;
			return *this;
		}

		DVInstStore(const DVInstStore& rhs){
#ifdef SCALARISE
			scalarise = rhs.scalarise;
#endif
			id = rhs.id;
			dvqid = rhs.dvqid;
			pc = rhs.pc;
			warpid = rhs.warpid;
			storecount = rhs.storecount;
			valid_inst = rhs.valid_inst;
			store_started = rhs.store_started;
			length = rhs.length;
			valid = rhs.valid;
			stores.resize(storecount);
			smask.resize(length);
			seqno.resize(length);
			commit_started.resize(length);
			tid.resize(length);
			for(int i = 0 ; i< storecount ;i++){
				stores[i] = rhs.stores[i];
			}

			for(int i = 0 ;i < length; i++){
				smask[i]= rhs.smask[i];
				seqno[i] = rhs.seqno[i];
				commit_started[i] = rhs.commit_started[i];
				tid[i] = rhs.tid[i];
			}
		}

	};

	struct FU_DVInst{
		int dvqid;
		LONG pc;
		int cycles_remaining;
		int valid_inst;
		bool has_load;
		bool exec_completed;
		bool load_done;
		LONG id;
		bool is_fptype;
		bool is_simd, is_uop;
		sa_uoptype uop_type;
		int num_insts;
		LONG rob_id;
		int tag, pathT, pathNT; //pathX used as temp vars for uops (dependency tags)

#ifdef SCALARISE
		bool lastinst;
		int scalarised_inst_count;
#endif
		FU_DVInst(){
#ifdef SCALARISE
			lastinst = false;
			scalarised_inst_count = 0;
#endif
			is_fptype = false;
			is_simd = false; is_uop = false;
			id = 0; rob_id = 0;
			num_insts = 0;
			dvqid = -1;
			pc =0ULL;
			cycles_remaining = 0;
			valid_inst = -1;
			exec_completed = false;
			load_done = false;
			uop_type = SA_UOP_UNKNOWN;
			tag = -1, pathT = -1, pathNT = -1;
		}
		FU_DVInst& operator=(const FU_DVInst& rhs) {
#ifdef SCALARISE
			lastinst = rhs.lastinst;
			scalarised_inst_count = rhs.scalarised_inst_count;
#endif
			is_simd = rhs.is_simd; is_uop = rhs.is_uop;
			id = rhs.id;
			rob_id = rhs.rob_id;
			num_insts = rhs.num_insts;
			dvqid = rhs.dvqid;
			pc = rhs.pc;
			cycles_remaining = rhs.cycles_remaining;
			valid_inst = rhs.valid_inst;
			has_load = rhs.has_load;
			exec_completed = rhs.exec_completed;
			load_done = rhs.load_done;
			uop_type = rhs.uop_type;
			is_fptype = rhs.is_fptype; 
			tag = rhs.tag, pathT = rhs.pathT, pathNT = rhs.pathNT;
			return *this;
		}

		FU_DVInst(const FU_DVInst& rhs){
#ifdef SCALARISE
			lastinst = rhs.lastinst;
			scalarised_inst_count = rhs.scalarised_inst_count;
#endif
			id = rhs.id;
			is_simd = rhs.is_simd; is_uop = rhs.is_uop;
			rob_id = rhs.rob_id;
			num_insts = rhs.num_insts;
			dvqid = rhs.dvqid;
			pc = rhs.pc;
			cycles_remaining = rhs.cycles_remaining;
			valid_inst = rhs.valid_inst;
			has_load = rhs.has_load;
			exec_completed = rhs.exec_completed;
			load_done = rhs.load_done;
			uop_type = rhs.uop_type;
			is_fptype = rhs.is_fptype; 
			tag = rhs.tag, pathT = rhs.pathT, pathNT = rhs.pathNT;
		}
	};
	
	static LONG dvinstid = 0;
	class DVInst{

	public:
		//for Register Renaming (with PIRAT) -- read these values during issue (renamed vals) ///////
		int sources, dests;	
		std::vector<char> dest_type;	//source or path type
		std::vector<int>reads, pirat_type;
		std::vector<int> writes, pirat_wtype; 
		std::vector<int> orig_dests; //for WB to PIRAT (index with orig_dest, check if writes is in partial for valid bit)
		/////////////////////////////////////////////////////

		int inst_count;

		std::vector<DVLoad> loads;
		int load_count;
		int store_count;
		std::vector<DVStore> stores;
		int length;
		bool valid;
		int warpid;

		int dviqid;
		std::vector<Instruction> insts;
		std::vector<int> tid;
		bool has_load;
		bool has_store;
		bool load_started;
		bool load_done;
		int load_delay;
		bool store_started;
		int cycles_remaining;

		bool is_phi; //these are empty. Just used temporarily during rename
		std::vector<int> default_reg, partial_reg, phi_reg, reg_type, phi_dests;
		std::vector<bool> v_partial, v_default;

		sa_uoptype uop_type;

		bool dv_q_delete;

		ADDRINT dv_pc;
		ADDRINT dv_sp;
		int valid_inst;

		bool is_fp;
		bool is_simd;
		bool new_path, div_pred, div_actual; //new_path = actual divergence, div_pred = prediction
		bool is_branch, branch_taken, branch_taken_pred;

		LONG dv_seqno; //NO LONGER NEEDED
		LONG id;

		//for path table and prediction /////////////////////
		bool is_uop; /////
		std::vector<int> smask; //incorrect - always read from path table 
		int exception_mask;
		bool exception_bit;
		int actual_mask;
		int pathT, pathNT; //used for div and reconv (c_uop) IS
		int tag;
		//////////////////////////////////////////////////

		
		
	
		
		//////////////////////////////////////////////////////
		
		void update(int index, bool first = false){

			if(first){
				sources = 0; //reads.resize(MAX_OPERANDS); pirat_type.resize(MAX_OPERANDS);
				dests = 0; //writes.resize(MAX_OPERANDS); pirat_wtype.resize(MAX_OPERANDS);

				is_fp = insts[index].is_fp; pathT = -1; pathNT = -1;
				is_simd = insts[index].is_simd;
				is_branch = insts[index].is_branch;
				is_uop = false; is_phi = false; 
				new_path = false; tag = 0;
				div_pred = false; div_actual = false;
				//exception_bit = false;

				valid_inst = index;
				dv_q_delete = false;
				dv_pc = insts[index].pc;
				dv_sp = insts[index].sp;
				//warpid = insts[index].warpid;
				cycles_remaining = insts[index].cycles_remaining;
				uop_type = insts[index].uop_type;
			
				has_load = insts[index].load;
				has_store = insts[index].store;

				load_count = insts[index].rmem_num;
				store_count =insts[index].wmem_num;

				loads.resize(insts[index].rmem_num);
				stores.resize(insts[index].wmem_num);
				for(int ld = 0; ld<load_count;ld++){
					loads[ld].create(length, inst_count, insts[index].is_sse,insts[index].is_avx); //TODO:Inst_count is not relevant here;
				}
				for(int st = 0; st<store_count;st++){
					stores[st].create(length,inst_count, insts[index].is_sse,insts[index].is_avx);
				}
			}
			assert(insts[index].pc == dv_pc);
			smask[index] = true;
			//exception_mask = 0;
			tid[index] = insts[index].threadid;

			for(int ld = 0; ld<load_count;ld++){
				loads[ld].update(index,insts[index].rmem[ld],insts[index].rmem_size[ld], insts[index].seq_no, insts[index].threadid, insts[index].warpid);
			}
			for(int st = 0; st<store_count;st++){
				stores[st].update(index,insts[index].wmem[st] ,insts[index].wmem_size[st], insts[index].seq_no, insts[index].threadid, insts[index].warpid);
			}
		}

			
		void create(Config conf, LONG seqno, int _inst_count = 0, int wid = -1){ // FIXME: FOr horizontal scalarization inst_count = 0
			sources = 0; 
			dests = 0; 

			warpid = wid;
			dv_seqno = seqno; //take out
			length = conf.threads_per_warp;
			insts.resize(length);
			smask.resize(length); 
			exception_mask = 0;
			tid.resize(length);
			tag = -1;
			//pathT = -1; pathNT = -1;
			actual_mask = -1;

			dviqid = -1;
			load_delay = 0;
			valid = true;
			load_started = false;
			store_started = false;
			load_done  = false;
			dv_q_delete =false;
			is_uop = false; is_phi = false; 
			//exception_bit = false;
			id = dvinstid++;
			inst_count = _inst_count;
			is_simd = false; branch_taken = false; branch_taken_pred = false; is_branch = false;
			new_path = false; div_pred = false; div_actual = false;
	
			for(int i = 0 ; i< length; i++){
				smask[i] = false;
				tid[i] = -1;
			}

		}
					//default reg, partial reg, phi reg, tag, pirat type
		void create_phi(Config conf, int reg1, int reg2, int phi, int mask, int type, int entry){
			is_uop = false; 
			exception_bit = false;
			is_phi = true;
			is_branch = false; new_path = false; branch_taken = false; branch_taken_pred = false;
			sources = 2; dests = 1;
			reads.push_back(reg1); //r_bit
			reads.push_back(reg2); //v_bit			
			writes.push_back(phi); pirat_wtype.push_back(type);
			orig_dests.push_back(entry);//entry number
			pirat_type.push_back(type); pirat_type.push_back(type); //for bypassing
			tag = mask;
			pathT = -1; pathNT = -1;
			actual_mask = -1;
		}
	
		void create_cuop(Config conf, LONG seqno, int _inst_count = 0, int pt = 0, int pnt = 0){
			is_uop = true; 
			exception_bit = false;
			is_phi = false;
			is_branch = false; new_path = true;  branch_taken = false; branch_taken_pred = false;
			div_pred = false; div_actual = false;

			dv_seqno = seqno;
			length = conf.threads_per_warp;
			insts.resize(length);
			smask.resize(length); 
			tid.resize(length);
			tag = -1;
			pathT = pt; pathNT = pnt; ///////
			actual_mask = -1;

			sources = 0; dests = 0; 
			dviqid = -1;
			load_delay = 0;
			valid = true;
			load_started = false;
			store_started = false;
			load_done  = false;
			dv_q_delete =false;
			id = dvinstid++;
			inst_count = _inst_count;
			is_simd = false;
			exception_mask = 0;
	
			for(int i = 0 ; i< length; i++){
				smask[i] = false;
				tid[i] = -1;
			}
		}


		DVInst(){

			sources = 0; //reads.resize(MAX_OPERANDS); pirat_type.resize(MAX_OPERANDS);
			dests = 0; //writes.resize(MAX_OPERANDS); pirat_wtype.resize(MAX_OPERANDS);	

			warpid = 0;
			inst_count = 0;
			is_simd = false;
			is_fp = false;
			is_uop = false; is_phi = false; 
			exception_bit = false;
			exception_mask = 0;
			is_branch = false; branch_taken = false; branch_taken_pred = false;
			new_path = false; div_pred = false; div_actual = false;
			dv_seqno =0; pathT = -1; pathNT = -1;
			valid = false;
			id = 0;
			valid_inst =-1;
			dv_pc = 0Ull;
			dv_sp = 0Ull;
		
			load_count = 0;
			store_count = 0;

			dviqid =-1;
			length =0;

			cycles_remaining = 0;

			has_load = false;
			has_store = false;
			load_started =false;
			store_started = false;
			load_delay = 0;
			dv_q_delete = false;
			actual_mask = -1;

		}


		bool operator==(const DVInst & rhs) const{
			return (id == rhs.id);
		}
		bool operator==(const DVInstStore & rhs) const{
			return (id == rhs.id);
		}

		bool operator==(const FU_DVInst & rhs) const{
			return (id == rhs.id);
		}


		DVInst& operator=(const DVInst &rhs){
			is_phi = rhs.is_phi; is_uop = rhs.is_uop; 
			sources = rhs.sources; reads = rhs.reads; pirat_type = rhs.pirat_type;
			dests = rhs.dests; writes = rhs.writes; pirat_wtype = rhs.pirat_wtype;;	
			pathT = rhs.pathT; pathNT = rhs.pathNT;
			orig_dests = rhs.orig_dests;
			tag = rhs.tag;
			actual_mask = rhs.actual_mask;
			exception_bit = rhs.exception_bit;

			is_simd = rhs.is_simd;
			is_fp = rhs.is_fp;
			is_branch = rhs.is_branch; branch_taken = rhs.branch_taken; branch_taken_pred = rhs.branch_taken_pred;
			inst_count = rhs.inst_count;
			dv_seqno = rhs.dv_seqno;
			id = rhs.id;
			load_count = rhs.load_count;
			store_count = rhs.store_count;
			load_delay = rhs.load_delay;
			uop_type = rhs.uop_type;
			new_path = rhs.new_path;   div_pred = rhs.div_pred;	div_actual = rhs.div_actual;

			dv_q_delete = rhs.dv_q_delete;

			dv_pc = rhs.dv_pc;
			dv_sp = rhs.dv_sp;
			valid_inst = rhs.valid_inst;
			dviqid = rhs.dviqid;
			length = rhs.length;
			insts.resize(length);
	
			loads.resize(load_count);
			stores.resize(store_count);
			tid.resize(length);

			smask.resize(length); 
			valid = rhs.valid;
			warpid = rhs.warpid;

			cycles_remaining = rhs.cycles_remaining;

			has_load = rhs.has_load;
			has_store = rhs.has_store;
			load_started = rhs.load_started;
			store_started = rhs.store_started;
			load_done = rhs.load_done;
			exception_mask = rhs.exception_mask;

			for(int i= 0 ;i<length;i++){
				smask[i] = rhs.smask[i];
				tid[i] = rhs.tid[i];
				if(rhs.smask[i]){
					insts[i] = rhs.insts[i];
				}

			}
			for(int ld = 0; ld<load_count;ld++){
				loads[ld] = rhs.loads[ld];
			}

			for(int st = 0; st<store_count;st++){
				stores[st] = rhs.stores[st];
			}
			return *this;
		}

		DVInst(const DVInst& rhs){
			is_phi = rhs.is_phi; is_uop = rhs.is_uop;
			sources = rhs.sources; reads = rhs.reads; pirat_type = rhs.pirat_type;
			dests = rhs.dests; writes = rhs.writes; pirat_wtype = rhs.pirat_wtype;
			pathT = rhs.pathT; pathNT = rhs.pathNT;
			orig_dests = rhs.orig_dests;
			tag = rhs.tag;
			actual_mask = rhs.actual_mask;
			exception_bit = rhs.exception_bit;

			is_fp = rhs.is_fp;
			is_simd = rhs.is_simd;
			is_branch = rhs.is_branch; branch_taken = rhs.branch_taken; branch_taken_pred = rhs.branch_taken_pred;
			inst_count = rhs.inst_count;
			dv_seqno = rhs.dv_seqno;
			id = rhs.id;
			load_count = rhs.load_count;
			store_count = rhs.store_count;
			load_delay = rhs.load_delay;
			new_path = rhs.new_path;  div_pred = rhs.div_pred;    div_actual = rhs.div_actual;

			uop_type = rhs.uop_type;

			dv_q_delete = rhs.dv_q_delete;

			dv_pc = rhs.dv_pc;
			dv_sp = rhs.dv_sp;
			valid_inst = rhs.valid_inst;

			dviqid = rhs.dviqid;
			length = rhs.length;
			insts.resize(length);
			loads.resize(load_count);
			stores.resize(store_count);
			tid.resize(length);

			smask.resize(length); 
			valid = rhs.valid;

			cycles_remaining = rhs.cycles_remaining;

			has_load = rhs.has_load;
			has_store = rhs.has_store;
			load_started = rhs.load_started;
			store_started = rhs.store_started;
			load_done = rhs.load_done;
			warpid = rhs.warpid;
			exception_mask = rhs.exception_mask;

			for(int i= 0 ;i<length;i++){
				if(rhs.smask[i]){
					insts[i] = rhs.insts[i];
				}
				tid[i] = rhs.tid[i];
				assert(smask[i]<2);
				smask[i] = rhs.smask[i];
			}
			for(int ld = 0; ld<load_count;ld++){
				loads[ld] = rhs.loads[ld];
			}

			for(int st = 0; st<store_count;st++){
				stores[st] = rhs.stores[st];
			}
		}

	};

	///////////////////OoO DITVA Datapath Structures ////////////////////////
	
	class IS_Table{
		public:
			LONG prog_counter, sp, last_scheduled;
			std::vector<int> is;
			std::vector<int> path;
			std::vector<LONG> pc;
			std::vector<LONG> call_depth;
			std::vector<LONG> time_stamp;
			std::vector<int> barrier, exception;			
	};

	class Path_Table{
		public:	//Path table//////////////////////////////////
			std::vector<int> rbit; //0 = speculative path, 1 = committed, -1 invalid
			std::vector<int> mask; //indexed by tag (vectors)
			std::vector<int> actual_mask;
			std::vector<int> ref_counter; //counter for num references to path
			int free_ptr, commit_ptr;
			
	};	


	class PIRAT_Table{
		public:
			std::vector<int> default_reg;
			std::vector<int> partial_reg;
			std::vector<bool> mt_bit;
			std::vector<bool> r_bit; //for partial
			std::vector<bool> v_bit; //for default
			std::vector<int> mask_tag;
	};

	class Free_List{
		public:
			std::list<int> reg;
			std::list<bool> taken;
	};

	class ROB{
		public:
			std::list<DVInst> inst;	
			std::list<int> exception_mask;//, warpid;
			std::list<std::vector<int> > ow_register, ow_location, ow_type;
			std::list<LONG> id; //previous register which this destination register overwrites, entry ID
			std::list<std::vector<int> >dest, pirat_type; //!HERE
			std::list<std::vector<char> > dest_type; // ----- " ------- (source or path)
			std::list<bool> entry_valid, data_valid, issued, done;
			//done set when bypassed/WB, data_valid set when destination value ready (in execute), issued set when 
			//entry_valid set when pushed onto IQ.
	};

	class PhysicalRegFile{
		public:
			std::vector<bool> valid;
	};

	///////////////////////////////////////////////////////////////////

	struct BlockedLoadStore{
		//int dviqid;
		//std::vector <int> old_mask;
		std::vector <int> smask;
		std::vector <LONG> seqno;
		std::vector <int> tid;
		//uint length;
		bool valid;
		uint length;

		BlockedLoadStore(){};
		void init(uint _numthreads){
			length = _numthreads;
			//old_mask.resize(numthreads);
			smask.resize(length);
			seqno.resize(length);
			tid.resize(length);
			//dviqid = -1;
			valid = false;
			for(uint i = 0; i < length ; i++){
				//old_mask[i] = 0;
				smask[i] = 0;
				seqno[i] = 0;
				tid[i] = -1;
			}

		}

		void reset(){
			//dviqid = -1;
			valid = false;
			for(uint i = 0; i < length ; i++){
				//old_mask[i] = 0;
				smask[i] = 0;
				seqno[i] = UINTMAX_MAX;
				tid[i] = -1;
			}
		}

		BlockedLoadStore& operator=(const BlockedLoadStore &rhs){
			length = rhs.length;
			//	dviqid = rhs.dviqid;
			valid = rhs.valid;
			//old_mask.resize(numthreads);
			smask.resize(length);
			seqno.resize(length);
			tid.resize(length);
			for(uint i = 0; i < length ; i++){
				//old_mask[i] = rhs.old_mask[i];
				smask[i] = rhs.smask[i];
				seqno[i] = rhs.seqno[i];
				tid[i] = rhs.tid[i];

			}
			return *this;
		}
		BlockedLoadStore(const BlockedLoadStore& rhs){
			length = rhs.length;
			//	dviqid = rhs.dviqid;
			valid = rhs.valid;
			//old_mask.resize(numthreads);
			smask.resize(length);
			seqno.resize(length);
			tid.resize(length);
			for(uint i = 0; i < length ; i++){
				//old_mask[i] = rhs.old_mask[i];
				smask[i] = rhs.smask[i];
				seqno[i] = rhs.seqno[i];
				tid[i] = rhs.tid[i];
			}
		}

	};


	


	class DITVLogger
	{
	public:
		DITVLogger(Loglevels _loglevel, std::ofstream &out):outfile(out) {
			/* _buffer << _loglevel << " :"
            << std::string(
                _loglevel > logDEBUG
                ? (_loglevel - logDEBUG) * 4
                : 1
                , ' ');*/
		}

		template <typename T>
		DITVLogger & operator<<(T const & value)
		{
			_buffer << value;
			return *this;
		}

		DITVLogger & operator<<(std::ostream&(*f)(std::ostream&)){
			_buffer <<f;
			return *this;
		}

		~DITVLogger()
		{
			//_buffer << std::endl;
			//std::cout << _buffer.str();
			outfile << _buffer.str();
		}

	private:
		std::ostringstream _buffer;
		std::ofstream &outfile;
	};

	class TracePath
	{
	public:
		TracePath(TraceLevel _loglevel, std::ofstream &out):outfile(out) {
		
		}

		template <typename T>
		TracePath & operator<<(T const & value)
		{
			_buffer << value;
			return *this;
		}

		TracePath & operator<<(std::ostream&(*f)(std::ostream&)){
			_buffer <<f;
			return *this;
		}

		~TracePath()
		{
			//_buffer << std::endl;
			outfile << _buffer.str();
		}

	private:
		std::ostringstream _buffer;
		std::ofstream &outfile;
	};

	//extern const Loglevels loglevel;
	extern Loglevels loglevel;
	extern TraceLevel tracelevel;
	extern  std::ofstream logTrace;
	extern  std::ofstream logPeriodic;

	//Loglevels loglevel = logERROR;
#endif

	const int log_interval = 100000;


#define log(level) \
		if (level > loglevel) ; \
		else DITVLogger(level, logTrace)

#define tracepath(level,tracer) \
		if (level != tracelevel) ; \
		else TracePath(level,tracer)

#define hex16(val) std::setw(16)<<std::setfill('0')<<std::right<<std::hex<<val<<std::dec

#ifndef unlikely
#define unlikely(x) (__builtin_expect(!!(x), 0))
#endif
#ifndef likely
#define likely(x) (__builtin_expect(!!(x), 1))
#endif

	typedef LONG W64;

	using namespace std;

	/*#define LOG_FATAL    (1)
#define LOG_ERR      (2)
#define LOG_WARN     (3)
#define LOG_INFO     (4)
#define LOG_DBG      (5)

#define LOG(level, ...) do {  \
                            if (level <= debug_level) { \
                                fprintf(dbgstream,"%s:%d:", __FILE__, __LINE__); \
                                fprintf(dbgstream, __VA_ARGS__); \
                                fprintf(dbgstream, "\n"); \
                                fflush(dbgstream); \
                            } \
                        } while (0)
extern FILE *dbgstream;
extern int  debug_level;
	 */


	//(11 - Strongly Taken, 10 - Weakly Taken, 01 - Weakly Non-Taken, 00 - Strongly Non-Taken)
	class BranchPredictor{
		//2-bit branch predictor
		int pred_table[BRP_SIZE];
	public:
		BranchPredictor(){
			for(int i = 0 ; i< BRP_SIZE;i++){
				pred_table[i] = 0;
			}
		}

		bool predicted(W64 target, bool taken);


	};

	struct Statistics{

		std::ofstream logPeriodic;
		bool print;
		UINT num_threads;
		int issue_width;

	public:
		std::vector<LONG> ins_commit;
		std::vector<LONG> load_count;
		std::vector<LONG> store_count;

		std::vector<LONG> load_count_at_fetch;
		std::vector<LONG> store_count_at_fetch;

		std::vector<LONG> cache_miss;
		std::vector<LONG> ls_l1_hit;
		std::vector<LONG> ls_l2_hit;
		std::vector<LONG> ls_l2_miss;

		LONG partial_miss;

		std::vector<LONG> cache_read_access;
		std::vector<LONG> cache_write_access;
		std::vector<LONG> cache_miss_nonscalar;
		std::vector<LONG> cache_read_miss_nonscalar;
		std::vector<LONG> cache_write_miss_nonscalar;

		std::vector<LONG> reg_read_int;
		std::vector<LONG> reg_read_float;
		std::vector<LONG> reg_write_int;
		std::vector<LONG> reg_write_float;

		std::vector<LONG> thread_occupancy;
		std::vector<int> phi_frequency;
		std::vector<int> phi_frequency_fp;

		LONG branch_misprediction;
		//for DV-Insts
		LONG full_misprediction;
		LONG partial_misprediction;
		LONG full_misprediction2;
		LONG correct_predict;

		std::vector<LONG> branches;

		std::vector<LONG> lsdivergence;

		std::vector<LONG> inst_int;
		std::vector<LONG> scalarized_instruction;
		std::vector<LONG> inst_fp;
		std::vector<LONG> inst_ld;
		std::vector<LONG> inst_st;

		LONG total_dv_committed;
		LONG total_inst_committed;
		LONG rob_stalls;
		LONG decBuffer_stalls;
		LONG issue_stalls;
		LONG reg_rename_stalls;
		LONG waw_stalls, pirat_stalls;

		LONG dv_int_count;
		LONG dv_fp_count;
		LONG dv_branch;
		LONG buop_count;
		LONG phi_count;
		LONG branch_nops;
		std::vector<LONG> max_paths;

		LONG read_merges;
		LONG write_merges;
		LONG read_fp_merges;
		LONG write_fp_merges;
		
		LONG pirat_wr_accesses;
		LONG pirat_rd_accesses;
		LONG pirat_fp_wr_accesses;
		LONG pirat_fp_rd_accesses;
		

		LONG pirat_default_rd;
		LONG pirat_partial_rd;	
		LONG pirat_fp_default_rd;
		LONG pirat_fp_partial_rd;	
		
		LONG simd_count;
		LONG no_issuable_instructions;

		LONG icache_access_cycles;

		LONG ialu_access;
		LONG fpu_access;
		LONG mul_access;
		LONG div_access;

		LONG int_reg_file_read;
		LONG int_reg_file_write;
		LONG float_reg_file_read;
		LONG float_reg_file_write;

		LONG dv_inst_count;
		LONG fetch_inst_count;
		LONG int_inst_count;
		LONG fp_inst_count;
		LONG exe_dvinst_count;
		LONG exe_inst_count;
		LONG exe_cycles;
		LONG issue_inst_count;

		LONG dv_load; LONG counting_loads;
		LONG dv_store; LONG counting_stores;
		LONG convergence_count;
		LONG divergence_count;
		LONG outstandingls;
		LONG exception_uops;

		LONG iw_full;
		LONG fu_full;
		LONG dependant;

		std::vector<LONG> issue_count;
		std::vector<LONG> thread_issue_count;


		std::vector<LONG> combine_count;
		std::vector<LONG> shared_data;
		LONG total_exec_inst;

		LONG cycle;
		std::vector<LONG> ins_count;
		LONG total_ins_count;
		LONG iq_empty;
		std::vector<LONG> iq_blocked_count;

		//Periodic

		std::vector<LONG> periodic_ins_commit;

		std::vector<LONG> periodic_load_count;
		std::vector<LONG> periodic_store_count;

		std::vector<LONG> periodic_load_count_at_fetch;
		std::vector<LONG> periodic_store_count_at_fetch;

		std::vector<LONG> periodic_cache_miss;
		std::vector<LONG> periodic_ls_l1_hit;
		std::vector<LONG> periodic_ls_l2_hit;
		std::vector<LONG> periodic_ls_l2_miss;

		std::vector<LONG> periodic_cache_read_miss;
		std::vector<LONG> periodic_cache_write_miss;
		std::vector<LONG> periodic_cache_miss_nonscalar;
		std::vector<LONG> periodic_cache_read_miss_nonscalar;
		std::vector<LONG> periodic_cache_write_miss_nonscalar;

		std::vector<LONG> periodic_reg_read_int;
		std::vector<LONG> periodic_reg_read_float;
		std::vector<LONG> periodic_reg_write_int;
		std::vector<LONG> periodic_reg_write_float;


		std::vector<LONG> periodic_branch_misprediction;
		std::vector<LONG> periodic_branches;

		std::vector<LONG> periodic_lsdivergence;

		std::vector<LONG> periodic_inst_int;
		std::vector<LONG> periodic_scalarized_instruction;
		std::vector<LONG> periodic_inst_fp;
		std::vector<LONG> periodic_inst_ld;
		std::vector<LONG> periodic_inst_st;

		LONG periodic_dv_inst_count;
		LONG periodic_dv_load;
		LONG periodic_dv_store;
		LONG periodic_convergence_count;
		LONG periodic_divergence_count;
		LONG periodic_outstandingls;

		LONG periodic_iw_full;
		LONG periodic_fu_full;
		LONG periodic_dependant;

		LONG l1_hit_r, l1_hit_w;
		LONG l1_miss_r, l1_miss_w;
		LONG l2_hit;
		LONG l2_miss;


		std::vector<LONG> periodic_issue_count;
		std::vector<LONG> periodic_thread_issue_count;
		std::vector<LONG> periodic_combine_count;
		std::vector<LONG> periodic_shared_data;
		LONG periodic_total_exec_inst;

		LONG periodic_cycle;
		std::vector<LONG> periodic_ins_count;
		LONG periodic_total_ins_count;
		LONG periodic_iq_empty;
		std::vector<LONG> periodic_iq_blocked_count;

		LONG num_cuops;
		///////////////////////////////////////////////////////
		//For OoO DITVA Stats Purposes
		/*LONG conv_cnt;
		LONG div_cnt;
		LONG branch_cnt;
		LONG cnt_insts;
		LONG cnt_dvinsts;
		LONG reg_reads;
		LONG reg_writes;*/
		
		LONG r_equal_masks;
		LONG r_non_inter_masks;
		LONG r_strict_sub;
		LONG r_strict_super;
		LONG r_other;
	
		LONG c_w_equal_masks, d_w_equal_masks;
		LONG w_non_inter_masks;
		LONG w_strict_sub;
		LONG w_strict_super;
		LONG w_other;

		LONG w_merge_event, r_merge_event, merge_event;
		//////////////////////////////////////////////////////

		void init(Config conf){
			//////////////////////////////////////////////////////////
			//For OoO DITVA Stats
			/////////////////////////////////////////////////////////
		
			r_equal_masks = 0;
			r_non_inter_masks = 0;
			r_strict_sub = 0;	
			r_strict_super = 0; r_other = 0;
			c_w_equal_masks = 0; d_w_equal_masks = 0;
			w_non_inter_masks = 0;
			w_strict_sub = 0;	
			w_strict_super = 0; w_other = 0;
			w_merge_event = 0, r_merge_event = 0, merge_event = 0;
			/////////////////////////////////////////////////////////
			num_cuops = 0;

			total_dv_committed = 0;
			total_inst_committed = 0;

			print = false;
			logPeriodic.open(conf.periodicfile);
			num_threads= conf.num_threads;
			issue_width = conf.issue_width;


			simd_count = 0;
			no_issuable_instructions = 0;

			int_reg_file_read = 0;
			int_reg_file_write = 0;
			float_reg_file_read = 0;
			float_reg_file_write = 0;

			reg_rename_stalls = 0;
			pirat_stalls = 0;
			waw_stalls = 0;
			rob_stalls = 0;
			decBuffer_stalls = 0;
			issue_stalls = 0;

			ialu_access = 0;
			fpu_access = 0;
			mul_access = 0;
			div_access = 0;

			icache_access_cycles = 0;
			total_exec_inst = 0;
			dv_int_count = 0;
			dv_fp_count = 0;
			dv_branch = 0;

			buop_count = 0;
			phi_count = 0; branch_nops = 0;
			write_merges = 0;
			read_merges = 0;
			write_fp_merges = 0;
			read_fp_merges = 0;	

			pirat_wr_accesses = 0;
			pirat_rd_accesses = 0;
			pirat_fp_wr_accesses = 0;
			pirat_fp_rd_accesses = 0;

			pirat_default_rd = 0;
			pirat_partial_rd = 0;	
			pirat_fp_default_rd = 0;
			pirat_fp_partial_rd = 0;

			dv_inst_count = 0;
			fetch_inst_count = 0;
			int_inst_count = 0;
			fp_inst_count = 0;
			exe_dvinst_count = 0;
			exe_inst_count = 0;
			exe_cycles = 0;
			issue_inst_count = 0;

			dv_load = 0; counting_loads = 0;
			dv_store = 0; counting_stores = 0;
			convergence_count = 0;
			divergence_count = 0;
			iw_full = 0;
			fu_full = 0;
			dependant = 0;
			outstandingls = 0;
			exception_uops = 0;

			l1_hit_r = 0; l1_hit_w = 0;
			l1_miss_r = 0; l1_miss_w = 0;
			l2_hit = 0;
			l2_miss = 0;


			cycle = 0;
			total_ins_count = 0;
			iq_empty = 0;

			//Periodic
			periodic_total_exec_inst = 0;
			periodic_dv_inst_count = 0;
			periodic_dv_load = 0;
			periodic_dv_store = 0;
			periodic_convergence_count = 0;
			periodic_divergence_count = 0;
			periodic_iw_full = 0;
			periodic_fu_full = 0;
			periodic_dependant = 0;
			periodic_outstandingls = 0;

			periodic_cycle = 0;
			periodic_total_ins_count = 0;
			periodic_iq_empty = 0;
			full_misprediction = 0;
			partial_misprediction = 0; correct_predict = 0;
			branch_misprediction = 0;
			for(int i = 0 ; i< conf.issue_width+1; i++){
				issue_count.push_back(0);
				periodic_issue_count.push_back(0);
			}

			for(int i = 0; i < conf.num_warps; i++){
				max_paths.push_back(0);
			}

			for(int i = 0; i < (int)(conf.num_threads/conf.num_warps); i++){
				thread_occupancy.push_back(0);
			}

	
			for(int i = 0; i < (conf.issue_width*3)+1; i++){ //max 3 phis that may be generated per instruction
				phi_frequency.push_back(0);
				phi_frequency_fp.push_back(0);
			}

			for(int i = 0 ; i< (conf.issue_width*(int)num_threads)+1; i++){
				thread_issue_count.push_back(0);
				periodic_thread_issue_count.push_back(0);
			}
			//combine_count = new std::vector<LONG>[numthreads+1];
			for(int i = 0 ;i< conf.num_threads+1; i++){
				//combine_count[i] = 0ull;
				iq_blocked_count.push_back(0);
				combine_count.push_back(0);
				shared_data.push_back(0);
				ins_commit.push_back(0);
				load_count.push_back(0);
				store_count.push_back(0);

				load_count_at_fetch.push_back(0);
				store_count_at_fetch.push_back(0);

				reg_read_int.push_back(0);
				reg_write_int.push_back(0);
				reg_read_float.push_back(0);
				reg_write_float.push_back(0);
				//branch_misprediction.push_back(0);

				branches.push_back(0);
				lsdivergence.push_back(0);

				inst_int.push_back(0);
				scalarized_instruction.push_back(0);
				inst_fp.push_back(0);
				inst_ld.push_back(0);
				inst_st.push_back(0);

				ins_count.push_back(0);

				ls_l1_hit.push_back(0);
				ls_l2_hit.push_back(0);
				ls_l2_miss.push_back(0);

				partial_miss = 0;

				//Periodic
				periodic_iq_blocked_count.push_back(0);
				periodic_combine_count.push_back(0);
				periodic_shared_data.push_back(0);
				periodic_ins_commit.push_back(0);
				periodic_load_count.push_back(0);
				periodic_store_count.push_back(0);

				periodic_load_count_at_fetch.push_back(0);
				periodic_store_count_at_fetch.push_back(0);

				periodic_reg_read_int.push_back(0);
				periodic_reg_write_int.push_back(0);
				periodic_reg_read_float.push_back(0);
				periodic_reg_write_float.push_back(0);
				periodic_branch_misprediction.push_back(0);
				periodic_branches.push_back(0);
				periodic_lsdivergence.push_back(0);

				periodic_inst_int.push_back(0);
				periodic_scalarized_instruction.push_back(0);
				periodic_inst_fp.push_back(0);
				periodic_inst_ld.push_back(0);
				periodic_inst_st.push_back(0);


				periodic_ins_count.push_back(0);

				periodic_ls_l1_hit.push_back(0);
				periodic_ls_l2_hit.push_back(0);
				periodic_ls_l2_miss.push_back(0);
			}
			for(int i = 0 ; i< NUM_CACHE+1 ; i++){

				cache_miss.push_back(0);
				cache_read_access.push_back(0);
				cache_write_access.push_back(0);
				cache_miss_nonscalar.push_back(0);
				cache_read_miss_nonscalar.push_back(0);
				cache_write_miss_nonscalar.push_back(0);
				//Periodic
				periodic_cache_miss.push_back(0);
				periodic_cache_read_miss.push_back(0);
				periodic_cache_write_miss.push_back(0);
				periodic_cache_miss_nonscalar.push_back(0);
				periodic_cache_read_miss_nonscalar.push_back(0);
				periodic_cache_write_miss_nonscalar.push_back(0);
			}
			//l1_miss = 0;
			//l2_miss = 0;


		}

		void reinit(Config conf){
			total_dv_committed = 0;
			total_inst_committed = 0;
			reg_rename_stalls = 0;
			pirat_stalls = 0;
			waw_stalls = 0;

			print = true;
			total_exec_inst = 0;
			dv_inst_count = 0;
			dv_load = 0; counting_loads = 0;
			dv_store = 0; counting_stores = 0;
			convergence_count = 0;
			divergence_count = 0;
			outstandingls = 0;

			iw_full= 0;
			fu_full = 0;
			dependant =0;


			cycle = 0;
			total_ins_count = 0;
			iq_empty = 0;

			//branch_misprediction = 0;

			//Periodic
			periodic_total_exec_inst = 0;
			periodic_dv_inst_count = 0;
			periodic_dv_load = 0;
			periodic_dv_store = 0;
			periodic_convergence_count = 0;
			periodic_divergence_count = 0;
			periodic_outstandingls = 0;

			periodic_iw_full= 0;
			periodic_fu_full = 0;
			periodic_dependant =0;

			periodic_cycle = 0;
			periodic_total_ins_count = 0;
			periodic_iq_empty = 0;
			for(int i = 0 ; i< conf.issue_width +1; i++){
				issue_count[i] = 0;
				periodic_issue_count[i] = 0;
			}

			for(int i = 0 ; i< (conf.issue_width * (int)num_threads )+1; i++){
				thread_issue_count[i] = 0;
				periodic_thread_issue_count[i] = 0;
			}
			//combine_count = new std::vector<LONG>[numthreads+1];
			for(int i = 0 ;i< conf.num_threads+1; i++){
				//combine_count[i] = 0ull;
				iq_blocked_count[i] = 0;
				combine_count[i] = 0;
				shared_data[i] = 0;
				ins_commit[i] = 0;
				load_count[i] = 0;
				store_count[i] = 0;

				load_count_at_fetch[i] = 0;
				store_count_at_fetch[i] = 0;

				reg_read_int[i] = 0;
				reg_write_int[i] = 0;
				reg_read_float[i] = 0;
				reg_write_float[i] = 0;
				//branch_misprediction[i] = 0;
				branches[i] = 0;
				periodic_lsdivergence[i] = 0;

				inst_int[i] = 0;
				scalarized_instruction[i] = 0;
				inst_fp[i] = 0;
				inst_ld[i] = 0;
				inst_st[i] = 0;

				ins_count[i] = 0;
				ls_l1_hit[i] = 0;
				ls_l2_hit[i] = 0;
				ls_l2_miss[i] = 0;

				partial_miss = 0;

				//Periodic
				periodic_iq_blocked_count[i] = 0;
				periodic_combine_count[i] = 0;
				periodic_shared_data[i] = 0;
				periodic_ins_commit[i] = 0;
				periodic_load_count[i] = 0;
				periodic_store_count[i] = 0;

				periodic_load_count_at_fetch[i] = 0;
				periodic_store_count_at_fetch[i] = 0;

				periodic_reg_read_int[i] = 0;
				periodic_reg_write_int[i] = 0;
				periodic_reg_read_float[i] = 0;
				periodic_reg_write_float[i] = 0;
				periodic_branch_misprediction[i] = 0;
				periodic_branches[i] = 0;

				periodic_inst_int[i] = 0;
				periodic_scalarized_instruction[i] = 0;
				periodic_inst_fp[i] = 0;
				periodic_inst_ld[i] = 0;
				periodic_inst_st[i] = 0;

				periodic_ins_count[i] = 0;
				periodic_ls_l1_hit[i] = 0;
				periodic_ls_l2_hit[i] = 0;
				periodic_ls_l2_miss[i] = 0;


			}
			for(int i = 0 ; i< NUM_CACHE+1 ; i++){
				cache_miss[i] = 0;
				cache_read_access[i] = 0;
				cache_write_access[i] = 0;
				cache_miss_nonscalar[i] = 0;
				cache_read_miss_nonscalar[i] = 0;
				cache_write_miss_nonscalar[i] = 0;

				//Periodic
				periodic_cache_miss[i] = 0;
				periodic_cache_read_miss[i] = 0;
				periodic_cache_write_miss[i] = 0;
				periodic_cache_miss_nonscalar[i] = 0;
				periodic_cache_read_miss_nonscalar[i] = 0;
				periodic_cache_write_miss_nonscalar[i] = 0;
			}
			//l1_miss = 0;
			//l2_miss = 0;


		}

		void reset(){

			//Periodic
			periodic_total_exec_inst = total_exec_inst;
			periodic_dv_inst_count = dv_inst_count;
			periodic_dv_load = dv_load;
			periodic_dv_store = dv_store;
			periodic_convergence_count = convergence_count;
			periodic_divergence_count = divergence_count;
			periodic_outstandingls = outstandingls;

			periodic_iw_full= iw_full;
			periodic_fu_full = fu_full;
			periodic_dependant =dependant;

			periodic_cycle = cycle;
			periodic_total_ins_count = total_ins_count;
			periodic_iq_empty = iq_empty;
			for(int i = 0 ; i< issue_width +1; i++){
				periodic_issue_count[i] = issue_count[i];
			}
			for(int i = 0 ; i < ((int)num_threads*issue_width) +1; i++){
				periodic_thread_issue_count[i] = thread_issue_count[i];
			}
			//combine_count = new std::vector<LONG>[numthreads+1];
			for(UINT i = 0 ;i< num_threads+1; i++){
				//combine_count[i] = 0ull;

				//Periodic
				periodic_iq_blocked_count[i] = iq_blocked_count[i];
				periodic_combine_count[i] = combine_count[i];
				periodic_shared_data[i] = shared_data[i];
				periodic_ins_commit[i] = ins_commit[i];
				periodic_load_count[i] = load_count[i] ;
				periodic_store_count[i] = store_count[i];

				periodic_load_count_at_fetch[i] = load_count_at_fetch[i];
				periodic_store_count_at_fetch[i] = store_count_at_fetch[i];

				periodic_reg_read_int[i] = 	reg_read_int[i];
				periodic_reg_write_int[i] = reg_write_int[i];
				periodic_reg_read_float[i] = reg_read_float[i];
				periodic_reg_write_float[i] = reg_write_float[i] ;
				periodic_branch_misprediction[i] = branch_misprediction;
				periodic_branches[i] = branches[i] ;
				periodic_lsdivergence[i] = lsdivergence[i];

				periodic_inst_int[i] = inst_int[i];
				periodic_scalarized_instruction[i] = scalarized_instruction[i];
				periodic_inst_fp[i] = inst_fp[i];
				periodic_inst_ld[i] = inst_ld[i];
				periodic_inst_st[i] = inst_st[i];

				periodic_ins_count[i] = ins_count[i];

				periodic_ls_l1_hit[i] = ls_l1_hit[i];
				periodic_ls_l2_hit[i] = ls_l2_hit[i];
				periodic_ls_l2_miss[i] = ls_l2_miss[i];


			}
			for(int i = 0 ; i< NUM_CACHE+1 ; i++){

				//Periodic
				periodic_cache_miss[i] = cache_miss[i] ;
				periodic_cache_read_miss[i] = cache_read_access[i];
				periodic_cache_write_miss[i] = cache_write_access[i];
				periodic_cache_miss_nonscalar[i] = cache_miss_nonscalar[i];
				periodic_cache_read_miss_nonscalar[i] = cache_read_miss_nonscalar[i] ;
				periodic_cache_write_miss_nonscalar[i] = cache_write_miss_nonscalar[i];
			}

		}
		void printperiodic(){
			if(!print){
				return;
			}
			LONG cycle_count = cycle-periodic_cycle;
			logPeriodic<<"Cycle: "<<cycle<<" IPC: " << (total_ins_count-periodic_total_ins_count)/(double)(cycle_count) << " DIV: "<<(divergence_count - periodic_divergence_count);
			logPeriodic<<" Conv: "<<convergence_count - periodic_convergence_count<<" DVInst: "<<dv_inst_count - periodic_dv_inst_count <<" DVLoad: "<<dv_load - periodic_dv_load;
			logPeriodic<<" DVStore: "<<dv_store-periodic_dv_store<<" Outstandingls: "<<outstandingls - periodic_outstandingls<< " Dep: "<<dependant - periodic_dependant<< " FU: "<<fu_full- periodic_fu_full;

			for(UINT i = 0 ;i<num_threads;i++){
				logPeriodic<<" T"<<i<<": "<<ins_count[i]-periodic_ins_count[i];
			}
			for(UINT i = 0 ;i<num_threads;i++){
				logPeriodic<<" B"<<i<<": "<<branches[i]-periodic_branches[i];
			}
			/*for(UINT i = 0 ;i<num_threads;i++){
				logPeriodic<<" MB"<<i<<": "<<branch_misprediction[i]-periodic_branch_misprediction[i];
			}*/
			logPeriodic<<" CM1: "<<cache_miss_nonscalar[1]-periodic_cache_miss_nonscalar[1]<<" CM2: "<<cache_miss_nonscalar[2]-periodic_cache_miss_nonscalar[2];

			for(int i = 0 ; i< issue_width +1; i++){
				logPeriodic<<" IC"<<i<<": "<< issue_count[i] -  periodic_issue_count[i];
			}
			logPeriodic<<" IQMT: "<<iq_empty - periodic_iq_empty;
			for(UINT i = 0 ;i<num_threads+1;i++){
				logPeriodic<<" IQBl"<<i<<": "<<iq_blocked_count[i] - periodic_iq_blocked_count[i];
			}

			for(UINT i = 0 ;i<num_threads+1;i++){
				logPeriodic<<" ls_l1_hit"<<i<<": "<<ls_l1_hit[i]-periodic_ls_l1_hit[i];
			}

			for(UINT i = 0 ;i<num_threads+1;i++){
				logPeriodic<<" ls_l2_hit"<<i<<": "<<ls_l2_hit[i]-periodic_ls_l2_hit[i];
			}

			for(UINT i = 0 ;i<num_threads+1;i++){
				logPeriodic<<" ls_l2_miss"<<i<<": "<<ls_l2_miss[i]-periodic_ls_l2_miss[i];
			}
			for(UINT i = 0 ;i<num_threads+1;i++){
				logPeriodic<<" lsdiv"<<i<<": "<<lsdivergence[i]-periodic_lsdivergence[i];
			}

			for(UINT i = 0 ;i < (issue_width*num_threads)+1;i++){
				logPeriodic<<" IT"<<i<<": "<<thread_issue_count[i]-periodic_thread_issue_count[i];
			}
			logPeriodic<<endl;
			reset();
		}
		Statistics(){};
	};



	



#define SASSERT_DECODING(cond) if (!(cond)) {fprintf(stderr,"\nExecution Aborted.\nFile %s assert at line %d\n",__FILE__,__LINE__); abort();}


#ifndef SA_TRACER
	struct MmtInstruction{
		std::map<sa_uoptype, uint32_t> instruction_latency;
		std::map<sa_uoptype, uint32_t> dyn_instruction_latency;


	public:
		MmtInstruction(){
			instruction_latency[SA_UOP_UNKNOWN] = 1;
			instruction_latency[SA_UOP_SYSCALL] = 1;
			instruction_latency[SA_UOP_MAGIC] = 1;
			instruction_latency[SA_UOP_NOP] = 1;
			instruction_latency[SA_UOP_ALU] = 1;
			instruction_latency[SA_UOP_MOV] = 1;
			instruction_latency[SA_UOP_BRANCH] = 1;
			instruction_latency[SA_UOP_SHIFT] = 1;
			instruction_latency[SA_UOP_ADDR] = 1; //NOt correct
			instruction_latency[SA_UOP_LOAD] = 1;
			instruction_latency[SA_UOP_STORE] = 1;
			instruction_latency[SA_UOP_FP] = 4;
			instruction_latency[SA_UOP_SSE] = 2;

			instruction_latency[SA_UOP_AVX] = 4; //Not correct
			instruction_latency[SA_UOP_SSE2] = 4; //Not correct

			instruction_latency[SA_UOP_MMX] = 1;
			instruction_latency[SA_UOP_IMUL] = 3;
			instruction_latency[SA_UOP_IDIV] = 20;
			instruction_latency[SA_UOP_INTLONG] = 1;
			instruction_latency[SA_UOP_FPDIV] = 7;

			instruction_latency[SA_UOP_FPMUL] = 4;

			instruction_latency[SA_UOP_FPLONG] = 4;
			instruction_latency[SA_UOP_LAST] = 1; //Not correct

			instruction_latency[SA_UOP_POP] = 1;
			instruction_latency[SA_UOP_PUSH] = 1;
			instruction_latency[SA_UOP_SEMAPHORE] = 1;
			instruction_latency[SA_UOP_SYSTEM] = 1;

			dyn_instruction_latency[SA_UOP_UNKNOWN] = 1;
			dyn_instruction_latency[SA_UOP_SYSCALL] = 1;
			dyn_instruction_latency[SA_UOP_MAGIC] = 1;
			dyn_instruction_latency[SA_UOP_NOP] = 1;
			dyn_instruction_latency[SA_UOP_ALU] = 2;
			dyn_instruction_latency[SA_UOP_MOV] = 1;
			dyn_instruction_latency[SA_UOP_BRANCH] = 1;
			dyn_instruction_latency[SA_UOP_SHIFT] = 1;
			dyn_instruction_latency[SA_UOP_ADDR] = 2; //NOt correct
			dyn_instruction_latency[SA_UOP_LOAD] = 1;
			dyn_instruction_latency[SA_UOP_STORE] = 1;
			dyn_instruction_latency[SA_UOP_FP] = 4;
			dyn_instruction_latency[SA_UOP_SSE] = 2;

			dyn_instruction_latency[SA_UOP_AVX] = 4; //Not correct
			dyn_instruction_latency[SA_UOP_SSE2] = 4; //Not correct

			dyn_instruction_latency[SA_UOP_MMX] = 1;
			dyn_instruction_latency[SA_UOP_IMUL] = 4;
			dyn_instruction_latency[SA_UOP_IDIV] = 7;
			dyn_instruction_latency[SA_UOP_INTLONG] = 1;
			dyn_instruction_latency[SA_UOP_FPDIV] = 7;

			dyn_instruction_latency[SA_UOP_FPMUL] = 4;

			dyn_instruction_latency[SA_UOP_FPLONG] = 4;
			dyn_instruction_latency[SA_UOP_LAST] = 1; //Not correct

			dyn_instruction_latency[SA_UOP_POP] = 1;
			dyn_instruction_latency[SA_UOP_PUSH] = 1;
			dyn_instruction_latency[SA_UOP_SEMAPHORE] = 1;
			dyn_instruction_latency[SA_UOP_SYSTEM] = 1;


		}
		~MmtInstruction(){

		}

		string getUopString(int index);
		int getInstructionLatency(sa_uoptype uop);
		int getInstructionLatency(sa_uoptype uop, bool dynvec);
		int getScalarInstCount(sa_uoptype uop);

		sa_uoptype optype(UINT32 category, UINT32 opcode, UINT32 extension);
	};
#else
	struct DVInstLatency{
		std::map<sa_uoptype, uint32_t> instruction_latency;
	public:
		DVInstLatency(){

			instruction_latency[SA_UOP_UNKNOWN] = 1;
			instruction_latency[SA_UOP_SYSCALL] = 1;
			instruction_latency[SA_UOP_MAGIC] = 1;
			instruction_latency[SA_UOP_NOP] = 1;
			instruction_latency[SA_UOP_ALU] = 1;
			instruction_latency[SA_UOP_MOV] = 1;
			instruction_latency[SA_UOP_BRANCH] = 1;
			instruction_latency[SA_UOP_SHIFT] = 1;
			instruction_latency[SA_UOP_ADDR] = 1;
			instruction_latency[SA_UOP_LOAD] = 1;
			instruction_latency[SA_UOP_STORE] = 1;
			instruction_latency[SA_UOP_FP] = 4;
			instruction_latency[SA_UOP_SSE] = 1;
			instruction_latency[SA_UOP_SSE_FPVEC] = 4;
			instruction_latency[SA_UOP_SSE_FPSCAL] = 4;
			instruction_latency[SA_UOP_SSE_FPMULVEC] = 4;
			instruction_latency[SA_UOP_SSE_FPMULSCAL] = 4;
			instruction_latency[SA_UOP_SSE_FPDIVVEC] = 20;
			instruction_latency[SA_UOP_SSE_FPDIVSCAL] = 20;
			instruction_latency[SA_UOP_SSE_IMUL] = 3;
			instruction_latency[SA_UOP_SSE_IDIV] = 20;
			instruction_latency[SA_UOP_SSE_IMOV] = 1;
			instruction_latency[SA_UOP_SSE_FPMOVVEC] = 1;
			instruction_latency[SA_UOP_SSE_FPMOVSCAL] = 1;
			instruction_latency[SA_UOP_AVX] = 1;
			instruction_latency[SA_UOP_AVX_FPVEC] = 4;
			instruction_latency[SA_UOP_AVX_FPSCAL] = 4;
			instruction_latency[SA_UOP_AVX_FPMULVEC] = 4;
			instruction_latency[SA_UOP_AVX_FPMULSCAL] = 4;
			instruction_latency[SA_UOP_AVX_FPDIVVEC] = 20;
			instruction_latency[SA_UOP_AVX_FPDIVSCAL] = 20;
			instruction_latency[SA_UOP_AVX_IMUL] = 3;
			instruction_latency[SA_UOP_AVX_IDIV] = 20;
			instruction_latency[SA_UOP_AVX_IMOV] = 1;
			instruction_latency[SA_UOP_AVX_FPMOVVEC] = 1;
			instruction_latency[SA_UOP_AVX_FPMOVSCAL] = 1;
			instruction_latency[SA_UOP_SSE2] = 4;
			instruction_latency[SA_UOP_MMX] = 1;
			instruction_latency[SA_UOP_IMUL] = 3;
			instruction_latency[SA_UOP_IDIV] = 20;
			instruction_latency[SA_UOP_INTLONG] = 1;
			instruction_latency[SA_UOP_FPDIV] = 7;
			instruction_latency[SA_UOP_FPMUL] = 4;
			instruction_latency[SA_UOP_FPLONG] = 4;
			instruction_latency[SA_UOP_LAST] = 1;
			instruction_latency[SA_UOP_POP] = 1;
			instruction_latency[SA_UOP_PUSH] = 1;
			instruction_latency[SA_UOP_SEMAPHORE] = 1;
			instruction_latency[SA_UOP_SYSTEM] = 1;

		}
		int getInstructionLatency(sa_uoptype uop);
	};

#endif

	struct cache_conflict{
		LONG address;
		int tid;
		W64 current_cycle;
		int fuid;
		int refcount;
		W64 issue_cycle;
		int clusterid;
		cache_conflict(const cache_conflict& rhs ) {
			this->address = rhs.address;
			this->tid = rhs.tid;
			this->current_cycle = rhs.current_cycle;
			this->fuid = rhs.fuid;
			this->refcount = rhs.refcount;
			this->issue_cycle = rhs.issue_cycle;
			this->clusterid = rhs.clusterid;
		}
	};

	struct mem_req{
		std::vector<int> coreid;
		LONG address;
		LONG block_address;
		int size;
		int tid;
		int delay;
		Memoperation operation;
		int ins_count;
		int tlb_index;
		int bankindexhashed;
		int bankindexlow;
		//	int delay;
		bool write_back;
		bool scalarise;
		bool issued;
		bool is_sse;
		bool is_avx;
		int inst_delay;
#ifdef V_SCALARISE
		int word;
#endif

		bool operator==(const mem_req & rhs) const{
			return (block_address == rhs.block_address);
		}
		mem_req(){
#ifdef V_SCALARISE
			word = 0;
#endif
			is_sse = false;
			is_avx = false;
			issued = false;
			inst_delay = 0;
			ins_count = 0;
			scalarise = false;
			block_address = 0;
			address = 0;
			size = 0;
			tid= 0 ;
			delay = 0;
			operation = MEM_NONE;

			tlb_index = 0;
			bankindexhashed = 0;
			bankindexlow = 0;
			//	int delay;
			write_back = false;
		};
		mem_req& operator=(const mem_req& rhs) {
			is_sse = rhs.is_sse;
#ifdef V_SCALARISE
			word = rhs.word;
#endif
			is_avx = rhs.is_avx;
			inst_delay = rhs.inst_delay;
			issued = rhs.issued;
			ins_count = rhs.ins_count;
			address = rhs.address;
			scalarise = rhs.scalarise;
			size = rhs.size ;
			tid = rhs.tid;
			delay = rhs.delay;
			operation = rhs.operation;

			tlb_index = rhs.tlb_index;
			bankindexhashed = rhs.bankindexhashed;
			bankindexlow = rhs.bankindexlow;
			delay = rhs.delay;
			write_back = rhs.write_back;
			coreid = rhs.coreid;
			block_address = rhs.block_address;
			return *this;
		}
		mem_req(const mem_req& rhs){
#ifdef V_SCALARISE
			word = rhs.word;
#endif
			is_sse = rhs.is_sse;
			is_avx = rhs.is_avx;
			inst_delay = rhs.inst_delay;
			issued = rhs.issued;
			address = rhs.address;
			ins_count = rhs.ins_count;
			scalarise = rhs.scalarise;
			size = rhs.size ;
			tid = rhs.tid;
			delay = rhs.delay;
			operation = rhs.operation;

			tlb_index = rhs.tlb_index;
			bankindexhashed = rhs.bankindexhashed;
			bankindexlow = rhs.bankindexlow;
			delay = rhs.delay;
			write_back = rhs.write_back;
			coreid = rhs.coreid;
			block_address = rhs.block_address;
		}

	};

	
	class MSHR{
	private:
		unordered_map<ADDRINT, int > item_map;
		size_t _size;
		LONG _current_cycle;
		int byte_offset;

	private:
	
		void clean(){
			for (unordered_map<ADDRINT, int >::iterator it = item_map.begin(); it != item_map.end(); ) {
				if(it->second == 1){
					//cout<<"Removing MSHR "<<hex16(it->first)<<endl;
					log(logDEBUG4)<<"Removing "<<hex16(it->first)<<" second "<<it->second<< " @cycle "<<_current_cycle<<endl;
					it = item_map.erase(it);
				}else{
					it++;
				}
			}
		}

		void put(const ADDRINT &key){

			auto it = item_map.find(key);
			if(it != item_map.end()){
				//assert(val >= it->second);
				return;
			}
			if(item_map.size() > _size){
				cout<<"Too big"<<endl;
			}
			assert(item_map.size() + 1 < _size);
			item_map.insert(make_pair(key, 0));
		
		};
		bool exist(const ADDRINT &key){
			return (item_map.count(key)>0);
		};


		W64 extract_value(W64 value, int low, int high){
			//value = value << high;
			//return value >> (low + high);
			return (value >> low) & ~(~0 << (high-low+1));
		}
	public:
		MSHR( int byte_offset_, int _size_):_size( _size_){
			;
			_current_cycle = 0ULL;
			//l1_cache = l1_cache_;
			byte_offset = byte_offset_;
		};

		MSHR(){
			_current_cycle = 0ULL;
			//l1_cache = NULL;
		};


		void update(const ADDRINT address){
			//W64 tag = extract_value(address, index_offset,63);
			W64 block = extract_value(address, byte_offset,63);
			if(exist(block)){
				auto it = item_map.find(block);
				log(logDEBUG4)<<"Updating "<<hex16(address)<<" second "<<_current_cycle<<endl;
				it->second = 1;
			}else{
				assert(false);
			}
		}

		void cleanmshr(LONG current_cycle){
			_current_cycle = current_cycle;
			clean();

		}

		bool insert(const ADDRINT address){
			assert(item_map.size()  <= _size);
			log(logDEBUG4)<<"Insert MSHR "<<hex16(address)<<" second "<<_current_cycle<<endl;
			W64 block = extract_value(address, byte_offset,63);

			if(exist(block)){
				auto it = item_map.find(block);
				bool avail = (it->second == 1);
				return avail;
			}else{

				put(block);
				return false;
			}
			//assert(exist(key));

		};

		bool mshrFULL(){
			return (item_map.size() + 2 > _size);
		}
		bool inMSHR(W64 address){

			W64 block = extract_value(address, byte_offset,63);
			return exist(block);
			//return exist(address);
		}


		bool ready1(const ADDRINT address){
			//log(logDEBUG4)<<"Ready "<<hex16(address)<< " @cycle "<<_current_cycle<<endl;
			W64 block = extract_value(address, byte_offset,63);
			if(exist(block)){
				auto it = item_map.find(block);
				bool avail = (it->second == 1);

				return avail;
			}else{
				return false;
			}
		};

	};

	class MSHR_OLD{
	private:
		//list< pair<KEY_T,VAL_T> > item_list;
		//unordered_map<KEY_T, decltype(item_list.begin()) > item_map;
		unordered_map<ADDRINT, LONG > item_map;
		size_t _size;
		LONG _current_cycle;
		//Cache *l1_cache;
		int index_offset;


	private:
		
		void clean(LONG current_cycle){
			for (unordered_map<ADDRINT, LONG >::iterator it = item_map.begin(); it != item_map.end(); ) {
				if(it->second < current_cycle){
					log(logDEBUG4)<<"Removing "<<it->first<<" second "<<it->second<< " @cycle "<<current_cycle<<endl;
					it = item_map.erase(it);
				}else{
					it++;
				}
			}
		}

		void put(const ADDRINT &key, const LONG &val){
		
			auto it = item_map.find(key);
			if(it != item_map.end()){
				assert(val >= it->second);
				return;
			}
			if(item_map.size() > _size){
				cout<<"Too big"<<endl;
			}
			assert(item_map.size() + 1 < _size);
			//item_list.push_front(make_pair(key,val));
			item_map.insert(make_pair(key, val));
			//return true;
			//clean();
		};
		bool exist(const ADDRINT &key){
			/*if(current_cycle > _current_cycle){
			clean(current_cycle);
			_current_cycle = current_cycle;
		}*/
			return (item_map.count(key)>0);
		};


		W64 extract_value(W64 value, int low, int high){
			//value = value << high;
			//return value >> (low + high);
			return (value >> low) & ~(~0 << (high-low+1));
		}
	public:
		MSHR_OLD( int index_offset_, int _size_):_size( _size_*6){
			;
			_current_cycle = 0ULL;
			//l1_cache = l1_cache_;
			index_offset = index_offset_;
		};

		MSHR_OLD(){
			_current_cycle = 0ULL;
			//l1_cache = NULL;
		};


		bool ready(const ADDRINT address, LONG current_cycle, LONG avail_cycle){
			if(current_cycle > _current_cycle){
				clean(current_cycle);
				_current_cycle = current_cycle;
			}
			//W64 tag = address;
			W64 tag = extract_value(address, index_offset,63);
			if(exist(tag)){
				auto it = item_map.find(tag);
				//item_list.splice(item_list.begin(), item_list, it->second);
				bool avail = (it->second <= current_cycle);
				/*if(avail){
				item_map.erase(it);
			}*/
				return avail;
			}else{
				assert(avail_cycle >= current_cycle);
				//if(avail_cycle == 33896){
				//cout<<"33896 found"<<endl;
				//}
				put(tag,avail_cycle);
				log(logDEBUG4)<<"Adding "<<tag<<" second "<<avail_cycle<< " @cycle "<<current_cycle<<endl;
				return false;
			}
			//assert(exist(key));

		};

	};

	struct Cacheline{
		LONG dat;
		bool dirty;
		LONG address; //Need not be stored. Just for writebacks
	};

	//template <class KEY_T, class VAL_T>

	class LRU{
	private:
		list< pair<W64,Cacheline> > item_list;
		unordered_map<W64, decltype(item_list.begin()) > item_map;
		size_t cache_size;
	private:
		Cacheline clean(void){
			if(item_map.size() > cache_size){
				auto last_it = item_list.end(); last_it --; //last element
				item_map.erase(last_it->first); //remove from the map
				Cacheline ret = last_it->second; // removed cacheline
				item_list.pop_back();
				//cout<<"Popping "<<cache_size<<endl;
				return ret;
			}else{
				Cacheline ret;
				ret.address = 0ULL;
				return ret;
			}
		};
	public:
		LRU(int cache_size_):cache_size(cache_size_){
			;
		};

		LRU(){};

		void set_size(int cache_size_){
			cache_size = cache_size_;
		}

		Cacheline put(const W64 &key, const Cacheline &val){
			auto it = item_map.find(key);
			if(it != item_map.end()){
				item_list.erase(it->second); // Remove the cacheline from its current position
				item_map.erase(it); //To reinsert the head remove the map
			}
			item_list.push_front(make_pair(key,val)); // Insert the cacheline in the head (LRU)
			item_map.insert(make_pair(key, item_list.begin())); // Insert the cache line in the map
			return clean();
		};
		bool exist(const W64 &key){
			return (item_map.count(key)>0);
		};
		Cacheline get(const W64 &key, bool dirty){
			assert(exist(key));
			auto it = item_map.find(key);
			if(dirty){
				it->second->second.dirty = true;
			}
			item_list.splice(item_list.begin(), item_list, it->second);
			return it->second->second;
		};

	};

	struct Request{
		std::vector<int> coreid;
		W64 block_address;
		W64 avail_cycle;
		//W64 address;
		int block_cycles;
		bool miss;
		mem_req memreq;


		/*Request(W64 _tag){
			tag = _tag;
			avail_cycle = 0Ull;

		}*/

		Request(){block_address = 0; avail_cycle = 0Ull; block_cycles = 0;miss=true;};
		Request(W64 _tag, W64 _avail_cycle, W64 _address, int _block_cycles, int _coreid){
			block_address = _tag;
			avail_cycle = _avail_cycle;
			//address = _address;
			block_cycles = _block_cycles;
			miss = true;
			coreid.push_back(_coreid);
		}

		Request(W64 _tag, W64 _avail_cycle, mem_req req){
			block_address = _tag;
			avail_cycle = _avail_cycle;
			memreq = req;
			block_cycles = 0;
			miss = true;
			coreid=req.coreid;
		}

		/*bool operator==(const Request &lhs, const Request &rhs) {
			return lhs.tag == rhs.tag;
		}*/

		bool operator==(const Request &rhs) const {
			return block_address == rhs.block_address;
		}

		Request& operator=(const Request& rhs) {
			block_address = rhs.block_address;
			avail_cycle = rhs.avail_cycle;
			//address = rhs.address;
			memreq = rhs.memreq;
			block_cycles = rhs.block_cycles;
			miss = rhs.miss;
			coreid = rhs.coreid;
			return *this;
		}

		Request(const Request& rhs){
			block_address = rhs.block_address;
			avail_cycle = rhs.avail_cycle;
			memreq = rhs.memreq;
			//address = rhs.address;
			miss = rhs.miss;
			block_cycles = rhs.block_cycles;
			coreid = rhs.coreid;
		}
	};


	//enum LS_Operation {NONE,LOAD,STORE};
	struct Cache{

		//std::multimap<W64,W64> mshr_map;
		CacheParams params;
		W64 histogram[sizeof(W64)*8];
		FILE *histogramfile;

		int cache_size; //in kb
		int cache_ways;
		int cache_line_size;
		int cache_latency;
		//W64 *cache_timestamp;
		//W64 *cache_line;
		LRU *cache_set;

		W64 *cache_avail_cycle;
		//bool *cache_ready;
		int cache_line_numbers;
		int byte_offset;
		int index_offset;
		int tag_offset;
		int cache_level;
		Cache *l2_cache;
		TLB *split_tlb_8;
		TLB *unified_tlb_8;
		TLB *split_tlb_16;
		TLB *unified_tlb_16;

		W64 cache_cycle;
		int bank_conflict_current_cycle;

		int num_ports;
		int *port_fuid; //TODO: Dont hardcode

		int used_port;

		int gs_count;
		W64 port_cycle;
		//int block_cycle;
		bool blocked;
		W64 *port_issue_cycle;
		Memoperation *port_operation;

		std::vector<mem_req> mem_request_list;
		std::deque<mem_req> llc_miss_list;
		std::vector<mem_req> delayed_load;

		Config conf;
		Statistics *stats;

		MSHR *mshr;

		int number_of_bits(int number){
			int count = 0;
			while(number > 0){
				count++;
				number = number>>1;
			}
			return count;
		}

		W64 extract_value(W64 value, int low, int high){
			//value = value << high;
			//return value >> (low + high);
			return (value >> low) & ~(~0 << (high-low+1));
		}




		std::deque<Request> next_level_buffer; // For write back from
		std::deque<Request> previous_level_buffer;

		std::vector<Request> *cache_miss_list;
		std::vector<Request> *cache_hit_list;
		std::vector<Request> *serviced_request_list;


		void clock_tick();
		int evict(int index, W64 current_cycle, W64 newtag, W64 address);
		int evict_new(W64 address);
		W64 get_block(W64 address);

		void reset_cache_port(W64 current_cycle, int &bank_conflict ,int &bank_conflict_hashed, int &mem_op_count);
		void update_cycle(W64 current_cycle, int &bank_conflict ,int &bank_conflict_hashed, int &mem_op_count);
		void cache_warm_reset_cache_port(W64 current_cycle, int &bank_conflict ,int &bank_conflict_hashed);
		bool cache_warm_dv_load(DVLoad *dvl, W64 current_cycle, int ins_count,std::vector<int> *smask, bool _scalarise);
		bool cache_warm_dv_store(DVStore *dvl, W64 current_cycle,  int ins_count,std::vector<int> *smask, bool _scalarise);
		bool dv_load(DVLoad *dvl, W64 current_cycle,  int ins_count,std::vector<int> *smask, bool _scalarise);
		bool dv_store(DVStore *dvl, W64 current_cycle,  int ins_count,std::vector<int> *smask, bool _scalarise);
		bool load(LONG address, int tid, W64 current_cycle, int fuid, int refcount, W64 issue_cycle, int size,bool &no_port, int &bank_conflict_num, int &bank_conflict_reduced_num, bool &is_tag_conflict, int count);
		int intbitXor(int x, int y);
		W64 bitXor(W64 x, W64 y);

		void print_histogram();
		void print_histogram_csv();
		void delayed_loads(DVLoad inst, Memoperation operation, int clusterid, W64 current_cycle, int ins_count);
		void mem_request_add_dv(DVLoad *inst, Memoperation operation, W64 current_cycle, int ins_count,std::vector<int> *smask, bool _scalarise);
		void mem_request_add_dv_st(DVStore *inst, Memoperation operation, W64 current_cycle, int ins_count,std::vector<int> *smask, bool _scalarise);
		void mem_request_add(LONG address, int tid, W64 current_cycle, int fuid, int refcount, W64 issue_cycle, int clusterid,int size, bool port_reset);
		bool isOverlappingBanks(int b1, int num_b1, int b2, int num_b2, int num_banks);
		bool bank_conflict(LONG address, int tid, W64 current_cycle, int fuid, int refcount, W64 issue_cycle, int size);
		int bank_conflict_new();
		int bank_conflict_orig_new();
		bool bank_conflict_reduced(LONG address, int tid, W64 current_cycle, int fuid, int refcount, W64 issue_cycle, int size);
		bool tag_conflict(LONG address, int tid, W64 current_cycle, int fuid, int refcount, W64 issue_cycle );
		bool access(LONG address, int tid, W64 current_cycle, Memoperation operation, int ins_count, int block_cycles);
		bool access_new(mem_req memreq);
		void memory_access(mem_req memreq);
		bool poll_cache(LONG address, int tid, W64 current_cycle, Memoperation operation, int &cache_level, LONG &avail_cycle);
		bool poll_cache_new(LONG address, int tid, W64 current_cycle, Memoperation operation, int &cache_level, LONG &avail_cycle);

		vector<W64> l1_hit_list; //Not the right place to keep it

		void add_mshr(MSHR *_mshr){
			mshr = _mshr;
		}

		void init(Config config, Statistics *_stats , int size, int associativity, int latency, int level,Cache *next){


			//int num_ports;
			conf = config;

			if(conf.base){
				num_ports = config.num_ports; //Removed *2
			}else{
				num_ports = config.num_ports;
			}
			used_port = 0;
			
			port_fuid =(int*) malloc(num_ports*sizeof(int));
			port_issue_cycle = (W64*)malloc(num_ports*sizeof(W64));
			port_operation = (Memoperation*)malloc(num_ports*sizeof(Memoperation));
			
			cache_miss_list= new std::vector<Request>[conf.num_threads];
			cache_hit_list = new std::vector<Request>[conf.num_threads];
			serviced_request_list = new std::vector<Request>[conf.num_threads];
			
			split_tlb_8 = new TLB[conf.threads_per_warp]();
			split_tlb_16 = new TLB[conf.threads_per_warp]();
			unified_tlb_8 = new TLB();
			unified_tlb_16 = new TLB();
			unified_tlb_8->init(8*conf.num_threads,4096);
			unified_tlb_16->init(16*conf.num_threads,4096);

			for(int i = 0;i< conf.threads_per_warp;i++){
				split_tlb_8[i].init(8*conf.num_warps,4096);
				split_tlb_16[i].init(16*conf.num_warps,4096);
			}

			for(int i = 0 ;i< (int)sizeof(W64)*8;i++){
				histogram[i] = 0;
			}

			bank_conflict_current_cycle = 0;
			cache_cycle = 0ULL;
			gs_count = 0;
			cache_level = level;
			l2_cache = next;
			stats = _stats;
			cache_size = size;
			cache_ways = associativity;
			cache_line_size = 64;
			cache_latency = latency;
			cache_line_numbers = (size * 1024/cache_line_size);
			//cache_set = new (LRU<W64,W64>(cache_ways)) LRU<W64,W64>[cache_line_numbers/cache_ways];
			cache_set = new LRU[cache_line_numbers/cache_ways];
			
			cache_avail_cycle = (W64*)malloc(cache_line_numbers*sizeof(W64));

			byte_offset = number_of_bits(cache_line_size - 1);
			index_offset = number_of_bits((cache_line_numbers / cache_ways) - 1) + byte_offset;
			tag_offset = 64 - (index_offset + byte_offset);

			params.index_offset = index_offset;
			params.byte_offset = byte_offset;
		
			for(int i = 0; i<(cache_line_numbers/cache_ways);i++){
				cache_set[i].set_size(cache_ways);
			}
			//for(int j=0;j<config.num_clusters; j++){
			for(int i=0 ; i < num_ports;i++ ){
				port_fuid[i] = -1;
				port_issue_cycle[i] = 0ull;
			}

			port_cycle = 0ull;
			//block_cycle = -1;
			blocked = false;
			//}
		}

		CacheParams getparams(){
			return params;
		}

		bool l1_hit(const W64 block){
			if(std::find(l1_hit_list.begin(), l1_hit_list.end(), block)!=l1_hit_list.end()){
				//cout<<"L1 hit for "<<hex16(block)<<" cycle "<<cache_cycle<<endl;
				return true;
			}
			return false;
		}

		bool ready(const ADDRINT address){
	
			//W64 tag = address;
			W64 block = extract_value(address, byte_offset,63);
			if(l1_hit(block)){
				return true;
			}
			return mshr->ready1(address);
			//assert(exist(key));

		};
		Cache(){};
		~Cache(){}
	};


	struct Opqword{
		ADDRINT op[MAX_QWORD_SIZE];
		int size;
		Opqword(){
			size = 0;
		};
		~Opqword(){
			//free(op);
		};
		void init(int size){
			//op = (ADDRINT*)malloc(sizeof(ADDRINT)*size); //TODO: Too many allocations. Avoid this
			//cout<<"SZ" << size<<endl;
		}

		void add(ADDRINT *operand, int sz){
			size = sz;
			init(size);

			for(int i = 0;i<size;i++){
				op[i] = operand[i];
			}
		}
		void reset(){
			size = 0;
			//free(op);
		}

		void print(){
			cout<<"Oplist : size: "<<size<<" : ";
			for(int i = 0; i < size;i++){
				cout<<op[i]<<" ";
			}
			cout<<endl;
		}

		bool compare(ADDRINT *operand, int s){
			if(s!= size){
				return false;
			}
			for(int i = 0 ;i < size; i++){
				if(op[i] != operand[i]){
					return false;
				}
			}
			return true;
		}
	};


	struct DVI_Load_Wait{
		//std::vector<LONG> pc;
		//std::vector<int> dvi;
		std::vector<int> count;
		std::vector<LONG> dvinstid;
		//std::vector<int> count;

	public:
		DVI_Load_Wait(){};
#ifdef SCALARISE
		void add(LONG _pc, int _dvi, LONG _dvinstid, int _inst_count){
			log(logDEBUG4)<<"Adding load wait of "<<hex16(_pc)<<" dvinstid "<<_dvinstid<<endl;
			//cout<<"Adding load wait of "<<hex16(_pc)<<" dviq "<<_dvi<<" inst count "<<_inst_count<<" _dvinstid "<<_dvinstid<<endl;
			//	pc.push_back(_pc);
			//	dvi.push_back(_dvi);
			//count.push_back(_inst_count); //There are no multiple DVloads
			count.push_back(1);
			dvinstid.push_back(_dvinstid);
		}
#else
		void add(LONG _pc, int _dvi, LONG _dvinstid){
			log(logDEBUG4)<<"Adding load wait of "<<hex16(_pc)<<" dviq "<<_dvi<<endl;
			//	pc.push_back(_pc);
			//	dvi.push_back(_dvi);
			count.push_back(1);
			dvinstid.push_back(_dvinstid);
		}
#endif

		bool load_done(LONG _dvinstid){

			std::vector<LONG>::iterator iter = std::find(dvinstid.begin(), dvinstid.end(), _dvinstid);
			size_t index = std::distance(dvinstid.begin(), iter);
			if(index == dvinstid.size()){
				assert(false);
				return false;
			}
			log(logDEBUG4)<<"Waiting for load "<<_dvinstid<<endl;
			assert(count[index] >= 0);
			if(count[index] == 0){
				return true;
			}else{
				return false;
			}
			
		}

		bool erase(LONG _dvinstid){
			std::vector<LONG>::iterator iter = std::find(dvinstid.begin(), dvinstid.end(), _dvinstid);
			size_t index = std::distance(dvinstid.begin(), iter);
			
			if(index == dvinstid.size()){
				return false;
			}
			dvinstid.erase(dvinstid.begin() + index);
			count.erase(count.begin() + index);
			return true;
		}

		
		void remove(LONG _dvinstid){
			//log(logDEBUG4)<<"Removing load wait of "<<hex16(_pc)<<" dviq "<<_dvi<<endl;
			log(logDEBUG4)<<"Removing load wait of "<<_dvinstid<<endl;
		
			std::vector<LONG>::iterator iter = std::find(dvinstid.begin(), dvinstid.end(), _dvinstid);
			size_t index = std::distance(dvinstid.begin(), iter);
			if(index == dvinstid.size()){
				return;
			}else{
#ifdef SCALARISE
				count[index]--;
				assert(count[index] >= 0);
#else
				count[index] = 0;
#endif
			}
		}
	};

	

	struct FetchTable{
	public:
		ADDRINT pc;
		ADDRINT sp;
		std::vector< int > mask;
		
	};

#endif /* GLOBALS_HPP_ */
