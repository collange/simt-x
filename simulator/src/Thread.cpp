#include "Thread.hpp"
#include <stdio.h>
#include <algorithm>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <limits.h>
#include <pthread.h>
#include <set>

Register reglist;
#ifndef SA_TRACER
MmtInstruction mmtinst;
#else
DVInstLatency mmtinst;
#endif

#ifndef SA_TRACER
static void readf(FILE* arq, void* ptr, UINT size){
	unsigned char* pos = (unsigned char*) ptr;
	for (UINT i=0; i<size; i++){
		pos[i] = fgetc(arq);
	}
}
static bool teste_eof(FILE* arq){
	bool end = feof(arq);
	return end;
}

#endif

Instruction::~Instruction()
{

}


#ifndef SA_TRACER
bool Instruction::read(FILE * file)
{
	unsigned char mqflags;
	UINT32 op_temp;

	combined = false;
	readf(file, &pc, sizeof(ADDRINT));
	readf(file, &sp, sizeof(ADDRINT));
	// read opcode added by sajith
	readf(file, &opc, sizeof(UINT32));

	readf(file, &mqflags, sizeof(char));
	readf(file, &reg_size, sizeof(char));
	readf(file, &cf_flags, sizeof(char));

#ifdef COMPILER_SUPPORT
	readf(file, &bb_priority, sizeof(UINT));
#endif

	if (teste_eof(file) ){
		//fclose(file);
		return false;
	}

	barrier = (mqflags & 0x80) != 0; //128
	basic_block = (mqflags & 0x40) != 0; //64
#ifdef COMPILER_SUPPORT
	pragma_fun_call = (mqflags & 0x20) != 0; //32
	pragma_fun_ret = (mqflags & 0x10) != 0; //16
	bool pdom_pragmas = (mqflags & 0x08) != 0; //8
	if (pdom_pragmas){
		readf(file, &pragma_begin_list, sizeof(UINT));

		readf(file, &pragma_end_counter, sizeof(char));
		for (UINT k=0; k<pragma_end_counter; k++){
			readf(file, &pragma_end_list[k], sizeof(UINT));
		}
	}
	else {
		pragma_begin_list = 0;
		pragma_end_counter = 0;
	}
#endif
	sa_uoptype uoptemp;
	readf(file, &uoptemp, sizeof(sa_uoptype));
	uop = (UINT32)uoptemp;
	uop_type = uoptemp;
	orig_uop = uoptemp;
	type = UNKNOWN;
	if(uop > SA_UOP_FREE){
		printf("%.16lx ", pc);
		cout<<"Error "<<uoptemp<<endl;
		assert(uop <= SA_UOP_FREE);
	}
	
	ustring = mmtinst.getUopString(uop);

	//scalarized_instructions_in_waiting = mmtinst.getScalarInstCount(uoptemp);
	//TODO:temporarily not scalarizing
	//cycles_remaining = mmtinst.getInstructionLatency(uoptemp)*scalarized_instructions_in_waiting;
	cycles_remaining =  mmtinst.getInstructionLatency(uoptemp);

	for(int i = 0 ;i < MAX_OPERANDS; i++){
		readf(file, &op_temp, sizeof(UINT32));
		operands[i] = op_temp;
		/*if(op_temp > 3000){
			cout << "problem"<<endl;
		}*/
		UINT32 isint;
		readf(file, &isint, sizeof(UINT32));
		if(op_temp < 32 ){

			op_iswrite[i]=false;
			if(isint == 2 ){
				op_isint[i] = false;
			}else{
				op_isint[i] = true; //x87?
			}
			if(isint == 2){
				ADDRINT val;
				UINT32 sz;
				readf(file, &sz, sizeof(UINT32));
				qwordreg_size[i] = sz;
	
				for(uint s = 0; s < sz; s++){
					readf(file, &val, sizeof(ADDRINT));
					//reg_value[i][s] = val;
				}
			}else if(isint == 1){
				ADDRINT val;
				readf(file, &val, sizeof(ADDRINT));
				//printf(" val %lx",val);
				qwordreg_size[i] = 1;
				
				reg_value_valid[i] = true;
			}else{
				qwordreg_size[i] = 0;
				//	reg_value[i] = (ADDRINT*)malloc(sizeof(ADDRINT));
				//reg_value[i][0] = ULONG_MAX;
				reg_value_valid[i] = false;
			}
		}else{
			int type = reglist.getRegType(op_temp);
			if(type == 2 ){
				//reg_value[i][0] = ULONG_MAX;
				//reg_value_valid[i] = false;
				op_isint[i] = false;
				op_iswrite[i] = false;
			}else if(type == 3){
				op_iswrite[i] = true;
				if(isint == 2){
					op_isint[i] = false;
				}else{
					op_isint[i] = true;
				}
			}else if(type == 4){
				op_iswrite[i] = true;
				if(isint == 2){
					op_isint[i] = false;
				}else{
					op_isint[i] = true;
				}
			}else{
				op_iswrite[i] = false;
				op_isint[i] = true;
			}
			//reg_value[i] = (ADDRINT*)malloc(sizeof(ADDRINT));
			//	reg_value[i][0] = ULONG_MAX;
			reg_value_valid[i] = false;
			//op_isint[i] = false;
		}
		operands_string[i] = reglist.getRegName(op_temp);
		//cout<<" "<<operands_string[i];
	}


	char isbr;
	readf(file,&isbr, sizeof(char));
	if(isbr == 'b'){
		is_branch = true;
		UINT32 b_tk;
		readf(file,&b_tk, sizeof(UINT32));
		if(b_tk == 0){
			branch_taken = false ;
		}else{
			branch_taken = true;
			ADDRINT b_tg;
			readf(file,&b_tg, sizeof(ADDRINT));
			branch_target = b_tg;
		}
	}else{
		is_branch = false;
	}

	//cout<<endl;
	char type;
	wmem_num = 0;
	rmem_num = 0;
	rmem_done = 0;
	wmem_done = 0;

	while(1){
		readf(file,&type, sizeof(char));
		//cout<<"T "<<type<<endl;
		//End flag
		if(type == 'e'){
			break;
		}else if(type == 'r'){ //Memop is read
			LONG memaddr;
			UINT32 mem_size;
			readf(file,&mem_size, sizeof(UINT32));
			readf(file,&memaddr, sizeof(LONG));

			rmem_size[rmem_num] = mem_size;
			rmem[rmem_num++] = memaddr;
		}else if(type == 'w'){ //Memop is write
			LONG memaddr;
			UINT32 mem_size;
			readf(file,&mem_size, sizeof(UINT32));
			readf(file,&memaddr, sizeof(LONG));

			wmem_size[wmem_num] = mem_size;
			wmem[wmem_num++] = memaddr;
		}else{
			assert(false); // This should not happen
		}
	}

	if(rmem_num > 0){
		load = true;
		load_start = false;
		//		load_end = false;
	}else{
		load = false;
	}
	if(wmem_num > 0){
		store = true;
		store_start = false;
		//		store_end = false;
	}else{
		store = false;
	}


	mem_quant = mqflags & 0x07;  // Lowest 3 bits
	for(UINT j=0; j<mem_quant; j++){
		readf(file, &mem_address[j], sizeof(ADDRINT));
	}

	if (reg_size > 0){
		readf(file, reg_data, reg_size);
	}
	return true;
}
#else

static bool toggle_br[10][64];
static int lcount[10][64];
static int scount[10][64];
static int bcount[10][64];

W64 extract_value(W64 value, int low, int high){
	//value = value << high;
	//return value >> (low + high);
	return (value >> low) & ~(~0 << (high-low+1));
}

bool Instruction::read(sa_tracer *tracer, int threadid, LONG seqno, int _warp, int _coreid){

	int inum=0;
	bool eof_read;

	eof_read = tracer->trace_inum.read(inum);

	if (eof_read) {
		cout << "trace_inum break " << threadid << endl;
		//SASSERT(0);
		return eof_read;
	}


	seq_no = seqno;
	sa_inst & inst = tracer->getinst(inum);
	SASSERT(inst.decoded);
	pc = inst.pc;
	this->threadid = threadid;
	this->warpid = _warp;
	//cout<<"INUM "<<inum<<" PC "<<hex16(pc)<<" tid "<<threadid<<endl;
	//nuops += inst.nuops;
	rmem_num =  inst.nloads;
	wmem_num = inst.nstores;
	uop_type = inst.idvsim.optype;


	lcount[_coreid][threadid]+=rmem_num;
	scount[_coreid][threadid]+= wmem_num;
	//is_branch = inst.is_branch_cond;

	if(rmem_num > 0){
		load = true;
	}else{
		load = false;
	}
	if(wmem_num > 0){
		store = true;
	}else{
		store = false;
	}
	cycles_remaining = mmtinst.getInstructionLatency(uop_type);

	uint flags = 0 ;
	tracer->trace_flags.read(flags);
	//barrier = (flags & 0x80) != 0; //128
	barrier = (flags & 0x100) != 0; //256
	basic_block = (flags & 0x40) != 0; //64

	std::set<sa_reg> rop;
	std::set<sa_reg> wop;

	std::set<sa_reg_type> rop_type;
	std::set<sa_reg_type> wop_type;

	fp_reg_read_count = 0;
	int_reg_read_count = 0;
	fp_reg_write_count = 0;
	int_reg_write_count = 0;

	//cout<<endl;
	int size;
	bool _fp = false;
	bool _int = false;


	string uop_str = uop_string[uop_type];
	std::size_t found = uop_str.find("AVX");
	if(found != std::string::npos){
		//is_simd = true;
		is_avx = false;
		is_sse = false;
		found = uop_str.find("FP");
		if(found != std::string::npos){
			is_fp = true;
			found = uop_str.find("SCAL");
			if(found != std::string::npos){
				is_simd = false;
			}else {
				found = uop_str.find("VEC");
				if(found != std::string::npos){
					is_simd = true;
				}else{
					assert(false);
					//is_simd = true;
				}
			}
		}else{
			is_fp = false;
			found = uop_str.find("SCAL");
			if(found != std::string::npos){
				is_simd = false;
			}else {
				found = uop_str.find("VEC");
				if(found != std::string::npos){
					is_simd = true;
				}else{
					//assert(false);
					is_simd = true;
				}
			}
		}
	}else{
		found = uop_str.find("SSE");
		if(found != std::string::npos){
			//is_simd = true;
			is_sse = false;
			is_avx = false;
			//assert(false);
			found = uop_str.find("FP");
			if(found != std::string::npos){
				is_fp = true;
				found = uop_str.find("SCAL");
				if(found != std::string::npos){
					is_simd = false;
				}else{
					found = uop_str.find("VEC");
					if(found != std::string::npos){
						is_simd = true;
					}else{
						assert(false);
					}
				}
			}else{
				is_fp = false;
				found = uop_str.find("SCAL");
				if(found != std::string::npos){
					is_simd = false;
				}else {
					found = uop_str.find("VEC");
					if(found != std::string::npos){
						is_simd = true;
					}else{
						//assert(false);
						is_simd = true;
					}
				}
			}
		}else{
			//assert(false);
			is_simd = false;
			is_avx = false;
			is_sse = false;
			//is_fp = (insts[index].fp_reg_write_count > 0 || insts[index].fp_reg_read_count > 0);
		}
	}



	if(is_simd){
		for(int uo = 0; uo < inst.idvsim.num_oregs ;uo++){
			if(inst.idvsim.rdtype[uo] != SA_REG_TMP){
				//if(inst.uop[uo].rdtype == SA_REG_FP){
				if(is_fp){
					is_float_reg = true;
					is_int_reg = false;
					_fp = true;
				}else{
					//assert(!is_float_reg);
					if(!is_float_reg){
						is_int_reg = true;
					}
					_int = true;
				}
				wop.insert(inst.idvsim.oregs[uo]);
				if(_fp){
					if(inst.idvsim.rdtype[uo] == SA_REG_XMM){
						if(!is_avx){
							is_sse = true;
						}
						//assert(false);
						fp_reg_write_count+=2;
					}else if(inst.idvsim.rdtype[uo] == SA_REG_YMM){
						is_avx = true;
						fp_reg_write_count+=4;
					}else{
						//assert(false);
					}
				}else{
					if(inst.idvsim.rdtype[uo] == SA_REG_XMM){
						if(!is_avx){
							is_sse = true;
						}
						int_reg_write_count+=2;
					}else if(inst.idvsim.rdtype[uo] == SA_REG_YMM){
						is_avx = true;
						int_reg_write_count+=4;
					}else{
						//assert(false);
					}
				}
				//}
				_int = false;
				_fp = false;
			}
		}
		for(int nuop = 0; nuop < inst.idvsim.num_iregs;nuop ++){
			if(inst.idvsim.rstype[nuop] != SA_REG_TMP){
				if(is_fp){
					//assert(!is_int_reg);
					is_float_reg = true;
					is_int_reg = false;
					_fp = true;
				}else{
					if(!is_float_reg){
						is_int_reg = true;
					}
					_int = true;
				}
				//size = rop.size();
				rop.insert(inst.idvsim.iregs[nuop]);
				//if(rop.size()!=size){
				if(_fp){
					if(inst.idvsim.rstype[nuop] == SA_REG_XMM){
						if(!is_avx){
							is_sse = true;
						}
						fp_reg_read_count+=2;
					}else if(inst.idvsim.rstype[nuop] == SA_REG_YMM){
						is_avx = true;
						fp_reg_read_count+=4;
					}else{
						//assert(false);
					}
				}else{
					if(inst.idvsim.rstype[nuop] == SA_REG_XMM){
						if(!is_avx){
							is_sse = true;
						}
						int_reg_read_count+=2;
					}else if(inst.idvsim.rstype[nuop] == SA_REG_YMM){
						is_avx = true;
						int_reg_read_count+=4;
					}else{
						//assert(false);
					}
				}
				//}
				_int = false;
				_fp = false;
			}
			//cout<<"rs "<<inst.uop[uo].rs[nuop]<<" t "<<inst.uop[uo].rstype[nuop]<<endl;
		}

		if(!is_avx && !is_sse){
			is_simd = false; //FIXME: I will just consider them as non simd instructions
			//assert(inst.idvsim.num_iregs == 0 && inst.idvsim.num_oregs == 0); //insts like vzeroupper. vcvttss2si does have general purpose dest operand
			assert(uop_type == SA_UOP_AVX || uop_type==SA_UOP_SSE);
			std::size_t found = uop_str.find("AVX");
			if(found != std::string::npos){
				is_avx = false;
				//is_avx = false;
				//assert(false);

			}else{
				//is_sse = false;
				is_sse = false;
				//assert(false);
			}
		}

		//cout<<" rd "<<inst.uop[uo].rd<<" t "<<inst.uop[uo].rdtype<<endl;

	}else{
		for(int uo = 0; uo < inst.idvsim.num_oregs ;uo++){
			if(inst.idvsim.rdtype[uo] != SA_REG_TMP){
				if(inst.idvsim.rdtype[uo] == SA_REG_FP || is_fp){
					//assert(!is_int_reg);
					is_float_reg = true;
					is_int_reg = false;
					_fp = true;
				}else{
					//assert(!is_float_reg);
					if(!is_float_reg){
						is_int_reg = true;
					}
					_int = true;
				}
				size = wop.size();
				wop.insert(inst.idvsim.oregs[uo]);
				if(wop.size()!=size){
					if(_fp){
						//						assert(false);
						fp_reg_write_count++;
					}else{
						int_reg_write_count++;
					}
				}
				_int = false;
				_fp = false;
			}
		}
		for(int nuop = 0; nuop < inst.idvsim.num_iregs; nuop ++){
			if(inst.idvsim.rstype[nuop] != SA_REG_TMP){
				if(inst.idvsim.rstype[nuop] == SA_REG_FP || is_fp){
					//assert(!is_int_reg);
					is_float_reg = true;
					is_int_reg = false;
					_fp = true;
				}else{
					if(!is_float_reg){
						is_int_reg = true;
					}
					_int = true;
				}
				size = rop.size();
				rop.insert(inst.idvsim.iregs[nuop]);
				if(rop.size()!=size){
					if(_fp){
						//					assert(false);
						fp_reg_read_count++;
					}else{
						int_reg_read_count++;
					}
				}
				_int = false;
				_fp = false;
			}
			//cout<<"rs "<<inst.uop[uo].rs[nuop]<<" t "<<inst.uop[uo].rstype[nuop]<<endl;
		}
		is_fp = (fp_reg_write_count > 0 || fp_reg_read_count > 0);
		//cout<<" rd "<<inst.uop[uo].rd<<" t "<<inst.uop[uo].rdtype<<endl;
	}

	int index = 0;

	for (std::set<sa_reg>::iterator it = rop.begin(); it != rop.end(); ++it){
		roperands[index] = *it;
		//cout<<" R "<< rroperands[index];
		index++;
	}

	num_roperands = index;
	index = 0;
	for (std::set<sa_reg>::iterator it = wop.begin(); it != wop.end(); ++it){
		woperands[index] = *it;
		//cout<<" W "<< woperands[index];

		index++;
	}
	num_woperands = index;
	//cout << endl;
	
	assert((num_roperands + num_woperands) <= MAX_OPERANDS);
	branch_taken = false;
	char addr_status;
	
#ifdef SCALARISE
	uint addr_status_temp;
	uint addr_op_size = (uint) 0;
#else
	char addr_status_temp;
	char addr_op_size = (char) 0;
	//uint addr_status_temp;
	//uint addr_op_size = (uint) 0;
#endif
	uint max_addr_op_size = (uint)0;
	int addr_nr = 0;
	int addr_nw = 0;
	sa_addr addr_val = 0Ull;
	sa_addr sp_temp = 0Ull;

	{
		bool eof;
		eof = tracer->trace_sp.read(sp_temp);
		sp = sp_temp;
		if (eof) SASSERT(0);
		//cout << "trace_sp.read sp_addr " << hex << sp << dec << endl;
	}

	is_branch = inst.idvsim.is_branch;
	if(is_branch){
		bcount[_coreid][threadid]++;
		bool eof;
		bool toggle = false;
		eof = tracer->trace_br_taken.read(branch_taken);
		if (eof) SASSERT(0);
		tracer->trace_br_taken.read(toggle);
		//cout<<"toggle "<<toggle<<endl;
		SASSERT(toggle_br[_coreid][threadid] == toggle);
		toggle_br[_coreid][threadid] = !toggle_br[_coreid][threadid];
	}

#if 1
	// read addr_status
	if (inst.nloads || inst.nstores) {
		while (1) {
			bool eof;

			eof =  tracer->trace_addr_status.read(addr_status_temp);
			addr_status = addr_status_temp;
			if(eof){
				cout<<"ASSERT FOR "<<threadid<<" inum "<<inum<<endl;
			}
			if (eof) SASSERT(0);
			//cout << "trace_addr_status.read addr_status ch " << addr_status << " PC mem " <<hex16(pc)<< " tid "<<threadid << endl;

			if ( !((addr_status == 'r') || (addr_status == 'w') || (addr_status == 'e')) ) {
				//cout << "trace_addr_status.read *invalid* addr_status ch " << addr_status << " PC mem " <<hex16(pc) <<endl;
				SASSERT(0);
			}

			if (addr_status == 'e') {
				break;
			}
			if (addr_status == 'r') {
				//cout << "trace_addr_status.read *invalid* addr_status ch " << addr_status << " reading addr_op_size " << " addr_nr " << addr_nr << endl;
				eof =  tracer->trace_addr_status.read(addr_op_size);
				if (eof) SASSERT(0);

				eof =  tracer->trace_addr.read(addr_val);
				if (eof) SASSERT(0);
				//cout << "trace_addr_status.read *invalid* addr_status ch " << addr_status << " reading addr_op_size " << " addr_nr " << addr_nr << " addr_val " << hex << addr_val << dec << endl;
#ifdef SCALARISE
			
				if(inst.mreadsize > MAXMEMACCWIDTH){
					rmem_size[addr_nr] = 8;
				}else{
					rmem_size[addr_nr] = inst.mreadsize * 8;//mreadsize is in bytes
				}
				if(max_addr_op_size < addr_op_size){
					max_addr_op_size = addr_op_size;
				}
#else
				if(inst.mreadsize > MAXMEMACCWIDTH){
					rmem_size[addr_nr] = 8;
				}else{
					rmem_size[addr_nr] = inst.mreadsize * 8;//mreadsize is in bytes
				}
				//rmem_size[addr_nr] = 32;
				rmem_size[addr_nr] = inst.mreadsize;
#endif
				assert(rmem_size[addr_nr] >= 0);
				rmem[addr_nr] = addr_val;
				
				addr_nr++;
			}
			if (addr_status == 'w') {
				//cout << "trace_addr_status.read *invalid* addr_status ch " << addr_status << " reading addr_op_size " << " addr_nr " << addr_nr << endl;
				eof =  tracer->trace_addr_status.read(addr_op_size);
				if (eof) SASSERT(0);

				eof =  tracer->trace_addr.read(addr_val);
				if (eof) SASSERT(0);
				//cout << "trace_addr_status.read *invalid* addr_status ch " << addr_status << " reading addr_op_size " << " addr_nr " << addr_nr << " addr_val " << hex << addr_val << dec << endl;
#ifdef SCALARISE
		
				if(inst.mwritesize > MAXMEMACCWIDTH){
					wmem_size[addr_nw] = 8;
				}else{
					wmem_size[addr_nw]= inst.mwritesize*8; //mwritesize is in bytes
				}
				if(max_addr_op_size < addr_op_size){
					max_addr_op_size = addr_op_size;
				}
				wmem[addr_nw] = addr_val;
#else
				//wmem_size[addr_nw] = 32;
				if(inst.mwritesize > MAXMEMACCWIDTH){
					wmem_size[addr_nw] = 8;
				}else{
					wmem_size[addr_nw]= inst.mwritesize*8; //mwritesize is in bytes
				}
				wmem_size[addr_nw]= inst.mwritesize;
				wmem[addr_nw] = addr_val;
#endif
				//wmem_size[addr_nw] = 32; //TODO: We are only considering 32 but data. THIS IS JUST A TEMPORARY FIX
				assert(wmem_size[addr_nw] > 0);
				addr_nw++;
			}
		}

		sa_addr pc_adr = 0Ull;
		//TODO:Remove
		tracer->trace_addr.read(pc_adr);
		if(inst.nloads != addr_nr){
			cout<<"Read was for pc "<<hex16(pc_adr)<<" ORIG "<<hex16(pc)<<endl;
			cout<<" inum "<<inum<<endl;
			cout<<"NUM_LOADS "<<inst.nloads<<" NUM_ST "<<inst.nstores<<" ADDR_NR "<<addr_nr<<" ADD_NW "<<addr_nw<<endl;
		}
		if(pc_adr != pc){
			cout<<"Thread id "<<threadid<<" pc_adr "<<hex16(pc_adr)<<" inum "<<inum <<" pc "<< hex16(pc) <<endl;
		}
		//SASSERT(inst.nloads < 2);
		SASSERT (pc_adr == pc);
		SASSERT (inst.nloads == addr_nr);
		SASSERT (inst.nstores == addr_nw);
	}

#endif
	//cout<<"l "<<lcount[threadid]<<" s "<<scount[threadid]<< " b "<<bcount[threadid]<<endl;
	return false;
}
#endif

#ifndef TAGE_PREDICTOR
void Thread::load(const char* file, int id){
#else
	void Thread::load(const char* file, int id, int _warpid, int _threadid, int _coreid){
#endif
		char temp_string[128];

		//open the thread's instruction file
		sprintf(temp_string,"%s",file);
#ifndef SA_TRACER
		thread_arq = fopen(temp_string,"r");

		if (thread_arq == NULL){ //check if file valid, else throw exception
			char* msg = new char[200];
			sprintf(msg, "Invalid input file (with the thread): %s.", file);
			throw Exception(msg);
		}
		
#else
		thread_arq.open(temp_string); ///////////////////////
		cout << "Opening file " << temp_string << endl;
#endif
		//reset/set statistics for the core for this thread
		coreid = _coreid;
		//REMOVE
		toggle_br[_coreid][id] = false;
		lcount[_coreid][id] = 0;
		scount[_coreid][id] = 0;
		bcount[_coreid][id] = 0;

		char outtemp[128];
		sprintf(outtemp,"/local/atino/threadout-%d", id);
		outfile =fopen(outtemp,"w");

		tracelength = 0Ull;
		is_active = true;
		finished = false;
		q_empty = false;
		set_finish_cycle = false;
		finish_cycle = 0Ull;

		ignore = false;

		fetch_isactive = false;
		fetch_qempty = false;

		basic_block = false;
		cf_flags = 0;
#ifdef REAL_MULTI_CORE
		this->threadid = _threadid;
		//assert(threadid < conf.)
#else
		this->threadid = _threadid;
#endif
		warpid = _warpid;
		cout<<"Loading thread "<<threadid<<" warp "<<warpid<< " file "<< temp_string<<endl;
#ifdef COMPILER_SUPPORT
		pragma_fun_call = false;
		pragma_fun_ret = false;
		pragma_begin_list = 0;
		pragma_end_counter = 0;
#endif

	}
#ifndef SA_TRACER
	void Thread::fetch(Instruction const & insn)
	{
		pc = insn.pc;
		sp = insn.sp;
		opcode = insn.opc;
		uop = insn.uop;
		//ustring = insn.ustring;
		orig_uop = insn.orig_uop;
		t_rmem_num = insn.rmem_num;
		t_wmem_num = insn.wmem_num;
		//cache_missed = insn.cache_missed;
		for(int i = 0;i < MAX_OPERANDS ; i++){

			operands[i] = insn.operands[i];


			//operands_string[i] = insn.operands_string[i];
			t_rmem[i] = insn.rmem[i];
			t_wmem[i] = insn.wmem[i];
		}
		//reg_size = insn.reg_size;
		barrier = insn.barrier;
		//barrier_orig = barrier;
		basic_block = insn.basic_block;
		//cf_flags = insn.cf_flags;
	}
#endif

	void Thread::unset_barrier(){
		prefetch_instruction_queue_buffer.front().barrier = false;
	}


	Thread::Thread() : eof(true)
	{
		//fetch_almost_full = false;
		iq_full = false;
		fetch_seq_no = 0;

#ifdef SAMPLING
		sampling_seq_no = 0;
#endif
		issue_seq_no = 0;
		previous_instruction_queue_size = -1;
		fetch_stall = false; branch_stall = false;
		ins_count = 0;
		reader_finished = false;
		issue_later = false;
		last_fetch_cycle = 0;
		skip_issue = false;
		end_of_simulation = false;
		prefetchq_notfull = PTHREAD_COND_INITIALIZER;
		mutex_Var=(pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
		iq_mutex = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
		prefetchq_full = PTHREAD_COND_INITIALIZER;
		mutex_Var_full=(pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
		active_last_fetch = false;
		forward_jump = false;
		fetch_divergent = false;
		primary_heur_fetch = false;
		exception = false;
#ifdef SAMPLING
		wait = false;
#endif

	}

