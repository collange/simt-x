#ifndef SASIM_MEMORY_HPP
#define SASIM_MEMORY_HPP

#include "sasim_cache.hpp"

class sasim_memory {
 public:
  saqueue<sa_addr> bus;
  sasim_cache * prev;
  int latency;
  int64_t nread;
  int64_t nwrite;

  void init(sasim_cache * p, int mem_lat, int bandwidth);
  void onecycle_write(int64_t cycle);
  void onecycle_read(int64_t cycle);
  void onecycle_prefetch(int64_t cycle);
  void onecycle(int64_t cycle);
  void debugprint_queues();
  void printstats(FILE *stream);
};

#endif // End ifndef SASIM_MEMORY_HPP