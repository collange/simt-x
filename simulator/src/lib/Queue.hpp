/*!
 * @file: sasim_saqueue_template.hpp
 * @author: Pierre Michaud
 * 
 * @section DESCRIPTION
 * 
 * The class saqueue represents a flexible queue of any type of data as required.
 */ 

#ifndef SASIM_SAQUEUE_TEMPLATE_H
#define SASIM_SAQUEUE_TEMPLATE_H

#include <iostream>

#include "sasim_defs.hpp"
#include "sasim_uarch_defs.hpp"

using namespace std;

/*!
 * Class: saqueue
 * 
 * The queue is q of type QTYPE. 
 * Array t is used to store the value cycle + latency.
 * Other variables used for the implementation are: size, latency, occupancy, head, tail.
 */
template <class QTYPE> class saqueue {
public:
	QTYPE * q;
	int64_t * t;
	int size;
	int latency;
	int occupancy;
	int head;
	int tail;
	int64_t totalcount;
	bool debug;

	int readptr;
	int unread;
	int scandir;

	int readptr2;
	int unread2;
	int scandir2;

	/*!
	 * Data read by the function read() is taken from the head of the queue
	 *
	 * @return void. The function does not return any value
	 */
	void scan_forward()
	{
		scandir = 1;
		readptr = head;
		unread = occupancy;
	}

	/*!
	 * Data read by the function read() is taken from the tail of the queue
	 *
	 * @return void. The function does not return any value
	 */
	void scan_backward()
	{
		scandir = -1;
		readptr = tail;
		DECPTR(readptr,size);
		unread = occupancy;
	}

	/*!
	 * Stores a copy of the current read pointer
	 *
	 * @return void. The function does not return any value
	 */
	void save_readptr()
	{
		readptr2 = readptr;
		unread2 = unread;
		scandir2 = scandir;
	}

	/*!
	 * Restores the value of read pointer previously stored
	 *
	 * @return void. The function does not return any value
	 */
	void restore_readptr()
	{
		readptr = readptr2;
		unread = unread2;
		scandir = scandir2;
	}

	/*!
	 * Initializes the queue q with size qsize. The latency is also initialized.
	 *
	 * @param qsize Specifies the size of the queue
	 * @param qlatency Specifies the latency for write operation
	 * @return void. The function does not return any value
	 */
	void init(int qsize, int qlatency = 0)
	{
		SASSERT(qsize>0);
		q = new QTYPE [qsize];
		t = new int64_t [qsize];
		size = qsize;
		latency = qlatency;
		occupancy = 0;
		head = 0;
		tail = 0;
		scan_forward();
		totalcount = 0;
		debug = false;
	}

	void debugprint()
	{
		debug = true;
	}

	/*!
	 * Flushes the content of the queue q and resets the rest of variables
	 *
	 * @return void. The function does not return any value
	 */
	void flush()
	{
		occupancy = 0;
		head = 0;
		tail = 0;
		scan_forward();
	}

	~saqueue()
	{
		delete [] q;
		delete [] t;
	}

	/*!
	 * Returns the free entries of queue q
	 *
	 * @return integer. Number of free entries.
	 */
	int free_entries()
	{
		return size - occupancy;
	}

	/*!
	 * Returns true of the queue is full
	 *
	 * @return Boolean.
	 */
	bool full()
	{
		return (occupancy == size);
	}

	/*!
	 * Returns true if the queue is currently empty
	 *
	 * @return Boolean.
	 */
	bool empty()
	{
		return (occupancy == 0);
	}

	/*!
	 * Data is added to the tail of the queue. This function increases the tail pointer of queue q.
	 *
	 * @return void. The function does not return any value
	 */
	void advance_tail()
	{
		SASSERT(occupancy < size);
		INCPTR(tail,size);
		occupancy++;
		unread++;
	}

	/*!
	 * Inserts a new element into the queue and increases tail pointer
	 *
	 * @param val Value to be inserted
	 * @param cycle Current cycle of execution
	 * @return Reference to the entry inserted.
	 */
	QTYPE & enqueue(QTYPE val, int64_t cycle = 0)
	{
		SASSERT((occupancy==0) || (tail!=head));
		QTYPE & e = q[tail];
		e = val;
		t[tail] = cycle + latency;
		advance_tail();
		if (debug) {
			cout << val << " " << cycle << " " << cycle+latency << " " << occupancy << endl;
		}
		return e;
	}

	/*!
	 * Inserts a new element into the queue overwritten the oldest entry in case the queue is full.
	 *
	 * @param val Value to be inserted
	 * @param cycle Current cycle of execution
	 * @return void. The function does not return any value
	 */
	void push(QTYPE val, int64_t cycle = 0)
	{
		QTYPE & e = q[tail];
		e = val;
		t[tail] = cycle + latency;
		INCPTR(tail,size);
		if (occupancy < size) {
			occupancy++;
		} else {
			// the oldest entry is overwritten
			INCPTR(head,size);
		}
		if (debug) {
			//cout << val << " " << cycle << " " << cycle+latency << " " << occupancy << endl;
			cout << val << endl;
		}
	}

	/*!
	 * Returns true if the data on top of the queue has already completed the number of cycles needed
	 * to be written into the queue.
	 *
	 * @param cycle Current cycle of execution
	 * @return Boolean
	 */
	bool ready(int64_t cycle = -1)
	{
		if (occupancy==0) {
			return false;
		}
		if (cycle < 0) {
			return true;
		}
		return (cycle >= t[head]);
	}

	/*!
	 * Reads one entry previously stored.
	 * This function returns the reference to the entry in the queue and not its value.
	 *
	 * @param k Entry to be read
	 * @return Reference to the entry accessed
	 */
	QTYPE & access(int k)
	{
		SASSERT(k>=0);
		SASSERT(k<occupancy);
		int j = head + k;
		if (j >= size) {
			j -= size;
		}
		SASSERT(j>=0);
		SASSERT(j<size);
		return q[j];
	}

	/*!
	 * Returns a pointer to the entry in the queue currently pointed by read pointer (readptr).
	 *
	 * @return Reference to the entry accessed
	 */
	QTYPE * peek()
	{
		if (unread==0) {
			return NULL;
		}
		return &q[readptr];
	}

	/*!
	 * Returns the value on top or at the bottom of the queue q.
	 * If the function scan_forward() was previously called, the data is taken from the top.
	 * If the function scan_backward() was previously called, the data is taken from the bottom
	 * Data is read from the queue as long as the number of cycles required for writing that particular entry has been completed.
	 *
	 * @param cycle Current cycle of execution
	 * @return Reference to the entry read
	 */
	QTYPE * read(int64_t cycle = -1)
	{
		SASSERT(unread>=0);
		if (unread==0) {
			return NULL;
		}
		if ((cycle >= 0) && (cycle < t[readptr])) {
			return NULL;
		}
		QTYPE * p = &q[readptr];
		if (scandir >= 0) {
			INCPTR(readptr,size);
		} else {
			DECPTR(readptr,size);
		}
		unread--;
		return p;
	}

	/*!
	 * Returns the oldest entry of the queue and increases the value of head by one to point to the next element in the queue
	 *
	 * @return Reference to the entry dequeued
	 */
	QTYPE dequeue()
	{
		SASSERT(!empty());
		QTYPE val = q[head];
		INCPTR(head,size);
		occupancy--;
		totalcount++;
		return val;
	}

	/*!
	 * Returns the reference of the next available entry in the queue
	 *
	 * @return Reference to the entry accessed
	 */
	QTYPE & next()
	{
		return q[tail];
	}

	/*!
	 * Returns the reference of the oldest value stored in the queue
	 *
	 * @return Reference to the entry accessed
	 */
	QTYPE & oldest()
	{
		SASSERT(occupancy > 0);
		return q[head];
	}

	/*!
	 * Returns the reference of the second oldest value stored in the queue
	 *
	 * @return Reference to the entry accessed
	 */
	QTYPE & second_oldest()
	{
		SASSERT(occupancy > 1);
		int j = head;
		INCPTR(j,size);
		return q[j];
	}

	/*!
	 * Returns the reference of the last value stored in the queue
	 *
	 * @return Reference to the entry accessed
	 */
	QTYPE & last()
	{
		SASSERT(occupancy > 0);
		int j = tail;
		DECPTR(j,size);
		return q[j];
	}

	/*!
	 * Finds a particular value inside the queue
	 *
	 * @param val Value to find
	 * @param pos Pointer to the variable where the index of the vector will be written in case the value is found
	 * @return Boolean value
	 */
	bool search(QTYPE val, int * pos = NULL)
	{
		int j = head;
		for (int i=0; i<occupancy; i++) {
			if (q[j]==val) {
				if (pos) {
					*pos = i;
				}
				return true;
			}
			INCPTR(j,size);
		}
		return false;
	}

	/*!
	 * Determines if a particular value inside the queue has a very long period of execution
	 *
	 * @param val Value to find
	 * @return Boolean value
	 */
	bool search_suspended(QTYPE val)
	{
		int j = tail;
		DECPTR(j,size);
		for (int i=0; i<occupancy; i++) {
			if (q[j]==val)  {
				return (t[j]==INFINITE_INTVAL);
			}
			DECPTR(j,size);
		}
		return false;
	}

	/*!
	 * Determines if a particular value inside the queue has already been written into the queue
	 * and returns a reference to it.
	 *
	 * @param val Value to find
	 * @param cycle Number of cycles
	 * @return Reference to the entry accessed
	 */
	QTYPE * search_ready(QTYPE val, int64_t cycle = INFINITE_INTVAL)
	{
		int j = head;
		for (int i=0; i<occupancy; i++) {
			if (q[j]==val) {
				if (cycle >= t[j]) {
					return & q[j];
				} else {
					return NULL;
				}
			}
			INCPTR(j,size);
		}
		return NULL;
	}

	/*!
	 * Sets a particular cycle value to the entry that contains the value specified
	 *
	 * @param val Entry to change the cycle value
	 * @param cycle New cycle value for the entry
	 *
	 * @return Boolean value.
	 */
	bool search_settime(QTYPE val, int64_t cycle)
	{
		bool hit = false;
		int j = head;
		for (int i=0; i<occupancy; i++) {
			if (q[j]==val) {
				hit = true;
				t[j] = cycle;
			}
			INCPTR(j,size);
		}
		return hit;
	}

	/*!
	 * Deletes the value specified from the queue and returns TRUE if it was deleted
	 *
	 * @param val Value to be eliminated
	 * @return Boolean value.
	 */
	bool search_delete(QTYPE val)
	{
		bool hit = false;
		int j = head;
		for (int i=0; i<occupancy; i++) {
			//cout<<"q "<<q[j]<<" ";
			if (q[j]==val) {
				hit = true;
				break;
			}
			INCPTR(j,size);
		}
		if (!hit) {
			return hit; // miss
		}
		// hit: delete the entry
		if (j==head) {
			INCPTR(head,size);
		} else {
			int k = j;
			INCPTR(k,size);
			while (k != tail) {
				q[j] = q[k];
				t[j] = t[k];
				j = k;
				INCPTR(k,size);
			}
			tail = j;
			//Added bu sajith
			DECPTR(readptr,size);
			//end
		}
		//Added bu sajith
		//DECPTR(readptr,size);
		//end
		SASSERT(occupancy>0);
		occupancy--;
		return hit;
	}

};

#endif // End ifndef SASIM_SAQUEUE_TEMPLATE_H
