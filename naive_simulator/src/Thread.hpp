#ifndef __HEADER_THREAD__
#define __HEADER_THREAD__

#include "Types.hpp"
#include <stdio.h>
#include <deque>
#include "lib/globals.hpp"
#include <string>
#include <pthread.h>


#ifdef SA_TRACER
#include "../Sasim/src/sasim_tracer.hpp"
#endif

using std::vector;


class Thread{
public:

	//CacheParams l1_params;
#ifdef SAMPLING
	LONG sampling_seq_no;
#endif
	LONG fetch_seq_no;
	LONG issue_seq_no;
	LONG ins_count;
#ifndef TAGE_PREDICTOR
	BranchPredictor branchpredictor;
#else
	//PREDICTOR *branchpredictor;
#endif
#ifndef SA_TRACER
	FILE* thread_arq;
#else
	sa_tracer thread_arq;
#endif

	int warpid;
	int threadid;
	int previous_instruction_queue_size;

	std::vector<LONG> ready_to_commit_instructions;
	std::deque<DecodedInstruction> decoded_instructions;
	std::deque<Instruction> prefetch_instruction_queue;
	std::deque<Instruction> prefetch_instruction_queue_buffer;
	std::deque<Instruction> instructions_in_flight;
	//std::deque<Instruction> scalarized_instruction_start;
	std::deque<Instruction> cache_miss_instructions;

	FILE *outfile;

	pthread_cond_t prefetchq_notfull;	
	pthread_mutex_t mutex_Var;
	pthread_cond_t prefetchq_full;
	pthread_mutex_t mutex_Var_full;

	bool cache_missed;
	bool reissued;

	bool exception;

	bool eof;
	bool barrier;
	bool finished;
	LONG finish_cycle;
	bool set_finish_cycle;
#ifdef SAMPLING
	bool wait;
#endif
	//bool ignore;
	bool q_empty;
	bool reader_finished;

	bool end_of_simulation;

	bool fetch_qempty; //finished executing traces, eof = no more
	//bool fetch_almost_full;

	bool skip_issue;
	bool issue_later;

	bool dep;
	bool iw;
	bool fu;

	//Interface for heuristics
	bool is_active; //if threads is active in a warp
	bool is_finished; //if thread has finished executing its traces
	bool ignore; //thread in a barrier
	bool active_last_fetch;
	bool forward_jump;
	bool fetch_divergent;
	bool primary_heur_fetch;
	bool iq_full;

	bool fetch_isactive;

	bool fetch_stall, branch_stall; //for exceptions
	LONG tracelength;
	int coreid;


	ADDRINT pc;
	ADDRINT sp;

	ADDRINT fetch_pc; //Next pc
	ADDRINT fetch_sp;

	int tag;

	UINT32 last_fetch_cycle;

	UINT32 uop;
	string ustring;
	UINT32 opcode;
	UINT32 operands[MAX_OPERANDS];
	string operands_string[MAX_OPERANDS];

	int t_wmem_num;
	int t_rmem_num;
	LONG t_wmem[MAX_OPERANDS];
	LONG t_rmem[MAX_OPERANDS];

	int clusterid;

	sa_uoptype orig_uop;
	pthread_mutex_t iq_mutex ;//= PTHREAD_MUTEX_INITIALIZER;

	bool basic_block; //Hardware basic block
	bool fetch_basic_block;

	unsigned char cf_flags;

	unsigned char mem_quant;
	unsigned char reg_size;

	ADDRINT mem_address[4];
	unsigned char reg_data[256];

#ifdef COMPILER_SUPPORT
	UINT bb_priority;
	UINT fetch_bb_priority;
	bool pragma_fun_call;
	bool pragma_fun_ret;
	UINT pragma_begin_list;
	unsigned char pragma_end_counter;
	UINT pragma_end_list[256];
#endif


public:
#ifndef TAGE_PREDICTOR
	void load(const char* file, int id);
#else
	//void load(const char* file, int id, PREDICTOR *predictor, int _warpid);
	void load(const char* file, int id, int _warpid, int _threadid, int _coreid);
#endif

	void fetch(Instruction const & insn);
	void refetch(Instruction const & insn);

	void unset_barrier();

	Thread();
};


#endif
