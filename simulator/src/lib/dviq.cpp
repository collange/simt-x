#include "dviq.hpp"
#include <math.h>

#define REV_BIMODAL 1 //revised bimodal divergence predictor

using namespace std;

bool DVIQ::issame(int *lhs , int *rhs, int length){
	for(int i = 0 ;i < length ;i++){
		if(lhs[i] != rhs[i]){
			return false;
		}
	}
	return true;
}

bool DVIQ::issubset(int *qmask , int *smask, int length){
	bool zero = true;
	for(int i = 0 ;i < length ;i++){
		if(qmask[i]){
			zero = false;
			break;
		}
	}
	if(zero){ //if mask is null
		return false;
	}
	//Else continue to check if it is a subset
	for(int i = 0 ;i < length ;i++){
		if(!smask[i] && qmask[i]){ //i.e. 00 10 11
			return false;
		}
	}
	return true; //i.e. smask = 0 and qmask = 1
}

void DVIQ::clear(){
	for(int dv =0 ;dv < num_threads; dv++){
		blocked[dv] = false;
	}
}

void DVIQ::empty_queue(){
	clear();
	for(int dv =0 ;dv < num_threads; dv++){
		int current_sz = dviq[dv].size();
		DVInst smask;
		/*if(!dviq[dv].empty()){
			smask = dviq[dv].front();
		}*/
		dviq[dv].clear();
		log(logDEBUG4)<<"DV "<<dv<<" cleared"<<" size "<<current_sz<<endl;
	}
}

int get_max_mask(int threads_per_warp){
	int max = 0;
	for(int i = 0; i < threads_per_warp; i++)
		max += pow(2, i);

	return max;	
}

int get_curr_mask(int smask[], int threads_per_warp){
	int maskval = 0;
	for(int i = 0; i < threads_per_warp; i++){
		if(smask[i])
			maskval += pow(2, i);
	}
	return maskval;
}

void DVIQ::get_new_mask(DVInst &dv_temp, int active_mask[]){
	dv_temp.actual_mask = get_curr_mask(active_mask, threads_per_warp);
}

/***************************************************************
	Biomodal divergence predictor
***************************************************************/
bool DVIQ::divergence_predictor(){
	//00 = strong not divergent, 01 = weak not divergent, 10 = weak divergent, 11 = strong divergent
	//true = divergent, false = not divergent

	bool pred = false;
	int active_threads = 0;
	int t = 0, nt = 0;
	int active_mask[threads_per_warp];

	if(div_pred[0] == 0 && div_pred[1] == 0){
		pred = false;
	}else if(div_pred[0] == 1 && div_pred[1] == 0){
#ifdef REV_BIMODAL
		pred = true; // better to assume D than ND (reduce full misprediction cost)
#else
		pred = false;
#endif
	}else if(div_pred[0] == 0 && div_pred[1] == 1){
		pred =  true;
	}else if(div_pred[0] == 1 && div_pred[1] == 1){
		pred = true;
	}

	return pred;
}

void DVIQ::divergence_predictor_udpate(bool correct, bool updown){
	//if correct = false AND updown = true increment counter, else decrement
	//if correct = true, exit
	if(!correct){ //made a wrong prediction
		if(updown){ //increment towards divergent
			if(div_pred[0] == 0){
				div_pred[0] = 1;
			}else if(div_pred[1] == 0){
				div_pred[1] = 1;
			}
		}else{ //decrement towards non-divergent
			if(div_pred[0] == 1){
				div_pred[0] = 0;
			}else if(div_pred[1] == 1){
				if(div_pred[0] == 0){
					div_pred[0] = 1;
					div_pred[1] = 0;
				}else{
					div_pred[0] = 0;
				}				
			}
		}
	}
}

void DVIQ::compute_branch_mask(int x_smask[], int inv_smask[], DVInst &dv_temp){
	int active_mask[threads_per_warp];
#ifdef DEBUG
	cout << "Actual NT Branch outcome: ";
#endif
	for(UINT k=0 ; k < threads_per_warp; k++){
		if(x_smask[k]){
			if(dv_temp.insts[k].branch_taken) {	
#ifdef DEBUG
				cout << "T ";
#endif
				active_mask[k] = 0;
				inv_smask[k] = 1;
			}else{    
#ifdef DEBUG
				cout << "NT ";
#endif
				active_mask[k] = 1;
				inv_smask[k] = 0;
			}
		}else{
			active_mask[k] = 0;	
			inv_smask[k] = 0;
#ifdef DEBUG	
			cout << "X ";
#endif
		}
	}
#ifdef DEBUG
	cout << endl;
#endif

	for(int i = 0; i < threads_per_warp; i++)
		x_smask[i] = active_mask[i];
}


/***************************************************************
	Implements Divergence Predictor for 
	OoO DITVA prototype (FOR perfect prediction)
***************************************************************/
bool DVIQ::branch_div_predictor(int x_smask[], DVInst &dv_temp, int offset, bool speculating){
	int div_counter = 0, active_threads = 0;
	int t = 0, nt = 0;
	int new_mask[threads_per_warp], active_mask[threads_per_warp];

	#ifdef DEBUGBR
		cout << "Actual Branch outcome: ";
	#endif
		for(UINT k=0 ; k < threads_per_warp; k++){
			if(x_smask[k]){
				active_threads++;	
				if(dv_temp.insts[k].branch_taken) {
					t++;	
	#ifdef DEBUGBR
					cout << "T ";
	#endif
					active_mask[k] = 0;
				}else{    
					nt++;	
	#ifdef DEBUGBR
					cout << "NT ";
	#endif
					active_mask[k] = 1;
				}
			}else{
				active_mask[k] = 0;	
	#ifdef DEBUGBR	
				cout << "X ";
	#endif
			}
		}
	#ifdef DEBUGBR
		cout << endl;
	#endif
	if(!speculating){
		//"perfect" prediction to be returned
		if((!(t == active_threads ^ nt == active_threads)) && active_threads > 1){ //"predict" divergent
			dv_temp.new_path = true; //prediction
	#ifdef DEBUGBR
			cout << "ACTUALLY <DIVERGENT>: act = "<< active_threads << endl; 
	#endif
			//get_new_mask(dv_temp, active_mask); //sets actual mask to dvInst
			for(int k=0; k < threads_per_warp; k++){
				x_smask[k] = active_mask[k];
			}
			return true; 

		}else{ //it remains the same or is convergent (i.e. "predict" not divergent)		
			dv_temp.new_path = false;
	#ifdef DEBUGBR
			cout << "<ACTUALLY> Non-Divergent. " << endl;	
	#endif
			return false;
		}
	}else{
		//actual prediction should be....
		if((!(t == active_threads ^ nt == active_threads)) && active_threads > 1){ //"predict" divergent
	#ifdef DEBUGBR
			cout << "ACTUALLY <DIVERGENT>: act = "<< active_threads << endl; 
	#endif
			//get_new_mask(dv_temp, active_mask); //sets actual mask to dvInst
			for(int k=0; k < threads_per_warp; k++){
				x_smask[k] = active_mask[k];
			}
			return true; 

		}else{ //it remains the same or is convergent (i.e. "predict" not divergent)		
	#ifdef DEBUGBR
			cout << "<ACTUALLY> Non-Divergent. " << endl;	
	#endif
			return false;
		}
	}

	return false;	
		
}

bool DVIQ::almost_full(int  dvqid){
	return (dviq[dvqid].size() >= (uint)dviq_size - 4);
}


bool DVIQ::all_active_threads(int smask[], int length){
	for(int i = 0; i < length; i++){
		if(smask[i] == 0)
			return false;
	}
	return true;
}


void DVIQ::set_load_started(int dviq_id){
	dviq[dviq_id].front().load_started = true;
}

void DVIQ::end_of_cachewarm(std::vector<LONG> seqno){
	for(int i = 0; i< num_threads ;i++){
		issue_seq_no[i] = seqno[i];
	}
}
bool DVIQ::inorder_issue(int dviqid){
	const int* smask_ptr = &dviq[dviqid].front().smask.front();
	const Instruction* inst_ptr = &dviq[dviqid].front().insts.front();
	const int* seq_tid_ptr = &dviq[dviqid].front().tid.front();
	for(int i = 0; i< threads_per_warp ;i++){
		if(!smask_ptr[i]){
			continue;
		}
		//if(dviq[dviqid].front().insts[i].seq_no != issue_seq_no[dviq[dviqid].front().tid[i]]){
		if(inst_ptr[i].seq_no != issue_seq_no[seq_tid_ptr[i]]){
			log(logDEBUG4)<<"DVIQ "<<dviqid<< " seq["<<seq_tid_ptr[i]<<"] "<<inst_ptr[i].seq_no<<" Issue seq "<<issue_seq_no[seq_tid_ptr[i]]<<endl;
			inorder_blocked[dviqid] = true;
			return false;
		}
	}
	inorder_blocked[dviqid] = false;
	return true;
}



void DVIQ::update_dv_type(int x_smask[], int threads_per_warp, INSERT_TYPE &type){
		if(all_active_threads(x_smask, threads_per_warp))
			type = RECONV;
		else
			type = SAME;
}

int DVIQ::pick(LONG current_cycle){

	int warp_count = 0;

	while(is_issuable()){
		int ref = -1;

		uint offset = current_warp * threads_per_warp;

		_stat_zero[current_warp]++;
		for (UINT i=0+offset; i<threads_per_warp+offset; i++){
			if (dviq[i].size() != 0 && !blocked[i]){
				if ( ref == -1 ||dviq[i].front().dv_seqno < dviq[ref].front().dv_seqno){
					if(inorder_issue(i)){
						//if(!inorder_blocked[i]){
						ref = i;
					}
				}
			}
		}

		if(ref == -1){
			//warp_count++;
			current_warp = (current_warp+1)%num_warps;
			continue;
		}
		//	warp_count = 0;
		for (UINT i=0+offset; i<threads_per_warp+offset; i++){
			inorder_blocked[i] = false;
		}
		_stat_picked[current_warp]++;
		current_warp = (current_warp+1)%num_warps;
		return ref;
	}
	//}
	return -1;
}
int DVIQ::pick_old(LONG current_cycle){

	if(base){
		do{
			rr_position = (rr_position + 1)%num_threads;
			if(!is_issuable()){
				return -1;
			}
		}while(dviq[rr_position].size() == 0 || blocked[rr_position]);
		return rr_position;
	}else{

		int warp_count = 0;
		while(warp_count < num_warps){
			int ref = -1;

			uint offset = current_warp * threads_per_warp;
			if(warp_cycle[current_warp] > threads_per_warp + 1){

				for (UINT i=0+offset; i<threads_per_warp+offset; i++){
					if(!active[i]){

						if (dviq[i].size() != 0 && !blocked[i] && inorder_issue(i)){
							ref = i;
							active[ref] = true;

							for (UINT i=0+offset; i<threads_per_warp+offset; i++){
								inorder_blocked[i] = false;
							}
							warp_count = 0;
							_stat_picked[current_warp]++;
							
							return ref;
						}
						//	cout<<"RR Failed "<<current_warp<<" id "<<i<< " warp cycle "<<warp_cycle[current_warp]<<endl;
					}
				}
				
				warp_count++;
				warp_cycle[current_warp] = 0;
				for (UINT i=0+offset; i<threads_per_warp+offset; i++){
					active[i] = false;
				}
				//current_warp = (current_warp+1)%num_warps;
				continue;
				//	}


			}else{
				int b = 0;
				int z = 0;
				//int i = 0;
				for (UINT i=0+offset; i<threads_per_warp+offset; i++){
					if (dviq[i].size() != 0 && !blocked[i]){
						if ( ref == -1 ||dviq[i].front().dv_sp < dviq[ref].front().dv_sp || ( dviq[i].front().dv_sp == dviq[ref].front().dv_sp && dviq[i].front().dv_pc < dviq[ref].front().dv_pc ) ){
							if(inorder_issue(i)){
								//if(!inorder_blocked[i]){
								ref = i;
							}else{
								_stat_inorder[current_warp]++;
							}
						}
					}else{
						if(blocked[i]){
							b++;
						}else{
							z++;
						}
					}
				}
				if(b == threads_per_warp){
					_stat_blocked[current_warp]++;
				}
				if(z == threads_per_warp){
					_stat_zero[current_warp]++;
				}
				//if(ref == -1 || blocked[ref] || !inorder_issue(ref)){
				if(ref == -1){
					
					if(current_cycle > dviq_cycle[current_warp]){
						dviq_cycle[current_warp] = current_cycle;
						warp_cycle[current_warp]++;
					}
					//cout<<"MINSP Failed "<<current_warp<<" id "<<ref<< " warp cycle "<<warp_cycle[current_warp]<<endl;
					warp_count++;
					current_warp = (current_warp+1)%num_warps;
					continue;
				}

				for (UINT i=0+offset; i<threads_per_warp+offset; i++){
					inorder_blocked[i] = false;
				}

				active[ref] = true;

				//warp_count++;
				warp_count = 0;
				
				if(current_cycle > dviq_cycle[current_warp]){
					dviq_cycle[current_warp] = current_cycle;
					warp_cycle[current_warp]++;
				}
				_stat_picked[current_warp]++;
				//cout<<"MINSP Pick "<<current_warp<<" id "<<ref<< " warp cycle "<<warp_cycle[current_warp]<<endl;
				return ref;
			}
		}
		//cout<<"No more pick"<<endl;
		return -1;
	}
}
//return pos;

bool DVIQ::peek(int &id,LONG current_cycle){
	//Round robin heuristics
	//int init = rr_position;
	int position = pick(current_cycle);
	if(position == -1){
		return false;
	}

	id = position;

	//dvinsn.valid = true;
	return true;
}

/************************************************
   Updates the dviq for issuing
   If the first instruction in the FIFO is blocked,
   keeps the dviq blocked, else pops the front for
   the issue/execute
*************************************************/
void DVIQ::update_queue_status(int dviq_id, bool block){
	
	const int* smask_ptr = &dviq[dviq_id].front().smask.front();
	const int* seq_tid_ptr = &dviq[dviq_id].front().tid.front();
	if(block){
		blocked[dviq_id] = true;
	}else{
		for(int i = 0 ; i< threads_per_warp;i++){
			if(smask_ptr[i]){
				issue_seq_no[seq_tid_ptr[i]]++;
				log(logDEBUG4)<<"ISSUE SEQNO ["<<seq_tid_ptr[i]<<"] "<<issue_seq_no[seq_tid_ptr[i]]<<endl;
			}
		}
		blocked[dviq_id] = false;
		dviq[dviq_id].pop_front();
	}
}

bool DVIQ::isEmpty(){
	int zero_count = 0;
	for(int i = 0 ;i < num_threads;i++){
		if(dviq[i].size() == 0){
			zero_count++;
		}
	}
	if(zero_count == num_threads){
		return true;
	}else{
		return false;
	}
}

int DVIQ::blocked_count(){
	int count=0;
	for(int i = 0 ;i < num_threads;i++){
		if(dviq[i].size() != 0 && ( blocked[i])){
			count++;
		}
	}
	return count;
}

void DVIQ::print_queue_masks(){
	int counter;
	if(loglevel < logDEBUG4){
		return;
	}
	for(int i = 0; i< num_threads;i++){
		counter = 0;
		log(logDEBUG4)<<"Queue "<<i<<" [";
		for(int j = 0 ; j < threads_per_warp;j++){
			log(logDEBUG4)<<q_mask[i][j];
		}
		log(logDEBUG4)<<"] - ";
		for(std::deque<DVInst>::iterator dv = dviq[i].begin(); dv != dviq[i].end(); dv++) {
			log(logDEBUG4)<<"("<<counter++<<")"<<"[";
			for(int j = 0 ; j < threads_per_warp;j++){
				log(logDEBUG4)<<dv->smask[j];
			}
			log(logDEBUG4)<<"]";

		}
		log(logDEBUG4)<<endl;
	}
}

void DVIQ::print_queue_seq(){
	int counter;
	if(loglevel < logDEBUG4){
		return;
	}
	for(int i = 0; i< num_threads;i++){
		counter = 0;
		int offset = (int)(i/threads_per_warp) * threads_per_warp;
		log(logDEBUG4)<<"Queue "<<i<<" [";
		for(int j = 0+offset ; j < threads_per_warp+offset;j++){
			log(logDEBUG4)<<issue_seq_no[j]<<" ";

		}
		log(logDEBUG4)<<"] - ";
		for(std::deque<DVInst>::iterator dv = dviq[i].begin(); dv != dviq[i].end(); dv++) {
			log(logDEBUG4)<<"("<<counter++<<")"<<"{";
			for(int j = 0 ; j < threads_per_warp;j++){
				if(dv->smask[j]){
					log(logDEBUG4)<<"["<<j<<"]="<<dv->insts[j].seq_no<<" ";
				}
			}
			log(logDEBUG4)<<"} ";

		}
		log(logDEBUG4)<<endl;
	}
}

